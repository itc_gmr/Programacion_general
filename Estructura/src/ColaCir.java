
public class ColaCir<T> {
	private int Frente;
	private int Fin;
	private int Max;
	public T   Dr;
	private T [] C;
	public ColaCir(){
		this(10);
	}
	public ColaCir(int Tam){
		Max=Tam;
		Frente=Fin=-1;
		C=(T[]) new Object[Tam];
	}
	public boolean Inserta(T Dato){
		if(Llena())
			return false;
		if(Fin==Max-1)
			Fin=0;
		else
			Fin++;
		C[Fin]=Dato;
		if(Frente==-1)
			Frente=0;
		return true;
		
	}
	public boolean Retira(){
		if(Vacia())
			return false;
		Dr=C[Frente];
		C[Frente]=null;
		if(Frente==Fin)
			Frente=Fin=-1;
		else
			if(Frente==Max-1)
				Frente=0;
			else
				Frente++;
		return true;
	}
	public boolean Llena(){
		return Frente==0 && Fin==Max-1 || Fin+1==Frente;
	}
	public boolean Vacia(){
		return Frente==-1;	
	}
}
