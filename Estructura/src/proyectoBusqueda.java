import java.util.*;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
public class proyectoBusqueda {
	static long NoRegistro = 0;
	static Scanner leer = new Scanner(System.in);
	public static String PonBlancos(String Nom,int Ancho){
	       for(int i=0;Nom.length()<Ancho;i++)
	          Nom=Nom+" ";
	          return Nom;
	     }
	public static void main(String[] args) throws IOException{
		File F1=new File("AccsesoAlumnos.dat");
		RandomAccessFile accseso= new RandomAccessFile( F1,"rw" );
		File F2=new File("Alumnos.dat");
		RandomAccessFile alumnos= new RandomAccessFile( F2,"rw" );
		int seleccion = 0;
		while(true){
			System.out.println("----------Men� de Opciones----------");
			System.out.println("Inserte la opci�n deseada");
			System.out.println("1.- Registrar Alumno");
			System.out.println("2.- Eliminar Alumnos");
			System.out.println("3.- Consultar Alumno"); 
			System.out.println("4.- Mostrar Alumnos registrados");
			System.out.println("5.- Salir");
			seleccion = leer.nextInt();
			switch(seleccion){
			case 1:
				registro(accseso, alumnos);
				break;
			case 2:
				System.out.println("Mencione N�mero de control del alumno que se desea consultar");
				long n = leer.nextLong();
				bajaAlumno(accseso, alumnos, 16, 72, n);
				break;
			case 3:
				System.out.println("Mencione N�mero de control del alumno que se desea consultar");
				long m = leer.nextLong();
				consulta(accseso, alumnos, 16, m);
				break;
			case 4:
				muestra(accseso, alumnos, 16, 76);
				break;
			case 5:
				accseso.close();
				alumnos.close();
				return;
			}
		}
	}
	
	public static void registro(RandomAccessFile accseso, RandomAccessFile alumnos) throws IOException{
		long NoControl = -1;
		String nombre, carrera;
		int edad, EdoCivil, estatus = 1;
		while(true){
			accseso.seek(accseso.length());
			alumnos.seek(alumnos.length());
			System.out.println("Proporcione el N�mero de Control del Alumno (Precione 0 para cancelar)");
			NoControl = new Scanner(System.in).nextLong();
			if(NoControl == 0){
				break;
			}
			if(busquedaExterna(NoControl, NoRegistro, accseso, 16)==-1){
				System.out.println("Proporcione el Nombre del Alumno");
				nombre = new Scanner(System.in).nextLine();
				nombre = PonBlancos(nombre, 40);
				System.out.println("Proporcione la carrera del Alumno");
				carrera = new Scanner(System.in).nextLine();
				carrera = PonBlancos(carrera, 20);
				System.out.println("Proporcione la Edad del Alumno");
				edad = new Scanner(System.in).nextInt();
				if (edad < 17){
					System.out.println("Edad invalida, vualva a introducir los datos");
					continue;
				}
				System.out.println("Proporcione el Estado Civil del Alumno (0 = Soltero, 1 = Casado)");
				EdoCivil = new Scanner(System.in).nextInt();
				if (EdoCivil > 1 || EdoCivil < 0){
					System.out.println("Solo se acepta 1 = Casado y 0 = Soltero");
					System.out.println("Reingrese los datos");
					continue;
				}
				accseso.seek(accseso.length());
				alumnos.seek(alumnos.length());
				accseso.writeLong(NoControl);
				accseso.writeLong(NoRegistro++);
				ordenaIntercambio(accseso, 16);
				alumnos.writeUTF(nombre);
				alumnos.writeInt(edad);
				alumnos.writeUTF(carrera);
				alumnos.writeInt(EdoCivil);
				alumnos.writeInt(estatus);
				System.out.println(accseso.length());
				System.out.println(alumnos.length());
			}
			else{
				System.out.println("N�mero de control ya esta registrado");
				continue;
			}
			
		}
	}
	
	public static long busquedaExterna(long Cuenta, long NoRegistro, RandomAccessFile Arch, long longitud) throws IOException {
		Arch.seek(0);
		long Arriba=(long)Arch.length()/longitud;
		long Abajo=1;
		long Centro;
		long cuentaleida;
		while (Abajo <=Arriba){
			Centro=(Arriba+Abajo)/2;
			Arch.seek((Centro-1) *longitud );
			cuentaleida=Arch.readLong();
			if (cuentaleida==Cuenta){
				NoRegistro = Arch.readLong();
				return NoRegistro;
			}
			if(Cuenta>cuentaleida){
			Abajo=Centro+1;
			continue;
  			}
  			Arriba=Centro-1;
		}
		return -1;
	}
	
	public static void ordenaIntercambio(RandomAccessFile Arch, long longitud) throws IOException{
		Arch.seek(0);
		long auxControl, auxRegistro,  noControl, noControl2, noRegistro, noRegistro2;
		long TotalReg=(long)Arch.length()/longitud;
		for(int i = 0; i < TotalReg; i++){
			for(int j = i+1; j < TotalReg; j++){
				Arch.seek(longitud*j);
				noControl = Arch.readLong();
				noRegistro = Arch.readLong();
				Arch.seek(longitud*i);
				noControl2 = Arch.readLong();
				noRegistro2 = Arch.readLong();
				if(noControl2 > noControl){
					auxControl = noControl;
					auxRegistro = noRegistro;
					noControl = noControl2;
					noRegistro = noRegistro2;
					noControl2 = auxControl;
					noRegistro2 = auxRegistro;
				}
				Arch.seek(longitud*j);
				Arch.writeLong(noControl);
				Arch.writeLong(noRegistro);
				Arch.seek(longitud*i);
				Arch.writeLong(noControl2);
				Arch.writeLong(noRegistro2);
			}
		}
	}
	
	public static void muestra(RandomAccessFile accseso, RandomAccessFile alumnos,long longitudAC, long longitudAL) throws IOException{
		 int RTAC = (int) (accseso.length()/longitudAC);
		 int RTAL = (int) (alumnos.length()/longitudAL);
		 int edad, edoCivil, estatus;
		 String nombre, carrera;
	   	 long NoControl, numRegistro;
	   	 
	     for(int i = 0; i<RTAC; i++){
	    	 NoControl=accseso.readLong();
	  		 numRegistro = accseso.readLong();
	  		 System.out.println("N�mero de control del alumno: " + NoControl);
	     }
	   	 for(int i = 0; i<RTAL; i++){
	   		 nombre = alumnos.readUTF();
      		 carrera=alumnos.readUTF();
      		 edad = alumnos.readInt();
      		 edoCivil = alumnos.readInt();
      		 estatus = alumnos.readInt();
      		 String E;
      		 if(estatus == 0){
      			 E = "Activo";
      		 }
      		 else{
      			 E = "Baja";
      		 }
      		 System.out.println("Infprmaci�n del Alumno: ");
      		 System.out.println(nombre + "  " + carrera + "  " + edad + "  " + edoCivil + "  " + E);
      		
	   	 }
	 }
	
	
	
	public static void consulta(RandomAccessFile accseso, RandomAccessFile alumnos, long longitud, long NoControl)throws IOException{
		 long RT = (long) (accseso.length()/longitud);
		 long RTAlumnos = (long)(alumnos.length()/76); 
		 long NRegistro = busquedaExterna(NoControl, 0, accseso, 16);
		 String nombre, carrera;
		 int edad, edocivil, estatus;
		 if(NRegistro == -1){
			 System.out.println("No se encuentra el n�mero de control especificado");
			 return;
		 }
		 for(int i = 0; i < RT; i++){
			 accseso.seek(i*longitud);
			 NoControl = accseso.readLong();
			 if (NRegistro == i){
				 for(int j = 0; j < RTAlumnos; j++){
					 if(i == j){
						 alumnos.seek(j*longitud);
						 nombre = alumnos.readUTF();
						 carrera = alumnos.readUTF();
						 edad = alumnos.readInt();
						 edocivil = alumnos.readInt();
						 estatus = alumnos.readInt();
						 System.out.println("N�mero de control: " + NoControl);
						 System.out.println("Alumno: " + nombre);
						 System.out.println("Carrera: " + carrera);
						 System.out.println("Edad: " + edad);
						 if(edocivil == 0){
							 System.out.println("Estado civil soltero");
						 }
						 else{
							 System.out.println("Estado civil casado");
						 }
						 if(estatus == 1){
							 System.out.println("Estatus del alumno Activo");
						 }
						 else{
							 System.out.println("Estatus del alumno Baja");
						 }
						 
					 }
				 }
			 }
		 }
	}
	
	public static void bajaAlumno(RandomAccessFile accseso, RandomAccessFile alumnos, long longitudAC, long longitudAL, long NoControl)throws IOException{
		long registro = busquedaExterna(NoControl, 0, accseso, 16);
		long RTAlumnos = (long)(alumnos.length()/longitudAL);
		long RT = (long) (accseso.length()/longitudAC);
		int estatus;
		if(registro == -1){
			 System.out.println("No se encuentra el n�mero de control especificado");
			 return;
		 }
		 for(int i = 0; i < RT; i++){
			 accseso.seek(i*longitudAC);
			 NoControl = accseso.readLong();
			 if (registro == i){
				 for(int j = 0; j < RTAlumnos; j++){
					 if(i == j){
						 alumnos.seek(j*72+68);
						 estatus = alumnos.readInt();
						 System.out.println("Estatus del N�mero anterior " + NoControl + " es " + estatus);
						 estatus = 0;
						 alumnos.seek(j*72+68);
						 alumnos.writeInt(estatus);
						 System.out.println("Estatus del N�mero actual " + NoControl + " es " + estatus);
					 }
				 }
			 }
		 }
	}
}
