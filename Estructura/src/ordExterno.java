import java.*;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
public class ordExterno {

	public static void main(String[] args) throws IOException {
		File F=new File("Ordenamiento.dat");
		RandomAccessFile ordenaExt= new RandomAccessFile( F,"rw" );
		ordenaExt.seek(ordenaExt.length());
		int n;
		for(int i=0 ;i<50;i++){
            n = MyRandom.nextInt(1, 500);
            ordenaExt.writeInt(n);
// Long. Reg=4
         }
		Muestra(ordenaExt);
		//ordenaBurbuja(ordenaExt, 4);
		ordenaIntercambio(ordenaExt, 4);
		System.out.println("------------------------------");
		Muestra(ordenaExt);
		System.out.println(BusquedaBinaria(500, ordenaExt, 4));
		ordenaExt.close();
	}
	
	public static void Muestra(RandomAccessFile Arch) throws IOException{
   	 Arch.seek(0);
   	 int n;
   	 while(true){
   		 try{
       		 n=Arch.readInt();
       		 System.out.println(n);
       	 
   		 }catch(Exception e){
   			 break;
   		 }
   	 }
   }
	
	public static void ordenaIntercambio(RandomAccessFile Arch, int longitud) throws IOException{
		Arch.seek(0);
		int aux,  numero, numero2;
		int TotalReg=(int)Arch.length()/longitud;
		for(int i = 0; i < TotalReg; i++){
			for(int j = i+1; j < TotalReg; j++){
				Arch.seek(longitud*j);
				numero = Arch.readInt();
				Arch.seek(longitud*i);
				numero2 = Arch.readInt();
				if(numero2 > numero){
					aux = numero;
					numero = numero2;
					numero2 = aux;
				}
				Arch.seek(longitud*j);
				Arch.writeInt(numero);
				Arch.seek(longitud*i);
				Arch.writeInt(numero2);
			}
		}
	}
	
	public static void ordenaBurbuja(RandomAccessFile Arch, int largo) throws IOException{
		Arch.seek(0);
		int temp, i;
		int Vi, Vj;
		boolean bandera = true;
		int superior =(int)Arch.length()/largo;
		while(bandera){
			bandera = false;
			superior--;
			for(i=0; i < superior; i++){
				Arch.seek(largo*i);
				Vi = Arch.readInt();
				Arch.seek(largo*(i+1));
				Vj = Arch.readInt();
				if(Vi > Vj){
					temp = Vi;
					Vi = Vj;
					Vj = temp;
					bandera = true;
				}
				Arch.seek(largo*i);
				Arch.writeInt(Vi);
				Arch.seek(largo*(i+1));
				Arch.writeInt(Vj);
			}
		}
	}
	
	public static int BusquedaBinaria(int Cuenta,RandomAccessFile Arch_Index, int longitud) throws IOException {
		Arch_Index.seek(0);
		int Arriba=(int)Arch_Index.length()/longitud;
		int Abajo=1;
		int Centro;
		int cuentaleida;
		while (Abajo <=Arriba){
			Centro=(Arriba+Abajo)/2;
			Arch_Index.seek((Centro-1) *longitud );
			cuentaleida=Arch_Index.readInt();
			if (cuentaleida==Cuenta)
				return Centro;
			if(Cuenta>cuentaleida){
			Abajo=Centro+1;
			continue;
  			}
  			Arriba=Centro-1;
		}
		return -1;
	}
}
