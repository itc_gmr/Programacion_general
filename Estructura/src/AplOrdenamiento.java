import java.util.Arrays;


public class AplOrdenamiento {

	public static void main(String[] args) {
		Ordenamiento Obj=new Ordenamiento();
		int N=1900;
		int [] V=new int[N];
		for(int i=0;i<N;i++){
			V[i]=MyRandom.nextInt(1, 100000);
		}
		System.out.println("Información original");
		Imprime(V);
		System.out.println(System.currentTimeMillis());
		Obj.QuickSort(V);
		System.out.println(System.currentTimeMillis());
	 //   Obj.Burbuja(V);
	//	Obj.Intercambio(V);
	  //  Obj.OrdenInsercion(V);
	//	Arrays.sort(V);
		
		System.out.println("Información ORDENADA");
		Imprime(V);

	}
	public static void Imprime(int [] V){
		for(int i=0;i<10;i++)
			System.out.print(V[i]+"  ");
		System.out.println();
	}
}
