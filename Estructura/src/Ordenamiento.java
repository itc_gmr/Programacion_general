
public class Ordenamiento {
	   public void Burbuja (int [] v)
	   { int superior, temp, i;
	     boolean bandera=true;
	     superior=v.length;
	     while (bandera) 
	     {  bandera=false;
	        superior--;
	        for(i=0 ; i < superior ; i ++)
	        {  if (v[i] > v[i+1])
	           {   temp=v[i];
	               v[i]=v[i+1];
	               v[i+1]=temp;
	               bandera=true;
	           } 
	        }
	     }
	   }
	   void OrdenInsercion( int [] v)
	   {  int i,j,aux, n=v.length;
	      for( i=1; i< n ; i++)  //inicia considerando el elemento 0 ordenado
	      {
	          j=i;  //para explorar los elementos v[j-1]..v[0] buscando la 
	                //posici�n correcta del dato v[i]
	          aux=v[i];
	          while (j>0 && aux < v[j-1])
	          {  //desplaza el elemento hacia arriba una posici�n
	             v[j]=v[j-1];
	             j--; 
	          }
	          v[j]=aux;
	       }
	   }
	   public void Intercambio(int [] V){
		   int Aux;
		   for(int i=0;i<V.length-1;i++)
			   for(int j=i+1;j<V.length;j++)
				   if(V[i]>V[j]){
					   Aux=V[i];
					   V[i]=V[j];
					   V[j]=Aux;
				   }
	   }
// desarrollado por Hoare en el a�o 1960.
	   public void QuickSort(int V[]) {
		   Quick(V,0,V.length-1);
		   
	   }
	   public void Quick(int A[], int izq, int der) {

		   int pivote=A[izq]; // tomamos primer elemento como pivote
		   int i=izq; // i realiza la b�squeda de izquierda a derecha
		   int j=der; // j realiza la b�squeda de derecha a izquierda
		   int aux;
		  
		   while(i<j){            // mientras no se crucen las b�squedas
		      while(A[i]<=pivote && i<j) i++; // busca elemento mayor que pivote
		      while(A[j]>pivote) j--;         // busca elemento menor que pivote
		      if (i<j) {                      // si no se han cruzado                      
		          aux= A[i];                  // los intercambia
		          A[i]=A[j];
		          A[j]=aux;
		      }
		    }
		    A[izq]=A[j]; // se coloca el pivote en su lugar de forma que tendremos
		    A[j]=pivote; // los menores a su izquierda y los mayores a su derecha
		    if(izq<j-1)
		       Quick(A,izq,j-1); // ordenamos subarray izquierdo
		    if(j+1 <der)
		       Quick(A,j+1,der); // ordenamos subarray derecho
		 }	   
}
