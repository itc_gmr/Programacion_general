//Guillermo Marquez Ruiz
public class SerpientesYEscaleras {
static class Dato{
	int NoCasilla;
	int posiciones;
	String Tipo;
	
}
static class Jugador{
	String jugador;
	int dado;
	public Jugador(String nombre){
		jugador = nombre;
	}
}


	

	public static void main(String[] args) {
		System.out.println("Empieza el Juego");
		Jugador j1 = new Jugador("J1");
		Jugador j2 = new Jugador("J2");
		ListaDBL<Dato> Tablero = new ListaDBL<Dato>();
		gererarTablero(Tablero);
		generaEscalera(Tablero);
		generaSerpiente(Tablero);
		
		jugar(Tablero, j1, j2);
	}
	
	public static void imprimirTablero(ListaDBL<Dato> Ta){
		NodoDBL<Dato> Aux = Ta.DameFrente();
		int cont = 0;
		while(Aux != null){
			if(Aux.Info.NoCasilla == 100)
				System.out.println(Aux.Info.NoCasilla + " " + "Tipo: " + Aux.Info.Tipo);
			else
				System.out.print(Aux.Info.NoCasilla + " " + "Tipo: " + Aux.Info.Tipo + "   ");
			cont++;
			if(cont == 10){
				System.out.println();
				cont = 1;
			}
			Aux = Aux.DameSig();
		}
	}
	
	public static void gererarTablero(ListaDBL<Dato> T){
		System.out.println("Generando tablero...");
		Dato nuevo;
		for(int i = 0; i < 100; i++){
			nuevo = new Dato();
			nuevo.NoCasilla = (i+1);
			nuevo.posiciones = 0;
			nuevo.Tipo = "N";
			T.InsertaFin(nuevo);
		}
		System.out.println("Tablero Generado");
	}
	
	public static void generaEscalera(ListaDBL<Dato> T){
		int contE = 4, cont;
		int casilla;
		int avanza;
		for(int i = 0; i <= contE; i++){
			casilla = MyRandom.nextInt(5, 80);
			NodoDBL<Dato> Aux = T.DameFrente();
			while(Aux != null){
				if (Aux.Info.NoCasilla == casilla){
					if(!Aux.Info.Tipo.equals("N")){
						i--;
					}
					else{
						avanza = MyRandom.nextInt(5, 10); 
						Aux.Info.posiciones = avanza;
						Aux.Info.Tipo = "E";
						cont = avanza;
						NodoDBL<Dato> Aux2 = Aux;
						while (Aux2 != null){
							if (cont == 0){
								if(!Aux2.Info.Tipo.equals("N")){
									Aux2.Info.posiciones = MyRandom.nextInt(5,10);
									while(cont <= Aux2.Info.posiciones){
										cont++;
										Aux2 = Aux2.DameAnt();
									}
								}
								else{
									Aux2.Info.Tipo = "T";									
									break;
								}
							}
							cont--;
							Aux2 = Aux2.DameSig();
						}
					}
				}
				Aux = Aux.DameSig();
			}
		}
		
	}
	
	public static void generaSerpiente(ListaDBL<Dato> T){
		int contE = 4, cont;
		int casilla;
		int retrocede;
		for(int i = 0; i <= contE; i++){
			casilla = MyRandom.nextInt(20, 90);
			NodoDBL<Dato> Aux = T.DameFrente();
			while(Aux != null){
				if (Aux.Info.NoCasilla == casilla){
					if(!Aux.Info.Tipo.equals("N")){
						i--;
					}
					else{
						retrocede = MyRandom.nextInt(5, 10); 
						Aux.Info.posiciones = retrocede;
						Aux.Info.Tipo = "S";
						cont = retrocede;
						NodoDBL<Dato> Aux2 = Aux;
						while (Aux2 != null){
							if (cont == 0){
								if(!Aux2.Info.Tipo.equals("N")){
									Aux2.Info.posiciones = MyRandom.nextInt(5,10);
									while(cont <= Aux2.Info.posiciones){
										cont++;
										Aux2 = Aux2.DameSig();
									}
								}
								else{
									Aux2.Info.Tipo = "T";
									break;
								}
							}
							cont--;
							Aux2 = Aux2.DameAnt();
						}
					}
					
					
				}
				Aux = Aux.DameSig();
			}
		}
		
	}
	
	public static void jugar(ListaDBL<Dato> T, Jugador j1, Jugador j2){
		NodoDBL<Dato> Aux = T.DameFrente();
		NodoDBL<Dato> Aux2 = T.DameFrente();
		while(true){
			System.out.println("Turno jugador 1");
			while(true){//Jugador 1
				System.out.println("---------------");
				j1.dado = MyRandom.nextInt(2, 12);
				System.out.println("Dado: " + j1.dado);
				for(int i = 0; i < j1.dado; i++){
					if(Aux.DameSig() == null)
						break;
					Aux = Aux.DameSig();
				}
				System.out.println("No de Casilla: " + Aux.Info.NoCasilla + " " + Aux.Info.Tipo);
				if(Aux.Info.Tipo.compareTo("E")==0){
					System.out.println("Callo en escalera " + Aux.Info.posiciones);
					int cont = 0, iteraciones = Aux.Info.posiciones;
					while(Aux != null){
						if(cont == iteraciones)
							break;
						cont++;
						
						Aux = Aux.DameSig();
					}
					System.out.println(cont);
				}
				if(Aux.Info.Tipo.compareTo("S")==0){
					System.out.println("Callo en serpiente " + Aux.Info.posiciones);
					int cont = 0, iteraciones = Aux.Info.posiciones;
					while(Aux != null){
						if(cont == iteraciones)
							break;
						cont++;
						
						Aux = Aux.DameAnt();
					}
					System.out.println(cont);
				}
				System.out.println("No de Casilla: " + Aux.Info.NoCasilla + "" + Aux.Info.Tipo);
				break;
			}
			System.out.println("Turno jugador 2");
			while(true){ //Jugador 2
				System.out.println("---------------");
				j1.dado = MyRandom.nextInt(2, 12);
				System.out.println("Dado: " + j1.dado);
				for(int i = 0; i < j1.dado; i++){
					if(Aux2.DameSig() == null)
						break;
					Aux2 = Aux2.DameSig();
				}
				System.out.println("No de Casilla: " + Aux2.Info.NoCasilla + " " + Aux2.Info.Tipo);
				if(Aux2.Info.Tipo.compareTo("E")==0){
					System.out.println("Callo en escalera " + Aux2.Info.posiciones);
					int cont = 0, iteraciones = Aux2.Info.posiciones;
					while(Aux2 != null){
						if(cont == iteraciones)
							break;
						cont++;
						
						Aux2 = Aux2.DameSig();
					}
					System.out.println(cont);
				}
				if(Aux2.Info.Tipo.compareTo("S")==0){
					System.out.println("Callo en serpiente " + Aux2.Info.posiciones);
					int cont = 0, iteraciones = Aux2.Info.posiciones;
					while(Aux2 != null){
						if(cont == iteraciones)
							break;
						cont++;
						
						Aux2 = Aux2.DameAnt();
					}
					System.out.println(cont);
				}
				System.out.println("No de Casilla: " + Aux2.Info.NoCasilla + "" + Aux2.Info.Tipo);
				break;
			}
			if(Aux2.DameSig() == null)
				break;
		}
	}

}
