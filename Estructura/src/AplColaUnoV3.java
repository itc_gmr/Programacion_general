public class AplColaUnoV3 {
	static class Dato {
		public int Numero;
		public int Contador;
		public Dato(int Numero){
			this.Numero=Numero;
			Contador=1;
		}
	}
	public static void main(String [] a){
		Cola<Integer> Obj=new Cola<Integer>(1000);
		Cola<Integer> Aux=new Cola<Integer>(1000);
		Cola<Dato> Fre=new Cola<Dato>(50);
		Cola<Dato> FreAux=new Cola<Dato>(50);
		int Cont=0;
		
		while(Obj.Inserta(new Integer(MyRandom.nextInt(1,5))));
		 
		
		
		while(Obj.Retira() && Aux.Inserta(Obj.Dr)){
			
			// Buscar el numero de obj en Fre
			
			boolean Enc=false;
			while(Fre.Retira() && FreAux.Inserta(Fre.Dr)){
				if(Fre.Dr.Numero==Obj.Dr){
					Enc=true;
					break;
				}
			}
			if(Enc){
				Fre.Dr.Contador++;
			}
			else{
				if(!FreAux.Inserta(new Dato(Obj.Dr))){
					System.out.println("CAPACIDAD DE LA COLA DE FRECUENCIA INSUFICIENTE");
					return;
				}
			}
				
			
			while(Fre.Retira() && FreAux.Inserta(Fre.Dr));
			while(FreAux.Retira() && Fre.Inserta(FreAux.Dr));
		}
		
		System.out.println("Tabla de frecuencia\nNo, \t Frecuencia");
		while(Fre.Retira() && FreAux.Inserta(Fre.Dr)){
			System.out.println(Fre.Dr.Numero+"\t"+Fre.Dr.Contador);
		}
		
		
	}
}
