
public class ArbolBB <T>{
	private NodoABB<T> Root;
	public  T           Dr;

	public ArbolBB(){
		Root=null;
		Dr=null;
	}
	
	public NodoABB<T> DameRaiz(){
		return Root;
	}
	public boolean Inserta(T Dato){
		return Inserta(Root,Dato);
	}
	private boolean Inserta(NodoABB<T> Raiz, T Dato){
		if(Root==null){
			Root=new NodoABB<T>(Dato);
			return true;
		}
		if(Dato.toString().compareTo(Raiz.Info.toString()   )==0  ){
			return false;
		}
		if(Dato.toString().compareTo(Raiz.Info.toString()   )<0  ){
			if(Raiz.DameSubIzq()!=null){
				return Inserta(Raiz.DameSubIzq(),Dato);
			}
			else {
				NodoABB<T> Nuevo=new NodoABB<T>(Dato);
				Raiz.setSubIzq(Nuevo);
				return true;
			}
		}
		else {
			if(Raiz.DameSubDer()!=null){
				return Inserta(Raiz.DameSubDer(),Dato);
			}
			else {
				NodoABB<T> Nuevo=new NodoABB<T>(Dato);
				Raiz.setSubDer(Nuevo);
				return true;
			}			
			
		}
	}
	public int length(NodoABB<T> Raiz){
		if(Raiz==null)
			return 0;
		return length(Raiz.DameSubIzq()) +1 + length(Raiz.DameSubDer() );
	}
	public int Altura(NodoABB<T> Raiz,int Nivel){
		if(Raiz==null)
			return 0;
		int NSIzq=Altura(Raiz.DameSubIzq(),Nivel+1);
		if(Nivel>NSIzq)
			NSIzq=Nivel;
		int NSDer=Altura(Raiz.DameSubDer(),Nivel+1);
		if(NSIzq>NSDer)
			return NSIzq;
		return NSDer;
	}
	public boolean Busca(NodoABB<T> Raiz, T Dato){
		if(Raiz==null){
			Dr=null;
			return false;
		}
		if( Dato.toString().compareTo(Raiz.Info.toString())==0    ){
			Dr=Raiz.Info;
			return true;
		}
		if( Dato.toString().compareTo(Raiz.Info.toString())>0    ){
			
			return Busca(Raiz.DameSubDer(),Dato);
		}
		return Busca(Raiz.DameSubIzq(),Dato);
	}

}