
public class NodosPorNivel {
static class numero{
	int entero;
	public numero(int entero){
		this.entero = entero;
	}
	public String toString(){
		return PonCeros(entero, 10);
	}
	public String PonCeros(int Valor,int Tam){
		String Cad=Valor+"";
		while(Cad.length()<Tam)
			Cad="0"+Cad;
		return Cad;
	}
}
	public static void main(String[] args) {
		ArbolBB<numero> n = new ArbolBB<numero>();
		int longitud = MyRandom.nextInt(100, 150);
		for(int i = 0; i < longitud; i++){
			numero num = new numero(MyRandom.nextInt(1, 500));
			n.Inserta(num);
			System.out.println("N�mero " + num.entero);
		}
		int[] VF = new int[n.Altura(n.DameRaiz(),0)+1];
		System.out.println(n.Altura(n.DameRaiz(),0));
		ejercicio1(n.DameRaiz(), 0, VF);
		System.out.println("---------------------------");
		System.out.println("Nivel ----- NoNodos");
		for(int i = 0; i < VF.length; i++){
			System.out.println(i + "              " + VF[i]);
		}
	}
	public static int ejercicio1(NodoABB<numero> Raiz, int Nivel, int[] VF){
		if(Raiz==null)
			return 0;
		int NSIzq=ejercicio1(Raiz.DameSubIzq(),Nivel+1, VF);
		if(Nivel>NSIzq){
			NSIzq=Nivel;
		}
		int NSDer=ejercicio1(Raiz.DameSubDer(),Nivel+1, VF);
		VF[Nivel]++;
		if(NSIzq>NSDer){
			return NSIzq;
		}
		return NSDer;
	}

}
