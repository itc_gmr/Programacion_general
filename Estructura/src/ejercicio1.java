
public class ejercicio1 {

	public static void main(String[] args) {//Hacerlo con vector
		ArbolBB<Integer> obj = new ArbolBB<Integer>();
		for (int i = 0; i < 20; i++){
			obj.Inserta(MyRandom.nextInt(1, 100));
		}
		System.out.println(verifica(obj.DameRaiz(), 0));
	}
	
	public static boolean verifica(NodoABB<Integer> obj, int pasado){
		if(obj == null){
			return true;
		}
		if(obj.Info == pasado){
			return false;
		}
		verifica(obj.DameSubIzq(), obj.Info);
		if(obj.Info < pasado){
			return verifica(obj.SubIzq, obj.Info);
		}else{
			return verifica(obj.SubDer, obj.Info);
		}
		
	}

}
