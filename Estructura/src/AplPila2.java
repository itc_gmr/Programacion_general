import java.util.*;
class Dato{
	public int NoControl;
	public String Nombre;
	public int Edad;
	public int EC;
	public Dato(){
		this(0,"",0,0);
	}
	public Dato(int NoControl,String Nombre,int Edad,int EC){
		this.NoControl=NoControl;
		this.Nombre=Nombre;
		this.Edad=Edad;
		this.EC=EC;
	}
}
public class AplPila2 {

	public static void main(String[] args) {
		Pila<Dato> Obj=new Pila<Dato>();
		Scanner In=new Scanner(System.in);
		int Op;
		while (true){
			System.out.println("Funcionameto ED lineal est�tica: PILA");
			System.out.println("\n1. Inicializar");
			System.out.println("2. Insertar");
			System.out.println("3. Retirar");
			System.out.println("4. Buscar por No. control");
			System.out.println("5. Retirar por No. control");
			System.out.println("6. Estado actual");
			System.out.println("0. Salir");
			System.out.println("\n Opci�n ");
			Op=In.nextInt();
			switch (Op){
				case 1:Met1(Obj);break;
				case 2:Met2(Obj);break;
				case 3:Met3(Obj);break;
				case 4:Met4(Obj);break;
				case 5:Met5(Obj);break;
				case 6:Met6(Obj);break;
				case 0:return;
			
			
			}
			
		}

	}
	public static void Met1(Pila<Dato> P){
		System.out.println("Inicializando la PILA");
		while (P.Retira());
		
	}
	public static void Met2(Pila<Dato> P){
		Random R=new Random();
		Dato Cap=new Dato(R.nextInt(10)+1,MyRandom.nextName(),50,1);
		if(!P.Inserta(Cap)){
			System.out.println("DATO NO INSERTADO, PLA LLENA");
			return;
		}
		System.out.println("INSERCION EXITOSA!!!!!!!!!!!!");
	}
	public static void Met3(Pila<Dato> P){
		if(P.Retira()){
			System.out.println("Dato retirado "+P.Dr.NoControl+" "+P.Dr.Nombre);
		}
		else
			System.out.println("*** No retiro datos, PILA VACIA *****");
		
	}
	public static void Met4(Pila<Dato> P){
		System.out.println("BUSCANDO UN N�MERO DE CONTROL ");
		int NC=MyRandom.nextInt(1,10);
		Pila<Dato> Aux=new Pila<Dato>();
		boolean Band=false;
		while(P.Retira() && Aux.Inserta(P.Dr)){
			if(P.Dr.NoControl==NC){
				Band=true;
				break;
			}
		}
		if(Band)
			System.out.println("EL N�MERO DE CUENTA "+NC+" YA EST� EN LA PILA");
		else
			System.out.println("EL N�MERO DE CUENTA "+NC+" NONONO EST� EN LA PILA");
		while (Aux.Retira() && P.Inserta(Aux.Dr));
		
	}
	public static void Met5(Pila<Dato> P){
		System.out.println("ELIMINANDON�MERO DE CONTROL ");
		int NC=MyRandom.nextInt(1,10);
		Pila<Dato> Aux=new Pila<Dato>();
		boolean Band=false;
		while(P.Retira() && Aux.Inserta(P.Dr) ){
			if(P.Dr.NoControl==NC){
				Band=true;
				break;
			}
			
		}
		if(Band){
			Aux.Retira();
			System.out.println("EL N�MERO DE CUENTA "+NC+" fue eliminado de LA PILA");
		}		
		else
			System.out.println("EL N�MERO DE CUENTA "+NC+" NONONO EST� EN LA PILA");
		while (Aux.Retira() && P.Inserta(Aux.Dr));	
		
	}
	public static void Met6(Pila<Dato> P){
		Pila<Dato> Aux=new Pila<Dato>();
		while(P.Retira() && Aux.Inserta(P.Dr))
			System.out.println(P.Dr.NoControl+"\t"+P.Dr.Nombre+"\t"+P.Dr.Edad);
		while(Aux.Retira() && P.Inserta(Aux.Dr));
	}
}
