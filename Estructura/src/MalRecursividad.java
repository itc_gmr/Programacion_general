
public class MalRecursividad {

	
	public static void Met1(int n){
//		if(n>1000)
//			return;
		if(n<1001){
			long a,b,c,d,e;
			a=b=c=d=e=100;
			System.out.println("N="+n);
			Met1(n+1);
			System.out.println(n+" N="+(a+b+c+d+e));
		}
	}
	public static void A(int n){
		if(n>100)
			return;
		System.out.println("Valor "+n);
		B(n-1);
		System.out.println("Valor "+n);
	}
	public static void B(int n){
		if(n>100)
			return;
		System.out.println("Valor "+n);
		A(n-1);
		System.out.println("Valor "+n);
	}
	public static void main(String[] args) {
		System.out.println("Iniciando recursividad");
		//Met1(1);
		Met1(1);
		
		System.out.println("Finalizando recursividad");

	}

}
