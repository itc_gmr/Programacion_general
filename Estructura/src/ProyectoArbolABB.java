import java.util.*;
public class ProyectoArbolABB {
static class Producto{
	int IdProducto;
	int Existencia;
	public Producto(int id, int ex){
		IdProducto = id;
		Existencia = ex;
	}
	public String toString(){
		return PonCeros(IdProducto, 5);
	}
	public String PonCeros(int Valor,int Tam){
		String Cad=Valor+"";
		while(Cad.length()<Tam)
			Cad="0"+Cad;
		return Cad;
	}
}
	public static void main(String[] args) {
		ArbolBB<Producto> P = new ArbolBB<Producto>();
		Scanner leer = new Scanner(System.in);
		while(true){
			int controla = 0;
			System.out.println("----------Men� de Opciones----------");
			System.out.println("Inserte la opci�n deseada");
			System.out.println("1.- Insertar Producto");
			System.out.println("2.- Retirar Producto");
			System.out.println("3.- Registro de entrada a un producto");
			System.out.println("4.- Registro de entrada a productos con clave mayor a X");
			System.out.println("5.- Retirar productos con Existencia = 0");
			System.out.println("6.- Porcentaje de productos registrados");
			System.out.println("7.- Consulta de productos registrados");
			System.out.println("8.- Ubicaci�n de un producto en el �rbol");
			System.out.println("9.- Salir");
			controla = leer.nextInt();
			switch (controla){
			case 1:
				Producto nuevo = new Producto(MyRandom.nextInt(100, 5000), MyRandom.nextInt(0, 9));
				System.out.println("Se insert� el Producto con ID: " + nuevo.IdProducto);
				System.out.println("Existencia: " + nuevo.Existencia);
				P.Inserta(nuevo);
				break;
			case 2:
				retirarProducto(P, leer);
				break;
			case 3:
				aumenta(P, leer);
				break;
			case 4:
				int limInf = MyRandom.nextInt(100, 5000);
				System.out.println(limInf);
				aumentaAleatorio(P.DameRaiz(), limInf);
				break;
			case 5:
				retiraCero(P.DameRaiz(), P);
				break;
			case 6:
				porcentaje(P);
				break;
			case 7:
				productosRegistrados(P.DameRaiz());
				break;
			case 8:
				int nivel = 0;
				Producto ID = new Producto(0,0);
				System.out.println("---Proporcione ID de producto que se desea ubicar---");
				ID.IdProducto = leer.nextInt();
				if(!P.Busca(P.DameRaiz(), ID)){
					System.out.println("No se encuentra ID");
					break;
				}
				ubicacionProducto(P.DameRaiz(), nivel, ID, 0);
				break;
			case 9:
				return;
			}
		}
	}
	public static void productosRegistrados(NodoABB<Producto> Aux){
		if(Aux==null)
			return;
		productosRegistrados(Aux.DameSubDer());
		System.out.println("---------------------------------------");
		System.out.println("ID del producto: " + Aux.Info.IdProducto);
		System.out.println("Existencia: " + Aux.Info.Existencia);
		productosRegistrados(Aux.DameSubIzq());
	}
	
	public static void retirarProducto (ArbolBB<Producto> P, Scanner leer){
		Producto ID = new Producto(0,0);
		System.out.println("---Proporcione ID de producto que se desea retirar---");
		ID.IdProducto = leer.nextInt();
		if(!P.Retira(P.DameRaiz(), ID)){
			System.out.println("No se encontr� ID");
			return;
		}
		System.out.println("ID retirado: " + P.Dr.IdProducto);
		System.out.println("Existencia: " + P.Dr.Existencia);
	}
	
	public static void aumenta(ArbolBB<Producto> P, Scanner leer){
		Producto ID = new Producto(0,0);
		System.out.println("---Proporcione ID de producto que se desea editar---");
		ID.IdProducto = leer.nextInt();
		if(!P.Busca(P.DameRaiz(), ID)){
			System.out.println("No se encontr� ID");
			return;
		}
		System.out.println("---Proporcione cantidad de productos que se desea aumentar---");
		int Aumenta = leer.nextInt();
		System.out.println("ID retirado: " + P.Dr.IdProducto);
		System.out.println("Existencia Inicial: " + P.Dr.Existencia);
		P.Dr.Existencia = P.Dr.Existencia + Aumenta;
		System.out.println("Existencia Final: " + P.Dr.Existencia);
	}
	
	public static void aumentaAleatorio(NodoABB<Producto> Aux, int limInf){
		if(Aux == null)
			return;
		aumentaAleatorio(Aux.SubIzq, limInf);
		if(Aux.Info.IdProducto > limInf){
			int n = MyRandom.nextInt(1, 9);
			System.out.println("ID Encontrado: " + Aux.Info.IdProducto);
			System.out.println("Existencia Inicial: " + Aux.Info.Existencia);
			Aux.Info.Existencia = Aux.Info.Existencia + n;
			System.out.println("Existencia Final: " + Aux.Info.Existencia);
		}
		aumentaAleatorio(Aux.SubDer, limInf);
	}
	
	public static void retiraCero(NodoABB<Producto> Aux, ArbolBB<Producto> P){
		if(Aux == null)
			return;
		retiraCero(Aux.SubIzq, P);
		if(Aux.Info.Existencia == 0){
			P.Retira(P.DameRaiz(), Aux.Info);
		}
		retiraCero(Aux.SubDer, P);
	}
	
	public static void porcentaje(ArbolBB<Producto> P){
		float Por = (P.length(P.DameRaiz()) * 100)/4900f;
		System.out.println("Porcentaje: " + Por + "%");
	}
	
	public static void ubicacionProducto(NodoABB<Producto> Aux, int nivel, Producto ID, int subA){
		if(Aux == null)
			return; 
		ubicacionProducto(Aux.DameSubIzq(), nivel+1, ID, 1);
		if(Aux.Info.IdProducto == ID.IdProducto){
			if(subA == 1){
				System.out.println("Producto se encuentra en el Nivel: " + nivel);
				System.out.println("En el Sub-�rbol Izquierdo");
			}
			else{
				if(nivel == 0){
					System.out.println("Producto se encuentra en el Nivel: " + nivel);
					System.out.println("En la Ra�z");
					return;
				}
				System.out.println("Producto se encuentra en el Nivel: " + nivel);
				System.out.println("En el Sub-�rbol Derecho");
			}
			return;
		}
		ubicacionProducto(Aux.DameSubDer(), nivel+1, ID, 2);
	}
}
