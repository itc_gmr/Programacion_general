//Guillermo Marquez Ruiz
public class ExamenListaDBL {
static class Producto{
	public int IdProducto;
	public int Existencia;
	public Producto(int IdP, int Exi){
		IdProducto = IdP;
		Existencia = Exi;
	}
}
	public static void main(String[] args) {
		ListaDBL<Producto> Productos = new ListaDBL<Producto>();
		ListaDBL<Producto> copiaProductos = new ListaDBL<Producto>();
		int numProductos = MyRandom.nextInt(40, 100); 
		crea(Productos, copiaProductos, numProductos);
		movimientos(Productos, numProductos);
		reporte(Productos, copiaProductos);
	}
	
	public static void crea(ListaDBL<Producto> P, ListaDBL<Producto> copiaP, int numProductos){
		
		int nID = 0, Ex = 0;
		for(int i = 0; i < numProductos; i++){
			nID = MyRandom.nextInt(1, 5000);
			Ex = MyRandom.nextInt(1000, 2000);
			if(Busca(P, nID)){
				i--;
				continue;
			}
			P.InsertaFin(new Producto(nID, Ex));
			copiaP.InsertaFin(new Producto(nID, Ex));
		}
		
	}
	
	public static boolean Busca(ListaDBL<Producto> P, int nIDP){
		NodoDBL<Producto> Aux = P.DameFrente();
		while(Aux != null){
			if (nIDP == Aux.Info.IdProducto){
				return true;
			}
			Aux = Aux.DameSig();
		}
		return false;
	}
	
	public static void movimientos(ListaDBL<Producto> P, int numProductos){
		int IdMov = MyRandom.nextInt(1, 2), nMovimientos = MyRandom.nextInt(100, 200);
		for(int i = 0; i < nMovimientos; i++){
			if (IdMov == 1){ //Entrada de Productos
				aumenta(P);
			}
			if (IdMov == 2){
				disminuye(P, i);//Salida de Productos
			}
		}
	}
	
	public static void aumenta(ListaDBL<Producto> P){
		NodoDBL<Producto> Aux = P.DameFrente();
		int aumenta = MyRandom.nextInt(1, 50);
		int m = MyRandom.nextInt(40, 100);
		int cont = 0;
		while(Aux != null){
			if(cont == m){
				Aux.Info.Existencia = Aux.Info.Existencia + aumenta;
			}
			cont++;
			Aux = Aux.DameSig();
		}
	}
	
	public static void disminuye(ListaDBL<Producto> P, int i){
		NodoDBL<Producto> Aux = P.DameFrente();
		int disminuye;
		int m = MyRandom.nextInt(40, 100);
		int cont = 0;
		while(Aux != null){
			if(cont == m){
				disminuye = MyRandom.nextInt(1, Aux.Info.Existencia);
				Aux.Info.Existencia = Aux.Info.Existencia - disminuye;
				if(Aux.Info.Existencia <= 0){
					i--;
				}
			}
			cont++;
			Aux = Aux.DameSig();
		}
	}
	
	public static void reporte(ListaDBL<Producto> P, ListaDBL<Producto> copiaP){
		NodoDBL<Producto> Aux = P.DameFrente();
		NodoDBL<Producto> Aux2 = copiaP.DameFrente();
		while(true){
			System.out.print("IdProducto: " + Aux2.Info.IdProducto + " Existencia Inicial: " + Aux2.Info.Existencia + " ");
			System.out.println("Existencia Final: " + Aux.Info.Existencia);
			Aux2 = Aux2.DameSig();
			Aux = Aux.DameSig();
			if(Aux  == null){
				break;
			}
		}
		
	}

}
