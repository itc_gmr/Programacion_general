
public class AplColaUnoV2 {
	static class Dato {
		public int Contador;
		public Dato(){
			Contador=0;
		}
	}
	public static void main(String [] a){
		Cola<Integer> Obj=new Cola<Integer>(100);
		Cola<Integer> Aux=new Cola<Integer>(100);
		Cola<Dato> Fre=new Cola<Dato>(10);
		Cola<Dato> FreAux=new Cola<Dato>(10);
		int Cont=0;
		while(Fre.Inserta(new Dato()));
		while(Obj.Inserta(new Integer(MyRandom.nextInt(1,10))));
		System.out.println("Total de inserciones"+Cont);
		
		while(Obj.Retira() && Aux.Inserta(Obj.Dr)){
		
			int i=0;
			for(i=0;i<Obj.Dr;i++){
				Fre.Retira();
				FreAux.Inserta(Fre.Dr);
			}
			Fre.Dr.Contador++;
		
			while(Fre.Retira() && FreAux.Inserta(Fre.Dr));
			while(FreAux.Retira() && Fre.Inserta(FreAux.Dr));
		}
		
		System.out.println("Tabla de frecuencia\nNo, \t Frecuencia");
		while(Fre.Retira() && FreAux.Inserta(Fre.Dr)){
			System.out.println((++Cont)+"\t"+Fre.Dr.Contador);
		}
		
		
	}
}
