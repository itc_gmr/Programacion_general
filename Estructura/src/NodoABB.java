
public class NodoABB <T>{
	public NodoABB <T>   SubIzq;
	   public T Info;
	   public NodoABB<T>   SubDer;
	   public NodoABB(T d)
	   {  Info=d;
	      SubIzq=null; SubDer=null;
	   }
	   public NodoABB<T> DameSubDer(){
			 return SubDer;
		 }
		 public NodoABB<T> DameSubIzq(){
			 return SubIzq;
		 }
		 public void setSubIzq(NodoABB<T> Ap){
			 this.SubIzq=Ap;
		 }
		 public void setSubDer(NodoABB<T> Ap){
			 this.SubDer=Ap;
		 }	 

}
