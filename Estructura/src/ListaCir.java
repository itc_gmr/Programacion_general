public class ListaCir <T> {
	private Nodo<T> Frente;
	private Nodo<T> Fin;
	public T        Dr;
	public ListaCir(){
		Frente=Fin=null;
		Dr=null;
	}
	public Nodo<T> DameFrente(){
		return Frente;
	}
	public Nodo<T> DameFin(){
		return Fin;
	}
	public boolean InsertaFrente(T Dato){
		Nodo<T> Nuevo=new Nodo<T>(Dato);
		if(Nuevo==null)
			return false;
		if(Frente==null){
			Frente=Fin=Nuevo;
			Frente.Sig=Frente;
			return true;
		}
		Nuevo.Sig=Frente;
		Frente=Nuevo;
		Fin.Sig=Frente;
		return true;
		
	}
	public boolean InsertaFin(T Dato){
		Nodo<T> Nuevo=new Nodo<T>(Dato);
		if(Nuevo==null)
			return false;
		if(Frente==null){
			Frente=Fin=Nuevo;
			Frente.Sig=Frente;
			return true;
		}
		Fin.Sig=Nuevo;
		Fin=Nuevo;
		Fin.Sig=Frente;
		return true;
			
	}
	
	public boolean Retira(T Dato){
		String Id_Nuevo=Dato.toString();
		String Id_Nodo;
		//Buscar el nodo que contiene id_nuevo
		Nodo<T> Tra=Frente,Ant=null;
		boolean Band=false;
		while (Tra!=null){
			Id_Nodo=Tra.Info.toString();
			if(Id_Nuevo.equals(Id_Nodo)){
				Band=true;
				break;
			}
			if(Tra.Sig==Frente)
				break;
			Ant=Tra;
			Tra=Tra.Sig;
		}
		if(!Band)
			return false;
		Dr=Tra.Info;
		//Borrar el �nico nodo de la lista
		if(Frente==Fin){
			Frente=Fin=null;
			return true;
		}
		// Borrar el primer nodo de la lista
		if(Tra==Frente){
			Frente=Frente.Sig;
			Fin.Sig=Frente;
			return true;
		}
		//Borrar el �ltimo no de la lista
		if(Tra==Fin){
			Fin=Ant;
			Fin.Sig=Frente;
			return true;
		}
		// entre dos nodos
		Ant.Sig=Tra.Sig;
		return true;
	}
	public boolean InsertaOrd(T Dato){
		Nodo<T> Nuevo=new Nodo<T>(Dato);
		if(Nuevo==null)
			return false;
		//primer nodo de la lista
		if(Frente==null){
			Frente=Fin=Nuevo;
			Frente.Sig=Frente;
			return true;
		}
		// el nuevo menor que el que esta enn frente
		if(Nuevo.Info.toString().compareTo(Frente.Info.toString())<=0 )  {
			Nuevo.Sig=Frente;
			Frente=Nuevo;
			Fin.Sig=Frente;
			return true;
		}
		// el nuevo es mayor que el que esta como �ltimo
		if(Nuevo.Info.toString().compareTo(Fin.Info.toString())>=0 )  {
			Fin.Sig=Nuevo;
			Fin=Nuevo;
			Fin.Sig=Frente;
			return true;
		}
		// enntre dos nodos
		Nodo<T> Ant=null,Aux=Frente;
		while ( Aux.Info.toString().compareTo(Nuevo.Info.toString()) < 0 ){
			Ant=Aux;
			Aux=Aux.Sig;
		}
		Ant.Sig=Nuevo;
		Nuevo.Sig=Aux;
		return true;
	}
	public boolean Busca(T Dato){
		String IdDato=Dato.toString();
		boolean Band=false;
		Nodo<T> Aux=Frente;
		while (Aux !=null){
			if(IdDato.compareTo(Aux.Info.toString())==0){
				Band=true;
				Dr=Aux.Info;
				break;
			}
			if(Aux.Sig==Frente)
				break;
			Aux=Aux.Sig;
		}
		return Band;
	}

}
