
public class Recursividad {
	static int Contador=0;
	   public void Hanoi(char Inicial,char Central,char Final,int N) {
	   	  if(N==1) {
	         Contador++;
	   	  	System.out.println(Contador + " Move disco "+N+" de la torre "+Inicial+" a la torre "+Final);
	   	  }
	   	  else {
	   	  	Hanoi(Inicial,Final,Central,N-1);
	        Contador++;
	   	  	System.out.println(Contador   +    " Move disco "+N+" de la torre "+Inicial+" a la torre "+Final);
	   	  	Hanoi(Central,Inicial,Final,N-1);
	   	  }
	   }
	   
	public int Fact(int n){
		if(n==0 || n==1)
			return 1;
		return n*Fact(n-1);
	}
	public long Fib(int n){
		if(n<3)
			return 1;
		return Fib(n-1)+Fib(n-2);
	}
	
	public long Eleva(int X,int Y){
/*		if(Y==0)
			return 1;
		if(Y==1)
			return X;
		return X*Eleva(X,Y-1);
*/
		if(Y==0)
			return 1;
		if(Y==1)
			return X;
		if(Y%2==0)
			return Eleva(X*X,Y/2);
		return Eleva(X*X,Y/2)*X;
	}
	public int Suma(int [] V,int NE){
		if(NE==0)
			return 0;
		if(NE==1)
			return V[0];
		return Suma(V,NE-1)+ V[NE-1];
	}
	public int Menor(int [] V,int NE){
		if(NE==1)
			return V[0];
		int M=Menor(V,NE-1);
		return V[NE-1]<M? V[NE-1]:M;
/*		int M=Menor(V,NE-1);
		if(V[NE-1]<M)
			return V[NE-1];
		return M;
*/
	}
	public void Frecuencia(int [] V,int NE,int [] FRE){
		
		if(NE==0)
			return;
		Frecuencia(V,NE-1,FRE);
		FRE[V[NE-1]-1]++;  // int Sub=V[NE-1]; FRE[Sub-1]++;
	}
	public String Binario(int Num){
		if(Num==0 || Num==1)
			return Num+"";
		return Binario(Num/2)+Num%2;
	}
	public String Octal(int Num){
		if(Num<8)
			return Num+"";
		return Octal(Num/8)+Num%8;
		
	}
	String Cadena="0123456789ABCDEF";
	public String Convierte(int Num,int Base){
		if(Num<Base)
			return Cadena.charAt(Num)+"";
		return Convierte(Num/Base,Base)+Cadena.charAt(Num%Base);
	}
	public int MCD(int X,int Y){
		if(X==Y)
			return X;
		if(Y>X)
			return MCD(Y,X);
		return MCD(X-Y,Y);
	}
	
}
