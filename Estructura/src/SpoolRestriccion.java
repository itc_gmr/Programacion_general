import java.util.*;
class Reporte{
	public int NoComp;
	public int NoHojas;
	public int Cantidad;
	public Reporte(){
		this(0,0,0);
	}
	public Reporte(int NoComp,int NoHojas,int Cantidad){
		this.NoComp=NoComp;
		this.NoHojas=NoHojas;
		this.Cantidad=Cantidad;		
	}
}
public class SpoolRestriccion {
	static int Tam=MyRandom.nextInt(50,100);
	static int ElementosQuedan=Tam;
	static int NoAcciones=0,Existen=0;
	static ColaCir<Reporte> Spool=new ColaCir<Reporte> (Tam);
	static ColaCir<Reporte> Aux=new ColaCir<Reporte> (Tam);
	public static void main(String [] a){
		int Accion;
		while (NoAcciones < 50){
			Accion=MyRandom.nextInt(1,3);
			switch (Accion){
				case 1: EnviaReporte();break;
				case 2: ImprimeReporte();break;
				case 3: EdoActualSpool();break;
			}
		}
	}
	public static void EnviaReporte(){
		int NoComp=MyRandom.nextInt(1,5);
		int NoHojas=MyRandom.nextInt(1,500);
		int ElementosOcupa=NoHojas/100;
		if(NoHojas%100 >0)
			ElementosOcupa++;
		if(ElementosOcupa>ElementosQuedan){
			System.out.println(" No se puede Insertar, Requiere "+ElementosOcupa+" elementos, solo hay "+ElementosQuedan);
			return;
		}		
		NoAcciones++;
		System.out.println("REPORTE AL SPOOL COMP "+NoComp+" HOJAS "+NoHojas);
		int TotalHojas=NoHojas;
		Reporte Rep;
		while (NoHojas>0){

			if(NoHojas>99){
				Rep=new Reporte(NoComp,TotalHojas,100);
				NoHojas-=100;
			}
			else{
				Rep=new Reporte(NoComp,TotalHojas,NoHojas);
				NoHojas=0;
			}
			Spool.Inserta(Rep);				
		}
	}
	public static void ImprimeReporte(){
		int TotalHojas,Cantidad;
		if (Spool.Retira() ){
			System.out.println("REPORTE IMPRESO COMP "+Spool.Dr.NoComp+" paginas "+Spool.Dr.NoHojas);
			NoAcciones++;
			TotalHojas=Spool.Dr.NoHojas;
			Cantidad=Spool.Dr.Cantidad;
			while (Cantidad < TotalHojas)  {
				Cantidad+=Spool.Dr.Cantidad;
				Spool.Retira();
			}
		}
	}
	public static void EdoActualSpool(){
		System.out.println("_______________________________________");
		System.out.println("Estado actual del spool con restricción");
		System.out.println("No Comp \t Total Hojaas");
		int TotalHojas,Cantidad;
		while (Spool.Retira() && Aux.Inserta(Spool.Dr)){
			TotalHojas=Spool.Dr.NoHojas;
			Cantidad=Spool.Dr.Cantidad;
			System.out.println(Spool.Dr.NoComp+"\t"+Spool.Dr.NoHojas+"\t"+Spool.Dr.Cantidad);		
			while (Cantidad < TotalHojas)  {
	//			System.out.println(Spool.Dr.NoComp+"\t"+Spool.Dr.NoHojas+"\t"+Spool.Dr.Cantidad);
				Cantidad+=Spool.Dr.Cantidad;
				Spool.Retira();
				Aux.Inserta(Spool.Dr);
			}
	//		System.out.println(Spool.Dr.NoComp+"\t"+Spool.Dr.NoHojas+"\t"+Spool.Dr.Cantidad);
		}
		while(Aux.Retira() && Spool.Inserta(Aux.Dr));
		System.out.println("_______________________________________");
	}
}
