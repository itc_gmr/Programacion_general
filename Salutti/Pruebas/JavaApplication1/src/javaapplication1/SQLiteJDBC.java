package javaapplication1;

import java.sql.*;

public class SQLiteJDBC
{
    Connection c = null;
    Statement stmt = null;
     ResultSet rs = null;
  public SQLiteJDBC(){
    /*c = null;
    try {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:test.db");
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    System.out.println("Opened database successfully");*/
  }
  
  public void createTableClientes(){
    c = null;
    stmt = null;
    try {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:test.db");
      System.out.println("Opened database successfully");

      stmt = c.createStatement();
      String sql = "CREATE TABLE CONTACTOS " +
                   "(ID_Cliente INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL," +
                   " Nombre           TEXT    NOT NULL, " + 
                   " Apellido            TEXT     NOT NULL, " + 
                   " Direccion        TEXT, " + 
                   " Telefono         TEXT)"; 
      stmt.executeUpdate(sql);
      stmt.close();
      c.close();
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    System.out.println("Table created successfully");
  }
  
   public void createTableGastos(){
        c = null;
        stmt = null;
        try{
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:test.db");
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "CREATE TABLE PAGO " +
                   "(ID_PAGO INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL," +
                   " Fecha           TEXT    NOT NULL, " + 
                   " Descripcion            TEXT     NOT NULL, " + 
                   " MONTO        REAL  NOT NULL, " + 
                    " LUGAR        TEXT  NOT NULL, " + 
                   " FORMA_PAGO         TEXT    NOT NULL)"; 
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
         System.out.println("Table created successfully");
   }
}
