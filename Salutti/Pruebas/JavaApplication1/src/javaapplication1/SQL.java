/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;
import java.sql.*;
public class SQL {
    Connection c = null;
    Statement stmt = null;
    ResultSet rs = null;
    
     public void createTableFormaPago(){
         c = null;
         stmt = null;
         try{
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:control.db");
            System.out.println("Opened database successfully");
            stmt = c.createStatement();
            
             String sql = "CREATE TABLE FormaPago " +
                   "(ID_FP INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL," +
                   " Descripcion            TEXT     NOT NULL)";   
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
         }catch(Exception e){
              System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
         }
         System.out.println("Table created successfully");
     }
     
     public void createTableGastos(){
        c = null;
        stmt = null;
        try{
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:control.db");
            System.out.println("Opened database successfully");

            stmt = c.createStatement();
            String sql = "CREATE TABLE PAGO " +
                   "(ID_PAGO INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL," +
                   " Fecha           TEXT    NOT NULL, " + 
                   " Descripcion            TEXT     NOT NULL, " + 
                   " MONTO        REAL  NOT NULL, " + 
                    " LUGAR        TEXT  NOT NULL, " + 
                   " FORMA_PAGO     INTEGER    REFERENCES FormaPago)"; 
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
         System.out.println("Table created successfully");
   }
}
