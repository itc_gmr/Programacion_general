/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.numberaddition;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Ejemplar
 */
@Entity
@Table(name = "suma", catalog = "salutti", schema = "")
@NamedQueries({
    @NamedQuery(name = "Suma.findAll", query = "SELECT s FROM Suma s"),
    @NamedQuery(name = "Suma.findByNum1", query = "SELECT s FROM Suma s WHERE s.num1 = :num1"),
    @NamedQuery(name = "Suma.findByNum2", query = "SELECT s FROM Suma s WHERE s.num2 = :num2"),
    @NamedQuery(name = "Suma.findByRes", query = "SELECT s FROM Suma s WHERE s.res = :res"),
    @NamedQuery(name = "Suma.findById", query = "SELECT s FROM Suma s WHERE s.id = :id")})
public class Suma implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "num1")
    private Double num1;
    @Column(name = "num2")
    private Double num2;
    @Column(name = "res")
    private Double res;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    public Suma() {
    }

    public Suma(Long id) {
        this.id = id;
    }

    public Double getNum1() {
        return num1;
    }

    public void setNum1(Double num1) {
        Double oldNum1 = this.num1;
        this.num1 = num1;
        changeSupport.firePropertyChange("num1", oldNum1, num1);
    }

    public Double getNum2() {
        return num2;
    }

    public void setNum2(Double num2) {
        Double oldNum2 = this.num2;
        this.num2 = num2;
        changeSupport.firePropertyChange("num2", oldNum2, num2);
    }

    public Double getRes() {
        return res;
    }

    public void setRes(Double res) {
        Double oldRes = this.res;
        this.res = res;
        changeSupport.firePropertyChange("res", oldRes, res);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        Long oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Suma)) {
            return false;
        }
        Suma other = (Suma) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "my.numberaddition.Suma[ id=" + id + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
