/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

/**
 *
 * @author Guillermo
 */
public abstract class Figura 
{
    protected String nombre;
    
    public Figura(String n)
    {
        this.nombre = n;
    }
     public abstract double calcularArea(double a, double b);
     
         
     
}
