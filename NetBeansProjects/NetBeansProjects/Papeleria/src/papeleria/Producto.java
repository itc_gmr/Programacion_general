/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package papeleria;
import java.util.Scanner;
/**
 *
 * @author Guillermo
 */
public abstract class Producto 
{
    String nombre;
    double precio;
    int existencia;
    int codigo;
    double caja;
    int k;
    Scanner s = new Scanner(System.in);
    
    
    public Producto()
    {
        System.out.println("Nombre del producto");
        this.nombre = s.nextLine();
        System.out.println("Precio del producto");
        this.precio = s.nextDouble();
        System.out.println("Codigo del producto");
        this.codigo = s.nextInt();
        System.out.println("Existencia del producto");
        this.existencia = s.nextInt();

        
    }
    
    public void Descontar()
    {
        double t;
        
        Scanner s = new Scanner(System.in);
        System.out.println("Cantidad a llevar: ");
        k = s.nextInt();
        if (k > this.existencia)
        {
           System.out.println("No hay suficiente producto");
          
        }
        else
        {
            this.existencia = this.existencia - k;
            t = this.precio * k;
            caja = t;
        }
    }
    
    
    
}
