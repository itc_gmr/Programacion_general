/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escuela;

/**
 *
 * @author ADM
 */
public class Empleado 
{
    int numEmp;
    private int sueldo;
    private int edad;
    private String nombre;
    
    public Empleado(int numEmp, int sueldo, int edad, String nombre)
    {
        this.edad = edad;
        this.nombre = nombre;
        this.numEmp = numEmp;
        this.sueldo = sueldo;
    }
    
    public String getNombre()
    {
        return nombre;
    }
    
    public void setNombre(String nom)
    {
        this.nombre = nom;
    }
       
    
    
}
