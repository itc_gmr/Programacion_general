/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escuela;

/**
 *
 * @author ADM
 */
public class Docente extends Empleado
{
    String materia;
    int hora;
    
    public Docente(int numEmp, String nombre, int edad, double sueldo, String materia, int hora)
    {
        super(numEmp, nombre, edad, sueldo);
        this.hora = hora;
        this.materia = materia;
        this.numEmp = numEmp;
        
    }
}
