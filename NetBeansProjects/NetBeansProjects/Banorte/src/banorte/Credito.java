/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banorte;

/**
 *
 * @author ADM
 */
public class Credito extends Cuenta
{
    public Credito(String tipo, int numCuenta, String nomCliente)
    {
        super(tipo, numCuenta, nomCliente);
    
    }
    
    public void depositar (double cantidad)
    {
        this.saldo -= cantidad;
    }
    
    public void comprar (double compra)
    {
        this.saldo += compra + (compra * 0.15);
    }
}
