/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banorte;

/**
 *
 * @author ADM
 */
public class Nomina extends Cuenta 
{
    public Nomina(String tipo, int numCuenta, String nomCliente)
    {
        super(tipo, numCuenta, nomCliente);
    }
    
    public void deposito(double cantidad)
    {
        this.saldo += cantidad;
        System.out.println("Usted Depositó: " + cantidad);
    }
    
    public void retiro(double cantidad)
    {
        if (this.saldo < cantidad)
        {
            System.out.println("Eres pobre, no tienes saldo");
            System.out.println("Tu saldo disponible es de: " + this.saldo);
        }
        else
        {
            this.saldo -= cantidad;
            System.out.println("Usted Retiró: " + cantidad);
            
        }
    }
}
