/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo;

/**
 *
 * @author ADM
 */
public class Docente extends Empleado
{
    public Docente(String nombre, int edad)
    {
        super(nombre, edad);
    }
    
    @Override
    public void trabajar()
    {
        System.out.println("Esta dando clases clase(Docente)");
    }
}
