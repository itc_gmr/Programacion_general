/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto.pila;
class disco{
    int numeroD;
    int vidaU;
    public disco(int numeroD, int vidaU){
        this.numeroD = numeroD;
        this.vidaU = vidaU;
    }
}
/**
 *
 * @author Ejemplar
 */
public class ProyectoPila {
    static int longitud;
    static Pila<disco> discos;
    static Pila<disco> aux;
    static Pila<disco> aux2;
    static disco d;
    
    public ProyectoPila(){
        longitud = MyRandom.nextInt(15, 50);
        discos = new Pila<disco>(longitud);
        aux = new Pila<disco>(longitud);
        aux2 = new Pila<disco>(longitud);
        
    }
    public static void main(String[] args) {
        ProyectoPila p = new ProyectoPila();
        genera();
        while(true){
            generaBaches();
            int cont2 = 0;
            while(discos.Retira() && aux2.Inserta(discos.Dr)){
                cont2++;
            }
            while(aux2.Retira() && discos.Inserta(aux2.Dr)){
            
            } 
            if(cont2 <= 4){
                while(discos.Retira() && aux2.Inserta(discos.Dr)){
                    System.out.println(discos.Dr.numeroD + " " +discos.Dr.vidaU);
                }
                while(aux2.Retira() && discos.Inserta(aux2.Dr)){
            
                } 
                System.out.println("Proceso terminado");
                return;
            }
        }
        
        
    }
    
    public static void genera(){
        int n = 1;
        while(!discos.Llena()){
            d = new disco (n++, MyRandom.nextInt(95, 100));
            discos.Inserta(d);
        }
        
    }
    
    public static void imprimeDiscos(){
       while(discos.Retira() && aux2.Inserta(discos.Dr)){
            System.out.println(discos.Dr.numeroD);
        }
        while(aux2.Retira() && discos.Inserta(aux2.Dr)){
            
        } 
    }
    
    public static void generaBaches(){
        int baches = MyRandom.nextInt(1, 3);
        int cont = 0;
        discos.Retira();
        System.out.print("Disco afectado: " + discos.Dr.numeroD + "   Vida inicial : " + discos.Dr.vidaU + "   ");
        discos.Dr.vidaU = discos.Dr.vidaU - baches;
        System.out.println("Vida restante: " + discos.Dr.vidaU + "   ");
        aux.Inserta(discos.Dr);
        while(discos.Retira() && aux2.Inserta(discos.Dr));
        aux2.Retira();
        baches = MyRandom.nextInt(1, 3);
        System.out.print("Disco afectado: " + aux2.Dr.numeroD + "   Vida inicial : " + aux2.Dr.vidaU + "   ");
        aux2.Dr.vidaU = aux2.Dr.vidaU - baches;
        System.out.println("Vida restante: " + aux2.Dr.vidaU + "   ");
        aux.Inserta(aux2.Dr);
        for(int i=0; i<(longitud-2)/2; i++){
            aux2.Retira();
            discos.Inserta(aux2.Dr);
        }
        aux.Retira();
        if(aux.Dr.vidaU > 30){
            discos.Inserta(aux.Dr);
        }
        else{
            longitud--;
            System.out.println("Disco "+ aux.Dr.numeroD + " destruido");
        }
        aux.Retira();
        if(aux.Dr.vidaU > 30){
            discos.Inserta(aux.Dr);
        }
        else{
            longitud--;
            System.out.println("Disco "+ aux.Dr.numeroD + " destruido");
        }
        while(aux2.Retira() && discos.Inserta(aux2.Dr));
    }
    
}
