//GUILLERMO MARQUEZ RUIZ
package aplcolaprojecto;

class dato{
    int NoComputadora;
    int NoHojas;
    public dato(int NoComputadora, int NoHojas){
        this.NoComputadora = NoComputadora;
        this.NoHojas = NoHojas;
    }
}

public class AplColaProjecto {

    public static int capacidad = MyRandom.nextInt(20, 50);
    public static Cola<dato> spool = new Cola<dato> (capacidad);
    public static Cola<dato> aux = new Cola<dato> (capacidad);
    
    public AplColaProjecto(){
        Actividades();
    }
 
    public static void main(String[] args) {
        AplColaProjecto obj = new AplColaProjecto();
        
    }
    
    public static void Actividades(){
        System.out.println("------Simulación de Cola de impresión------");
        
        int AC = 0;
        int act;
        while(true){
            act = MyRandom.nextInt(1, 3);
           
            switch(act){
                case 1:
                     System.out.println("Inserción de Dato");
                    dato Ac = new dato(MyRandom.nextInt(1, 5), MyRandom.nextInt(1, 500));
                    if(spool.Inserta(Ac)){
                        AC++;
                        System.out.println("Se insertó dato en Spool: " + Ac.NoComputadora + "  " + Ac.NoHojas);
                    }
                    else{
                         System.out.println("Error, Spool lleno");
                    }
                    break;
                case 2:
                    System.out.println("Retiro de Dato");
                    if(spool.Retira()){
                        AC++;
                        System.out.println("Se retiró dato en Spool: " + spool.Dr.NoComputadora + "  " + spool.Dr.NoHojas);
                    }
                    else{
                         System.out.println("Error, Spool vacio");
                    }
                    break;
                case 3:
                    impresion();
                    AC++;
                    break;
            }
            if(AC == 50)
                break;
        }
    }
    
    public static void impresion(){
        int cont = 0;
        int faltan = 0;
        int tiene = 0;
        int suma;
        dato dFalso = new dato(-1, -1);
        while(spool.Inserta(dFalso)){
            faltan++;
        }
        while(spool.Retira()){
            if(spool.Dr.NoComputadora > 0){ 
                aux.Inserta(spool.Dr);
                tiene++;
            }
        }
        suma = capacidad - (faltan + tiene);
        
        while(spool.Inserta(dFalso)){
            cont++;
            if(suma == cont)
                break;
        }
        while(spool.Retira()){
            if(spool.Dr.NoComputadora > 0)
                spool.Inserta(spool.Dr);
        }
        System.out.println("-------------------------------------------");
        System.out.println("Estado actual del Spool: ");
        while(aux.Retira() && spool.Inserta(aux.Dr)){
            System.out.print("Computadora No: " + aux.Dr.NoComputadora + "   ");
            System.out.println("imprimirá " + aux.Dr.NoHojas + " hojas");
        }
    }
    
}
