/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibonashi;

/**
 *
 * @author Ejemplar
 */
public class Fibonashi {
   
    public static int serieFibonacci(int n){
        

        if ((n == 0) || (n == 1 ) || (n == 2)){
            return 1;
        }
        
        else{
            
            return serieFibonacci(n-1) + serieFibonacci(n-2);
                    

        }
       
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      System.out.println(serieFibonacci(20));

        
    }
    
}
