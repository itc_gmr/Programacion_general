//Guillermo Márquez Ruiz EC01 Estructura de Datos
package polinomios;
import java.util.*;
class Datos{
       int base, expo;
       
   }
public class PolinomiosV2 {
    static Scanner Leer = new Scanner(System.in);
    
    public static void main(String[] args){
        Lista<Datos> P1 = new Lista<Datos>();
        Lista<Datos> P2 = new Lista<Datos>();
        Lista<Datos> R = new Lista<Datos>();
        
        ingresaDatos(P1);
        ingresaDatos(P2);
        sumaPoli(P1, P2, R);
        imprime(P1);
        imprime(P2);
        imprime(R);
        
    }
    
    public static void ingresaDatos(Lista<Datos> poli){
        int cont = 1;
        System.out.println(".:Ingrese monomios del Polinomio:.");
        Datos Monomio;
        while (true){
            Monomio = new Datos();
            System.out.println("Base del Monomio "+ cont + " : ");
            Monomio.base = Leer.nextInt();
            System.out.println("Exponente del Monomio "+ cont++ + " (ingrese 0 si desea terminar de agregar monomios en la primera expresión): ");
             Monomio.expo = Leer.nextInt();
           if(Monomio.expo == 0 || Monomio.base == 0)
                break;
            poli.InsertaFin(Monomio);
        }
        
    }
    
     public static void imprime(Lista<Datos> poli){
       Nodo<Datos> Aux = poli.DameFrente();
       while(Aux != null){
           System.out.print(Aux.Info.base + "X^" + Aux.Info.expo);
            if(Aux.Sig != null){
                if (Aux.Sig.Info.base >=0 )
                    System.out.print("+");
                else
                    System.out.print("");
            }
            Aux=Aux.Sig;
        }
       System.out.println();
    }
     
     public static void sumaPoli(Lista<Datos> P1, Lista<Datos> P2, Lista<Datos> R){
         Datos suma;
         for(Nodo<Datos> Aux=P1.DameFrente() ; Aux !=null ; Aux=Aux.Sig){
             suma = new Datos();
             suma.base = Aux.Info.base;
             suma.expo = Aux.Info.expo;
             R.InsertaFin(suma);
         }
         for(Nodo<Datos> Aux=P2.DameFrente() ; Aux !=null ; Aux=Aux.Sig){
             suma = new Datos();
             suma.base = Aux.Info.base;
             suma.expo = Aux.Info.expo;
             Nodo<Datos> Aux3 = R.DameFrente();
             while (Aux3 != null){
                 if(Aux3.Info.expo == suma.expo){
                     break;
                 }
                 Aux3 = Aux.Sig;
             }
             if(Aux3 != null)
                 Aux3.Info.base += suma.base;
             else
                 R.InsertaFrente(suma);
         }
     }
}
