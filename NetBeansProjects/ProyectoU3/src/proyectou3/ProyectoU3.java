/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectou3;

class disco {
    int NumDisco;
    int VidUtil;
    
    public disco(){
       this.NumDisco = 0;
       this.VidUtil = 0;
}
}
/**
 *
 * @author Ejemplar
 */
public class ProyectoU3 {
    
    public static void main(String[] args) {
        int longitud = MyRandom.nextInt(15, 50);
        Pila<disco> obj = new Pila<disco>(longitud);
        Pila<disco> aux2 = new Pila<disco>(longitud);
        disco d = new disco();
        while(true){
            d.NumDisco = MyRandom.nextInt(1000, 5000);
            d.VidUtil = MyRandom.nextInt(95, 100);
            while (obj.Inserta(d)){
                break;
            }
            if(obj.Llena())
                break;
        }
        
        while(obj.Retira()&& aux2.Inserta(obj.Dr))
                System.out.println(obj.Dr.NumDisco + "           " + obj.Dr.VidUtil);
        while(aux2.Retira()&& obj.Inserta(aux2.Dr));
        while(true){
            GeneraBauches(obj, d, longitud, aux2);
            int cont2 = 0;
            while(obj.Retira() && aux2.Inserta(obj.Dr)){
                cont2++;
            }
            while(aux2.Retira() && obj.Inserta(aux2.Dr));
            if(cont2 == 4)
                return;
        }
    } 
  
    public static void GeneraBauches(Pila<disco> p, disco d, int longitud, Pila<disco> aux2){
        Pila<disco> aux = new Pila<disco>(longitud); 
        int bauches = MyRandom.nextInt(1, 3);
        int cont = 0;
        p.Retira();
        System.out.print("Disco afectado: " + p.Dr.NumDisco + "   Vida inicial : " + p.Dr.VidUtil + "   ");
        p.Dr.VidUtil = p.Dr.VidUtil - bauches;
        System.out.println("Vida restante: " + p.Dr.VidUtil + "   ");
        aux.Inserta(p.Dr);
        while(p.Retira() && aux2.Inserta(p.Dr)){
            System.out.println(p.Dr.NumDisco + "   " + p.Dr.VidUtil);
        }
        aux2.Retira();
        bauches = MyRandom.nextInt(1, 3);
        System.out.print("Disco afectado: " + aux2.Dr.NumDisco + "   Vida inicial : " + aux2.Dr.VidUtil + "   ");
        aux2.Dr.VidUtil = aux2.Dr.VidUtil - bauches;
        System.out.println("Vida restante: " + aux2.Dr.VidUtil + "   ");
        aux.Inserta(aux2.Dr);
        while(aux2.Retira() && p.Inserta(aux2.Dr)){
             cont++;
            if(cont == (longitud/2)){
               
                 while(aux.Retira()){
                     
                     if(aux.Dr.VidUtil >= 30){
                         
                         p.Inserta(aux.Dr);
                         
                     }
                     
                 }
                 
            }
        }
        
    }
    
    public static disco generaNum(disco a){
        a.NumDisco = MyRandom.nextInt(1000, 5000);
        a.VidUtil = MyRandom.nextInt(95, 100);
        return a;
    }
        
    
    
}
