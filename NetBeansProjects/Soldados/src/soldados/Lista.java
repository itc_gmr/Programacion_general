/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soldados;

/**
 *
 * @author guill
 */
public class Lista <T> {
    private Nodo<T> Frente;
	private Nodo<T> Fin;
	public T        Dr;
	public Lista(){
		Frente=Fin=null;
		Dr=null;
	}
	public Nodo<T> DameFrente(){
		return Frente;
	}
	public Nodo<T> DameFin(){
		return Fin;
	}
	public boolean InsertaFrente(T Dato){
		Nodo<T> Nuevo=new Nodo<T>(Dato);
		if(Nuevo==null)
			return false;
		if(Frente==null){
			Frente=Fin=Nuevo;
			return true;
		}
		Nuevo.Sig=Frente;
		Frente=Nuevo;
		return true;
		
	}
	public boolean InsertaFin(T Dato){
		Nodo<T> Nuevo=new Nodo<T>(Dato);
		if(Nuevo==null)
			return false;
		if(Frente==null){
			Frente=Fin=Nuevo;
			return true;
		}
		Fin.Sig=Nuevo;
		Fin=Nuevo;
		return true;
			
	}
	
	public boolean Retira(T Dato){
		String Id_Nuevo=Dato.toString();
		String Id_Nodo;
		//Buscar el nodo que contiene id_nuevo
		Nodo<T> Tra=Frente,Ant=null;
		boolean Band=false;
		while (Tra!=null){
			Id_Nodo=Tra.Info.toString();
			if(Id_Nuevo.equals(Id_Nodo)){
				Band=true;
				break;
			}
			Ant=Tra;
			Tra=Tra.Sig;
		}
		if(!Band)
			return false;
		Dr=Tra.Info;
		//Borrar el único nodo de la lista
		if(Frente==Fin){
			Frente=Fin=null;
			return true;
		}
		// Borrar el primer nodo de la lista
		if(Tra==Frente){
			Frente=Frente.Sig;
			return true;
		}
		//Borrar el último no de la lista
		if(Tra==Fin){
			Fin=Ant;
			Fin.Sig=null;
			return true;
		}
		// entre dos nodos
		Ant.Sig=Tra.Sig;
		return true;
	}
	public boolean InsertaOrd(T Dato){
		
		Nodo<T> Nuevo=new Nodo<T>(Dato);
		if(Nuevo==null)
			return false;
		//primer nodo de la lista
		if(Frente==null){
			Frente=Fin=Nuevo;
			return true;
		}
		// el nuevo menor que el que esta enn frente
		if(Nuevo.Info.toString().compareTo(Frente.Info.toString())<=0 )  {
			Nuevo.Sig=Frente;
			Frente=Nuevo;
			return true;
		}
		// el nuevo es mayor que el que esta como último
		if(Nuevo.Info.toString().compareTo(Fin.Info.toString())>=0 )  {
			Fin.Sig=Nuevo;
			Fin=Nuevo;
			return true;
		}
		// enntre dos nodos
		Nodo<T> Ant=null,Aux=Frente;
		while ( Aux.Info.toString().compareTo(Nuevo.Info.toString()) < 0 ){
			Ant=Aux;
			Aux=Aux.Sig;
		}
		Ant.Sig=Nuevo;
		Nuevo.Sig=Aux;
		return true;
	}
	public boolean Busca(T Dato){
		String IdDato=Dato.toString();
		boolean Band=false;
		Nodo<T> Aux=Frente;
		while (Aux !=null){
			if(IdDato.compareTo(Aux.Info.toString())==0){
				Band=true;
				Dr=Aux.Info;
				break;
			}
			Aux=Aux.Sig;
		}
		return Band;
	}
}