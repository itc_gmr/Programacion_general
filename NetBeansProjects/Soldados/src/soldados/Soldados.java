//Guillermo Márquez Ruiz EC01 Estructura de Datos
package soldados;
class Dato{
    public int NoSoldado; 
    public int Edad;      
    public Dato(int NoSoldado, int Edad){
        this.NoSoldado = NoSoldado;
        this.Edad = Edad;
    }
    public String toString(){
		return PonCeros(NoSoldado,7);
	}
    
    public String PonCeros(int Valor,int Tam){
	String Cad=Valor+"";
	while(Cad.length()<Tam)
            Cad="0"+Cad;
	return Cad;
    }
   

}

public class Soldados {


    public static void main(String[] args) {
        Lista<Dato> L1 = new Lista<Dato>();
        for(int i = 0; i < MyRandom.nextInt(5, 10); i++){
            Dato Nvo = new Dato(MyRandom.nextInt(1, 10000), MyRandom.nextInt(18, 35));
            if (busca(L1, Nvo)){
                continue;
            }
            L1.InsertaFin(Nvo);
            System.out.println("No. " + Nvo.NoSoldado + " --- Edad: " + Nvo.Edad);
        }
        Nodo<Dato> Aux = L1.DameFrente();
        int Ale;
        while(true){
            Ale = MyRandom.nextInt(1, 2);
            for(int i = 0; i < Ale; i++){
                if(Aux.Sig == null)
                    Aux = L1.DameFrente();
                System.out.println(Aux.Info.NoSoldado);
                Aux = Aux.Sig;
            }
            System.out.println(!L1.Retira(Aux.Info));
            System.out.println("Aleatorio: " + Ale + "  Soldado Salvado: " + L1.Dr.NoSoldado);
            if(estadoActual(L1) == 1){
                System.out.println("Soldado a realizar la Misión = " + perdedor(L1));
                break;
            }
                
        }
        
    }
    
    public static int estadoActual(Lista<Dato> L1){
        int cont = 0;
        for(Nodo<Dato> Aux=L1.DameFrente() ; Aux !=null ; Aux=Aux.Sig){
            cont++;
        }
        return cont;
    }
    
    public static int perdedor(Lista<Dato> L1){
        Dato P = new Dato(0,0);
        for(Nodo<Dato> Aux=L1.DameFrente() ; Aux !=null ; Aux=Aux.Sig){
            P = new Dato(Aux.Info.NoSoldado, Aux.Info.Edad);
        }
        return P.NoSoldado;
    }
    
    public static boolean busca(Lista<Dato> L, Dato nuevo){
       Nodo<Dato>  Aux = L.DameFrente();
       while(Aux != null){
           if(nuevo.NoSoldado == Aux.Info.NoSoldado){
               return true;
           }
           Aux = Aux.Sig;
       }
       return false;
    }

}
