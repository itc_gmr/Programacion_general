/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ecuaciones_no_lineales;

import java.util.ArrayList;
import java.util.List;
import org.lsmp.djep.djep.DJep;
import org.nfunk.jep.JEP;
import org.nfunk.jep.Node;

/**
 *
 * @author Darwin
 */
public class Metodo_de_la_secante {

    private static List<String> x_i = new ArrayList<String>();// almacena las iteraciones realizadas

    public String calcular_raiz(String funcion, String a1, String b1, String tol) {
        String mensaje = null;
        double x0, x1;
        double fx0, fx1;// almacena el valor de f(x0) y f(x1)

        double fx;
        double tolerancia;
        double x = 0;//almacena el valorde  la raiz busdada


        try {

            JEP obj = new JEP();

            obj.parseExpression(tol); // paso de la expresi´on a evaluar
            tolerancia = obj.getValue();// toma el valor de f(x0)




            obj.addStandardFunctions(); // adiciona las funciones matem´aticas
            obj.addStandardConstants();
            obj.parseExpression(a1); // paso de la expresi´on a evaluar
            x0 = obj.getValue();// toma el valor de f(x0)

            obj.parseExpression(b1); // paso de la expresi´on a evaluar
            x1 = obj.getValue();// toma el valor de f(x0)




            JEP f = new JEP();
            f.addStandardFunctions(); // adiciona las funciones matem´aticas
            f.addStandardConstants(); // adiciona las constantes matem´aticas
            f.setImplicitMul(true);
            f.addVariable("x", x0);
            // obtener el resultado de evaluar la expresi´on
            f.parseExpression(funcion); // paso de la expresi´on a evaluar
            fx0 = f.getValue();

            f.addVariable("x", x1);
            // obtener el resultado de evaluar la expresi´on
            f.parseExpression(funcion); // paso de la expresi´on a evaluar
            fx1 = f.getValue();




            if (fx0 == 0 || Math.abs(fx0) <= tolerancia) {
                x = x0;
                mensaje = Double.toString(x);
            } else {

                if ((fx1 * (x0 - x1)) == 0 && (fx0 - fx1) == 0) {// controla que no se produzca una indeterminacion
                    x_i.clear();// borra todos los elemento de la coleccion
                    throw new Exception(" indeterminacion 0/0");
                }


                x = x1 - (fx1 * (x0 - x1)) / (fx0 - fx1);
                f.addVariable("x", x);
                // obtener el resultado de evaluar la expresi´on
                f.parseExpression(funcion); // paso de la expresi´on a evaluar
                fx = f.getValue();
                x_i.add(0, Double.toString(x));


                while (Math.abs(fx) > tolerancia && fx != 0) {// mientras |f(x)|>toleracia  y f(x)distinto de cero

                    x0 = x1;
                    x1 = x;


                    f.addVariable("x", x0);
                    // obtener el resultado de evaluar la expresi´on
                    f.parseExpression(funcion); // paso de la expresi´on a evaluar
                    fx0 = f.getValue();

                    f.addVariable("x", x1);
                    // obtener el resultado de evaluar la expresi´on
                    f.parseExpression(funcion); // paso de la expresi´on a evaluar
                    fx1 = f.getValue();



                    if ((fx1 * (x0 - x1)) == 0 && (fx0 - fx1) == 0) {// controla que no se produzca una indeterminacion
                        x_i.clear();// borra todos los elemento de la coleccion
                        throw new Exception(" indeterminacion 0/0");
                    }

                    x = x1 - (fx1 * (x0 - x1)) / (fx0 - fx1);


                    f.addVariable("x", x);
                    // obtener el resultado de evaluar la expresi´on
                    f.parseExpression(funcion); // paso de la expresi´on a evaluar
                    fx = f.getValue();

                    x_i.add(Double.toString(x));// adiere x a la coleccion




                }// fin (Math.abs(fx) > tolerancia && fx != 0)


                mensaje = Double.toString(x);// imprime el valor de la raiz aproximada




            }


        } catch (Exception e) {


            mensaje = e.getMessage();
        }




        return mensaje;
    }// fin metodo calcular_raiz

    public List retornaColeccion() {

        return x_i;
    }

    public void limpiarLista() {
        x_i.clear();

    }
}
