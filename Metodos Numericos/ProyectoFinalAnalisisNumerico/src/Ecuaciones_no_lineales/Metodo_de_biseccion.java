/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ecuaciones_no_lineales;

import java.util.ArrayList;
import java.util.List;
import org.nfunk.jep.JEP;

/**
 *
 * @author Darwin
 */
public class Metodo_de_biseccion {

    private static List<String> x_i = new ArrayList<String>();// almacena los valores de las iteraciones realizadas

    public String calcular_raiz(String funcion, String a1, String b1, String tol) {
        String mensaje = null;
        double a, b;
        double fa;
        double fb = 0;
        double fx;
        double tolerancia;
        double x = 0;


        try {
            JEP obj = new JEP();

            obj.parseExpression(tol); // paso de la expresi´on a evaluar
            tolerancia = obj.getValue();// toma el valor de tol





            obj.addStandardFunctions(); // adiciona las funciones matem´aticas
            obj.addStandardConstants();
            obj.parseExpression(a1); // paso de la expresi´on a evaluar
            a = obj.getValue();

            obj.parseExpression(b1); // paso de la expresi´on a evaluar
            b = obj.getValue();


            JEP f = new JEP();
            f.addStandardFunctions(); // adiciona las funciones matem´aticas
            f.addStandardConstants(); // adiciona las constantes matem´aticas
            f.setImplicitMul(true);
            f.addVariable("x", a);
            // obtener el resultado de evaluar la expresi´on
            f.parseExpression(funcion); // paso de la expresi´on a evaluar
            fa = f.getValue();

            // obtener el resultado de evaluar la expresi´on
            f.addVariable("x", b);
            f.parseExpression(funcion); // paso de la expresi´on a evaluar
            fb = f.getValue();



            if (fa == 0 || Math.abs(fa) <= tolerancia) {
                x = a;
                mensaje = Double.toString(x);
            }
            if (fa * fb < 0) {
                x = (a + b) / 2;
                f.addVariable("x", x);
                f.parseExpression(funcion); // paso de la expresi´on a evaluar
                fx = f.getValue();


                x_i.add(0, Double.toString(x));// adiere el primer elemento a la coleccion

                while (Math.abs(fx) > tolerancia && fx != 0) {
                    if (fa * fx < 0) {
                        b = x;
                    } else {
                        a = x;
                    }
                    x = (a + b) / 2;
                    f.addVariable("x", x);
                    f.parseExpression(funcion); // paso de la expresi´on a evaluar
                    fx = f.getValue();


                    x_i.add(Double.toString(x));// mientras no se cunpla la condicion establecida se adiere el valor de x a la colecion

                }
                mensaje = Double.toString(x);



            }
            if (fa * fb > 0) {
                mensaje = "f(a)*f(b)>0 ";
            }





        } catch (Exception e) {
            mensaje = "error ingreso datos";
        }




        return mensaje;
    }// fin metodo calcular_raiz

    public List retornaColeccion() {

        return x_i;
    }

    public void limpiarLista() {
        x_i.clear();

    }
}
