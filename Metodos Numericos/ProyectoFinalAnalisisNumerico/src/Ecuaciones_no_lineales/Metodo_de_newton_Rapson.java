/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ecuaciones_no_lineales;

import java.util.ArrayList;
import java.util.List;
import org.lsmp.djep.djep.DJep;
import org.nfunk.jep.JEP;
import org.nfunk.jep.Node;

/**
 *
 * @author Darwin
 */
public class Metodo_de_newton_Rapson {
    
    private static List<String> x_i=new ArrayList<String>();

    public String calcular_raiz(String funcion, String a1, String tol) {
        String mensaje = null;
        double x0;
        double fx0;// almacena el valor de f(x0)

        double fx;
        double tolerancia;
        double x = 0;//almacena el valorde  la raiz busdada


        try {
            
            
            JEP obj = new JEP();
            
            obj.parseExpression(tol); // paso de la expresi´on a evaluar
            tolerancia = obj.getValue();// toma el valor de f(x0)
            
            
            
           
            obj.addStandardFunctions(); // adiciona las funciones matem´aticas
            obj.addStandardConstants();
            obj.parseExpression(a1); // paso de la expresi´on a evaluar
            x0 = obj.getValue();// toma el valor de f(x0)




            JEP f = new JEP();
            f.addStandardFunctions(); // adiciona las funciones matem´aticas
            f.addStandardConstants(); // adiciona las constantes matem´aticas
            f.setImplicitMul(true);
            f.addVariable("x", x0);
            // obtener el resultado de evaluar la expresi´on
            f.parseExpression(funcion); // paso de la expresi´on a evaluar
            fx0 = f.getValue();


            DJep j = new DJep();//DJep es la clase encargada de la derivacion en su escencia
            String derivada = "";
            j.addStandardConstants();//agrega constantes estandares, pi, e, etc
            j.addStandardFunctions();//agrega funciones estandares cos(x), sin(x)
            j.addComplex();//por si existe algun numero complejo
            j.setAllowUndeclared(true);//permite variables no declarables
            j.setAllowAssignment(true);//permite asignaciones
            j.setImplicitMul(true);//regla de multiplicacion o para sustraccion y sumas
            j.addStandardDiffRules();



            Node node = j.parse(funcion);//coloca el nodo con una funcion preestablecida
            Node diff = j.differentiate(node, "x");//deriva la funcion con respecto a x
            Node simp = j.simplify(diff);//Simplificamos la funcion con respecto a x
            derivada = j.toString(simp);//Convertimos el valor simplificado en un String

            double dfx;// almacena el valor de f'(x0)


            f.addVariable("x", x0);
            // obtener el resultado de evaluar la expresi´on
            f.parseExpression(derivada); // paso de la expresi´on a evaluar
            dfx = f.getValue();







            if (fx0 == 0 || Math.abs(fx0) <= tolerancia) {
                x = x0;
                mensaje = Double.toString(x);
            } else {
                
                if (fx0==0 &&   dfx==0) {// controla que no se produzca la indeterminacion 0/0
                         x_i.clear();// se borra todos los elentos de la coleccion
                         throw new Exception("indeterminacion 0/0");                       
                    }
                    if (dfx==0) {// controla que no se produzca la division para cero
                        x_i.clear();// se borra todos los elentos de la coleccion
                        throw new Exception("división para cero encontrada");                       
                    }


                x = x0 - (fx0 / dfx);
                f.addVariable("x", x);
                // obtener el resultado de evaluar la expresi´on
                f.parseExpression(funcion); // paso de la expresi´on a evaluar
                fx = f.getValue();
                
                
                x_i.add(0,Double.toString(x));


                while (Math.abs(fx) > tolerancia && fx != 0) {
                    

                    x0 = x;


                    f.addVariable("x", x0);
                    // obtener el resultado de evaluar la expresi´on
                    f.parseExpression(funcion); // paso de la expresi´on a evaluar
                    fx0 = f.getValue();

                    f.addVariable("x", x0);
                    // obtener el resultado de evaluar la expresi´on
                    f.parseExpression(derivada); // paso de la expresi´on a evaluar
                    dfx = f.getValue();
                    
                    
                    if (fx0==0 &&   dfx==0) {// controla que no se produzca la indeterminacion 0/0
                         x_i.clear();// se borra todos los elentos de la coleccion
                        throw new Exception("indeterminacion 0/0 encontrada");                    
                    }
                    if (dfx==0) {// controla que no se produzca la division para cero
                        x_i.clear();// se borra todos los elentos de la coleccion
                        throw new Exception("división para cero encontrada");                       
                    }

                    x = x0 - (fx0 / dfx);
                    f.addVariable("x", x);
                    // obtener el resultado de evaluar la expresi´on
                    f.parseExpression(funcion); // paso de la expresi´on a evaluar
                    fx = f.getValue();
                    x_i.add(Double.toString(x));




                }
                
                
                
                mensaje = Double.toString(x);
            }




        } catch (Exception e) {
            mensaje=e.getMessage();
        }




        return mensaje;
    }// fin metodo calcular_raiz
    
    
    
    public  List retornaColeccion(){
        
        return x_i;
    }
    
    
    public  void limpiarLista(){
           x_i.clear();
                  
       }
}
