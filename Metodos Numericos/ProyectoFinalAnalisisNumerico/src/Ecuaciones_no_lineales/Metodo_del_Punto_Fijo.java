/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ecuaciones_no_lineales;

import java.util.ArrayList;
import java.util.List;
import org.nfunk.jep.JEP;

/**
 *
 * @author DESARROLLO
 */
public class Metodo_del_Punto_Fijo {

    private  List<String> x_i = new ArrayList<String>();// almacena los valores de las iteraciones realizadas

    public String calcularRaiz(String funcion, String g, String numMAX, String punto_fijo, String tol) {
        
        List<String> x_ = new ArrayList<String>();//
        String mensaje;
        double f_x=0;
        int n = 0 ;// contador para el numero de iteraciones realizadas
        int numeroMAX;
        double g_x;
        double x_o = 0;// almacena el valor del punto fijo ingresado
        double x_n_1;

        double tolerancia;//// almacena el valor de la tolerancia ingresada



        try {
            numeroMAX = Integer.parseInt(numMAX);

            JEP obj = new JEP();

            obj.parseExpression(tol); // paso de la expresi´on a evaluar
            tolerancia = obj.getValue();// toma el valor de f(x0)





            obj.addStandardFunctions(); // adiciona las funciones matem´aticas
            obj.addStandardConstants();
            obj.parseExpression(punto_fijo); // paso de la expresi´on a evaluar
            x_o = obj.getValue();



            JEP f = new JEP();
            f.addStandardFunctions(); // adiciona las funciones matem´aticas
            f.addStandardConstants(); // adiciona las constantes matem´aticas
            f.setImplicitMul(true);
            f.addVariable("x", x_o);
            // obtener el resultado de evaluar la expresi´on
            f.parseExpression(g); // paso de la expresi´on a evaluar
            x_n_1 = f.getValue(); //Xi+1= g(Xi)



            f.addVariable("x", x_n_1);
            // obtener el resultado de evaluar la expresi´on
            f.parseExpression(funcion); // paso de la expresi´on a evaluar
            f_x = f.getValue();

           
            x_.add(0, Double.toString(x_n_1) );// adiere el termino x_n_1  a la primera posicion de la colecion

           
             n=1;
             
            while ( Math.abs(f_x) > tolerancia  &&   f_x != 0  && n<numeroMAX) {
                                 
                x_o = x_n_1;// x0= (xn+1)
                f.addVariable("x", x_o);
                // obtener el resultado de evaluar la expresi´on
                f.parseExpression(g); // paso de la expresi´on a evaluar
                x_n_1 = f.getValue();

                f.addVariable("x", x_n_1);
                // obtener el resultado de evaluar la expresi´on
                f.parseExpression(funcion); // paso de la expresi´on a evaluar
                f_x = f.getValue();
                
                

                n++;
                x_.add(Double.toString(x_n_1) ); // adiere el termino x_n_1 a la coleccion


                



            }

            mensaje = Double.toString(x_n_1);


            if ( Math.abs(f_x) > tolerancia && n == numeroMAX ) {
                mensaje = "la funcion ingresada x=g(x) no converge o numero de ietaciones insuficientes";
                x_.clear();// borra los elemtos de la coleccion
            }
            
            
             
             
             x_i=x_;


        } catch (Exception e) {
            mensaje = "error ingreso datos";
        }



        return mensaje;
    }// fin metodo calcularRaiz

    
    
    public List retornaColeccion() {
        return x_i;
    }

    public void limpiarLista() {
        x_i.clear();// borra los elemtos de la coleccion x_i
    }
}
