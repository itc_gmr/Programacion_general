/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interpolacion;

import java.util.Arrays;
import javax.swing.JOptionPane;
import org.nfunk.jep.JEP;

/**
 *
 * @author Darwin
 */
public class SplinCubico {
boolean mensaje=false;
    public void splines() {
      
       try{
            int puntos = Integer.parseInt(JOptionPane.showInputDialog("ingrese numero de puntos"));// almacena el numero de puntos a ingresar
        String puntosSr[] = new String[puntos];
        String punto;
        double x[] = new double[puntos];// arreglo que almacena las componentes x de los puntos ingresados
        double y[] = new double[puntos];// arreglo que almacena las componentes Y de los puntos ingresados

        for (int i = 0; i < puntos; i++) {

            punto = JOptionPane.showInputDialog("ingrese el punto   ( x" + i + " , y" + i + ") ");
            String arregloString[] = punto.split(",");// almacena cada punto ingresado
            
            JEP b1= new JEP(); 
            b1.addStandardFunctions(); // adiciona las funciones matem´aticas
             b1.addStandardConstants();
             b1.parseExpression(arregloString[0]); // paso de la expresi´on a evaluar
             x[i] = b1.getValue();
             
             b1.parseExpression(arregloString[1]); // paso de la expresi´on a evaluar
             y[i] = b1.getValue();
           
        }// fin de for

        // AC=b
        double h[] = new double[puntos];
        
        
        
        
        for (int i = 0; i < puntos-1; i++) {
            h[i]=x[i+1]-x[i];
        }
        


        double b[] = new double[puntos];


        


        /**
         * paso a cargar la matriz b
         *  |-                                              -|
         *  |               0                                |
         *  |    3(x2-x1)/h1 - 3(x1-x0)/h0                   |
         *  |               .                                |
         *  |               .                                |
         *  |               .                                |        =b, con i=0,...,n-1
         *  |               .                                |
         *  |   3(Xn - Xn-1)/(hn-1) - 3(Xn-1 - Xn-2)/(hn-2)  |   
         *  |               0                                |
         *  |-                                              -|(n+1)x1
         */
        b[0] = 0;
        b[puntos - 1] = 0;
        for (int i = 1; i < puntos - 1; i++) {
            b[i] = ((3 / h[i]) * (y[i + 1] - y[i])) - ((3 / h[i - 1]) * (y[i] - y[i - 1]));

        }// fin cargar matriz b
        
        
       
        



        // 
        double matrizA[][] = new double[puntos][puntos];// de orden nxn, con i=0,...,n-1
        /**
         *  |-                                                                    -|
         *  |   1       0    .................................               0     |   
         *  |   h0  2(h0+h1)   h1    ................                        0     |
         *  |   0      h1   2(h1+h2)   h2  ................                  0     |
         *  |   .                                                                  |
         *  |   .                                                                  |           =A=matrizA, con i=0,...,n-1
         *  |   .                                                                  |
         *  |   0       0                            hn-2   2(hn-2 +hn-1)   hn-1   |  
         *  |   0       0   ......................               0            1    |
         *  |-                                                                 -|(n+1)x(n+1) 
         */
        matrizA[0][0] = 1;
        matrizA[puntos - 1][puntos - 1] = 1;

        for (int i = 1; i < puntos; i++) {
            matrizA[0][i] = 0;
        }
        for (int i = 0; i < puntos - 1; i++) {
            matrizA[puntos - 1][i] = 0;
        }

        
        
        // paso a cargar la matrizA  desde la fila i asta la fila n,  con  i=1,...n
        

        for (int i = 1; i < puntos; i++) {
            for (int j = 1; j < puntos - 1; j++) {
                
                if (i == j) {
                    matrizA[i][j - 1] = h[j - 1];
                    matrizA[i][j] = 2*(h[j - 1] + h[j]);
                    matrizA[i][j + 1] = h[j];
                } 

            }// fin buqle j

        }// fin bucle i
   // fin cargar la matirzA 
        
        
        
        
  /**
   * la matrizA1  es de dimension (puntos-2)x(puntos-2), usada para resolver el sistema de e uaciones
   * para en contrar las incognitas cj de la matriz C
   *Note que por las condiciones de frotera las componentes Cjo=0 y Cjn=0
   */      
        
        double matrizA1[][]= new double [puntos-2] [puntos-2];
        for (int i = 0; i < puntos-2; i++) {
            for (int j = 0; j < puntos-2; j++) {
                matrizA1[i][j]=matrizA[i+1][j+1];
            }// fin buqle j
        }// bucle i
        
        
        
        
       
        
        
         int n=puntos-2; 
        double r[]= new  double [n];// carreglo que almacenara los valores de Cji  desde i=1,...,n-2
        for (int l = 0; l < n; l++) {
             r[l]=b[l+1];
            
       }
      
        
        
        
        /**
         * acontinuacion usamos el metodo de caus jordan para encontar los valores de Cji  desde i=1,...,n-2
         */   
        double d,c1;
        for(int i=0;i<=n-1;i++){
            d=matrizA1[i][i];
            for(int s=0;s<=n-1;s++){
                    matrizA1[i][s]=((matrizA1[i][s])/d);
            }
            r[i]=((r[i])/d);
            for(int x1=0;x1<=n-1;x1++){
            if(i!=x1){
                    c1=matrizA1[x1][i];
                    for(int y1=0;y1<=n-1;y1++){
                            matrizA1[x1][y1]=matrizA1[x1][y1]-c1*matrizA1[i][y1];
                    }
                    r[x1]=r[x1]-c1*r[i];
            }// fin if
        }/// buble x1
        }// fin bucle i
        
        
        
        
        
         /**
         *  nesitamos que la matriz  C sea de la forma
         * 
         *      |-      -|
         *      |   0    |
         *      |   1    |
         *      |   1    |
         *      |   .    |
         *      |   .    |         =C
         *      |   .    |
         *      |   .    |
         *      |   1    |
         *      |   0    |
         *      |-      -|(n+1)x1
         * Note que por las condiciones de frotera las componentes Cjo=0 y Cjn=0
         */
         double cj[]=new double [puntos];
         cj[0]=0;
         cj[puntos-1]=0;
        for (int i = 1; i < puntos-1; i++) {
            cj[i]=r[i-1];
        }
        
        
        
        /**los polinomios de Splin cubicos esta dadas por la siguiente expresion
         *  Sj(X)=aj+bj(x-xj)+cj(x-xj)^2 +dj(x-xj)^3, donde  las aj=y[j]
         */
        
        
        double bj[]=new double [puntos-1];
         double dj[]=new double [puntos-1];
        for (int i = 0; i < puntos-1; i++) {
            bj[i]=((y[i+1]-y[i])/h[i])-((h[i]*(2*cj[i]+cj[i+1]))/3);
             dj[i]=(cj[i+1]-cj[i])/(3*h[i]);
        }
        String x_ = JOptionPane.showInputDialog("ingrese punto a evaluar");// almacena el valor en donde se evaluara el polinomio
         JEP b2= new JEP(); 
            b2.addStandardFunctions(); // adiciona las funciones matem´aticas
             b2.addStandardConstants();
              b2.parseExpression(x_); // paso de la expresi´on a evaluar
            double   x_evaluar = b2.getValue();
        
        
        
        double sx ;// variable que almacena el valor del polinomio en el punto ingresado
        for (int i = 0; i < puntos-1; i++) {
            if (x_evaluar>=x[i] && x_evaluar<=x[i+1] ) {// si el punto ingresado es elemento del intervale [Xi,Xi+1]
                // se evalua e punto ingresado en el polinomio Sj
                sx=y[i]+( bj[i]*(x_evaluar-x[i]) )  +  (cj[i]*Math.pow(x_evaluar-x[i], 2)) +(dj[i]*Math.pow(x_evaluar-x[i], 3));
                JOptionPane.showMessageDialog(null,"el valor s("+x_evaluar+")= "+sx);//  inprime el resultado de la evluacion anterior
                mensaje=true;
            
            }
            
        }// fin bucle i
        if (mensaje==false) {// si el punto ingresado no esta en el intervalo [x0,xn]
                JOptionPane.showMessageDialog(null,"punto a evaluar no está en el intervalo [ "+x[0]+" , "+x[x.length-1]+" ]");
            }
        
        
        
        // paso a determinar una particion del intervalo [Xo,Xn-1] para 
        // obteer los puntos  Sj  y poder realizar la grafica
        double xi[] = new double[200];// almacena los valores Xi de las paricines de cada intervalo [Xi,Xi+1]
       // se divide el intervalo [Xo,Xn-1] en 199 particiones
         double h1=(x[x.length-1]-x[0])/199;
        
         xi[0]=x[0];
              for (int i = 1; i < xi.length; i++) {
                  xi[i]=x[0]+(i*h1);// paso a determinar las valores Xi del intervalo [Xo,Xn-1]      
              }
        
        
       ParticionesSplinesCubicos obj1=new ParticionesSplinesCubicos();// se crea una inatancia de la clase ParticionesSplinesCubicos
        
       double l[]=new double[200];// arreglo que almacenara los valores Sj que devuelve el metodo puntosParticion de la clase ParticionesSplinesCubicos
       l=Arrays.copyOf(obj1.puntosParticion(puntos, x, y, xi,bj,cj,dj), l.length);
        
        GraficaSplinesCubicos obj=new GraficaSplinesCubicos ();// se crea una inatancia de la clase GraficaSplinesCubico
        obj.interpolacionSplinCubico(l,xi,y);// llamada al metodo interpolacionSplinCubico realizara la grafica  que recibe como
                                            // parametros  el arreglo l que almacena los valores Sj  del eje y
                                            //  el arreglo xi que que almacena las cooredenadas del eje x 
                                            //  el arreglo  y que almacena las coordenadas en el ejey de los puntos ingresados al inicio del programa
                                   
       }   // try
       
       catch (Exception e){
           JOptionPane.showMessageDialog(null, "error lectura de datos");
       }
  
        
    }// fin del metodo splines
}// fin de la lase SplinCubico
