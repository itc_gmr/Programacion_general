package Interpolacion;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Graphics;
import javax.swing.JOptionPane;

/**
 *
 * @author Darwin
 */
public class ParticionLagrange {
    
/**
 * recordemos que del intarvalo [a,b] podemos obtener varias particiones para poder encontrar puntos que estan en la funcion
 * y asi poder unir dichos puntos con lineas y obtener la grafica de la funcion
 */
    ParticionLagrange() {
       
    }

    
/**
 * 
 * @param puntos
 * @param x  arreglo que almacena las componentes x  de los puntos  del polinomio de Lagrange
 * @param y  arreglo que almacena las componentes y  de los puntos  del polinomio de Lagrange
 * @param xi arreglo que almacena las componentes xi de la particion del intervalo [a,b]
 * @return  devuelve un arreglo de doubles li[] que almacena las componentes y de la particion del intervalo [a,b]
        
 */
    public  double[] graficar( int puntos, double x[], double y[],double[] xi) {
        double li[]=new double[200];// almacena las componentes y de la particion del intervalo [a,b]
        
        for (int j = 0; j < li.length; j++) {
       
       
      
       
            
       double l[]=new double[puntos];// almacena los terminos Lk   
      
          if (xi[j]>= x[0]&& x[x.length-1]>=xi[j]) {
              for (int k = 0; k <puntos; k++) {
            double m=1;//contador para almacenar el valr de (X-X0)...(X-Xk-1)(X-Xk+1)...(X-Xn)
                    double n=1;//contador para almacenar el valr de (Xk-X0)...(Xk-Xk-1)(Xk-Xk+1)...(Xk-Xn)
            for (int i = 0; i < puntos; i++) {
                /**
                 * paso a encontrar lk=lk=((X-X0)...(X-Xk-1)(X-Xk+1)...(X-Xn))/((Xk-X0)...(Xk-Xk-1)(Xk-Xk+1)...(Xk-Xn)); con k=0,1,...,n
                 * con i distinto de k
                 */
                
                if (i!=k) {
                    m=m*(xi[j]-x[i]);
                    n=n*(x[k]-x[i]);
                }
                 else  {
                   m=m*1;
                   n=n*1;
                    
                }
                
                 
            }
           
            l[k]=m/n;// calcula el termino lk=((X-X0)...(X-Xk-1)(X-Xk+1)...(X-Xn))/((Xk-X0)...(Xk-Xk-1)(Xk-Xk+1)...(Xk-Xn); con k=0,1,...,n
          
        }
        
      double px=0;// contador para almacenar polinomio de Lagrange en el punto ingresado 
        for (int i = 0; i < puntos; i++) {
            px=px+(l[i]*y[i]);// calcula el valor de polinomio del polinomio de Lagrange en el punto ingresado
        }
        li[j]=px;
         
       
        
     
          
        }
        
       
    }
        return li;

    
}
}
