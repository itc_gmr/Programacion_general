/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Interpolacion;

import java.awt.image.BufferedImage;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Darwin Morocho
 */
public class GraficaSplinesCubicos extends java.awt.Frame {

    private BufferedImage grafica = null;
    private double x_[] = new double[200];//arreglo usado pra almacenar los punto en el eje x para la grafica
    private double y_[] = new double[200];//arreglo usado pra almacenar los punto en el eje y para la grafica
    double yi;
    private GraficaSplinesCubicos(double[] xi, double[] puntosParticion, double[] y) {
        super("");
        x_ = xi;// se inicializa los valores de los arreglos x_  y  y_
        y_ = puntosParticion;
        yi=y[0];
    }

    GraficaSplinesCubicos() {
        
    }
    
    
    private BufferedImage creaImagen()
    {
        //XYSeries es una clase que viene con el paquete JFreeChart
        //funciona como un arreglo con un poco mas de posibilidades
        
        XYSeries series = new XYSeries("");
        series.add(x_[0], yi);
        for (int i = 1; i < x_.length; i++) {
             series.add(x_[i], y_[i]);
             //como su nombre lo indica el primer valor sera asignado al eje X
              //y el segundo al eje Y
        }
        
        //se crea un objeto XYDataset requerido mas adelante por el metodo que grafica
        XYDataset juegoDatos= new XYSeriesCollection(series);
        
                /*aqui se hace la instancia de la nueva grafica invocando al metodo de ChartFactory
                que nos dibujara una grafica de lineas este metodo como casi todos los demas
                recibe los siguientes argumentos:
                
                tipo              valor
                String            nombre de la grafica , aparecera en la parte superior centro
                String            tutulo del eje X
                String            titulo del eje Y
                XYDataset         el conjunto de datos X y Y del tipo XYDataset (aqui cambian el parametro
                                  dependiendo del tipo de grafica que se quiere pueden ver todos los parametros
                                  en la documentacion aqui <a href="http://www.jfree.org/jfreechart/api/javadoc/index.html
" title="http://www.jfree.org/jfreechart/api/javadoc/index.html
">http://www.jfree.org/jfreechart/api/javadoc/index.html
</a>                              iremos notando los cambios mas adelante..
                 PlotOrientation  la orientacion del grafico puede ser PlotOrientation.VERTICAL o PlotOrientation.HORIZONTAL
                 boolean                  muestra u oculta leyendas     
                 boolean                  muestra u oculta tooltips
                 boolean                  muestra u oculta urls (esta opcion aun no la entiendo del todo)
                
                generalmente solo necesitaremos cambiar los primeros 3 parametros lo demas puede quedarse asi
                
                */
        JFreeChart chart = ChartFactory.createXYLineChart        ("Interpolación por Splines Cubicos",
        "Eje X","Eje Y",juegoDatos,PlotOrientation.VERTICAL,
        false,
        false,
        true                // Show legend
        );

        //donde guardaremos la imagen?? pues en un bufer 
        BufferedImage image = chart.createBufferedImage(400,400);
       
        
        return image;
    }

        // sobreescribimos el metodo paint de la clase Graphics
    public void paint(java.awt.Graphics g) {
        //super.paint(g);

        
       

        if(grafica == null)
        {
            grafica = this.creaImagen();
        }
        g.drawImage(grafica,30,30,null);
    }

    
    

    void interpolacionSplinCubico(double[] puntosParticion, double[] xi,double[] y) {
        GraficaSplinesCubicos miventana = new GraficaSplinesCubicos(xi,puntosParticion,y);// llamada al construtor e inicializa los punto a ser graficados
         miventana.setSize(450,450);
         miventana.show();
        
    }
    
    
    
    
    
}
