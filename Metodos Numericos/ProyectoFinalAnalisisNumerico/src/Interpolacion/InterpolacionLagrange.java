package Interpolacion;

/*
 * InterpolacionLagrange.java
 *
 * Created on 3 de noviembre de 2008, 8:11
 */
import java.awt.image.BufferedImage;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import menu.MenuPrincipal;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.nfunk.jep.JEP;
/**
 *
 * @author  Roberto Leon Cruz
 */
public class InterpolacionLagrange extends java.awt.Frame {
      
  private  BufferedImage grafica = null;
private double x_[]=new double [200];//arreglo usado pra almacenar los punto en el eje x para la grafica
 private double y_[]=new double [200];//arreglo usado pra almacenar los punto en el eje y para la grafica


    //constructor

    private InterpolacionLagrange(double[] xi, double[] lk) {
        super("");
        x_=xi;// se inicializa los valores de los arreglos x_  y  y_
        y_=lk;
    }

   

    private BufferedImage creaImagen()
    {
        //XYSeries es una clase que viene con el paquete JFreeChart
        //funciona como un arreglo con un poco mas de posibilidades
        
        XYSeries series = new XYSeries("");
  
        for (int i = 0; i < x_.length; i++) {
             series.add(x_[i], y_[i]);
             //como su nombre lo indica el primer valor sera asignado al eje X
              //y el segundo al eje Y
        }
        
        //se crea un objeto XYDataset requerido mas adelante por el metodo que grafica
        XYDataset juegoDatos= new XYSeriesCollection(series);
        
                /*aqui se hace la instancia de la nueva grafica invocando al metodo de ChartFactory
                que nos dibujara una grafica de lineas este metodo como casi todos los demas
                recibe los siguientes argumentos:
                
                tipo              valor
                String            nombre de la grafica , aparecera en la parte superior centro
                String            tutulo del eje X
                String            titulo del eje Y
                XYDataset         el conjunto de datos X y Y del tipo XYDataset (aqui cambian el parametro
                                  dependiendo del tipo de grafica que se quiere pueden ver todos los parametros
                                  en la documentacion aqui <a href="http://www.jfree.org/jfreechart/api/javadoc/index.html
" title="http://www.jfree.org/jfreechart/api/javadoc/index.html
">http://www.jfree.org/jfreechart/api/javadoc/index.html
</a>                              iremos notando los cambios mas adelante..
                 PlotOrientation  la orientacion del grafico puede ser PlotOrientation.VERTICAL o PlotOrientation.HORIZONTAL
                 boolean                  muestra u oculta leyendas     
                 boolean                  muestra u oculta tooltips
                 boolean                  muestra u oculta urls (esta opcion aun no la entiendo del todo)
                
                generalmente solo necesitaremos cambiar los primeros 3 parametros lo demas puede quedarse asi
                
                */
        JFreeChart chart = ChartFactory.createXYLineChart        ("Interpolación de Lagrange",
        "Eje X","Eje Y",juegoDatos,PlotOrientation.VERTICAL,
        false,
        false,
        true                // Show legend
        );

        //donde guardaremos la imagen?? pues en un bufer 
        BufferedImage image = chart.createBufferedImage(400,400);
       
        
        return image;
    }

        // sobreescribimos el metodo paint de la clase Graphics
    public void paint(java.awt.Graphics g) {
        //super.paint(g);

        
       

        if(grafica == null)
        {
            grafica = this.creaImagen();
        }
        g.drawImage(grafica,30,30,null);
    }

     
    public static void interpolacion() {
         double xi[]=new double[200];
         double lk[]=new double[200];
          boolean accion=false;   
          try{
           int puntos=Integer.parseInt(JOptionPane.showInputDialog("ingrese numero de puntos"));// almacena el numero de puntos a ingresar
       String puntosSr[]=new String[puntos];
       String punto;
       double x[]=new double[puntos];// arreglo qure lamacena las componentes x de los puntos ingresados
       double y[]=new double[puntos];// arreglo qure lamacena las componentes Y de los puntos ingresados
       
       for (int i = 0; i < puntos; i++) {
            
            punto=JOptionPane.showInputDialog("ingre el punto   ( x"+i+" , y"+i+") ");
             String arregloString[]=punto.split(",");// almacena cada punto ingresado
             JEP b1= new JEP(); 
             b1.addStandardFunctions(); // adiciona las funciones matem´aticas
             b1.addStandardConstants();
             b1.parseExpression(arregloString[0]); // paso de la expresi´on a evaluar
             x[i] = b1.getValue();
             
             b1.parseExpression(arregloString[1]); // paso de la expresi´on a evaluar
             y[i] = b1.getValue();
        }// fin de for
       
       
            
       double l[]=new double[puntos];// almacena los terminos Lk   
       double x_evaluar=Double.parseDouble(JOptionPane.showInputDialog("ingrese el punto a evaluar el polinomio"));// aqlmacena el valor en el que se evaluara el polinomio de Lagrange
          if (x_evaluar>= x[0]&& x[x.length-1]>=x_evaluar) {
              for (int k = 0; k <puntos; k++) {
            double m=1;//contador para almacenar el valr de (X-X0)...(X-Xk-1)(X-Xk+1)...(X-Xn)
                    double n=1;//contador para almacenar el valr de (Xk-X0)...(Xk-Xk-1)(Xk-Xk+1)...(Xk-Xn)
            for (int i = 0; i < puntos; i++) {
                /**
                 * paso a encontrar lk=lk=((X-X0)...(X-Xk-1)(X-Xk+1)...(X-Xn))/((Xk-X0)...(Xk-Xk-1)(Xk-Xk+1)...(Xk-Xn)); con k=0,1,...,n
                 * con i distinto de k
                 */
                
                if (i!=k) {
                    m=m*(x_evaluar-x[i]);
                    n=n*(x[k]-x[i]);
                }
                 else  {
                   m=m*1;
                   n=n*1;
                    
                }
                
                 
            }
           
            l[k]=m/n;// calcula el termino lk=((X-X0)...(X-Xk-1)(X-Xk+1)...(X-Xn))/((Xk-X0)...(Xk-Xk-1)(Xk-Xk+1)...(Xk-Xn); con k=0,1,...,n
          
        }
        
      double px=0;// contador para almacenar polinomio de Lagrange en el punto ingresado 
        for (int i = 0; i < puntos; i++) {
            px=px+(l[i]*y[i]);// calcula el valor de polinomio del polinomio de Lagrange en el punto ingresado
        }
       double r=px;
         JOptionPane.showMessageDialog(null, " P ("+x_evaluar+")="+r);// impriime el valor de polinomio de Lagrange en el punto ingresado
       
        
     
         
         
         
         
         
         
         
         double h=(x[x.length-1]-x[0])/199;
        
         xi[0]=x[0];
              for (int i = 1; i < xi.length; i++) {
                  xi[i]=x[0]+(i*h);
                  
              }
         
              accion=true;
              if (accion=true) {
                 
                  ParticionLagrange obj =new ParticionLagrange();
                  lk=Arrays.copyOf(obj.graficar(puntos,x,y,xi),lk.length);// el arreglo lk copia los valres devueltos por el arreglo li que se tedermina en la clase Grafica
                   InterpolacionLagrange miventana = new InterpolacionLagrange(xi,lk);// llamada al construtor e inicializa los punto a ser graficados
                  miventana.setSize(450,450);
                  
                  miventana.show();
                  JOptionPane.showMessageDialog(null, " click en Aceptar para volver al menu ");// aparece un cuadro de dialogo
              
              
              }
         
            
 
          
          }// fin if
          else{
              JOptionPane.showMessageDialog(null,"punto a evaluar no esta en el intervalo [ "+x[0]+" , "+x[x.length-1]+" ]");
          }
         
      }// fin try
       catch(NumberFormatException e){
          JOptionPane.showMessageDialog(null," error ingreso de datos ") ;
       }
          
          catch(ArrayIndexOutOfBoundsException e){
              JOptionPane.showMessageDialog(null," error ingreso de datos ") ;
          }
           catch(NullPointerException e){
              
       
        JOptionPane.showMessageDialog(null," error ingreso de datos ") ;
        
          }
        
   /**
     *solo nos resta invocar a nuestra grafica
     * 
     */
        
       
       
        
        
        
      
        
    }

}
