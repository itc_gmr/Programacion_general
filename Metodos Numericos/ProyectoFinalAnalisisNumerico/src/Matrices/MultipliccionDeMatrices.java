package Matrices;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 *
 * @author Darwin
 */
public class MultipliccionDeMatrices {

    static double matrizA[][];
    static double matrizB[][];
    static double matrizC[][];

    public static double[][] getMatrizC() {
        return matrizC;
    }

    public static void setMatrizC(double[][] matrizC) {
        MultipliccionDeMatrices.matrizC = matrizC;
    }

    public void generarMatriz(String m1, String n1, String o1, String p1, String ruta) throws FileNotFoundException, IOException {
        Scanner leer;

        int n, m, o, p;


        try {
            leer = new Scanner(new File(ruta));


            m = Integer.parseInt(m1);

            n = Integer.parseInt(n1);

            o = Integer.parseInt(o1);

            p = Integer.parseInt(p1);

            /**
             * el bucle while compara que las dimensiones de las matrices sean
             * compatibles para realizar la multiplicacion, Caso contrario
             * vuelve a pedir las dimensiones
             */
            if (m <= 0 || n <= 0 || o <= 0 || p <= 0 || n != o) {
                JOptionPane.showMessageDialog(null, " Dimensiones no compatibles ");
            } else {

                //System.out.println("************MAtriz A************");
                matrizA = new double[m][n];
                for (int i = 0; i < m; i++) {
                    for (int j = 0; j < n; j++) {
                        matrizA[i][j] = leer.nextDouble();

                    }

                }

                //System.out.println("************MAtriz B************");
                matrizB = new double[o][p];
                for (int i = 0; i < o; i++) {
                    for (int j = 0; j < p; j++) {
                        matrizB[i][j] = leer.nextDouble();
                    }
                }





                 matrizC = new double[m][p];
                for (int i = 0; i < m; i++) {
                    for (int j = 0; j < p; j++) {
                        double s = 0;// acumulador para la posicion de AB
                        for (int k = 0; k < n; k++) {
                            s = s + matrizA[i][k] * matrizB[k][j];

                        }
                        matrizC[i][j] = s;


                    }

                }




            }// fin else

        }//fin try
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, " Error ingreso de datos ");
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, " Archivo no encontrado ");
        } catch(Exception e){
          JOptionPane.showMessageDialog(null, " error lectura de el archivo ");
      }




    }
}