package Matrices;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 *
 * @author Darwin
 */
public class MatrizTranspuesta {
  
   
    static double matrizA[][];
      static double matrizB[][];
      
      
      
     
  
    
    public void generarMatriz(String m1,String n1, String ruta) throws FileNotFoundException{
        
        Scanner leer;
      int n,m,o,p;
      
      try{
          leer = new Scanner(new File (ruta));
        
           
              m=Integer.parseInt(m1);
            
              n=Integer.parseInt(n1);

             
         
         /** el bucle while compara que las dimensiones de las matrices
          * sean compatibles para realizar la multiplicacion, Caso
          * contrario vuelve a pedir las dimensiones
          */
              if( m<=0 || n<=0 ){
                  JOptionPane.showMessageDialog(null, " Dimensiones no compatibles ");
              }
              else{
                          
        //System.out.println("************MAtriz A************");
        matrizA=new double [m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matrizA[i][j]=leer.nextDouble(); 
                
            }
            
        }
        
        //System.out.println("************MAtriz B************");
         matrizB=new double [n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrizB[i][j]=matrizA[j][i]; 
            }
        }
       
        
        
        
        
         
        
        
        
              }// fin else
          
      }//fin try
      catch(NumberFormatException e){
          JOptionPane.showMessageDialog(null, " Error ingreso de datos ");
      }
      catch(FileNotFoundException e){
          JOptionPane.showMessageDialog(null, " Archivo no encontrado ");
      }
      catch(Exception e){
          JOptionPane.showMessageDialog(null, " error lectura de el archivo ");
      }
      
        }// fin metodo generar matriz
   
}// fin de la clase


