package MetodosNum;

public class Metodos {
	
	static Grafica g = new Grafica();
	
	public static int menu1(){
		int n=0;
		System.out.println("****MENU****");
		System.out.println("1.- Raices de ecuaciones");
		System.out.println("2.- Soluci�n de sistema de ecuaciones");
		System.out.println("3.- Interpolaciones");
		System.out.println("4.- Diferenciacion Numerica");
		System.out.println("5.- Salir");
		do{
		System.out.print("Seleccione su opcion: ");
		n=Leer.datoInt();
		if(n<1|| n>5)
			System.out.println("La opci�n seleccinada no es valida");
		}while(n<1|| n>5);
		return n;
	}
	
	public static int menu3(){
		int n=0;
		System.out.println("****MENU DE SISTEMAS DE ECUACIONES****");
		System.out.println("1.- Gauss ");
		System.out.println("2.- Gauss Jordan ");
		System.out.println("3.- Montante ");
		System.out.println("4.- Cramer ");
		System.out.println("5.- Jacobi ");
		System.out.println("6.- Gauss-Seidel ");
		System.out.println("7.- Regresar ");
		do{
			System.out.print("Seleccione su opcion: ");
			n=Leer.datoInt();
			if(n<1|| n>7)
				System.out.println("La opci�n seleccinada no es valida");
			}while(n<1|| n>7);
		return n;
	}
	
	public static int menu2(){
		int op=0;
		
		System.out.println("****MENU DE RAICES DE ECUACIONES****");
		System.out.println("1.- M�todo de bisecci�n");
		System.out.println("2.- M�todo de falsa posici�n");
		System.out.println("3.- M�todo de la secante");
		System.out.println("4.- M�todo de Newton-Raphson");
		System.out.println("5.- M�todo de Aproximaciones sucecivas");
		System.out.println("6.- M�todo de Newton segundo orden");
		System.out.println("7.- Regresar");
		do{
		System.out.print("Seleccione su opcion: ");
		op=Leer.datoInt();
		if(op<1|| op>7)
			System.out.println("La opci�n seleccinada no es valida");
		}while(op<1||op>7);
		
		return op;
	}
	public static int menu4(){
		int n=0;
		System.out.println("****MENU DE INTERPOLACIONES****");
		System.out.println("1.- Newton ");
		System.out.println("2.- Lagrange ");
		System.out.println("3.- Minimos Cuadrados ");
		System.out.println("4.- Regresar ");
		do{
			System.out.print("Seleccione su opcion: ");
			n=Leer.datoInt();
			if(n<1|| n>4)
				System.out.println("La opci�n seleccinada no es valida");
			}while(n<1|| n>4);
		return n;
	}

	public static int menu5(){
		int n=0;
		System.out.println("****MENU DE DIFERENCIACI�N NUMERICA****");
		System.out.println("1.- Por medio de limites ");
		System.out.println("2.- M�todo del Trapecio ");
		System.out.println("3.- M�todo de Simpson ");
		System.out.println("4.- Regresar ");
		do{
			System.out.print("Seleccione su opcion: ");
			n=Leer.datoInt();
			if(n<1|| n>4)
				System.out.println("La opci�n seleccinada no es valida");
			}while(n<1|| n>4);
		return n;
	}
	//METODOS DE LAS RAICES DE ECUACIONES
	public static void MetodoBiseccion(){
	
		String f;
		int numitera=0;
		double a=0, b=0, error;
		
		System.out.println("M�TODO DE BISECCI�N");
		
		
		System.out.print("\nFunci�n: ");
		f = Leer.funcion();
	
		do{
		System.out.print("Intervalo A: ");
		a = Leer.datoDouble();
		
		System.out.print("Intervalo B: ");
		b= Leer.datoDouble();
		
		if(Leer.eval(f,a)==0 ||(Leer.eval(f,b))==0)   
		{
			if(Leer.eval(f,a)==0)
			{
				System.out.println("LA RAIZ DE LA FUNCI�N ESTA DADA POR EL PUNTO A\n LA RAIZ ES: "+a);
				System.out.print("\nPulse una tecla para ver la gr�fica...");
				Leer.datoString();
				
				g.mostrarGrafica(f, g);
				return;
			}
			if(Leer.eval(f,b)==0)
			{
				System.out.println("LA RAIZ DE LA FUNCI�N ESTA DADA POR EL PUNTO B\n LA RAIZ ES: "+b);
				System.out.print("\nPulse una tecla para ver la gr�fica...");
				Leer.datoString();
				
				g.mostrarGrafica(f, g);
				return;	
			}
			
		}
				else {
					if(Leer.eval(f, a)*Leer.eval(f, b)>0)
					{
						System.out.println("Los puntos tecleados no cumplen con el teorema de bolzano\n Teclee de nuevo los puntos a y b ");
				    }
		}
		}while(Leer.eval(f, a)*Leer.eval(f, b)>0);
		
	
			System.out.print("Error Permitido: ");
			error = Leer.datoDouble();
			
			do{
			System.out.print("No. Iteraciones: ");
			numitera= Leer.datoInt();
			if(numitera<20)
				System.out.println("Sugerencia: Usar mas de 20 iteraciones.");
			}while(numitera<20);
			
			System.out.println("\nI          X1                  X2                  X2-X1               Xm                  f(X1)                     f(Xm)");
			
			biseccion(f,a,b,error,numitera);
			
			System.out.print("\nPulse una tecla para ver la gr�fica...");
			Leer.datoString();
			
			g.mostrarGrafica(f, g);
		
		
	}
	public static void biseccion (String f, double a, double b, double e, int numitera)
	{
		double s, error=0;
		
		
		for(int i=1; i<=numitera; i++)
		{
			s=(a+b)/2;
			System.out.println(i+"	   "+blancos(a+"")+blancos(b+"")+blancos(b-a+"")+blancos(s+"")+blancos(Leer.eval(f,a)+"")+"      "+blancos(Leer.eval(f,s)+""));

			if((Leer.eval(f,s))==0)
			{
				System.out.println(i+"	   "+blancos(a+"")+blancos(b+"")+blancos(b-a+"")+blancos(s+"")+blancos(Leer.eval(f,a)+"")+"      "+blancos(Leer.eval(f,s)+""));
			}
			else {
				if ((Leer.eval(f,a))*(Leer.eval(f,s))<0){
				b=s;
			}
			else{
				a=s;
			}
			}
			error = (b-a)<0 ? (b-a)*-1 : b-a;
			if(error < e || Leer.eval(f, s)==0)
			{
				System.out.println("\nLA RAIZ ES: "+s);
				//Leer.Vec();
				break;
			}
			
				//System.out.println(i+"	   "+blancos(a+"")+blancos(b+"")+blancos(b-a+"")+blancos(s+"")+blancos(Leer.eval(f,a)+"")+"      "+blancos(Leer.eval(f,s)+""));
		}
	}

	public static void MetodoFalsaPosicion(){


		String f;
		int numitera;
		double a=0, b=0, error;
		
		System.out.println("M�TODO DE LA FALSA POSICI�N");
	
			System.out.print("\nFunci�n: ");
			f = Leer.funcion();
			do{
				System.out.print("Intervalo A: ");
				a = Leer.datoDouble();
				
				System.out.print("Intervalo B: ");
				b= Leer.datoDouble();
				
				if(Leer.eval(f,a)==0 ||(Leer.eval(f,b))==0)   
				{
					if(Leer.eval(f,a)==0)
					{
						System.out.println("LA RAIZ DE LA FUNCI�N ESTA DADA POR EL PUNTO A\n LA RAIZ ES: "+a);
						System.out.print("\nPulse una tecla para ver la gr�fica...");
						Leer.datoString();
						
						g.mostrarGrafica(f, g);
						return;
					}
					if(Leer.eval(f,b)==0)
					{
						System.out.println("LA RAIZ DE LA FUNCI�N ESTA DADA POR EL PUNTO B\n LA RAIZ ES: "+b);
						System.out.print("\nPulse una tecla para ver la gr�fica...");
						Leer.datoString();
						
						g.mostrarGrafica(f, g);
						return;	
					}
					
				}
						else {
							if(Leer.eval(f, a)*Leer.eval(f, b)>0)
							{
								System.out.println("Los puntos tecleados no cumplen con el teorema de bolzano\n Teclee de nuevo los puntos a y b ");
						    }
				}
				}while(Leer.eval(f, a)*Leer.eval(f, b)>0);
			
		
				System.out.print("Error Permitido: ");
				error = Leer.datoDouble();
				
				do{
					System.out.print("No. Iteraciones: ");
					numitera= Leer.datoInt();
					if(numitera<20)
						System.out.println("Sugerencia: Usar mas de 20 iteraciones.");
					}while(numitera<20);
				
				System.out.println("\nI          Xi                                 f(Xi)                                    Xi-X1                                      f(Xi)-f(X1)                              Xi+1                                   Error                                   f(Xi+1)");
				
				falsaposicion(f,a,b,error,numitera);
			
				System.out.print("\nPulse una tecla para ver la gr�fica...");
				Leer.datoString();
				
				g.mostrarGrafica(f, g);
		
	
	}
	public static void falsaposicion(String f, double a, double b, double e, int numitera)
	{
	
		double xi, delta=0;
		
		for(int i=1; i<=numitera; i++)
		{
			if (i==1)
			{
				System.out.println(i+"	   "+blancos2(a+"")+"     "+blancos2(Leer.eval(f,a)+"")+"           "+blancos2((0)+"")+"             "+blancos2((0)+"")+"           "+blancos2((0)+"")+"         "+blancos2((0)+"")+"          "+blancos2(0+""));
				continue;
			}
			
			xi = b-((Leer.eval(f,b) * (b-a)) / (Leer.eval(f,b) - Leer.eval(f,a)));
			
			System.out.println(i+"	   "+blancos2(b+"")+"     "+blancos2(Leer.eval(f,b)+"")+"           "+blancos2((b-a)+"")+"             "+blancos2((Leer.eval(f,b)-Leer.eval(f,a))+"")+"           "+blancos2((xi)+"")+"         "+blancos2((xi-b)+"")+"          "+blancos2(Leer.eval(f,xi)+""));	
			
			delta = (xi-b)<0 ? (xi-b)*-1 : xi-b;
			
			b = xi;
			
			if(Math.abs(delta) < e)
			{
				System.out.println("\nLA RAIZ ES: "+xi);
				//Leer.Vec();
				break;
			}
		}
	}
	
	public static void MetodoSecante(){

		String f;
		int numitera;
		double a, b, error;
		
		System.out.println("M�TODO DE LA SECANTE");
		
		
			System.out.print("\nFunci�n: ");
			f = Leer.funcion();
			
			System.out.print("Intervalo A: ");
			a = Leer.datoDouble();
						
			System.out.print("Intervalo B: ");
			b= Leer.datoDouble();
			
			if(Leer.eval(f,a)==0 ||(Leer.eval(f,b))==0)   
			{
				if(Leer.eval(f,a)==0)
				{
					System.out.println("LA RAIZ DE LA FUNCI�N ESTA DADA POR EL PUNTO A\n LA RAIZ ES: "+a);
					System.out.print("\nPulse una tecla para ver la gr�fica...");
					Leer.datoString();
					
					g.mostrarGrafica(f, g);
					return;
				}
				if(Leer.eval(f,b)==0)
				{
					System.out.println("LA RAIZ DE LA FUNCI�N ESTA DADA POR EL PUNTO B\n LA RAIZ ES: "+b);
					System.out.print("\nPulse una tecla para ver la gr�fica...");
					Leer.datoString();
					
					g.mostrarGrafica(f, g);
					return;	
				}
				
			}
			
			System.out.print("Error Permitido: ");
			error = Leer.datoDouble();
				
			do{
				System.out.print("No. Iteraciones: ");
				numitera= Leer.datoInt();
				if(numitera<20)
					System.out.println("Sugerencia: Usar mas de 20 iteraciones.");
				}while(numitera<20);
			
			System.out.println("\nI          Xi                                 Xi+1                                     f(Xi)                                      f(Xi+1)                                  Xi+2                                   Error                                   f(Xi+2)");
				
			secante(f,a,b,error,numitera);
			System.out.print("\nPulse una tecla para ver la gr�fica...");
			Leer.datoString();
			
			g.mostrarGrafica(f, g);
			
	}
	public static void secante(String f,double a, double b, double e, int numitera)
	{
	
		double xi, delta=0;
		
		for(int i=1; i<=numitera; i++)
		{
			
			xi = b - ((Leer.eval(f,b) * (b-a)) / (Leer.eval(f,b) - Leer.eval(f,a)));
			
			System.out.println(i+"	   "+blancos2(a+"")+"     "+blancos2(b+"")+"           "+blancos2(Leer.eval(f,a)+"")+"             "+blancos2(Leer.eval(f,b)+"")+"           "+blancos2(xi+"")+"         "+blancos2(xi-b+"")+"          "+blancos2(Leer.eval(f,xi)+""));
			
			delta = (xi-b)<0 ? (xi-b)*-1 : xi-b;
			
			a = b;
			
			b = xi;
			
			if(Math.abs(delta) < e)
			{
				System.out.println("\nLA RAIZ ES: "+xi);
				//Leer.Vec();
				break;
			}
		}
	}

	public static void MetodoNewtonRaphson(){
		
		String f, d1;
		int numitera;
		double x0, error;
		
		System.out.println("M�TODO DE NEWTON-RAPHSON");
		
			System.out.print("\nFunci�n: ");
			f = Leer.funcion();
			
			System.out.print("Primera Derivada: ");
			d1 = Leer.funcion();
			
			System.out.print("Valor De Partida x0: ");
			x0 = Leer.datoDouble();
			
			if(Leer.eval(f, x0)==0)
			{
				System.out.println("LA RAIZ DE LA FUNCI�N ESTA DADA POR EL PUNTO A\n LA RAIZ ES: "+x0);
				System.out.print("\nPulse una tecla para ver la gr�fica...");
				Leer.datoString();
				
				g.mostrarGrafica(f, g);
				return;
			}
			
			System.out.print("Error Permitido: ");
			error = Leer.datoDouble();
				
			do{
				System.out.print("No. Iteraciones: ");
				numitera= Leer.datoInt();
				if(numitera<20)
					System.out.println("Sugerencia: Usar m�s de 20 iteraciones.");
				}while(numitera<20);
						
			System.out.println("\nI          Xi                                       f(Xi)                                  f'(Xi)                                  Xi+1                                   Error");
				
			raphson(f, d1, x0, error, numitera);
			
			System.out.print("\nPulse una tecla para ver la gr�fica...");
			Leer.datoString();
			
			g.mostrarGrafica(f, g);
	}
	public static void raphson(String f, String d1, double x, double e, int numitera ){
		
		double xi, delta;
		
		for(int i=1; i<=numitera;i++){
		
			xi= x-(Leer.eval(f, x)/Leer.eval(d1, x));
		
			System.out.println(i+"	   "+blancos2(x+"")+"           "+blancos2(Leer.eval(f,x)+"")+"         "+blancos2(Leer.eval(d1,x)+"")+"          "+blancos2(xi+"")+"         "+blancos2(xi-x+""));
			
			delta=Math.abs(xi-x);
			x=xi;
			
			if(delta<e)
			{
				System.out.println("\nLA RAIZ ES: "+xi);
				//Leer.Vec();
				break;
			}
			
			
			
		}
		
		
	}
	
	public static void MetodoAproxSucesivas(){
	

		String f;
		int numitera;
		double a, error;
		
		System.out.println("M�TODO DE LAS APROXIMACIONES SUCESIVAS");
		
		
			System.out.print("\nFunci�n: ");
			f = Leer.funcion();
			
			System.out.print("Da un valor de partida o valor de x0: ");
			a = Leer.datoDouble();
						
			System.out.print("Error Permitido: ");
			error = Leer.datoDouble();
				
			do{
				System.out.print("No. Iteraciones: ");
				numitera= Leer.datoInt();
				if(numitera<20)
					System.out.println("Sugerencia: Usar mas de 20 iteraciones.");
				}while(numitera<20);
			
			System.out.println("\nI          Xi                                         g(xi)                                     Error                                      f(Xi) ");
				
			AproximacionesSucesivas(f,a,error,numitera);
			
			System.out.print("\nPulse una tecla para ver la gr�fica...");
			Leer.datoString();
			
			g.mostrarGrafica(f, g);
	
	}
	public static void AproximacionesSucesivas(String f, double x0, double e, int numitera)
	{
	
		double delta=0,gx,xn;
		
		
		for(int i=1; i<=numitera; i++)
		{
			gx=Leer.eval(f,x0)+x0;
			xn=gx;
			
			System.out.println(i+"	   "+blancos2(x0+"")+"           "+blancos2(xn+"")+"         "+blancos2(xn-x0+"")+"          "+blancos2(Leer.eval(f,x0)+""));
			
		
			delta=(xn-x0);
			 
			if(Math.abs(delta) < e)
			{
				System.out.println("\nLA RAIZ ES: "+xn);
				break;	
			}
			x0 = xn;

		}
	}

	public static void NewtonSegundoOrden(){

		String f, d1, d2;
		int numitera;
		double x0, error;
		
		System.out.println("M�TODO DE NEWTON DE SEGUNDO ORDEN");
		
			System.out.print("\nFunci�n: ");
			f = Leer.funcion();
			
			System.out.print("Primera Derivada: ");
			d1 = Leer.funcion();
			
			System.out.print("Segunda Derivada: ");
			d2 = Leer.funcion();
			
			System.out.print("Valor De Partida x0: ");
			x0 = Leer.datoDouble();
			
			if(Leer.eval(f, x0)==0)
			{
				System.out.println("LA RAIZ DE LA FUNCI�N ESTA DADA POR EL PUNTO A\n LA RAIZ ES: "+x0);
				System.out.print("\nPulse una tecla para ver la gr�fica...");
				Leer.datoString();
				
				g.mostrarGrafica(f, g);
				return;
			}
			
			System.out.print("Error Permitido: ");
			error = Leer.datoDouble();
				
			do{
				System.out.print("No. Iteraciones: ");
				numitera= Leer.datoInt();
				if(numitera<20)
					System.out.println("Sugerencia: Usar mas de 20 iteraciones.");
				}while(numitera<20);
						
			System.out.println("\nI          Xi                                       f(Xi)                                  f'(Xi)                                  f''(Xi)                                   Xi+1                                     f(Xi+1)                                   Error");
				
			newton(f, d1, d2, x0, error, numitera);
		
			System.out.print("\nPulse una tecla para ver la gr�fica...");
			Leer.datoString();
			
			g.mostrarGrafica(f, g);
	}
	public static void newton (String f, String d1, String d2, double x, double e, int numitera)
	{
		double xi=0, delta;
		
		for(int i=1; i<=numitera; i++)
		{
			xi = x - Leer.eval(f, x) / (Leer.eval(d1, x) - (Leer.eval(f, x) * Leer.eval(d2, x) / (2 * Leer.eval(d1, x))));
			
			System.out.println(i+"	   "+blancos2(x+"")+"           "+blancos2(Leer.eval(f, x)+"")+"         "+blancos2(Leer.eval(d1, x)+"")+"          "+blancos2(Leer.eval(d2,x)+"")+"            "+blancos2(xi+"")+"           "+blancos2(Leer.eval(f, xi)+"")+"            "+blancos2(x-xi+""));

			delta = (x-xi)<0 ? (x-xi)*-1 : x-xi;
			
			x = xi; 
			
			if (Math.abs(delta) < e)
			{
				System.out.println("\nLA RAIZ ES: "+xi);
				//Leer.Vec();
				break;
			}	
		}
	}
	
	public static String blancos2 (String exp)
	{
		int dif = 30-exp.length();
		for(int i=0; i<dif; i++)
			exp = exp+" ";
		return exp;
	}
	public static String blancos (String exp)


	{
		int dif=20-exp.length();
		for(int i=0; i<dif; i++)
			exp=exp+" ";
		return exp;
	}
	
	//METODOS DE SOLUCI�N DE ECUACIONES..
	
	public static void  Gauss(){

		double Matriz [][];
		int n;
		System.out.println("**METODO DE GAUSS**\n");
		System.out.println("TECLEE EL TAMA�O DE LA MATRIZ CUADRADA:");
		n=Leer.datoInt();
		
		Matriz=new double [n][n+1];
		double Aux [][]= new double [n][n+1];
		
		System.out.println("TECLEE LOS VALORES DEL SISTEMA DE ECUACIONES SEGUN EL ORDEN");
		for (int i=0; i < Matriz.length; i++) {
			  for (int j=0; j < Matriz[i].length; j++) {
				  if(j==n){
						System.out.print("Incognita Num:"+(i+1)+" ");
					}
				  System.out.print("a["+(i+1)+(j+1)+"]:");  
				  Matriz[i][j]=Leer.datoDouble();	
				  Aux[i][j]	= Matriz[i][j];	 
			  }
			}
		
		double m=0;
		
		for(int k=0; k<n-1;k++)
		{
			for(int i=k+1; i<n;i++)
			{
				m=Matriz[i][k]/Matriz[k][k];
				for(int j=k; j<n+1;j++){
					Matriz[i][j]=(Matriz[i][j])-(m*Matriz[k][j]);
				}
			}
		}
		double x []=new double [n];
		int o=n-1;
		
		x[o]= (Matriz[o][o+1]/Matriz[o][o]);
		double suma;	
		for(int i=o-1; i>=0;i--){
			 suma=0;
			for(int j=i+1;j<=o;j++)
			{
				suma=suma+(Matriz[i][j]*x[j]);
			}
			x[i]=(Matriz[i][o+1]-suma)/Matriz[i][i];
		}
		System.out.println("\n\n");
		System.out.println("SISTEMA DE ECUACIONES ORIGINAL");
		int conta=0;
		for (int i=0; i < Aux.length; i++) {
			for (int j=0; j < Aux[i].length; j++) {
				  System.out.print( Aux[i][j]+"\t");
				  conta++;
				  if(conta==n+1){
					  System.out.println();
					  conta=0;
				  }
			  }
			}
		
		System.out.println("\n\n");
		System.out.println("MATRIZ TRIANGULAR SUPERIOR");
		int conta2=0;
		for (int i=0; i < Matriz.length; i++) {
			  for (int j=0; j < Matriz[i].length; j++) {
				  System.out.print( Matriz[i][j]+"\t");
				  conta2++;
				  if(conta2==n+1){
					  System.out.println();
					  conta2=0;
				  }
			  }
			}		
		System.out.println("\n\n");
		System.out.println("RESULTADOS");
		for(int i=0; i<x.length;i++){
			System.out.println("Valor de x"+(i+1)+" es: "+x[i]);
		}
	}
	public static void Gauss_Jordan(){
		
		double Matriz [][];
		int n;
		double pivote,cero=0;
		System.out.println("**METODO DE GAUSS-JORDAN**\n");
		System.out.println("TECLEE EL TAMA�O DE LA MATRIZ CUADRADA:");
		n=Leer.datoInt();
		
		Matriz=new double [n][n+1];
		double Aux [][]= new double [n][n+1];
		
		System.out.println("TECLEE LOS VALORES DEL SISTEMA DE ECUACIONES SEGUN EL ORDEN");
		for (int i=0; i < Matriz.length; i++) {
			  for (int j=0; j < Matriz[i].length; j++) {
				  if(j==n){
						System.out.print("Incognita Num:"+(i+1)+" ");
					}
				  System.out.print("a["+(i+1)+(j+1)+"]:");  
				  Matriz[i][j]=Leer.datoDouble();	
				  Aux[i][j]	= Matriz[i][j];	 
			  }
			}
		
		for(int i=0;i<Matriz.length;i++)//AQUI HACE LOS CALCULOS
	    {
			 pivote=Matriz[i][i];
	    	for(int j=0;j<Matriz[i].length;j++)
	    	{
	    		Matriz[i][j]=Matriz[i][j]/pivote;
	    		
	    	}
	    	for(int k=0;k<Matriz.length;k++)
	    	{
	    		if(k!=i)
	    		{
	    			 cero= Matriz[k][i];
	    		
	    		for(int j=0; j<Matriz[i].length; j++)
	    		{
	    			
	    			Matriz[k][j]=Matriz[k][j]-cero*Matriz[i][j];
	    		}
	    	    }
	    	}
	    
	    }
		
		System.out.println("\n\n");
		System.out.println("SISTEMA DE ECUACIONES ORIGINAL");
		int conta=0;
		for (int i=0; i < Aux.length; i++) {
			for (int j=0; j < Aux[i].length; j++) {
				  System.out.print( Aux[i][j]+"\t");
				  conta++;
				  if(conta==n+1){
					  System.out.println();
					  conta=0;
				  }
			  }
			}
		
		System.out.println();
		System.out.println("MATRIZ IDENTIDAD");//IMPRIME MATRIZ IDENTIDAD
		for(int i=0; i<Matriz.length;i++){
			for(int j=0; j<Matriz[i].length; j++){
				System.out.print(Matriz[i][j]+"\t");
			}
			System.out.println();
		}
		
		System.out.println();
		System.out.println("SOLUCIONES");//IMPRIME LAS SOLUCIONES
		for(int i=0; i<Matriz.length;i++){
			System.out.println("X"+(i+1)+" = "+Matriz[i][Matriz.length]);
		}
	}
	
	public static void Cramer(){
		
		double Matriz [][];
		int n;
		System.out.println("**METODO DE CRAMER**\n");
		System.out.println("TECLEE EL TAMA�O DE LA MATRIZ CUADRADA:");
		n=Leer.datoInt();
		
		Matriz=new double [n][n];
		double Aux [][]= new double [n][n];
	
		System.out.println("TECLEE LOS VALORES DEL SISTEMA DE ECUACIONES");
		for (int i=0; i < Matriz.length; i++) {
			  for (int j=0; j < Matriz[i].length; j++) {
				  
				  System.out.print("a["+(i+1)+(j+1)+"]:");  
				  Matriz[i][j]=Leer.datoDouble();	
				  
			  }
			}
		
		double b[]=new double [n];
		System.out.println("TECLEE LOS TERMINOS INDEPENDIENTES DEL SISTEMA");
		for(int i=0;i<b.length;i++){
			System.out.print("b["+(i+1)+"] = ");
			b[i]=Leer.datoDouble();
		}
		
		duplica(Matriz,Aux);
		double det=determinante(Matriz);
		double x[]=new double[n];
		
		if(det==0){
			 System.out.println("Sistema sin soluci�n");
			 return;
		 }
		 else{
			 duplica(Aux,Matriz);
		 }
		
		 for(int j=0;j<n;j++){
			 for(int i=0;i<n;i++){
				 Aux[i][j]=b[i];
			 }
			 double detx=determinante(Aux);
			 duplica(Matriz,Aux);
			 x[j]=detx/det;
			 
		 }
		 System.out.println("MATRIZ DEL SISTEMA");
		 for(int i=0;i<Matriz.length;i++){
			 System.out.println();
			 for(int j=0;j<Matriz[i].length;j++){
				 System.out.print(Matriz[i][j]+"\t");
			 }
		 }
		 System.out.println("\nDETERMINANTE DEL SISTEMA");
		 System.out.println(det);
		 
		 System.out.println("SOLUCIONES DEL SISTEMA");
		 for(int i=0;i<n;i++){
			 System.out.println("X"+(i+1)+" = "+x[i]);
		 }
		
	}

	public static void Jacobi(){
		
		double Matriz [][];
		double MatrizAux [][];
		int  m;
		System.out.println("**METODO DE JACOBI**\n");
		System.out.println("TECLEE EL TAMA�O DE LA MATRIZ CUADRADA:");
		m=Leer.datoInt();
		
		Matriz=new double [m][m+1];
		
		MatrizAux=new double [m][m];
		double x []= new double [m];
	
		System.out.println("TECLEE LOS VALORES DEL SISTEMA DE ECUACIONES");
		for (int i=0; i < Matriz.length; i++) {
			  for (int j=0; j < Matriz[i].length; j++) {
				  if(j==m){
						System.out.print("Incognita Num:"+(i+1)+" ");
					}
				  System.out.print("a["+(i+1)+(j+1)+"]:");  
				  Matriz[i][j]=Leer.datoDouble();	
				  if(j!=m){
					  MatrizAux[i][j]=Matriz[i][j];
				  }
			  }
			}
		double det=determinante(Matriz);
	
		
		if(det==0){
			 System.out.println("Sistema sin soluci�n");
			 return;
		 }
		
		System.out.println("TECLEE EL ERROR PREESTABLECIDO ");
		double e=Leer.datoDouble();
		
		System.out.println("TECLEE EL N�MERO DE ITERACIONES");
		int iter=Leer.datoInt();
		
		int cont=1;
		
		for(int i=0;i<m;i++){
			x[i]=0;
		}
	
		System.out.println("I      x1                          x2                              x3                              x1                                 x2                                 x3");
		boolean fin;
		double y[]=new double [m];
		
		do{
			fin=true;
			for(int i=0;i<m;i++){
				y[i]=Matriz[i][m];
				for(int j=0;j<m;j++){
					if(i!=j){
						y[i]=y[i]-Matriz[i][j]*x[j];
					}
				} 
				y[i]=y[i]/Matriz[i][i];				
			
				double delta = Math.abs(x[i]-y[i]);
				if(delta > e){
					fin=false;
				}
			}
			if(cont==1){
			System.out.println((cont)+"      "+blancos(x[0]+"")+"        "+blancos(x[1]+"")+"             "+blancos(x[2]+"")+"           "+blancos(y[0]+"")+"              "+blancos(y[1]+"")+"                  "+blancos(y[2]+""));
			
			}
			if(cont!=1){
				System.out.println(cont+"      "+blancos(x[0]+"")+"        "+blancos(x[1]+"")+"             "+blancos(x[2]+"")+"           "+blancos(y[0]+"")+"              "+blancos(y[1]+"")+"                  "+blancos(y[2]+""));
				}
			
			
			for(int i=0;i<m;i++){
				
				x[i]=y[i];
			}
			/*System.out.println("Iteraci�n: "+cont);
			for (int j=0;j<m;j++){
				System.out.println("x["+(j+1)+"]= "+x[j]);
			}
				*/
			if(cont==iter)
				fin=true;
			
		cont++;
		}while(!fin);
		
		System.out.println("\nSOLUCION DEL SISTEMA ");
		for(int i=0;i<m;i++){
			System.out.println("X"+(i+1)+"= "+x[i]);
		}
		
	}
	
	public static void GaussSeidel(){

		double Matriz [][];
		double MatrizAux [][];
		int  m;
		System.out.println("**METODO DE GAUSS-SEIDEL**\n");
		System.out.println("TECLEE EL TAMA�O DE LA MATRIZ CUADRADA:");
		m=Leer.datoInt();
		
		Matriz=new double [m][m+1];
		double x []= new double [m];
		MatrizAux=new double [m][m];
		
		System.out.println("TECLEE LOS VALORES DEL SISTEMA DE ECUACIONES");
		for (int i=0; i < Matriz.length; i++) {
			  for (int j=0; j < Matriz[i].length; j++) {
				  if(j==m){
						System.out.print("Incognita Num:"+(i+1)+" ");
					}
				  System.out.print("a["+(i+1)+(j+1)+"]:");  
				  Matriz[i][j]=Leer.datoDouble();	
				  if(j!=3){
					  MatrizAux[i][j]=Matriz[i][j];
				  }
				  
			  }
			}
		
		double det=determinante(MatrizAux);
		if(det==0){
			 System.out.println("Sistema sin soluci�n");
			 return;
		 }
		
		
		System.out.println("TECLEE EL ERROR PREESTABLECIDO ");
		double e=Leer.datoDouble();
		
		System.out.println("TECLEE EL N�MERO DE ITERACIONES");
		double iter=Leer.datoDouble();
		
		for(int i=0;i<m;i++){
			x[i]=0;
		}
				
		System.out.println("I      x1                          x2                              x3                              x1                                 x2                                 x3");
		boolean fin;
		double y;
		double aux[]=new double[m];
		int cont=1;
		do{
			fin=true;
			for(int i=0;i<m;i++){
				y=Matriz[i][m];
				for(int j=0;j<m;j++){
					if(i!=j){
						y=y-Matriz[i][j]*x[j];
					}
				} 
				y=y/Matriz[i][i];				
				
			
				double delta = Math.abs(x[i]-y);
				if(delta > e){
					fin=false;
				}
				x[i]=y;	
			}
		
			//System.out.println("Iteraci�n: "+cont);
			/*for (int j=0;j<m;j++){
				System.out.println("x["+(j+1)+"]= "+x[j]);
			}
			*/	
			if(cont==iter)
				fin=true;
			
			if(cont==1){
				System.out.println((cont)+"      "+blancos(0+"")+"        "+blancos(0+"")+"             "+blancos(0+"")+"           "+blancos(x[0]+"")+"              "+blancos(x[1]+"")+"                  "+blancos(x[2]+""));
				
				}
				if(cont!=1){
					System.out.println(cont+"      "+blancos(aux[0]+"")+"        "+blancos(aux[1]+"")+"             "+blancos(aux[2]+"")+"           "+blancos(x[0]+"")+"              "+blancos(x[1]+"")+"                  "+blancos(x[2]+""));
					}
				
				for(int i=0;i<m;i++){
					aux[i]=x[i];
				}
				
			cont++;
			
		}while(!fin);
		
		System.out.println("\nSOLUCION DEL SISTEMA ");
		for(int i=0;i<m;i++){
			System.out.println("X"+(i+1)+"= "+x[i]);
		}
		
	}
	
	public static void duplica(double orig [][], double cop[][]){
		
		for (int i=0; i < orig.length; i++) {
			  for (int j=0; j < orig[i].length; j++) {
				  cop[i][j]=orig[i][j];
				  
			  }
		}
	}
	
	public static double determinante(double[][] matriz)
	{
	    double det;
	    if(matriz.length==2)
	    {
	        det=(matriz[0][0]*matriz[1][1])-(matriz[1][0]*matriz[0][1]);
	        return det;
	    }
	    double suma=0;
	    for(int i=0; i<matriz.length; i++){
	    double[][] nm=new double[matriz.length-1][matriz.length-1];
	        for(int j=0; j<matriz.length; j++){
	            if(j!=i){
	                for(int k=1; k<matriz.length; k++){
	                int indice=-1;
	                if(j<i)
	                indice=j;
	                else if(j>i)
	                indice=j-1;
	                nm[indice][k-1]=matriz[j][k];
	                }
	            }
	        }
	        if(i%2==0)
	        suma+=matriz[i][0] * determinante(nm);
	        else
	        suma-=matriz[i][0] * determinante(nm);
	    }
	    return suma;
	}
	public static void montante(){

		double Matriz [][];
		int n;
		System.out.println("**METODO DEL MONTANTE**\n");
		System.out.println("TECLEE EL TAMA�O DE LA MATRIZ CUADRADA:");
		n=Leer.datoInt();
		
		int m=n+1;
		Matriz=new double [n][m];
		double Aux [][]= new double [n][m];
		
		System.out.println("TECLEE LOS VALORES DEL SISTEMA DE ECUACIONES SEGUN EL ORDEN");
		for (int i=0; i < Matriz.length; i++) {
			  for (int j=0; j < Matriz[i].length; j++) {
				  if(j==n){
						System.out.print("Incognita Num:"+(i+1)+" ");
					}
				  System.out.print("a["+(i+1)+(j+1)+"]:");  
				  Matriz[i][j]=Leer.datoDouble();	
				  Aux[i][j]	= Matriz[i][j];	 
			  }
			}
		
		double pivoteant=1;
		
		for(int k=0; k<n;k++)
		{
			for(int i=0; i<n;i++)
			{
				if(i!=k){
				for(int j=n; j>=0;j--){
					Matriz[i][j]=(Matriz[k][k]*Matriz[i][j]-Matriz[i][k]*Matriz[k][j])/pivoteant;
					}
				}
			}
			pivoteant=Matriz[k][k];
		}
		
		double determinante=Matriz[0][0];
		
		for(int i=0; i<n;i++){
			
			for(int j=0;j<n;j++)
			{
				if(i==j){
					Matriz[i][n]=Matriz[i][n]/determinante;
				}
			}
		}
		
		System.out.println("\n\n");
		System.out.println("SISTEMA DE ECUACIONES ORIGINAL");
		int conta=0;
		for (int i=0; i < Aux.length; i++) {
			for (int j=0; j < Aux[i].length; j++) {
				  System.out.print( Aux[i][j]+"\t");
				  conta++;
				  if(conta==n+1){
					  System.out.println();
					  conta=0;
				  }
			  }
			}
		
		System.out.println("\n\n");
		System.out.println("MATRIZ RESULTANTE");
		int conta2=0;
		for (int i=0; i < Matriz.length; i++) {
			  for (int j=0; j < Matriz[i].length; j++) {
				  System.out.print( Matriz[i][j]+"\t");
				  conta2++;
				  if(conta2==n+1){
					  System.out.println();
					  conta2=0;
				  }
			  }
			}		
		System.out.println("\n\n");
		System.out.println("RESULTADOS");
		for(int i=0; i<n;i++){
			System.out.println("Valor de x"+(i+1)+" es: "+Matriz[i][n]);
		}
	}
 	
	//INTERPOLACIONES LINEALES Y AJUSTE POLINOMIAL
	public static void internewton(){
		double xi[], yi[][];
		int m;
		System.out.println("**METODO DE NEWTON**\n");
		System.out.println("Teclee los puntos con los que va a trabajar: ");
		m=Leer.datoInt();
		
		xi=new double [m];
		yi=new double [m][m];
		for(int i=0; i<m;i++){
			System.out.print("x"+(i+1)+"= ");
			xi[i]=Leer.datoDouble();
			System.out.print("y["+(i+1)+"]["+(0)+"]=");
			yi[i][0]=Leer.datoDouble();
		}
		
		for(int j=1;j<m-1;j++)
			for(int i=0;i<m-j;i++)
			{
				yi[i][j]=yi[i+1][j-1]-yi[i][j-1];
			}
			
	//AQUI DESPLEGAMOS LAS COLUMNAS DEL CUADRO DE DIFERENCIAS FINITAS
		for(int i=0;i<m+1;i++){
			if(i==0){
			System.out.print(blancos("x"));
			continue;
			}
			if(i==1){
			System.out.print(blancos("y"));
			continue;
			}
			
			System.out.print(blancos("D^"+(i-1)+"y"));
		}
		
		System.out.println();
		for(int i=0;i<m;i++){
			System.out.print(blancos(xi[i]+""));
			for(int j=0;j<m;j++)
			{
				System.out.print(blancos(yi[i][j]+""));
			}
				
			System.out.println();
		}
		
		
		System.out.println("TECLEE EL VALOR QUE DECEA CALCULAR");
		double x=Leer.datoDouble();
		
		double k=((x-xi[0])/(xi[2]-xi[1]));
		double y=0;
		
		for(int i=1;i<m;i++){
			double num=1;
			double j=0;
			while(j<=(i-2)){
				num=num*(k-j);
				j++;
			}
			y=y+num/factorial(i-1) * yi[0][i-1];
		}
		
		System.out.println("El valor de y para x="+x+" es: "+y);
	}
	public static void lagrange(){
		double xi[], yi[];
		
		System.out.println("***METODO DE LAGRANGE***");
		System.out.println("TECLEE LOS PUNTOS A USAR ");
		int m=Leer.datoInt();
		xi=new double [m];
		yi=new double [m];
		
		for(int i=0; i<m;i++){
			System.out.print("x["+(i+1)+"]= ");
			xi[i]=Leer.datoDouble();
			System.out.print("y["+(i+1)+"]= ");
			yi[i]=Leer.datoDouble();
		}
		System.out.print("TECLEE EL VALOR DE X QUE DECEA CALCULAR:");
		double x=Leer.datoDouble();
		double y=0;
		
		for(int i=0;i<m;i++){
			double num=1;
			double den=1;
			for(int j=0; j<m;j++){
				if(i!=j){
					num=num*(x-xi[j]);
					den=den*(xi[i]-xi[j]);
				}
			}
			y=y+(num/den) * yi[i];
		}
		System.out.print("PARA EL VALOR DE X="+x+" EL VALOR DE Y ES IGUAL A: "+y);
		
	}
	  public static double factorial(double number) {
	        if (number <= 1)
	            return 1;
	        else
	            return number * factorial(number - 1);
	    }

	  public static void MinCuadrados(){
		  System.out.println("*****MINIMOS CUADRADOS*****");
		  System.out.println();
		 double [] a, b,x,y;
		  
		  
		  System.out.print("Numero de puntos a utilizar: ");
		  int n = Leer.datoInt();
		  System.out.print("Teclee el orden de la funsion: ");
		  int m=Leer.datoInt();
		  double Mat [] []=new double [n][n];
		  a=new double[(m*2)+1];
		  b=new double [m+1];
		  x=new double [n];
		  y=new double [n];
		  a[0]=n;
		  b[0]=0;
		  
		  for(int i=0;i<n;i++ )
		  {
			  System.out.print("Teclee x["+(i+1)+"]: ");
			  x[i]=Leer.datoDouble();
			  Mat[i][0]=x[i];
			  System.out.print("Teclee y["+(i+1)+"]: ");
			  y[i]=Leer.datoDouble();
			  Mat[i][1]=y[i];
			  b[0]=b[0]+y[i];
			  
			  for(int j=1; j<=(m*2);j++){
				a[j]= a[j]+ Math.pow(x[i], j);
				if(j!=1)
				Mat[i][j]=Math.pow(x[i], j);
			  }
			 for(int j=1;j<=m;j++){
				 b[j]=b[j] + y[i]*Math.pow(x[i], j);
				 if(j==1){
					 Mat[i][5]=y[i]*Math.pow(x[i], j);
				 }
				 else
				 {
					 Mat[i][6]=y[i]*Math.pow(x[i], j);;
				 }
			 }
			  
		  }
		  
		  int cont=0;
		  for(int i=0;i<n;i++){
			  
			  if(i==1)
				  System.out.print(blancos("y"+""));
			  if(i<4)
			  System.out.print(blancos("x^"+(i+1)+""));
			  if(i>=4 && i<6){
				  cont++;
				  System.out.print(blancos("x^"+cont+"y"+""));
			  }
			  
		  }
		  System.out.println();
		  
		  
		  for(int i=0;i<n;i++){
			  for(int j=0;j<n;j++){
				  System.out.print(blancos(Mat[i][j]+""));
			  }
			  System.out.println();		
		}
		  
		  
		  
		  
		  double c[][]=new double [m+1][m+2];
		  for(int i=1; i<=m+1; i++)
		  {
			  for(int j=1;j<=m+1;j++){
				  c[i-1][j-1]=a[i+j-2];
			  }
			  c[i-1][m+1]=b[i-1];
		  }
		  System.out.println("SISTEMA RESULTANTE DE LAS OPERACIONES ES:");
		  for(int i=0;i<m+1;i++){
			  for(int j=0; j<m+2;j++){
				System.out.print(blancos(c[i][j]+"a"+(j+1)+"")); 
			  }
			  System.out.println();
		  }
		  double [] res=NewCuadrados(c,m);
			
		  
		  System.out.println("\n**RESULTADOS**");
		  for(int i=0; i<m+1;i++){
			  System.out.println("a["+(i+1)+"] ="+res[i]);
		  }
		  
		  System.out.println("La Funcion f(x)= "+res[2]+"x^2+"+res[1]+"x+"+res[0]);
		  String Funcion=res[2]+"x^2+"+res[1]+"x+"+res[0];
		 Leer.funcion(Funcion);
		  System.out.println("Decea buscar un valor para x con la funcion?");
		  int opcion=0;
		  do{
		  System.out.print("Teclee 1 para Si o 2 para no: ");
		  opcion=Leer.datoInt();
		 }while(opcion<1 && opcion >2);
		  
		  if(opcion==2){
			  return;
		  }
		  else{
			  System.out.print("Teclee el valor de x: ");
			  double Val=Leer.datoDouble();
			  
			  System.out.println("EL VALOR DE  f("+Val+")= "+Leer.eval(Funcion, Val));
		  }
		  
	  }
	  
	  public static double [] NewCuadrados(double [][] Matriz, int m){
		  double resultado[]=new double [m+1];
		  double pivote, cero;
			
		  for(int i=0;i<Matriz.length;i++)//AQUI HACE LOS CALCULOS
		    {
				 pivote=Matriz[i][i];
		    	for(int j=0;j<Matriz[i].length;j++)
		    	{
		    		Matriz[i][j]=Matriz[i][j]/pivote;
		    		
		    	}
		    	for(int k=0;k<Matriz.length;k++)
		    	{
		    		if(k!=i)
		    		{
		    			 cero= Matriz[k][i];
		    		
		    		for(int j=0; j<Matriz[i].length; j++)
		    		{
		    			
		    			Matriz[k][j]=Matriz[k][j]-cero*Matriz[i][j];
		    		}
		    	    }
		    	}
		    
		    }
			
			for(int i=0; i<m+1;i++){
				
				resultado[i]= Matriz[i][m+1];
			}
		 
		  return resultado;
	  }
	  
	//DIFERENCIACION NUMERICA
	  public static void Trapecio(){

			System.out.println("*****METODO DE TRAPECIO***** ");
			System.out.println();
			System.out.println("SI VA A INTRODUCIR UNA FUNCION TECLEE 1, SINO TECLEE 2 PARA INTRODUCIR UNA TABLA DE VALORES");
			int op=Leer.datoInt();
			if(op==1){
			System.out.println("TECLEE LA FUNCION:");
		    String f=Leer.funcion();
			
			System.out.print("TECLEE EL LIMITE INFERIOR"+"["+"a"+"]:");
		    double a= Leer.datoDouble();
		    
		    System.out.print("TECLEE EL LIMITE SUPERIOR"+"["+"b"+"]:");
		    int b= Leer.datoInt();
		    
		    System.out.print("TECLEE EL NUMERO DE INTERVALOS:");
		    int n=Leer.datoInt();
		    trapecio(f,a,b,n);
			}
			else
			{
				System.out.println("TECLEE EL NUMERO DE INTERVALOS x & y: ");
				int N=Leer.datoInt();
				System.out.println("TECLEE LA DIFERENCIA"+"["+"h"+"]");
				double h=Leer.datoDouble();
				trapecio2(N,h);
			}
	  }
	  

		public static void trapecio(String f,double a,int b,int n)
		{	    
	    double x,area,sum1=0,sum2=0,sum3=0;
	    double y[]=new double[n];
	    double h = (b-a)/(n);
	    double e;
	        System.out.println("         x"+"                         "+"y"+"                                     RESULTADO");
	    for(int j=0; j<=y.length; j++)
	    {
	    	x=a;
	    	e=Leer.eval(f, x);
	    	if(j==0||j==y.length)
	    	{
          System.out.println(blancos(x+"")+"      "+blancos(e+"")+"                         "+blancos(e+""));
	    	 sum1=sum1+e;
	    	}
	    	else
	    	{
	    		System.out.println(blancos(x+"")+"      "+blancos(e+"")+"                         "+blancos(e*2+""));
	    	 sum2=sum2+e*2;
	    	}
	    		x=x+h;
	    	a=x;
	    }
	    
	    sum3=sum1+sum2;
	     area=h/2*sum3;
	     System.out.println();
	     System.out.println("AREA DE LA FUNCION: "+area);
	    
	}
		
	public static void trapecio2(int n,double h)
	{
		double y[]= new double[n];
		double x[]=new double[n];
		double sum1=0,sum2=0,sum3=0,area;
	for(int i=0; i<n; i++)
	{
		System.out.print("TECLEE EL VALOR DE X"+"["+(i+1)+"]");
		x[i]=Leer.datoDouble();
		System.out.print("TECLEE EL VALOR DE Y"+"["+(i+1)+"]");
		 y[i]=Leer.datoDouble();
	}
	       System.out.println("      x"+"                         "+"y"+"                                     RESULTADO");
	for(int i=0; i<n; i++)
	{
		
		if(i==0||i==n-1)
		{
			System.out.println(blancos(x[i]+"")+"      "+blancos(y[i]+"")+"                         "+blancos(y[i]+""));
	    	 sum1=sum1+y[i];
		}
		else{
			System.out.println(blancos(x[i]+"")+"      "+blancos(y[i]+"")+"                         "+blancos(y[i]*2+""));
	    	 sum2=sum2+y[i]*2;
		}
	}
	sum3=sum1+sum2;
	area=h/2*sum3;
  System.out.println();
  System.out.println("AREA DE LA FUNCION: "+area);
	
	}
	public static void Simpson(){
		System.out.println("*****METODO DE SIMPSON*****");
		System.out.println();
		System.out.println("SI VA A INTRODUCIR UNA FUNCION TECLEE 1, SINO TECLEE 2 PARA INTRODUCIR UNA TABLA DE VALORES");
		int op=Leer.datoInt();
		if(op==1){
		System.out.println("TECLEE LA FUNCION:");
	    String f=Leer.funcion();
		
		System.out.print("TECLEE EL LIMITE INFERIOR"+"["+"a"+"]:");
	    double a= Leer.datoDouble();
	    
	    System.out.print("TECLEE EL LIMITE SUPERIOR"+"["+"b"+"]:");
	    int b= Leer.datoInt();
	    
	    System.out.print("TECLEE EL NUMERO DE INTERVALOS:");
	    int n=Leer.datoInt();
	    Simpson(f,a,b,n);
		}
		else
		{
			System.out.println("TECLEE EL NUMERO DE INTERVALOS x & y: ");
			int N=Leer.datoInt();
			System.out.println("TECLEE LA DIFERENCIA"+"["+"h"+"]");
			double h=Leer.datoDouble();
			Simpson2(N,h);
		}
	}
	 
	public static void Simpson(String f,double a,int b,int n)
	{	    
    double x,area,sum1=0,sum2=0,sum3=0,sum4=0;
    double y[]=new double[n];
    double h = (b-a)/(n);
    double e;
        System.out.println("         x"+"                         "+"y"+"                  FACTOR MULTIPLICADOR                   RESULTADO");
    for(int j=0; j<=y.length; j+=1)
    {
    	x=a;
    	e=Leer.eval(f, x);
    	if(j==0||j==y.length)
    	{
        System.out.println(blancos(x+"")+"      "+blancos(e+"")+"                  "+"1"+"                        "+blancos(e+""));
    	 sum1=sum1+e;
    	}
    	else if(j%2==1)
    	{
    		System.out.println(blancos(x+"")+"      "+blancos(e+"")+"                  4                        "+blancos(e*4+""));
    	 sum2=sum2+e*4;
    	 
    	}
    	else{
    		System.out.println(blancos(x+"")+"      "+blancos(e+"")+"                  2                        "+blancos(e*2+""));
	    	 sum3=sum3+e*2;
    	}
    		x=x+h;
    	a=x;
    }
    
    sum4=sum1+sum2+sum3;
     area=h/3*sum4;
     System.out.println();
     System.out.println("AREA DE LA FUNCION: "+area);
    
}
	
    public static void Simpson2(int n,double h)
{
	double y[]= new double[n];
	double x[]=new double[n];
	double sum1=0,sum2=0,sum3=0,area,sum4=0;
for(int i=0; i<n; i++)
{
	System.out.print("TECLEE EL VALOR DE X"+"["+(i+1)+"]");
	x[i]=Leer.datoDouble();
	System.out.print("TECLEE EL VALOR DE Y"+"["+(i+1)+"]");
	 y[i]=Leer.datoDouble();
}
       System.out.println("      x"+"                         "+"y"+"                                     RESULTADO");
for(int i=0; i<n; i++)
{
	
	if(i==0||i==n-1)
	{
		System.out.println(blancos(x[i]+"")+"      "+blancos(y[i]+"")+"                         "+blancos(y[i]+""));
    	 sum1=sum1+y[i];
	}
	else if(i%2==1){
		System.out.println(blancos(x[i]+"")+"      "+blancos(y[i]+"")+"                         "+blancos(y[i]*4+""));
    	 sum2=sum2+y[i]*4;
	}
	else{
		System.out.println(blancos(x[i]+"")+"      "+blancos(y[i]+"")+"                         "+blancos(y[i]*2+""));
    	 sum3=sum3+y[i]*2;
	}
}
sum4=sum1+sum2+sum3;
area=h/3*sum4;
System.out.println();
System.out.println("AREA DE LA FUNCION: "+area);

}	
	
    public static void DMLimites(){
    	System.out.println("*****DIFERENCIACION NUMERICA POR LIMITES***** ");
		System.out.println();
		
		System.out.println("Teclee la funci�n f(x) :");
		String f = Leer.funcion();
		
		System.out.println("Teclee el valor para x que decee calcular: ");
		double x= Leer.datoDouble();
		
		System.out.println("Teclear el error  ");
		double e= Leer.datoDouble();
		
		System.out.println(blancos ("n")+"      "+blancos("h")+"      "+blancos("k")+"      "+blancos("x+h")+"      " + blancos ("f(x+h)")+"      " + blancos("f(x)")+"      " + blancos("f(x+h)-f(x)")+"      " + blancos("(f'(x)")+"      " +blancos("e"));
		
		int n=1;
		double k,d,delta=0,h;
		double da=1*Math.pow(10, 10);
		
		do{
			h=Math.pow(0.1,n);
			k=1/h;
			d=(Leer.eval(f, x+h)-Leer.eval(f, x)) * k;
			delta=Math.abs(da-d);
			System.out.println(blancos (n+"")+"      "+blancos(h+"")+"      "+blancos(k+"")+"      "+blancos((x+h)+"")+"      " + blancos (Leer.eval(f, x+h)+"")+"      " + blancos(Leer.eval(f, x)+"")+"      " + blancos((Leer.eval(f, x+h)-Leer.eval(f, x))+"")+"      " + blancos((Leer.eval(f, x+h)-Leer.eval(f, x))*k+"")+"      " +blancos((da-d)+""));
			da=d;
			n++;

		}while(delta > e);
		
		
    }
    
	public static void main(String[] args) {
	int opc1,opc2,opc3,opc4,opc5;
		String s;
	
	do{
		System.out.println();
		opc1=menu1();
		
		if(opc1==1){
			do{
				System.out.println();
				opc2=menu2();
				if(opc2==1){
					do{
						
						MetodoBiseccion();
						System.out.println("\nDecea Continuar: (S/N) ");
						s=Leer.datoString();
						g.eliminarGrafica();
					}while(s.equals("S")||s.equals("s"));
						g.eliminarGrafica();
				}
				
				if(opc2==2){
					do{
						
						MetodoFalsaPosicion();
						System.out.println("\nDecea Continuar: (S/N) ");
						 s=Leer.datoString();
						 g.eliminarGrafica();
						}while(s.equals("S")||s.equals("s"));
						g.eliminarGrafica();
				}
				
				if(opc2==3){
					do{
						MetodoSecante();
						System.out.println("\nDecea Continuar: (S/N) ");
						 s=Leer.datoString();
						 g.eliminarGrafica();
						}while(s.equals("S")||s.equals("s"));
						g.eliminarGrafica();	
				}
				
				if(opc2==4){
					do{
						MetodoNewtonRaphson();
						System.out.println("\nDecea Continuar: (S/N) ");
						 s=Leer.datoString();
						 g.eliminarGrafica();
						}while(s.equals("S")||s.equals("s"));
						g.eliminarGrafica();
				}
				
				if(opc2==5){
					do{
						MetodoAproxSucesivas();
						System.out.println("\nDecea Continuar: (S/N) ");
						 s=Leer.datoString();
						 g.eliminarGrafica();
					}while(s.equals("S")||s.equals("s"));
						g.eliminarGrafica();
				}
				
				if(opc2==6){
					do{
						NewtonSegundoOrden();
						System.out.println("\nDecea Continuar: (S/N) ");
						 s=Leer.datoString();
						 g.eliminarGrafica();
						}while(s.equals("S")||s.equals("s"));
						g.eliminarGrafica();
				}
				
			}while (opc2!=7);	
		}
		
		if(opc1==2){
		 do{
			    System.out.println();
				opc3=menu3();
				if(opc3==1){
					do{
						Gauss();
						System.out.println("\nDecea Continuar: (S/N) ");
						s=Leer.datoString();
					}while(s.equals("S")||s.equals("s"));
				}
				
				if(opc3==2){
					do{
						Gauss_Jordan();
						System.out.println("\nDecea Continuar: (S/N) ");
						s=Leer.datoString();
					}while(s.equals("S")||s.equals("s"));
				}

				if(opc3==3){
					do{
						montante();
						System.out.println("\nDecea Continuar: (S/N) ");
						s=Leer.datoString();
					}while(s.equals("S")||s.equals("s"));
				}
				
				if( opc3 ==4){
					do{
						Cramer();
						System.out.println("\nDecea Continuar: (S/N) ");
						s=Leer.datoString();
					}while(s.equals("S")||s.equals("s"));
					
				}
				
				if( opc3 ==5){
					do{
						Jacobi();
						System.out.println("\nDecea Continuar: (S/N) ");
						s=Leer.datoString();
					}while(s.equals("S")||s.equals("s"));
					
				}
				
				if( opc3 ==6){
					do{
						GaussSeidel();
						System.out.println("\nDecea Continuar: (S/N) ");
						s=Leer.datoString();
					}while(s.equals("S")||s.equals("s"));
					
				}
			
		  }while(opc3!=7);
		}
		
		if(opc1==3){
			do{
				 System.out.println();
					opc4=menu4();
					if(opc4==1)
					{
						do{
							internewton();
							System.out.println("\nDecea Continuar: (S/N) ");
							s=Leer.datoString();
						}while(s.equals("S")||s.equals("s"));
					}
					
					if(opc4==2)
					{
						do{
							lagrange();
							System.out.println("\nDecea Continuar: (S/N) ");
							s=Leer.datoString();
						}while(s.equals("S")||s.equals("s"));
					}
					
					if(opc4 == 3)
					{
						do{
							MinCuadrados();
							System.out.println("\nDecea Continuar: (S/N) ");
							s=Leer.datoString();
						}while (s.equals("S")||s.equals("s"));
						
					}
					
					
			}while(opc4!=4);
		}
		
		if(opc1==4){
			do{
				 System.out.println();
					opc5=menu5();
					if(opc5==1)
					{
						do{
							DMLimites();
							System.out.println("\nDecea Continuar: (S/N) ");
							s=Leer.datoString();
						}while(s.equals("S")||s.equals("s"));
					}
					
					if(opc5==2)
					{
						do{
							Trapecio();
							System.out.println("\nDecea Continuar: (S/N) ");
							s=Leer.datoString();
						}while(s.equals("S")||s.equals("s"));
					}
					
					if(opc5 == 3)
					{
						do{
							Simpson();
							System.out.println("\nDecea Continuar: (S/N) ");
							s=Leer.datoString();
						}while (s.equals("S")||s.equals("s"));
						
					}
					
					
			}while(opc5!=4);
		}
	}while(opc1!=5);
		System.out.println("\nAdios... (^_^) ");
		
	}

}




