package MetodosNum;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class Grafica 
{	
	JFreeChart Grafica; 
	JFrame MiVentana;
	XYSeriesCollection Datos = new XYSeriesCollection();
	String Titulo;
	String Etiquetax;
	String Etiquetay;
		
	public Grafica (String titulo, String etiquetax, String etiquetay)
	{
		Titulo = titulo;
		Etiquetax = etiquetax;
		Etiquetay = etiquetay;
		Grafica = ChartFactory.createXYLineChart(titulo, etiquetax, etiquetay, Datos, PlotOrientation.VERTICAL, true, true, true);
		MiVentana = new JFrame("Gr�fica");
		MiVentana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		MiVentana.setLocationRelativeTo(null);
		MiVentana.setSize(600, 400);
	}
		
	public Grafica ()		
	{
		this("Gr�fica", "x", "y");
	}
		
	public void agregarGrafica (String id, double [] x, double [] y)
	{
		XYSeries s = new XYSeries (id);
		int n = x.length;
		
		for (int i=0; i<n; i++)
			s.add(x[i], y[i]);
		
		Datos.addSeries(s);
	}
		
	public void crearGrafica (String id, double [] x, double [] y)
	{
		Datos.removeAllSeries();
		agregarGrafica(id, x, y);
	}
	
	public void mostrarGrafica(String f, Grafica g)
	{
		double [] x = g.rango(-5, 5, 0.5); 
		double [] y = Leer.eval(f, x); 
		
		g.crearGrafica(f, x, y); 
		
		JPanel mipanel = new JPanel();
		mipanel = g.obtieneGrafica();
		MiVentana.add(mipanel);
		MiVentana.setVisible(true);
	}
	public void eliminarGrafica()
	{
		MiVentana.dispose();
	}
	public JPanel obtieneGrafica()
	{
		return new ChartPanel(Grafica);
	}

	public double [] rango (double xinicio, double xfinal, double incremento)
	{
		int n = (int)(Math.abs(xfinal - xinicio) / incremento) + 1;
		double [] r = new double [n];
		
		for(int i=0; i<n; i++)
		{
			r[i] = xinicio;
			xinicio += incremento;
		}
	
		return r;
	}
}
