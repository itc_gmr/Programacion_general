package Met_Num;

import javax.swing.JOptionPane;

public class Biseccion extends javax.swing.JDialog {
    
    public Biseccion() {
        initComponents();
        setLocationRelativeTo(null);
        grafica.setEnabled(false); //Para que al abrir la ventana el botoón grafica se muestre deshabilitado.
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        intervaloA = new javax.swing.JTextField();
        error = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        numitera = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        funcion = new javax.swing.JTextField();
        intervaloB = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        derivada1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        derivada2 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        raiz = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        iteraciones = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jComboBox1 = new javax.swing.JComboBox();
        calcular = new javax.swing.JButton();
        limpiar = new javax.swing.JButton();
        grafica = new javax.swing.JButton();
        Cerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Método De Bisección");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel6.setText("SOLUCION DE RAICES DE ECUACIONES");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Introduzca los siguientes datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setText("Función:");

        jLabel7.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel7.setText("Intervalo A:");

        intervaloA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                intervaloAActionPerformed(evt);
            }
        });
        intervaloA.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                intervaloAKeyTyped(evt);
            }
        });

        error.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                errorKeyTyped(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel8.setText("Intervalo B:");

        jLabel9.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel9.setText("Error Permitido:");

        numitera.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                numiteraKeyTyped(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel10.setText("Número De Iteraciones:");

        funcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcionActionPerformed(evt);
            }
        });
        funcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                funcionKeyTyped(evt);
            }
        });

        intervaloB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                intervaloBActionPerformed(evt);
            }
        });
        intervaloB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                intervaloBKeyTyped(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Primera Derivada:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Segunda Derivada:");

        derivada2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                derivada2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(intervaloA))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(funcion))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(intervaloB))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(numitera))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(derivada1))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel9)
                            .addGap(18, 18, 18)
                            .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(derivada2))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(intervaloA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(intervaloB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(numitera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(derivada1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(derivada2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jLabel11.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel11.setText("La raíz es:");

        raiz.setEditable(false);
        raiz.setForeground(new java.awt.Color(237, 0, 0));

        iteraciones.setEditable(false);
        iteraciones.setColumns(20);
        iteraciones.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        iteraciones.setRows(5);
        jScrollPane1.setViewportView(iteraciones);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccione un Método", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione", "Método de Bisección", "Método de Falsa Posición", "Método de  la Secante", "Método de  Newton-Rapshon", "Método de  Aproximaciones Sucecivas", "Método de  Newton de Segundo Orden" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        calcular.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        calcular.setText("Calcular");
        calcular.setMaximumSize(new java.awt.Dimension(101, 25));
        calcular.setMinimumSize(new java.awt.Dimension(101, 25));
        calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcularActionPerformed(evt);
            }
        });

        limpiar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        limpiar.setText("Limpiar");
        limpiar.setMaximumSize(new java.awt.Dimension(101, 25));
        limpiar.setMinimumSize(new java.awt.Dimension(101, 25));
        limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiarActionPerformed(evt);
            }
        });

        grafica.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        grafica.setText("Gráfica");
        grafica.setMaximumSize(new java.awt.Dimension(101, 25));
        grafica.setMinimumSize(new java.awt.Dimension(101, 25));
        grafica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                graficaActionPerformed(evt);
            }
        });

        Cerrar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Cerrar.setText("Cerrar");
        Cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(calcular, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                    .addComponent(grafica, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(limpiar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Cerrar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(61, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(calcular, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Cerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(grafica, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(raiz, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(32, 32, 32))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(140, 140, 140))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(raiz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        jPanel1.getAccessibleContext().setAccessibleName("Seleccione un Método");
        jPanel1.getAccessibleContext().setAccessibleDescription("Seleccione un Método");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void intervaloAKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_intervaloAKeyTyped

        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!='-') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && intervaloA.getText().contains("."))
            evt.consume();

        if (evt.getKeyChar()=='-' && intervaloA.getText().contains("-"))
            evt.consume();
    }//GEN-LAST:event_intervaloAKeyTyped

    private void errorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_errorKeyTyped

        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && error.getText().contains("."))
            evt.consume();
    }//GEN-LAST:event_errorKeyTyped

    private void numiteraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numiteraKeyTyped

        if ((evt.getKeyChar() < '0' || evt.getKeyChar() > '9') && (evt.getKeyChar()!= evt.VK_BACK_SPACE))
            evt.consume();
    }//GEN-LAST:event_numiteraKeyTyped

    private void funcionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionKeyTyped

        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9')
            && evt.getKeyChar()!='*' && evt.getKeyChar()!='/'
            && evt.getKeyChar()!='+' && evt.getKeyChar()!='-'
            && evt.getKeyChar()!='^' && evt.getKeyChar()!='x'
            && evt.getKeyChar()!='e' && evt.getKeyChar()!='c'
            && evt.getKeyChar()!='o' && evt.getKeyChar()!='s'
            && evt.getKeyChar()!='i' && evt.getKeyChar()!='n'
            && evt.getKeyChar()!='l' && evt.getKeyChar()!='g'
            && evt.getKeyChar()!='t' && evt.getKeyChar()!='a'
            && evt.getKeyChar()!='p' && evt.getKeyChar()!='('
            && evt.getKeyChar()!=')' && evt.getKeyChar()!=evt.VK_BACK_SPACE)
            evt.consume();
    }//GEN-LAST:event_funcionKeyTyped

    private void intervaloBKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_intervaloBKeyTyped

        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!='-') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && intervaloB.getText().contains("."))
            evt.consume();
        
        if (evt.getKeyChar()=='-' && intervaloB.getText().contains("-"))
            evt.consume();
    }//GEN-LAST:event_intervaloBKeyTyped

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
       
        //Para que cuando se cierre la ventana se habiliten los componentes que estaban deshabilitados del menu principal. 
        /*MenuPrincipal.btnAcercaDe.setEnabled(true);
        MenuPrincipal.btnAceptar.setEnabled(true);
        MenuPrincipal.btnSalir.setEnabled(true);
        MenuPrincipal.jList1.setEnabled(true);
        MenuPrincipal.jList2.setEnabled(true);
        */
    }//GEN-LAST:event_formWindowClosed

    public void Biseccion(){
        
       raiz.setText("");
       iteraciones.setText("");
       grafica.setEnabled(false);
        
       try
       {
            String f = Leer.funcion(funcion.getText());
            double a = Double.parseDouble(intervaloA.getText());
            double b = Double.parseDouble(intervaloB.getText());
            int noitera = Integer.parseInt(numitera.getText());
            double ep = Double.parseDouble(error.getText());
           
            if(!f.equals("error"))
            {
                if(Leer.eval(f, a) * Leer.eval(f, b) < 0)
                {
                    grafica.setEnabled(true);
                    MiGrafica.g.mostrarGrafica(funcion.getText()); //Para que muestre la gráfica de la función que converge. 
                    
                    if(Leer.eval(f, a) == 0) 
                       raiz.setText(a+"");
                    else if(Leer.eval(f, b) == 0)
                       raiz.setText(b+"");
                    else
                        biseccion(f, a, b, ep, noitera);
                }
                else
                    JOptionPane.showMessageDialog(null, "La función no converge, vuelva a intentarlo.");  
            }
            else
                JOptionPane.showMessageDialog(null, "Expresión no válida, vuelva a intentarlo.");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
    }
    private void funcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_funcionActionPerformed

    private void intervaloAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_intervaloAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_intervaloAActionPerformed

    private void intervaloBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_intervaloBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_intervaloBActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
       
        int indice = jComboBox1.getSelectedIndex();
        
        if(indice == 1){
            derivada1.setEnabled(false);
             derivada2.setEnabled(false);
             raiz.setText("");
             grafica.setEnabled(false);
             iteraciones.setText("");
        }
              if(indice == 2){
             derivada1.setEnabled(false);
              derivada2.setEnabled(false);
             raiz.setText("");
             grafica.setEnabled(false);
             iteraciones.setText("");
        }
         if(indice == 3){
             derivada1.setEnabled(false);
             derivada1.setEnabled(false);
             raiz.setText("");
             grafica.setEnabled(false);
             iteraciones.setText("");
        }
         if(indice == 4){
             intervaloB.setEnabled(false);
             derivada1.setEnabled(true);
             derivada2.setEnabled(false);
             raiz.setText("");
             grafica.setEnabled(false);
             iteraciones.setText("");
        }
         if(indice == 5){
             intervaloB.setEnabled(false);
             derivada1.setEnabled(false);
             derivada2.setEnabled(false);
             raiz.setText("");
             grafica.setEnabled(false);
             iteraciones.setText("");
        }
         if(indice == 6){
             intervaloB.setEnabled(false);
             derivada1.setEnabled(true);
             derivada2.setEnabled(true);
             raiz.setText("");
             grafica.setEnabled(false);
             iteraciones.setText("");
        }
      
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void graficaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_graficaActionPerformed

        //MiGrafica.g.mostrarGrafica(funcion.getText());
        new MiGrafica().setVisible(true);
        //grafica.setEnabled(false);
    }//GEN-LAST:event_graficaActionPerformed

    private void limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limpiarActionPerformed

        int opcion = JOptionPane.showConfirmDialog(null, "¿Desea limpiar?", "Confirma", JOptionPane.YES_NO_CANCEL_OPTION);

        if(opcion == JOptionPane.YES_OPTION)
        {
            funcion.setText("");
            intervaloA.setText("");
            intervaloB.setText("");
            numitera.setText("");
            error.setText("");
            raiz.setText("");
            grafica.setEnabled(false);
            iteraciones.setText("");
        }

    }//GEN-LAST:event_limpiarActionPerformed

    private void calcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcularActionPerformed
        int indice = jComboBox1.getSelectedIndex();
        if(indice==1){
            Biseccion();
        }
        
        //Y ASI SUCECIVAMENTE PARA CADA UNO DE LOS CODIGOS!
        
        if(indice==2){
           //FalsaPosicion();
        }

    }//GEN-LAST:event_calcularActionPerformed

    private void derivada2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_derivada2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_derivada2ActionPerformed

    private void CerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CerrarActionPerformed
           this.dispose();
    }//GEN-LAST:event_CerrarActionPerformed

    public static void main(String args[]) {
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Biseccion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Cerrar;
    private javax.swing.JButton calcular;
    private javax.swing.JTextField derivada1;
    private javax.swing.JTextField derivada2;
    private javax.swing.JTextField error;
    private javax.swing.JTextField funcion;
    private javax.swing.JButton grafica;
    private javax.swing.JTextField intervaloA;
    private javax.swing.JTextField intervaloB;
    private javax.swing.JTextArea iteraciones;
    private static javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton limpiar;
    private javax.swing.JTextField numitera;
    private javax.swing.JTextField raiz;
    // End of variables declaration//GEN-END:variables
    
    public void biseccion (String f, double a, double b, double e, int numitera)
    {
	double s=0, error;
       
	iteraciones.append("I                        X1                                   X2                                   X2-X1                              Xm                                  f(X1)                                     f(Xm)\n");
        
        for(int i=1; i<=numitera; i++)
	{
            s = (a+b) / 2;
            
            iteraciones.append("\n"+i+"	   "+blancos(a+"")+blancos(b+"")+blancos(b-a+"")+blancos(s+"")+blancos(Leer.eval(f,a)+"")+"      "+blancos(Leer.eval(f,s)+""));
       
            error = (b-a)<0 ? (b-a)*-1 : b-a;
           
            if((Leer.eval(f,s)) == 0)
            {
                raiz.setText(s+"");
                break;
            }
            else if ((Leer.eval(f,a))*(Leer.eval(f,s))<0)
                b=s;
            else
                a=s;
						
            if(error < e)
            {
                raiz.setText(s+"");
                break;
            }
        }
        raiz.setText(s+"");
    }

    public String blancos (String exp)
    {
        int dif = 20-exp.length();
        for(int i=0; i<dif; i++)
          exp=exp+"  "; //Le agregue doble espacio para que se alinearán. 
        return exp;
    }
        
        
}