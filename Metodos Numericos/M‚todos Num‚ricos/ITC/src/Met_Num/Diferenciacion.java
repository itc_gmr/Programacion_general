package Met_Num;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Diferenciacion extends javax.swing.JDialog {

    public Diferenciacion() {
        initComponents();
        this.setLocationRelativeTo(null);
        bg1.add(limites);
        bg1.add(trapecio);
        bg1.add(simpson);
        bg2.add(porfuncion);
        bg2.add(portabla);
        String [] nomcolumnas = {"X","Y"};
        DefaultTableModel modelo = new DefaultTableModel(null, nomcolumnas);
        tabla.setModel(modelo);
        modelo.setColumnCount(2);
        funcion.setEnabled(false);
        intervalos.setEnabled(false);
        limiteinferior.setEnabled(false);
        limitesuperior.setEnabled(false);
        puntos.setEnabled(false);
        valordex.setEnabled(false);
        valordeh.setEnabled(false);
        error.setEnabled(false);
        tabla.setEnabled(false); 
        calcular.setEnabled(false);
        limpiar.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bg1 = new javax.swing.ButtonGroup();
        bg2 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        error = new javax.swing.JTextField();
        funcion = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        valordex = new javax.swing.JTextField();
        limitesuperior = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        limiteinferior = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        puntos = new javax.swing.JSpinner();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        valordeh = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        intervalos = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        resultado = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        limites = new javax.swing.JRadioButton();
        trapecio = new javax.swing.JRadioButton();
        simpson = new javax.swing.JRadioButton();
        porfuncion = new javax.swing.JRadioButton();
        portabla = new javax.swing.JRadioButton();
        calcular = new javax.swing.JButton();
        limpiar = new javax.swing.JButton();
        cerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Diferenciación E Integración Numérica");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setText("DIFERENCIACIÓN E INTEGRACIÓN NUMÉRICA");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Introduzca los siguientes datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N

        error.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                errorKeyTyped(evt);
            }
        });

        funcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                funcionKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel3.setText("Límite Inferior:");

        valordex.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                valordexKeyTyped(evt);
            }
        });

        limitesuperior.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                limitesuperiorKeyTyped(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel6.setText("Valor De X: ");

        limiteinferior.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                limiteinferiorKeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setText("Número De Puntos:");

        jLabel7.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel7.setText("Error Permitido:");

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setText("Límite Superior:");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("Función: ");

        puntos.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        puntos.setModel(new javax.swing.SpinnerNumberModel(0, 0, 10, 1));
        puntos.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                puntosStateChanged(evt);
            }
        });

        tabla.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jScrollPane2.setViewportView(tabla);

        jLabel8.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel8.setText("Valor De H: ");

        valordeh.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                valordehKeyTyped(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel9.setText("Número De Intervalos: ");

        intervalos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                intervalosKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(intervalos))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valordeh))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valordex))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(puntos))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(limitesuperior))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(limiteinferior))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(limiteinferior, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(limitesuperior, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(puntos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(intervalos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(valordex, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(valordeh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        resultado.setEditable(false);
        resultado.setColumns(20);
        resultado.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        resultado.setRows(5);
        jScrollPane1.setViewportView(resultado);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Selecciona un método", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N

        jSeparator1.setForeground(new java.awt.Color(255, 0, 51));

        limites.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        limites.setText("Método Por Límites");
        limites.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limitesActionPerformed(evt);
            }
        });

        trapecio.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        trapecio.setText("Método Del Trapecio");
        trapecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trapecioActionPerformed(evt);
            }
        });

        simpson.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        simpson.setText("Método De Simpson");
        simpson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpsonActionPerformed(evt);
            }
        });

        porfuncion.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        porfuncion.setText("Por Función");
        porfuncion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                porfuncionActionPerformed(evt);
            }
        });

        portabla.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        portabla.setText("Por Tabla");
        portabla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                portablaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(portabla)
                    .addComponent(simpson)
                    .addComponent(trapecio)
                    .addComponent(limites)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(porfuncion))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(limites)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(trapecio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(simpson)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(porfuncion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(portabla)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        calcular.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        calcular.setText("Calcular");
        calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcularActionPerformed(evt);
            }
        });

        limpiar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        limpiar.setText("Limpiar");
        limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiarActionPerformed(evt);
            }
        });

        cerrar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cerrar.setText("Cerrar");
        cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 688, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(calcular)
                                        .addGap(24, 24, 24)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel1)
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(calcular, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarActionPerformed
        dispose();
    }//GEN-LAST:event_cerrarActionPerformed

    private void limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limpiarActionPerformed
     
        int opcion = JOptionPane.showConfirmDialog(null, "¿Desea limpiar?", "Confirma", JOptionPane.YES_NO_CANCEL_OPTION);

        if(opcion == JOptionPane.YES_OPTION)
        {
            resultado.setText("");
            int n = Integer.parseInt(puntos.getValue().toString());
            String [] nomcolumnas = {"X", "Y"};
            DefaultTableModel modelo = new DefaultTableModel(null, nomcolumnas);
            tabla.setModel(modelo);
            modelo.setRowCount(n);
            funcion.setText("");
            intervalos.setText("");
            limiteinferior.setText("");
            limitesuperior.setText("");
            valordex.setText("");
            valordeh.setText("");
            error.setText("");
        }
    }//GEN-LAST:event_limpiarActionPerformed

    private void limitesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limitesActionPerformed
       
        if(limites.isSelected())
        {
            resultado.setText("");
            bg2.clearSelection();
         
            porfuncion.setEnabled(false);
            portabla.setEnabled(false);
            
            funcion.setEnabled(true);
            valordex.setEnabled(true);
            error.setEnabled(true);
            calcular.setEnabled(true);
            limpiar.setEnabled(true);
            
            limiteinferior.setEnabled(false);
            limitesuperior.setEnabled(false);
            puntos.setEnabled(false);
            intervalos.setEnabled(false);
            valordeh.setEnabled(false);
            tabla.setEnabled(false);
        }       
    }//GEN-LAST:event_limitesActionPerformed

    private void trapecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trapecioActionPerformed
     
        porfuncion.setEnabled(true);
        portabla.setEnabled(true);
        
       if(trapecio.isSelected())
        {           
            resultado.setText("");
            bg2.clearSelection();
             
            funcion.setEnabled(false);
            intervalos.setEnabled(false);
            limiteinferior.setEnabled(false);
            limitesuperior.setEnabled(false);
            puntos.setEnabled(false);
            valordex.setEnabled(false);
            valordeh.setEnabled(false);
            error.setEnabled(false);
            tabla.setEnabled(false);
            calcular.setEnabled(false);
            limpiar.setEnabled(false);
        }
    }//GEN-LAST:event_trapecioActionPerformed

    private void simpsonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpsonActionPerformed
        porfuncion.setEnabled(true);
        portabla.setEnabled(true);
        
       if(simpson.isSelected())
       {           
            resultado.setText("");
            bg2.clearSelection();
            
            funcion.setEnabled(false);
            intervalos.setEnabled(false);
            limiteinferior.setEnabled(false);
            limitesuperior.setEnabled(false);
            puntos.setEnabled(false);
            valordex.setEnabled(false);
            valordeh.setEnabled(false);
            error.setEnabled(false);
            tabla.setEnabled(false);  
            calcular.setEnabled(false);
            limpiar.setEnabled(false);
       }
    }//GEN-LAST:event_simpsonActionPerformed

    private void portablaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_portablaActionPerformed
        
        if((trapecio.isSelected() || simpson.isSelected()) && portabla.isSelected())
        {
            resultado.setText("");
            
            puntos.setEnabled(true);
            ((DefaultTableModel) this.tabla.getModel()).setRowCount(Integer.parseInt(puntos.getValue().toString()));
            tabla.setEnabled(true);
            valordeh.setEnabled(true);
            calcular.setEnabled(true);
            limpiar.setEnabled(true);
            
            funcion.setEnabled(false);
            limiteinferior.setEnabled(false);
            limitesuperior.setEnabled(false);
            intervalos.setEnabled(false);
            valordex.setEnabled(false);
            error.setEnabled(false);
        }  
    }//GEN-LAST:event_portablaActionPerformed

    private void puntosStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_puntosStateChanged
        ((DefaultTableModel) this.tabla.getModel()).setRowCount(Integer.parseInt(puntos.getValue().toString()));
    }//GEN-LAST:event_puntosStateChanged

    private void calcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcularActionPerformed
        
        if(limites.isSelected())
            Limites();
        else if(trapecio.isSelected() && porfuncion.isSelected())
            LeerDatos1("Trapecio");  
        else if(simpson.isSelected() && porfuncion.isSelected())
           LeerDatos1("Simpson"); 
        else if(trapecio.isSelected() && portabla.isSelected())
            LeerDatos2("Trapecio2");  
        else if(simpson.isSelected() && portabla.isSelected())
           LeerDatos2("Simpson2"); 
    }//GEN-LAST:event_calcularActionPerformed

    private void funcionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionKeyTyped

         //Validaciones.
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9')
            && evt.getKeyChar()!='*' && evt.getKeyChar()!='/'
            && evt.getKeyChar()!='+' && evt.getKeyChar()!='-'
            && evt.getKeyChar()!='^' && evt.getKeyChar()!='x'
            && evt.getKeyChar()!='e' && evt.getKeyChar()!='c'
            && evt.getKeyChar()!='o' && evt.getKeyChar()!='s'
            && evt.getKeyChar()!='i' && evt.getKeyChar()!='n'
            && evt.getKeyChar()!='l' && evt.getKeyChar()!='g'
            && evt.getKeyChar()!='t' && evt.getKeyChar()!='a'
            && evt.getKeyChar()!='p' && evt.getKeyChar()!='('
            && evt.getKeyChar()!=')' && evt.getKeyChar()!='.'
            && evt.getKeyChar()!=evt.VK_BACK_SPACE)
            evt.consume();
    }//GEN-LAST:event_funcionKeyTyped

    private void limiteinferiorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_limiteinferiorKeyTyped
 
        
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!='-') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && limiteinferior.getText().contains("."))
            evt.consume();
        
        if (evt.getKeyChar()=='-' && limiteinferior.getText().contains("-"))
            evt.consume();        // TODO add your handling code here:
    }//GEN-LAST:event_limiteinferiorKeyTyped

    private void limitesuperiorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_limitesuperiorKeyTyped
      
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!='-') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && limitesuperior.getText().contains("."))
            evt.consume();
        
        if (evt.getKeyChar()=='-' && limitesuperior.getText().contains("-"))
            evt.consume();    
    }//GEN-LAST:event_limitesuperiorKeyTyped

    private void intervalosKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_intervalosKeyTyped
       
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

    }//GEN-LAST:event_intervalosKeyTyped

    private void valordexKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_valordexKeyTyped
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();
    }//GEN-LAST:event_valordexKeyTyped

    private void valordehKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_valordehKeyTyped
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();
    }//GEN-LAST:event_valordehKeyTyped

    private void errorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_errorKeyTyped
         //Validaciones.
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && error.getText().contains("."))
            evt.consume();
    }//GEN-LAST:event_errorKeyTyped

    private void porfuncionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_porfuncionActionPerformed

        if((trapecio.isSelected() || simpson.isSelected())  && porfuncion.isSelected())
        {
            resultado.setText("");

            funcion.setEnabled(true);
            limiteinferior.setEnabled(true);
            limitesuperior.setEnabled(true);
            intervalos.setEnabled(true);
            calcular.setEnabled(true);
            limpiar.setEnabled(true);

            puntos.setEnabled(false);
            tabla.setEnabled(false);
            valordeh.setEnabled(false);
            valordex.setEnabled(false);
            error.setEnabled(false);
        }
    }//GEN-LAST:event_porfuncionActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Diferenciacion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bg1;
    private javax.swing.ButtonGroup bg2;
    private javax.swing.JButton calcular;
    private javax.swing.JButton cerrar;
    private javax.swing.JTextField error;
    private javax.swing.JTextField funcion;
    private javax.swing.JTextField intervalos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField limiteinferior;
    private javax.swing.JRadioButton limites;
    private javax.swing.JTextField limitesuperior;
    private javax.swing.JButton limpiar;
    private javax.swing.JRadioButton porfuncion;
    private javax.swing.JRadioButton portabla;
    private javax.swing.JSpinner puntos;
    private javax.swing.JTextArea resultado;
    private javax.swing.JRadioButton simpson;
    private javax.swing.JTable tabla;
    private javax.swing.JRadioButton trapecio;
    private javax.swing.JTextField valordeh;
    private javax.swing.JTextField valordex;
    // End of variables declaration//GEN-END:variables
    
    public void Limites()
    {
        try{
        String f = Leer.funcion(funcion.getText());
        double x = Double.parseDouble(valordex.getText());
        double e = Double.parseDouble(error.getText());
	
        resultado.setText("n                                h                                                                         k                                                                  x+h                                        f(x+h)                                               f(x)                                         f(x+h)-f(x)                                                 f'(x)                                                          e\n");
		
	int n = 1;
	double k, d , delta = 0, h;
	double da = 1 * Math.pow(10, 10);
		
	do
        {
            h = Math.pow(0.1, n);
            k=1 / h;
            d = (Leer.eval(f, x+h) - Leer.eval(f, x)) * k;
            delta = Math.abs(da - d);
            resultado.append("\n"+n+"                                "+Blancos(h+"")+"                                    "+Blancos(k+"")+"                             "+Blancos((x+h)+"")+"      "+ Blancos(Leer.eval(f, x+h)+"")+"                "+Blancos(Leer.eval(f, x)+"")+"        "+Blancos((Leer.eval(f, x+h)-Leer.eval(f, x))+"")+"                         "+Blancos((Leer.eval(f, x+h)-Leer.eval(f, x))*k+"")+"                          "+Blancos((da-d)+""));
            da = d;
            n++;
        }while(delta > e);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");

        }
    }
    
    public void LeerDatos1(String metodo)
    {
        try{
        String f = Leer.funcion(funcion.getText());
        double a = Double.parseDouble(limiteinferior.getText());
	double b = Double.parseDouble(limitesuperior.getText());
	int n = Integer.parseInt(intervalos.getText());
        
        if(metodo.equals("Trapecio"))
            Trapecio(f, a, b, n);
        if(metodo.equals("Simpson"))
            Simpson(f, a, b, n);
        }catch(Exception e){
         JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
    }
 
    public void Trapecio(String f, double a, double b, int n)
    {
        try{
        double x, area, sum1 = 0, sum2 = 0, sum3 = 0;
	double y[] = new double [n];
	double h = (b - a)/(n);
	double e;
	        
        resultado.setText("x                                                    y                                                        Resultado\n");
	    
        for(int j=0; j<=y.length; j++)
	{
            x = a;
	    e = Leer.eval(f, x);
	    
            if(j == 0 || j == y.length)
	    {
                resultado.append("\n"+x+"                  "+Blancos(e+"")+"                   "+Blancos(e+""));
	    	sum1 = sum1 + e;
	    }
	    else
            {
                resultado.append("\n"+x+"                  "+Blancos(e+"")+"                   "+Blancos(e*2+""));
	    	sum2 = sum2 + e*2;
            }
	    		
            x = x + h;
	    a = x;
	}
	    
	sum3 = sum1 + sum2;
	area = h / 2 * sum3;
        resultado.append("\n\nÁrea = "+area);
        }catch(Exception e){
        JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");

        }
    }
    
    public void Simpson(String f, double a, double b, int n)
    {
        try{
        double x, area, sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
        double y[] = new double [n];
        double h = (b - a) / (n);
        double e;
        
        resultado.setText("x                                                  y                                                Factor Multiplicador                            Resultado\n");
    
        for(int j=0; j<=y.length; j+=1)
        {
            x = a;
            e = Leer.eval(f, x);
            if(j == 0 || j == y.length)
            {
                resultado.append("\n"+x+"                "+Blancos(e+"")+"                       "+"1"+"                                               "+Blancos(e+""));
                 sum1 = sum1 + e;
            }
            else if(j % 2 == 1)
            {
                resultado.append("\n"+x+"                "+Blancos(e+"")+"                       "+"4"+"                                               "+Blancos(e*4+""));
                sum2 = sum2 + e * 4;
            }
            else
            {
                resultado.append("\n"+x+"                "+Blancos(e+"")+"                       "+"2"+"                                               "+Blancos(e*2+""));
                sum3 = sum3 + e * 2;
            }
            
            x = x + h;
            a = x;
        }

        sum4 = sum1 + sum2 + sum3;
        area = h / 3 * sum4;
        resultado.append("\n\nÁrea = "+area);
        }catch(Exception e){
         JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");

        }
    }
      
    public void LeerDatos2 (String metodo)
    {
       int N = Integer.parseInt(puntos.getValue().toString());
       double h = Double.parseDouble(valordeh.getText());
       
       if(metodo.equals("Trapecio2"))
          Trapecio2(N, h);
       if(metodo.equals("Simpson2"))
          Simpson2(N, h);
    }
    
    public void Trapecio2(int n, double h)
    {
        try{
        double y[] = new double [n];
	double x[] = new double [n];
        double sum1 = 0, sum2 = 0, sum3 = 0, area;
	int aux = 0;
        
        for(int i=0; i<n; i++)
	{
            x[i] = Double.parseDouble(tabla.getValueAt(i, aux++).toString());
            y[i] = Double.parseDouble(tabla.getValueAt(i, aux).toString());
            aux = 0;
	}
	
        resultado.setText(("x                                            y                                                Resultado\n"));
	
        for(int i=0; i<n; i++)
	{
            if(i == 0 || i == n - 1)
            {
                resultado.append("\n"+x[i]+"                                        "+Blancos(y[i]+"")+"                 "+Blancos(y[i]+""));
	    	 sum1 = sum1 + y[i];
            }
            else
            {
		resultado.append("\n"+x[i]+"                                        "+Blancos(y[i]+"")+"                 "+Blancos(y[i]*2+""));	
                sum2 = sum2 + y[i] * 2;
            }
	}
	
        sum3 = sum1 + sum2;
	area = h / 2 * sum3;
        resultado.append("\n\nÁrea = "+area);
        }catch(Exception e ){
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
            
        }
    }
    
    public void Simpson2(int n, double h)
    {
        try{
        double y[] = new double [n];
	double x[] = new double [n];
	double sum1 = 0, sum2 = 0, sum3 = 0, area, sum4 = 0;
        int aux = 0;

        for(int i=0; i<n; i++)
        {
            x[i] = Double.parseDouble(tabla.getValueAt(i, aux++).toString());
            y[i] = Double.parseDouble(tabla.getValueAt(i, aux).toString());
            aux = 0;
        }
       
        resultado.setText("x                                                    y                                                      Resultado\n");

        for(int i=0; i<n; i++)
        {
	
            if(i == 0 || i == n-1)
            {
                resultado.append("\n"+x[i]+"                                                "+Blancos(y[i]+"")+"                       "+Blancos(y[i]+""));
                sum1 = sum1 + y[i];
            }
            else if(i % 2 == 1)
            {
                resultado.append("\n"+x[i]+"                                                "+Blancos(y[i]+"")+"                       "+Blancos(y[i]*4+""));
                sum2 = sum2 + y[i] * 4;
            }
            else
            {
                resultado.append("\n"+x[i]+"                                                "+Blancos(y[i]+"")+"                       "+Blancos(y[i]*2+""));    
                sum3 = sum3 + y[i] * 2;
            }
        }
        
        sum4 = sum1 + sum2 + sum3;
        area = h / 3 * sum4;
        resultado.append("\n\nÁrea = "+area);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
            
        }
    }
    
    public String Blancos(String exp)
    {
        int dif = 20-exp.length();
        for(int i=0; i<dif; i++)
          exp=exp+"  "; //Le agregue doble espacio para que se alinearán. 
        return exp;
    }
}
