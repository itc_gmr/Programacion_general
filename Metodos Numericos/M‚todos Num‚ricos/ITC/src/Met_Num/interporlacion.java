
package Met_Num;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jfree.data.xy.Vector;

public class interporlacion extends javax.swing.JDialog {

    DefaultTableModel modelo = new DefaultTableModel();
    
    public interporlacion() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
         puntos.setEnabled(false);
         orden.setEnabled(false);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Tabla1 = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        metodos = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        valorx = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        orden = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        calcular = new javax.swing.JButton();
        limpiar = new javax.swing.JButton();
        cerrar = new javax.swing.JButton();
        puntos = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        resultado = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        desplegar = new javax.swing.JTextArea();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane4.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        Tabla1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "X", "Y"
            }
        ));
        jScrollPane1.setViewportView(Tabla1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        metodos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "MÉTODOS", "NEWTON", "LAGRANGE", "MINIMOS CUADRADOS" }));
        metodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                metodosActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setText("SELECCIONE UN MÉTODO");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("PUNTOS QUE VA A UTILIZAR");

        valorx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                valorxActionPerformed(evt);
            }
        });
        valorx.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                valorxKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("VALOR DE X A CALCULAR");

        orden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ordenKeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("ORDEN DE LA FUNSION");

        calcular.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        calcular.setText("Calcular");
        calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcularActionPerformed(evt);
            }
        });

        limpiar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        limpiar.setText("Limpiar");
        limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiarActionPerformed(evt);
            }
        });

        cerrar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cerrar.setText("Cerrar");
        cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarActionPerformed(evt);
            }
        });

        puntos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seleccione", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15" }));
        puntos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                puntosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 41, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(calcular, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(limpiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(valorx)
                    .addComponent(metodos, 0, 156, Short.MAX_VALUE)
                    .addComponent(orden)
                    .addComponent(puntos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(29, 29, 29))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(metodos, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(puntos, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valorx, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(orden, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(calcular, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(limpiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cerrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("METODOS DE INTERPOLACION LINEAL Y AJUSTE POLINOMIAL");

        jPanel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        resultado.setEditable(false);
        resultado.setColumns(20);
        resultado.setRows(5);
        jScrollPane2.setViewportView(resultado);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        desplegar.setEditable(false);
        desplegar.setColumns(20);
        desplegar.setRows(5);
        jScrollPane5.setViewportView(desplegar);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 590, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 32, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void valorxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_valorxActionPerformed

    }//GEN-LAST:event_valorxActionPerformed

    private void calcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcularActionPerformed
       int pun=metodos.getSelectedIndex();
       desplegar.setText("");
       resultado.setText("");
        
       if(pun == 0)
       {
           JOptionPane.showMessageDialog(null, "Seleccione un Método");
       }
       if(pun ==3 && orden.getText().isEmpty()){
           JOptionPane.showMessageDialog(null, "Defina el orden de la función");
       }
       if( valorx.getText().isEmpty()){
             JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos.");
             return;
        }
        
        if(pun == 1){
           internewton();
           return;
        }
       
         if(pun == 2){
        lagrange();
        return;
        }
         
         if(pun==3){
             MinCuadrados();
             return;
         }
     
        
    }//GEN-LAST:event_calcularActionPerformed

    private void puntosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_puntosActionPerformed
     
      Object Col [] = new Object [2];
      Col[0]="X";
      Col[1]= "Y";  
      if(puntos.getSelectedIndex()==0){
           modelo = new DefaultTableModel(Col, 0);
        Tabla1.setModel(modelo);
      }
      if(puntos.getSelectedIndex()!=0){
       int pun=Integer.parseInt((String) puntos.getSelectedItem());
    
        modelo = new DefaultTableModel(Col, pun);
        Tabla1.setModel(modelo);
      }
    }//GEN-LAST:event_puntosActionPerformed

    private void valorxKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_valorxKeyTyped
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='-') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && valorx.getText().contains("."))
            evt.consume();
        
         if (evt.getKeyChar()=='-' && valorx.getText().contains("-"))
            evt.consume();
    }//GEN-LAST:event_valorxKeyTyped

    private void ordenKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ordenKeyTyped
         if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9'))
            evt.consume();
    }//GEN-LAST:event_ordenKeyTyped

    private void metodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_metodosActionPerformed
       int metodo = metodos.getSelectedIndex();
       if(metodo==0){
           puntos.setEnabled(false);
           puntos.setSelectedIndex(0);
           orden.setEnabled(false);
           orden.setText("");
       }
       if(metodo==1){
           puntos.setEnabled(true);
           orden.setEnabled(false);
           orden.setText("");
       }
       
       if(metodo==2){
           puntos.setEnabled(true);
           orden.setEnabled(false);
           orden.setText("");
       }
       if(metodo==3){
           puntos.setEnabled(true);
           orden.setEnabled(true);
       }
       
       
    }//GEN-LAST:event_metodosActionPerformed

    private void cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarActionPerformed
       this.dispose();
    }//GEN-LAST:event_cerrarActionPerformed

    private void limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limpiarActionPerformed
        valorx.setText("");
        orden.setText("");
        resultado.setText("");
        desplegar.setText("");
        puntos.setSelectedIndex(0);
        metodos.setSelectedIndex(0);
        
         Object Col [] = new Object [2];
         Col[0]="X";
         Col[1]= "Y";  
         modelo = new DefaultTableModel(Col, 0);
         Tabla1.setModel(modelo);
        
    }//GEN-LAST:event_limpiarActionPerformed

  
    public static void main(String args[]) {
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new interporlacion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTable Tabla1;
    private javax.swing.JButton calcular;
    private javax.swing.JButton cerrar;
    public static javax.swing.JTextArea desplegar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JButton limpiar;
    private javax.swing.JComboBox metodos;
    public static javax.swing.JTextField orden;
    public static javax.swing.JComboBox puntos;
    public static javax.swing.JTextArea resultado;
    public static javax.swing.JTextField valorx;
    // End of variables declaration//GEN-END:variables

    
    public static void internewton(){
		
    try{
    double xi[], yi[][];
		int m=Integer.parseInt((String) puntos.getSelectedItem());
				
		xi=new double [m];
		yi=new double [m][m];
		for(int i=0; i<m;i++){
                    xi[i]=Double.parseDouble(String.valueOf(Tabla1.getValueAt(i, 0)));
                    for(int j=0; j<2;j++){
                        if(j==1)
                        yi[i][0]=Double.parseDouble(String.valueOf(Tabla1.getValueAt(i, j)));
                    }
                 }
                
		for(int j=1;j<m-1;j++)
			for(int i=0;i<m-j;i++)
			{
				yi[i][j]=yi[i+1][j-1]-yi[i][j-1];
			}
			
	//AQUI DESPLEGAMOS LAS COLUMNAS DEL CUADRO DE DIFERENCIAS FINITAS
		for(int i=0;i<m+1;i++){
			if(i==0){
			desplegar.append(blancos("x"));
			continue;
			}
			if(i==1){
			desplegar.append(blancos("y"));
			continue;
			}
			
			desplegar.append(blancos("D^"+(i-1)+"y"));
		}
		
		desplegar.append("\n");
                String cadena="";
		for(int i=0;i<m;i++){
                        cadena=blancos(xi[i]+"");
			desplegar.append(cadena);
			for(int j=0;j<m;j++)
			{       cadena=blancos(yi[i][j]+"");
				desplegar.append(cadena);
			}
				
			desplegar.append("\n");
		}
		
		
		
		double x=Double.parseDouble( valorx.getText());
		
		double k=((x-xi[0])/(xi[2]-xi[1]));
		double y=0;
		
		for(int i=1;i<m;i++){
			double num=1;
			double j=0;
			while(j<=(i-2)){
				num=num*(k-j);
				j++;
			}
			y=y+num/factorial(i-1) * yi[0][i-1];
		}
		
		resultado.append("El valor de y\n para x="+x+" \nes: "+y);
    }catch(Exception e){
          JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
 
    }
    
	}



	public static void lagrange(){
		
            try{
            
            double xi[], yi[];
		int m=Integer.parseInt((String) puntos.getSelectedItem());
		xi=new double [m];
		yi=new double [m];
		
		for(int i=0; i<m;i++){
			
			xi[i]=Double.parseDouble(String.valueOf(Tabla1.getValueAt(i, 0)));
			
			yi[i]=Double.parseDouble(String.valueOf(Tabla1.getValueAt(i, 1)));
		}
		
		double x=Double.parseDouble( valorx.getText());
		double y=0;
		
		for(int i=0;i<m;i++){
			double num=1;
			double den=1;
			for(int j=0; j<m;j++){
				if(i!=j){
					num=num*(x-xi[j]);
					den=den*(xi[i]-xi[j]);
				}
			}
			y=y+(num/den) * yi[i];
		}
		resultado.append("PARA EL VALOR DE X="+x+"\n EL VALOR DE Y ES IGUAL A: "+y);
		
            } catch (Exception e){
                     JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
             
                        }
	}
	  public static double factorial(double number) {
	        if (number <= 1)
	            return 1;
	        else
	            return number * factorial(number - 1);
	    }

	  public static void MinCuadrados(){
		
              try{
              desplegar.append("*****MINIMOS CUADRADOS*****");
		  desplegar.append("\n");
		 double [] a, b,x,y;
		  
		  
		 
		  int n = Integer.parseInt((String) puntos.getSelectedItem());
		 
		  int m=Integer.parseInt(orden.getText());
		  
                  double Mat[][]=new double [n][(m*2)+1+m];
                 
		  a=new double[(m*2)+1];
		  b=new double [m+1];
		  x=new double [n];
		  y=new double [n];
		  a[0]=n;
		  b[0]=0;
		  
		  for(int i=0;i<n;i++ )
		  {
			  x[i]=Double.parseDouble(String.valueOf(Tabla1.getValueAt(i, 0)));
			 Mat[i][0]=x[i];
			  y[i]=Double.parseDouble(String.valueOf(Tabla1.getValueAt(i, 1)));
			  Mat[i][1]=y[i];
			  b[0]=b[0]+y[i];
			  
			  for(int j=1; j<=(m*2);j++){
				a[j]= a[j]+ Math.pow(x[i], j);
				if(j!=1)
				Mat[i][j]=Math.pow(x[i], j);
			  }
			 for(int j=1;j<=m;j++){
				 b[j]=b[j] + y[i]*Math.pow(x[i], j);
				 if(j==1){
                                    Mat[i][m*2+1]=y[i]*Math.pow(x[i], j);
				 }
				 if(j==2)
				 {
                                    Mat[i][m*2+2]=y[i]*Math.pow(x[i], j);
				 }
                                 if(j==3){
                                    Mat[i][m*2+3]=y[i]*Math.pow(x[i], j);  
                                 }
			 }
			  
		  }
		  
		  int cont=0;
		  for(int i=0;i<((m*2)+m);i++){
			  
			  if(i==1)
				 desplegar.append(blancos("y"+""));
			  if(i<m*2)
			  desplegar.append(blancos("x^"+(i+1)+""));
			  if(i>m*2-1){
				  cont++;
				  desplegar.append(blancos("x^"+cont+"y"+""));
			  }
			  
		  }
                          
		 desplegar.append("\n");
		  
		  
		  for(int i=0;i<n;i++){
			  for(int j=0;j<((m*2)+1+m);j++){
                             desplegar.append(blancos(Mat[i][j]+""));
			  }
			 desplegar.append("\n");		
		}
		  
		  
		  
		  
		  double c[][]=new double [m+1][m+2];
		  for(int i=1; i<=m+1; i++)
		  {
			  for(int j=1;j<=m+1;j++){
				  c[i-1][j-1]=a[i+j-2];
			  }
			  c[i-1][m+1]=b[i-1];
		  }
		  desplegar.append("SISTEMA RESULTANTE DE LAS OPERACIONES ES:");
                  desplegar.append("\n");
		  for(int i=0;i<m+1;i++){
			  for(int j=0; j<m+2;j++){
				desplegar.append(blancos(c[i][j]+"a"+(j+1)+"")); 
			  }
			desplegar.append("\n");
		  }
		  double [] res=NewCuadrados(c,m);
			
		  
		 desplegar.append("\n**RESULTADOS**");
                 desplegar.append("\n");
		  for(int i=0; i<m+1;i++){
			  desplegar.append("a["+(i+1)+"] ="+res[i]);
                           desplegar.append("\n");
		  }
		  
		 desplegar.append("La Funcion f(x)= ");
                  int cn=m;
                   String Funcion="";
                  for(int i=m+1;i>0;i--){
                      if(i>1){
                      desplegar.append(res[i-1]+"x^"+cn);
                      Funcion=Funcion+res[i-1]+"x^"+cn;
                      desplegar.append("+");
                      Funcion=Funcion+"+";
                      }else
                      {desplegar.append(res[i-1]+"");
                      Funcion=Funcion+res[i-1];
                    
                      }
                      cn--;
                  }
		 
		  Leer.funcion(Funcion);
                  double Val=Double.parseDouble( valorx.getText());
		  resultado.append("EL VALOR DE  f("+Val+")= "+Leer.eval(Funcion, Val));
              }catch(Exception e){
          JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");

              }
		  
	  }
	  
	  public static double [] NewCuadrados(double [][] Matriz, int m){
		  double resultado[]=new double [m+1];
		  double pivote, cero;
			
		  for(int i=0;i<Matriz.length;i++)//AQUI HACE LOS CALCULOS
		    {
				 pivote=Matriz[i][i];
		    	for(int j=0;j<Matriz[i].length;j++)
		    	{
		    		Matriz[i][j]=Matriz[i][j]/pivote;
		    		
		    	}
		    	for(int k=0;k<Matriz.length;k++)
		    	{
		    		if(k!=i)
		    		{
		    			 cero= Matriz[k][i];
		    		
		    		for(int j=0; j<Matriz[i].length; j++)
		    		{
		    			
		    			Matriz[k][j]=Matriz[k][j]-cero*Matriz[i][j];
		    		}
		    	    }
		    	}
		    
		    }
			
			for(int i=0; i<m+1;i++){
				
				resultado[i]= Matriz[i][m+1];
			}
		 
		  return resultado;
	  }
    
          public static String blancos (String exp)
	{
		int dif=20-exp.length();
		for(int i=0; i<dif; i++)
			exp=exp+" ";
		return exp;
	}
    

}
