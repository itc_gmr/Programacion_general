package Met_Num;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class SistemasDeEcuaciones extends javax.swing.JDialog {

    public SistemasDeEcuaciones() {
        initComponents();
        this.setLocationRelativeTo(null);
        DefaultTableModel modelo = new DefaultTableModel(); //Creamos un modelo para la tabla.
        matriz.setModel(modelo); //Asignamos el modelo a la tabla. 
        modelo.setRowCount(2); //Asignamos el no. de filas por defecto.
        modelo.setColumnCount(3); //Asignamos el no. de columnas por defecto.
        tamaño.setEnabled(false);
        matriz.setEnabled(false);
        calcular.setEnabled(false);
        limpiar.setEnabled(false); 
        numitera.setEnabled(false);
        error.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        calcular = new javax.swing.JButton();
        opcion = new javax.swing.JComboBox();
        cerrar = new javax.swing.JButton();
        limpiar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        matriz = new javax.swing.JTable();
        tamaño = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        resultado = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        numitera = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        error = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Solución De Sistemas De Ecuaciones");
        setPreferredSize(new java.awt.Dimension(775, 765));
        setResizable(false);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel6.setText("SOLUCIÓN DE SISTEMAS DE ECUACIONES");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel1.setText("Selecciona el tamaño de la matriz:");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Selecciona un método", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N

        calcular.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        calcular.setText("Calcular");
        calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcularActionPerformed(evt);
            }
        });

        opcion.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        opcion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecciona una opción", "Método De Gauss", "Método De Gauss-Jordan", "Método De Montante", "Metodo De Cramer", "Método De Jacobi", "Método De Gauss-Seidel" }));
        opcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionActionPerformed(evt);
            }
        });

        cerrar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cerrar.setText("Cerrar");
        cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarActionPerformed(evt);
            }
        });

        limpiar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        limpiar.setText("Limpiar");
        limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(calcular, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(opcion, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 20, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(opcion, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(calcular, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(cerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        matriz.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jScrollPane2.setViewportView(matriz);

        tamaño.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        tamaño.setModel(new javax.swing.SpinnerNumberModel(2, 2, 10, 1));
        tamaño.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                tamañoStateChanged(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel3.setText("Teclea los valores del sistema de ecuaciones: ");

        resultado.setEditable(false);
        resultado.setColumns(20);
        resultado.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        resultado.setRows(5);
        jScrollPane1.setViewportView(resultado);

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("Número De Iteraciones: ");

        numitera.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                numiteraKeyTyped(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setText("Error Permitido:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 706, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(170, 170, 170)
                            .addComponent(jLabel6))
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(32, 32, 32)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel1)
                                            .addGap(10, 10, 10)
                                            .addComponent(tamaño, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel2)
                                            .addGap(4, 4, 4)
                                            .addComponent(numitera, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel4)
                                            .addGap(18, 18, 18)
                                            .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(125, 125, 125))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel3)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(18, 18, 18)))
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(32, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel6)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(201, 201, 201))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel1))
                            .addComponent(tamaño, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLabel2))
                            .addComponent(numitera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLabel4))
                            .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(13, 13, 13)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tamañoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_tamañoStateChanged
       
        int n = Integer.parseInt(tamaño.getValue().toString()); //Obtengo el número del JSpinner. 
        ((DefaultTableModel) this.matriz.getModel()).setRowCount(n); //Obtengo el modelo de la tabla matriz, casteamos y asiganmos el no. de filas.
        ((DefaultTableModel) this.matriz.getModel()).setColumnCount(n+1); //Obtengo el modelo de la tabla matriz, casteamos y asiganmos el no. de columnas.
    }//GEN-LAST:event_tamañoStateChanged

    private void cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarActionPerformed
       
        dispose();
    }//GEN-LAST:event_cerrarActionPerformed

    private void calcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcularActionPerformed
        
        int indice = opcion.getSelectedIndex();
        
        if(indice == 1) 
            LeerDatosTabla("Gauss");
        else if (indice == 2)
            LeerDatosTabla("GaussJordan");
        else if (indice == 3)
            LeerDatosTabla("Montante");
        else if (indice == 4)
            Cramer();
        else if (indice == 5)
            Jacobi();
        else if (indice == 6)
            GaussSeidel();
    }//GEN-LAST:event_calcularActionPerformed

    private void opcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionActionPerformed
        
        int indice = opcion.getSelectedIndex();
        
        if(indice !=0 )
        {
            tamaño.setEnabled(true);
            matriz.setEnabled(true);
            calcular.setEnabled(true);
            limpiar.setEnabled(true);
            numitera.setEnabled(false);
            error.setEnabled(false);
            
            if(indice == 5 || indice == 6)
            {
                numitera.setEnabled(true);
                error.setEnabled(true);
            }
        }
        else
        {
            tamaño.setEnabled(false);
            matriz.setEnabled(false);
            calcular.setEnabled(false);
            limpiar.setEnabled(false);
            numitera.setEnabled(false);
            error.setEnabled(false);
        }     
    }//GEN-LAST:event_opcionActionPerformed

    private void limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limpiarActionPerformed
        
        int opcion = JOptionPane.showConfirmDialog(null, "¿Desea limpiar?", "Confirma", JOptionPane.YES_NO_CANCEL_OPTION);
        int n = Integer.parseInt(tamaño.getValue().toString());
        
        if(opcion == JOptionPane.YES_OPTION)
        {
            resultado.setText("");
            matriz.setModel(new DefaultTableModel(n,n+1));
            numitera.setText("");
            error.setText("");
        }
    }//GEN-LAST:event_limpiarActionPerformed

    private void numiteraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numiteraKeyTyped
         if ((evt.getKeyChar() < '0' || evt.getKeyChar() > '9') && (evt.getKeyChar()!= evt.VK_BACK_SPACE))
            evt.consume();
    }//GEN-LAST:event_numiteraKeyTyped

    public static void main(String args[]) {
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SistemasDeEcuaciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton calcular;
    private javax.swing.JButton cerrar;
    private javax.swing.JTextField error;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton limpiar;
    private javax.swing.JTable matriz;
    private javax.swing.JTextField numitera;
    private javax.swing.JComboBox opcion;
    private javax.swing.JTextArea resultado;
    private javax.swing.JSpinner tamaño;
    // End of variables declaration//GEN-END:variables

    public void LeerDatosTabla(String metodo)
    {
        int n = Integer.parseInt(tamaño.getValue().toString());
        double Matriz [][] = new double [n][n+1];
        double Aux [][]= new double [n][n+1];
        
        try
        {
            for(int i=0; i<matriz.getRowCount(); i++) //Recorrido de las filas de la tabla.
                for(int j=0; j<matriz.getColumnCount(); j++) //Recorrido de las columnas de la tabla. 
                {
                    Matriz[i][j] = Double.parseDouble(matriz.getValueAt(i, j).toString());
                    Aux[i][j] = Matriz[i][j];                    	 
                }
            
            if(metodo.equals("Gauss"))
            {
                resultado.setText("");
                Gauss(Matriz, Aux, n);
            }
               
            else if(metodo.equals("GaussJordan"))
            {
                resultado.setText(""); 
                GaussJordan(Matriz, Aux, n);
            }
               
            else if(metodo.equals("Montante"))
            {
                 resultado.setText("");
                 Montante(Matriz, Aux, n);
            }     
        }
        catch(Exception e)
        {
             resultado.setText("");
            JOptionPane.showMessageDialog(null, "Ocurrió un error, revise la tabla de valores. Es posible que existan celdas vacías\n o los valores introducidos no sean válidos.");
        }
    }

    public void Gauss(double Matriz[][], double Aux[][], int n)
    {
        double m = 0;
		
	for(int k=0; k<n-1; k++)
	{
            for(int i=k+1; i<n; i++)
            {
		m = Matriz[i][k] / Matriz[k][k];
		for(int j=k; j<n+1; j++)
                    Matriz[i][j] = (Matriz[i][j]) - (m * Matriz[k][j]);
            }
	}
	
        double x [] = new double [n];
	int o = n-1;
	
        x[o] = (Matriz[o][o+1] / Matriz[o][o]);
    
        double suma;	
		
        for(int i=o-1; i>=0; i--)
        {
            suma = 0;
            
            for(int j=i+1; j<=o; j++)
                suma = suma + (Matriz[i][j] * x[j]);
			
            x[i] = (Matriz[i][o+1] - suma) / Matriz[i][i];
        }
        
        //Desplegamos los resultados.
        resultado.append("SISTEMA DE ECUACIONES ORIGINAL\n");
	
        int conta=0;
	
        for(int i=0; i < Aux.length; i++) 
        {
            for(int j=0; j < Aux[i].length; j++) 
            {
                resultado.append(Aux[i][j]+"\t");
		conta++;
		
                if(conta == n+1)
                {
                    resultado.append("\n");
		    conta=0;
		}
            }
        }
		
	resultado.append("\n");
	resultado.append("MATRIZ TRIANGULAR SUPERIOR\n");
	
        int conta2=0;
	
        for(int i=0; i < Matriz.length; i++) 
        {
            for(int j=0; j < Matriz[i].length; j++) 
            {
                resultado.append( Matriz[i][j]+"\t");
                conta2++;

                if(conta2 == n+1)
                {
                    resultado.append("\n");
                    conta2=0;
                }
            }
        }		
		
        resultado.append("\n");
        resultado.append("SOLUCIONES\n");
		
        for(int i=0; i<x.length; i++)
            resultado.append("X"+(i+1)+" = "+x[i]+"\n");
    }

    public void GaussJordan(double Matriz[][], double Aux[][], int n)
    {
        double pivote, cero=0;
        
        for(int i=0; i<Matriz.length; i++)
	{
	    pivote=Matriz[i][i];
	    	
            for(int j=0; j<Matriz[i].length; j++)
                Matriz[i][j] = Matriz[i][j] / pivote;
	    		
	    for(int k=0; k<Matriz.length; k++)
	    {
	    	if(k!=i)
	    	{
                    cero = Matriz[k][i];
                    
                    for(int j=0; j<Matriz[i].length; j++)	
	    		Matriz[k][j]=Matriz[k][j]-cero*Matriz[i][j];
	    	}
	    }
	}    
    
        //Desplegamos los resultados.
	resultado.append("SISTEMA DE ECUACIONES ORIGINAL\n");
	
        int conta=0;
	
        for(int i=0; i < Aux.length; i++) 
        {
            for (int j=0; j < Aux[i].length; j++) 
            {
                resultado.append( Aux[i][j]+"\t");
                conta++;
		
                if(conta==n+1)
                {
                    resultado.append("\n");
                    conta=0;
                }
            }
	}
		
        resultado.append("\n");
        resultado.append("MATRIZ IDENTIDAD\n");
	
        for(int i=0; i<Matriz.length; i++)
        {
            for(int j=0; j<Matriz[i].length; j++)
            resultado.append(Matriz[i][j]+"\t");
		
          resultado.append("\n");
	}
		
        resultado.append("\n");
	resultado.append("SOLUCIONES\n");
	
        for(int i=0; i<Matriz.length; i++)
            resultado.append("X"+(i+1)+" = "+Matriz[i][Matriz.length]+"\n");
    }

    public void Montante(double Matriz[][], double Aux[][], int n)
    {
        double pivoteant=1;
		
	for(int k=0; k<n; k++)
	{
            for(int i=0; i<n; i++)
            {
		if(i != k)
                {
                    for(int j=n; j>=0; j--)
                    	Matriz[i][j]=(Matriz[k][k]*Matriz[i][j]-Matriz[i][k]*Matriz[k][j])/pivoteant;  
		}
            }
	  pivoteant=Matriz[k][k];
	}
		
	double determinante = Matriz[0][0];
		
	for(int i=0; i<n; i++)
        {
            for(int j=0; j<n; j++)
            {
		if(i == j)
                    Matriz[i][n] = Matriz[i][n] / determinante;
            }
	}
    
        //Desplegamos los resultados.
        resultado.setText("SISTEMA DE ECUACIONES ORIGINAL\n");
		
        int conta=0;
		
        for (int i=0; i < Aux.length; i++) 
        {
            for (int j=0; j < Aux[i].length; j++) 
            {
                resultado.append( Aux[i][j]+"\t");
                conta++;
				  
                if(conta == n+1)
                {
                    resultado.append("\n");
                    conta=0;
                }
            }
        }
		
        resultado.append("\n");
        resultado.append("MATRIZ RESULTANTE\n");
		
        int conta2=0;
		
        for (int i=0; i < Matriz.length; i++)
        {
            for (int j=0; j < Matriz[i].length; j++) 
            {
		resultado.append(Matriz[i][j]+"\t");
		conta2++;
				  
                if(conta2 == n+1)
                {
                    resultado.append("\n");
                    conta2=0;
                }
            }
        }		
        
        resultado.append("\n");
        resultado.append("SOLUCIONES\n");
		
        for(int i=0; i<n; i++)
            resultado.append("X"+(i+1)+" = "+Matriz[i][n]+"\n");    
    }

    public void Cramer() 
    {
        int n = Integer.parseInt(tamaño.getValue().toString());
        double Matriz [][] = new double [n][n];;
	double Aux [][] = new double [n][n];
        double b [] = new double [n];
        
        try
        {
            //Leer la matriz sin los términos independientes. 
            for(int i=0; i<matriz.getRowCount(); i++) 
                for(int j=0; j<matriz.getColumnCount()-1; j++) 
                    Matriz[i][j] = Double.parseDouble(matriz.getValueAt(i, j).toString());
            
            //Leer los términos independientes del sistema. 
            for(int i=0; i<b.length; i++)
                b[i] = Double.parseDouble(matriz.getValueAt(i, n).toString());
        }
        catch(Exception e)
        {
          JOptionPane.showMessageDialog(null, "Ocurrió un error, revise la tabla de valores. Es posible que existan celdas vacías\n o los valores introducidos no sean válidos.");  
        }

        Duplica(Matriz,Aux);
	double det = Determinante(Matriz);
	double x[] = new double [n];
        
        if(det == 0)
        {
             
            resultado.setText("Sistema sin solución");
            return;
        }
        else
            Duplica(Aux,Matriz);
        
        double detx;
        
        for(int j=0; j<n; j++)
        {
            for(int i=0; i<n; i++)
                Aux[i][j] = b[i];
	  
          detx = Determinante(Aux);
          Duplica(Matriz,Aux);
	  x[j] = detx / det;
	} 
    
        //Desplegamos los resultados.
        resultado.setText("MATRIZ DEL SISTEMA");
		 
        for(int i=0; i<Matriz.length; i++)
        {
            resultado.append("\n");
            
            for(int j=0; j<Matriz[i].length; j++)
                resultado.append(Matriz[i][j]+"\t");
	}
		 
        resultado.append("\n\nDETERMINANTE DEL SISTEMA\n");
        resultado.append(det+"");
		 
	resultado.append("\n\nSOLUCIONES\n");
		 
        for(int i=0; i<n; i++)
            resultado.append("X"+(i+1)+" = "+x[i]+"\n");	
    }
        
    public void Jacobi()
    {
        int  m = Integer.parseInt(tamaño.getValue().toString());
	double Matriz [][] = new double [m][m+1];
        double MatrizAux [][] = new double [m][m];
	double x [] = new double [m];
        double e = Double.parseDouble(error.getText());
	int iter = Integer.parseInt(numitera.getText());
        int cont = 1;
        boolean fin;
	double y[] = new double [m];
        
        for(int i=0; i<m; i++)
            x[i]=0;
		
        for (int i=0; i<matriz.getRowCount(); i++) 
        {
            for (int j=0; j<matriz.getColumnCount(); j++) 
            {
		Matriz[i][j] = Double.parseDouble(matriz.getValueAt(i, j).toString());
		
                if(j != m)
                    MatrizAux[i][j] = Matriz[i][j];		  
            }
	}
	
        double det = Determinante(Matriz);
	
	if(det ==0 )
        {
            resultado.setText("Sistema sin solución");
            return;
        }
        else
        {
            resultado.setText("I                       x1                                           x2                                                 x3                                               X1                                                 X2                                                    X3\n");
            
            do
            {
                fin = true;
			
                for(int i=0; i<m; i++)
                {
                    y[i] = Matriz[i][m];
				
                    for(int j=0; j<m; j++)
                    {
			if(i != j)
                            y[i] = y[i] - Matriz[i][j] * x[j];
                    } 
				
                    y[i] = y[i] / Matriz[i][i];				
			
                    double delta = Math.abs(x[i] - y[i]);
				
                    if(delta > e)
                        fin = false;
		}
			
                if(cont == 1 )
                   resultado.append("\n"+cont+"                      "+Blancos(x[0]+"")+"        "+Blancos(x[1]+"")+"             "+Blancos(x[2]+"")+"           "+Blancos(y[0]+"")+"              "+Blancos(y[1]+"")+"                  "+Blancos(y[2]+""));
		
                if(cont != 1)
                   resultado.append("\n"+cont+"                      "+Blancos(x[0]+"")+"        "+Blancos(x[1]+"")+"             "+Blancos(x[2]+"")+"           "+Blancos(y[0]+"")+"              "+Blancos(y[1]+"")+"                  "+Blancos(y[2]+"")); 
				
		for(int i=0; i<m; i++)
                    x[i] = y[i];
			
		if(cont == iter)
                    fin = true;
			
		cont++;
            
            }while(!fin);
		
		resultado.append("\n\nSOLUCIONES\n");
		
                for(int i=0; i<m; i++)
                    resultado.append("X"+(i+1)+"= "+x[i]+"\n");
        }
    }
    
    public void GaussSeidel()
    {
        int m = Integer.parseInt(tamaño.getValue().toString());
	double Matriz [][] = new double [m][m+1];
	double MatrizAux [][] = new double [m][m];
	double x []= new double [m];
        double e = Double.parseDouble(error.getText());
	double iter = Integer.parseInt(numitera.getText());
		
	for(int i=0; i<matriz.getRowCount(); i++) 
        {
            for(int j=0; j<matriz.getColumnCount(); j++) 
            {
                Matriz[i][j] = Double.parseDouble(matriz.getValueAt(i, j).toString());
		
                if(j != 3)
                    MatrizAux[i][j] = Matriz[i][j];
            }
	}
		
	double det = Determinante(MatrizAux);
	
        if(det == 0)
        {
            resultado.setText("Sistema sin solución");
            return;
	}
        else
        {
            for(int i=0; i<m; i++)
                x[i] = 0;
	
            resultado.setText("I                       x1                                           x2                                                 x3                                               X1                                                 X2                                                    X3\n");
            
            boolean fin;
            double y;
            double aux[] = new double[m];
            int cont=1;
            double delta;
		
            do
            {
		fin = true;
			
                for(int i=0; i<m; i++)
                {
                    y = Matriz[i][m];
				
                    for(int j=0; j<m; j++)
                    {
			if(i != j)
                            y = y - Matriz[i][j] * x[j];
                    }
                      
                      y=y/Matriz[i][i];				
				
                      delta = Math.abs(x[i]-y);
				
                      if(delta > e)
                        fin = false;
                      
                      x[i]=y;	
                }
	
		if(cont == iter)
                    fin = true;
			
		if(cont == 1)
                    resultado.append("\n"+cont+"                      "+Blancos(0+"")+"        "+Blancos(0+"")+"             "+Blancos(0+"")+"           "+Blancos(0+"")+"              "+Blancos(x[1]+"")+"                  "+Blancos(x[2]+""));		
                
		if(cont != 1)
                    resultado.append("\n"+cont+"                      "+Blancos(aux[0]+"")+"        "+Blancos(aux[1]+"")+"             "+Blancos(aux[2]+"")+"           "+Blancos(x[0]+"")+"              "+Blancos(x[1]+"")+"                  "+Blancos(x[2]+""));					
                    
                for(int i=0; i<m; i++)
                    aux[i] = x[i];
				
		cont++;
			
            }while(!fin);
		
            resultado.append("\n\nSOLUCIONES\n");
		
            for(int i=0;i<m;i++)
                resultado.append("X"+(i+1)+"= "+x[i]+"\n");
        }
    }
    
    public void Duplica(double orig [][], double cop[][])
    {
	for(int i=0; i < orig.length; i++) 
        {
            for (int j=0; j < orig[i].length; j++) 
                cop[i][j] = orig[i][j];
	}
    }
	
    public double Determinante(double[][] matriz)
    {
	double det;
	    
        if(matriz.length == 2)
	{
	    det=(matriz[0][0] * matriz[1][1]) - (matriz[1][0] * matriz[0][1]);
	    return det;
	}
	    
        double suma=0;
	
        for(int i=0; i<matriz.length; i++)
        {
	    double[][] nm = new double[matriz.length-1][matriz.length-1];
	        
            for(int j=0; j<matriz.length; j++)
            {
                if(j != i)
                {
                    for(int k=1; k<matriz.length; k++)
                    {
	                int indice=-1;
	                
                        if( j< i)
                            indice=j;
	                else if (j > i)
                            indice = j - 1;
	                
                        nm[indice][k-1] = matriz[j][k];
	            }
	        }
	    }
	        
            if( i % 2 == 0)
	        suma += matriz[i][0] * Determinante(nm);
	    else
	        suma-=matriz[i][0] * Determinante(nm);
	}
        
        return suma;
    }

 public String Blancos(String exp)
    {
        int dif = 20-exp.length();
        for(int i=0; i<dif; i++)
          exp=exp+"  "; //Le agregue doble espacio para que se alinearán. 
        return exp;
    }
}

