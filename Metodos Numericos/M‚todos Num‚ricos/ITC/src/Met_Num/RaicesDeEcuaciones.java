package Met_Num;

import javax.swing.JOptionPane;

public class RaicesDeEcuaciones extends javax.swing.JDialog {
    
    public RaicesDeEcuaciones() {
        initComponents();
        setLocationRelativeTo(null);
        //Para que al abrir la ventana todos los componentes se muestren deshabilitados. 
        funcion.setEnabled(false);
        intervaloA.setEnabled(false);
        intervaloB.setEnabled(false);
        numitera.setEnabled(false);
        error.setEnabled(false);
        derivada1.setEnabled(false);
        derivada2.setEnabled(false);
        valordepartida.setEnabled(false);
        grafica.setEnabled(false); 
        calcular.setEnabled(false);
        limpiar.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        intervaloA = new javax.swing.JTextField();
        error = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        numitera = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        funcion = new javax.swing.JTextField();
        intervaloB = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        derivada1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        derivada2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        valordepartida = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        raiz = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        opcion = new javax.swing.JComboBox();
        calcular = new javax.swing.JButton();
        limpiar = new javax.swing.JButton();
        grafica = new javax.swing.JButton();
        cerrar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        iteraciones = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Solución De Raíces De Ecuaciones");
        setResizable(false);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel6.setText("SOLUCIÓN DE RAÍCES DE ECUACIONES");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Introduzca los siguientes datos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setText("Función:");

        jLabel7.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel7.setText("Intervalo A:");

        intervaloA.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                intervaloAKeyTyped(evt);
            }
        });

        error.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                errorKeyTyped(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel8.setText("Intervalo B:");

        jLabel9.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel9.setText("Error Permitido:");

        numitera.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                numiteraKeyTyped(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel10.setText("Número De Iteraciones:");

        funcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                funcionKeyTyped(evt);
            }
        });

        intervaloB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                intervaloBKeyTyped(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Primera Derivada:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Segunda Derivada:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Valor De Partida:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(intervaloA))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(funcion))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(intervaloB))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(18, 18, 18)
                        .addComponent(numitera))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(derivada1))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18)
                        .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(derivada2))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(valordepartida)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(intervaloA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(intervaloB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(numitera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(valordepartida, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(derivada1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(derivada2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel11.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel11.setText("La raíz es:");

        raiz.setEditable(false);
        raiz.setForeground(new java.awt.Color(237, 0, 0));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Selecciona un método", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 14))); // NOI18N

        opcion.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        opcion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selecciona una opción", "Método De Bisección", "Método De Falsa Posición", "Método De La Secante", "Método De Newton-Rapshon", "Método De Aproximaciones Sucesivas", "Método De Newton de Segundo Orden" }));
        opcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opcionActionPerformed(evt);
            }
        });

        calcular.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        calcular.setText("Calcular");
        calcular.setMaximumSize(new java.awt.Dimension(101, 25));
        calcular.setMinimumSize(new java.awt.Dimension(101, 25));
        calcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcularActionPerformed(evt);
            }
        });

        limpiar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        limpiar.setText("Limpiar");
        limpiar.setMaximumSize(new java.awt.Dimension(101, 25));
        limpiar.setMinimumSize(new java.awt.Dimension(101, 25));
        limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiarActionPerformed(evt);
            }
        });

        grafica.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        grafica.setText("Gráfica");
        grafica.setMaximumSize(new java.awt.Dimension(101, 25));
        grafica.setMinimumSize(new java.awt.Dimension(101, 25));
        grafica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                graficaActionPerformed(evt);
            }
        });

        cerrar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cerrar.setText("Cerrar");
        cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(opcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(calcular, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                            .addComponent(grafica, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(limpiar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cerrar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(opcion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(calcular, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(limpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(grafica, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        iteraciones.setColumns(20);
        iteraciones.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        iteraciones.setRows(5);
        jScrollPane2.setViewportView(iteraciones);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(170, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(170, 170, 170))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(raiz))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(32, 32, 32))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel6)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(raiz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(26, 26, 26)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );

        jPanel1.getAccessibleContext().setAccessibleDescription("Seleccione un Método");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void intervaloAKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_intervaloAKeyTyped

        //Validaciones. 
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!='-') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && intervaloA.getText().contains("."))
            evt.consume();

        if (evt.getKeyChar()=='-' && intervaloA.getText().contains("-"))
            evt.consume();
    }//GEN-LAST:event_intervaloAKeyTyped

    private void errorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_errorKeyTyped

        //Validaciones.
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && error.getText().contains("."))
            evt.consume();
    }//GEN-LAST:event_errorKeyTyped

    private void numiteraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_numiteraKeyTyped

        //Validaciones.
        if ((evt.getKeyChar() < '0' || evt.getKeyChar() > '9') && (evt.getKeyChar()!= evt.VK_BACK_SPACE))
            evt.consume();
    }//GEN-LAST:event_numiteraKeyTyped

    private void funcionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionKeyTyped

        //Validaciones.
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9')
            && evt.getKeyChar()!='*' && evt.getKeyChar()!='/'
            && evt.getKeyChar()!='+' && evt.getKeyChar()!='-'
            && evt.getKeyChar()!='^' && evt.getKeyChar()!='x'
            && evt.getKeyChar()!='e' && evt.getKeyChar()!='c'
            && evt.getKeyChar()!='o' && evt.getKeyChar()!='s'
            && evt.getKeyChar()!='i' && evt.getKeyChar()!='n'
            && evt.getKeyChar()!='l' && evt.getKeyChar()!='g'
            && evt.getKeyChar()!='t' && evt.getKeyChar()!='a'
            && evt.getKeyChar()!='p' && evt.getKeyChar()!='('
            && evt.getKeyChar()!=')' && evt.getKeyChar()!='.'
            && evt.getKeyChar()!=evt.VK_BACK_SPACE)
            evt.consume();
    }//GEN-LAST:event_funcionKeyTyped

    private void intervaloBKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_intervaloBKeyTyped

        //Validaciones.
        if ((evt.getKeyChar()<'0' || evt.getKeyChar()>'9') && (evt.getKeyChar()!='.') && (evt.getKeyChar()!='-') && (evt.getKeyChar()!=evt.VK_BACK_SPACE))
            evt.consume();

        if (evt.getKeyChar()=='.' && intervaloB.getText().contains("."))
            evt.consume();
        
        if (evt.getKeyChar()=='-' && intervaloB.getText().contains("-"))
            evt.consume();
    }//GEN-LAST:event_intervaloBKeyTyped

    private void opcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opcionActionPerformed
       
        int indice = opcion.getSelectedIndex();
       
        //Dependiendo del método elegido habilito, deshabilito o limpio los componentes correspondientes.
        if(indice == 0)
        {
            //Si es la primera opción deshabilito todos los componentes.
            funcion.setEnabled(false);
            intervaloA.setEnabled(false);
            intervaloB.setEnabled(false);
            numitera.setEnabled(false);
            error.setEnabled(false);
            derivada1.setEnabled(false);
            derivada2.setEnabled(false);
            valordepartida.setEnabled(false);
            grafica.setEnabled(false);
            iteraciones.setEnabled(false); 
            calcular.setEnabled(false);
            grafica.setEnabled(false);
            limpiar.setEnabled(false);
            iteraciones.setText("");
        }
       
        else if(indice == 1 || indice == 2 || indice == 3)
        {
            //Limpio y habilito los componentes que se necesitan.
            funcion.setEnabled(true);
            intervaloA.setEnabled(true);
            intervaloB.setEnabled(true);
            numitera.setEnabled(true);
            error.setEnabled(true); 
            calcular.setEnabled(true);
            limpiar.setEnabled(true);
                       
            //Limpio y deshabilito el resto de los componentes.  
            derivada1.setEnabled(false);
            derivada2.setEnabled(false);
            valordepartida.setEnabled(false);   
            grafica.setEnabled(false);
        }
        
        else if(indice == 4)
        {
            //Limpio y habilito los componentes que se necesitan.
            funcion.setEnabled(true);
            derivada1.setEnabled(true);
            valordepartida.setEnabled(true);
            error.setEnabled(true); 
            numitera.setEnabled(true);
            calcular.setEnabled(true);
            limpiar.setEnabled(true);
           
            //Limpio y deshabilito el resto de los componentes.  
            intervaloA.setEnabled(false);
            intervaloB.setEnabled(false);
            derivada2.setEnabled(false);   
            grafica.setEnabled(false);
        }
        
        else if(indice == 5)
        {
            //Limpio y habilito los componentes que se necesitan.
            funcion.setEnabled(true);
            valordepartida.setEnabled(true);
            error.setEnabled(true); 
            numitera.setEnabled(true);
            calcular.setEnabled(true);
            limpiar.setEnabled(true);
                       
            //Limpio y deshabilito el resto de los componentes.  
            intervaloA.setEnabled(false);
            intervaloB.setEnabled(false);
            derivada1.setEnabled(false);
            derivada2.setEnabled(false); 
            grafica.setEnabled(false);
        }
        
        else
        {
             //Limpio y habilito los componentes que se necesitan.
            funcion.setEnabled(true);
            derivada1.setEnabled(true);
            derivada2.setEnabled(true);  
            valordepartida.setEnabled(true);
            error.setEnabled(true); 
            numitera.setEnabled(true);
            calcular.setEnabled(true);
            limpiar.setEnabled(true);
            grafica.setEnabled(false);
        }
    }//GEN-LAST:event_opcionActionPerformed

    private void graficaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_graficaActionPerformed
        
        new MiGrafica().setVisible(true); 
        
    }//GEN-LAST:event_graficaActionPerformed

    private void limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limpiarActionPerformed

        int opcion = JOptionPane.showConfirmDialog(null, "¿Desea limpiar?", "Confirma", JOptionPane.YES_NO_CANCEL_OPTION);

        if(opcion == JOptionPane.YES_OPTION)
        {
            funcion.setText("");
            intervaloA.setText("");
            intervaloB.setText("");
            numitera.setText("");
            error.setText("");
            derivada1.setText("");
            derivada2.setText("");
            valordepartida.setText("");
            raiz.setText("");
            grafica.setEnabled(false);
            iteraciones.setText("");
        }
    }//GEN-LAST:event_limpiarActionPerformed

    private void calcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcularActionPerformed
        
        int indice = opcion.getSelectedIndex();
        
        if(indice == 1)
           LeerDatosBiseccion();
        else if(indice == 2)
           LeerDatosFalsaPosicion();
        else if(indice == 3)
            LeerDatosSecante();
        else if(indice == 4)
            LeerDatosNewtonRaphson();
        else if (indice == 5)
            LeerDatosAproximacionesSucesivas();
        else
            LeerDatosNewton2doOrden();
    }//GEN-LAST:event_calcularActionPerformed
   
    private void cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarActionPerformed
           
        this.dispose();
    }//GEN-LAST:event_cerrarActionPerformed

    public static void main(String args[]) {
      
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RaicesDeEcuaciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton calcular;
    private javax.swing.JButton cerrar;
    private javax.swing.JTextField derivada1;
    private javax.swing.JTextField derivada2;
    private javax.swing.JTextField error;
    private javax.swing.JTextField funcion;
    private javax.swing.JButton grafica;
    private javax.swing.JTextField intervaloA;
    private javax.swing.JTextField intervaloB;
    private javax.swing.JTextArea iteraciones;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton limpiar;
    private javax.swing.JTextField numitera;
    private static javax.swing.JComboBox opcion;
    private javax.swing.JTextField raiz;
    private javax.swing.JTextField valordepartida;
    // End of variables declaration//GEN-END:variables
    
     //Métodos de solución de raíces de ecuaciones.
    
    private void LeerDatosBiseccion()
    {
       try
       {
            String f = Leer.funcion(funcion.getText());
            double a = Double.parseDouble(intervaloA.getText());
            double b = Double.parseDouble(intervaloB.getText());
            int noitera = Integer.parseInt(numitera.getText());
            double ep = Double.parseDouble(error.getText());
           
            if(!f.equals("error"))
            {
                if(Leer.eval(f, a) * Leer.eval(f, b) < 0)
                {
                    grafica.setEnabled(true);
                    MiGrafica.g.mostrarGrafica(funcion.getText()); //Para que muestre la gráfica de la función que converge. 
                    
                    if(Leer.eval(f, a) == 0) 
                       raiz.setText("La raíz de la función está dada por el punto A = "+a);
                    else if(Leer.eval(f, b) == 0)
                       raiz.setText("La raíz de la función está está dada por el punto B = "+b);
                    else
                        Biseccion(f, a, b, ep, noitera);
                }
                else
                    JOptionPane.showMessageDialog(null, "La función no converge, vuelva a intentarlo.");  
            }
            else
                JOptionPane.showMessageDialog(null, "Expresión no válida, vuelva a intentarlo.");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
    }
   
    public void Biseccion (String f, double a, double b, double e, int numitera)
    {
	iteraciones.setText(""); //Limpio el componente que muestra las iteraciones.  
        
        iteraciones.append("I                        X1                                   X2                                   X2-X1                              Xm                                  f(X1)                                     f(Xm)\n");
       
        double s=0, error;
      
        for(int i=1; i<=numitera; i++)
	{
            s = (a+b) / 2;
            
            iteraciones.append("\n"+i+"	   "+Blancos(a+"")+Blancos(b+"")+Blancos(b-a+"")+Blancos(s+"")+Blancos(Leer.eval(f,a)+"")+"      "+Blancos(Leer.eval(f,s)+""));
       
            error = (b-a)<0 ? (b-a)*-1 : b-a;
           
            if((Leer.eval(f,s)) == 0)
            {
                raiz.setText(s+"");
                break;
            }
            else if ((Leer.eval(f,a))*(Leer.eval(f,s))<0)
                b=s;
            else
                a=s;
						
            if(error < e)
            {
                raiz.setText(s+"");
                break;
            }
        }
        raiz.setText(s+"");
    }
    
    public void LeerDatosFalsaPosicion()
    {
       try
       {
            String f = Leer.funcion(funcion.getText());
            double a = Double.parseDouble(intervaloA.getText());
            double b = Double.parseDouble(intervaloB.getText());
            int noitera = Integer.parseInt(numitera.getText());
            double ep = Double.parseDouble(error.getText());
           
            if(!f.equals("error"))
            {
                if(Leer.eval(f, a) * Leer.eval(f, b) < 0)
                {
                    grafica.setEnabled(true);
                    MiGrafica.g.mostrarGrafica(funcion.getText()); //Para que muestre la gráfica de la función que converge. 
                    
                    if(Leer.eval(f, a) == 0) 
                       raiz.setText("La raíz de la función está dada por el punto A = "+a);
                    else if(Leer.eval(f, b) == 0)
                       raiz.setText("La raíz de la función está dada por el punto B = "+b);
                    else
                        FalsaPosicion(f, a, b, ep, noitera);
                }
                else
                    JOptionPane.showMessageDialog(null, "La función no converge, vuelva a intentarlo.");  
            }
            else
                JOptionPane.showMessageDialog(null, "Expresión no válida, vuelva a intentarlo.");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
    }
    
    public void FalsaPosicion(String f, double a, double b, double e, int numitera)
    {
        iteraciones.setText(""); //Limpio el componente que muestra las iteraciones.  
        
        iteraciones.append("I                        Xi                                         f(Xi)                                           Xi-X1                                              f(Xi)-f(X1)                                   Xi+1                                         Error                                        f(Xi+1)\n");

        double xi, delta=0;

        for(int i=1; i<=numitera; i++)
	{
            if (i==1)
            {
		iteraciones.append("\n"+i+"	   "+Blancos(a+"")+"     "+Blancos(Leer.eval(f,a)+"")+"           "+Blancos((0)+"")+"             "+Blancos((0)+"")+"           "+Blancos((0)+"")+"         "+Blancos((0)+"")+"          "+Blancos(0+""));
		continue;
            }
			
            xi = b-((Leer.eval(f,b) * (b-a)) / (Leer.eval(f,b) - Leer.eval(f,a)));
			
            iteraciones.append("\n"+i+"	   "+Blancos(b+"")+"     "+Blancos(Leer.eval(f,b)+"")+"           "+Blancos((b-a)+"")+"             "+Blancos((Leer.eval(f,b)-Leer.eval(f,a))+"")+"           "+Blancos((xi)+"")+"         "+Blancos((xi-b)+"")+"          "+Blancos(Leer.eval(f,xi)+""));	
			
            delta = (xi-b)<0 ? (xi-b)*-1 : xi-b;
			
            b = xi;
			
            if(delta < e)
            {
		raiz.setText(xi+"");
		break;
            }
	}
    }
    
    public void LeerDatosSecante()
    {
       try
       {
            String f = Leer.funcion(funcion.getText());
            double a = Double.parseDouble(intervaloA.getText());
            double b = Double.parseDouble(intervaloB.getText());
            int noitera = Integer.parseInt(numitera.getText());
            double ep = Double.parseDouble(error.getText());
           
            if(!f.equals("error"))
            {
                grafica.setEnabled(true);
                MiGrafica.g.mostrarGrafica(funcion.getText()); //Para que muestre la gráfica de la función que converge. 
                    
                if(Leer.eval(f, a) == 0) 
                   raiz.setText("La raíz de la función está dada por el punto A = "+a);
                else if(Leer.eval(f, b) == 0)
                    raiz.setText("La raíz de la función está dada por el punto B = "+b);
                else
                    Secante(f, a, b, ep, noitera);  
            }
            else
                JOptionPane.showMessageDialog(null, "Expresión no válida, vuelva a intentarlo.");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
    }

    public void Secante(String f, double a, double b, double e, int numitera)
    {
        iteraciones.setText(""); //Limpio el componente que muestra las iteraciones.
        
        iteraciones.append("I                        Xi                                         Xi+1                                            f(Xi)                                             f(Xi+1)                                        Xi+2                                          Error                                        f(Xi+2)\n");
        
        double xi, delta=0;
		
        for(int i=1; i<=numitera; i++)
	{
            xi = b - ((Leer.eval(f,b) * (b-a)) / (Leer.eval(f,b) - Leer.eval(f,a)));
			
            iteraciones.append("\n"+i+"	   "+Blancos(a+"")+"     "+Blancos(b+"")+"           "+Blancos(Leer.eval(f,a)+"")+"             "+Blancos(Leer.eval(f,b)+"")+"           "+Blancos(xi+"")+"         "+Blancos(xi-b+"")+"          "+Blancos(Leer.eval(f,xi)+""));
			
            delta = (xi-b)<0 ? (xi-b)*-1 : xi-b;
			
            a = b;
            b = xi;
			
            if(delta < e)
            {
		raiz.setText(xi+"");
		break;
            }
	}
    }
    
    public void LeerDatosNewtonRaphson()
    {
       try
       {
            String f = Leer.funcion(funcion.getText());
            String d1 = Leer.funcion(derivada1.getText());
            double x0 = Double.parseDouble(valordepartida.getText());
            int noitera = Integer.parseInt(numitera.getText());
            double ep = Double.parseDouble(error.getText());
           
            if(!f.equals("error"))
            {
                grafica.setEnabled(true);
                MiGrafica.g.mostrarGrafica(funcion.getText()); //Para que muestre la gráfica de la función que converge. 
                    
                if(Leer.eval(f, x0) == 0) 
                   raiz.setText("La raíz de la función está dada por el valor de partida = "+x0);
                else
                    NewtonRaphson(f, d1, x0, ep, noitera);  
            }
            else
                JOptionPane.showMessageDialog(null, "Expresión no válida, vuelva a intentarlo.");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
    }
    
    public void NewtonRaphson(String f, String d1, double x, double e, int numitera)
    {
        iteraciones.setText(""); //Limpio el componente que muestra las iteraciones.
         
        iteraciones.append("I                        Xi                                               f(Xi)                                          f'(Xi)                                          Xi+1                                          Error\n");
        
        double xi, delta;
		
        for(int i=1; i<=numitera; i++)
        {
            xi= x-(Leer.eval(f, x)/Leer.eval(d1, x));

            iteraciones.append("\n"+i+"	   "+Blancos(x+"")+"           "+Blancos(Leer.eval(f,x)+"")+"         "+Blancos(Leer.eval(d1,x)+"")+"          "+Blancos(xi+"")+"         "+Blancos(xi-x+""));

            delta= Math.abs(xi-x);

            x = xi;

            if(delta < e)
            {
               raiz.setText(xi+"");
               break;
            }
        }
    }
    
    public void LeerDatosAproximacionesSucesivas()
    {
       try
       {
            String f = Leer.funcion(funcion.getText());
            double x0 = Double.parseDouble(valordepartida.getText());
            int noitera = Integer.parseInt(numitera.getText());
            double ep = Double.parseDouble(error.getText());
           
            if(!f.equals("error"))
            {
                grafica.setEnabled(true);
                MiGrafica.g.mostrarGrafica(funcion.getText()); //Para que muestre la gráfica de la función que converge. 
                    
                AproximacionesSucesivas(f, x0, ep, noitera);  
            }
            else
                JOptionPane.showMessageDialog(null, "Expresión no válida, vuelva a intentarlo.");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
    }
    
    public void AproximacionesSucesivas(String f, double x0, double e, int numitera)
    {
        iteraciones.setText(""); //Limpio el componente que muestra las iteraciones.
        
        iteraciones.append("I                         Xi                                              g(xi)                                        Error                                         f(Xi)\n");
    
        double delta=0, gx, xn;
		
	for(int i=1; i<=numitera; i++)
	{
            gx = Leer.eval(f,x0) + x0;
            
            xn = gx;
			
            iteraciones.append("\n"+i+"	   "+Blancos(x0+"")+"           "+Blancos(xn+"")+"         "+Blancos(xn-x0+"")+"          "+Blancos(Leer.eval(f,x0)+""));
			
            delta = (xn - x0);
			 
            if(Math.abs(delta) < e)
            {
		raiz.setText(xn+"");
		break;	
            }
	
            x0 = xn;

	} 
    }
    
    public void LeerDatosNewton2doOrden()
    {
       try
       {
            String f = Leer.funcion(funcion.getText());
            String d1 = Leer.funcion(derivada1.getText());
            String d2 = Leer.funcion(derivada2.getText());
            double x0 = Double.parseDouble(valordepartida.getText());
            int noitera = Integer.parseInt(numitera.getText());
            double ep = Double.parseDouble(error.getText());
           
            if(!f.equals("error"))
            {
                grafica.setEnabled(true);
                MiGrafica.g.mostrarGrafica(funcion.getText()); //Para que muestre la gráfica de la función que converge. 
                    
                if((Leer.eval(f, x0) == 0))
                    raiz.setText("La raíz de la función está dada por el valor de partida = "+x0);
                else
                    Newton2doOrden(f, d1, d2, x0, ep, noitera);  
            }
            else
                JOptionPane.showMessageDialog(null, "Expresión no válida, vuelva a intentarlo.");
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
    }
    
    public void Newton2doOrden(String f, String d1, String d2, double x, double e, int numitera)
    {
        iteraciones.setText(""); //Limpio el componente que muestra las iteraciones.
        
        iteraciones.append("I                        Xi                                               f(Xi)                                         f'(Xi)                                           f''(Xi)                                           Xi+1                                           f(Xi+1)                                        Error\n");
        
        double xi=0, delta;
		
	for(int i=1; i<=numitera; i++)
	{
            xi = x - Leer.eval(f, x) / (Leer.eval(d1, x) - (Leer.eval(f, x) * Leer.eval(d2, x) / (2 * Leer.eval(d1, x))));
			
            iteraciones.append("\n"+i+"	   "+Blancos(x+"")+"           "+Blancos(Leer.eval(f, x)+"")+"         "+Blancos(Leer.eval(d1, x)+"")+"          "+Blancos(Leer.eval(d2,x)+"")+"            "+Blancos(xi+"")+"           "+Blancos(Leer.eval(f, xi)+"")+"            "+Blancos(x-xi+""));

            delta = (x-xi)<0 ? (x-xi)*-1 : x-xi;
			
            x = xi; 
			
            if (delta < e)
            {
		raiz.setText(xi+"");
                break;
            }	
        }      
    }
    
    public String Blancos (String exp)
    {
        int dif = 20-exp.length();
        for(int i=0; i<dif; i++)
          exp=exp+"  "; //Le agregue doble espacio para que se alinearán. 
        return exp;
    }
}