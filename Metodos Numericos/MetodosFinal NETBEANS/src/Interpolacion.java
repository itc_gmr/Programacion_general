
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.nfunk.jep.JEP;

public class Interpolacion extends javax.swing.JFrame {
    JEP j = new JEP();
    DefaultTableModel modelo = new DefaultTableModel();
    public Interpolacion() {
        initComponents();
        setSize(900,600); 
        setLocationRelativeTo(null);
        txtorden.setEnabled(false);
        String [] nomcolumnas = {"X","Y"};
        DefaultTableModel modelo = new DefaultTableModel(null, nomcolumnas);
        Ttabla.setModel(modelo);
        modelo.setColumnCount(2);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtorden = new javax.swing.JTextField();
        metodos = new javax.swing.JComboBox();
        txtx = new javax.swing.JTextField();
        btncalcular = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Ttabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnpuntos = new javax.swing.JSpinner();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtaresultado = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtadatos = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("INTERPOLACION LINEAL Y AJUSTE POLINOMIAL");
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
        jPanel2.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Seleccionar un Metodo");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(250, 40, 220, 60);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Numero de Puntos a utilizar");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(10, 130, 240, 60);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Valor para X");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(10, 180, 220, 60);
        jPanel2.add(txtorden);
        txtorden.setBounds(160, 230, 160, 30);

        metodos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "MÉTODOS", "NEWTON", "LAGRANGE", "MINIMOS CUADRADOS" }));
        metodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                metodosActionPerformed(evt);
            }
        });
        jPanel2.add(metodos);
        metodos.setBounds(250, 100, 210, 30);
        jPanel2.add(txtx);
        txtx.setBounds(110, 190, 170, 30);

        btncalcular.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btncalcular.setText("Calcular");
        btncalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncalcularActionPerformed(evt);
            }
        });
        jPanel2.add(btncalcular);
        btncalcular.setBounds(70, 270, 110, 40);

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/limpiar.png"))); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton5);
        jButton5.setBounds(10, 270, 50, 40);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Orden de la Funcion");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(10, 220, 210, 60);

        Ttabla.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(0, 0, 0), new java.awt.Color(0, 0, 0)));
        Ttabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "X", "Y"
            }
        ));
        jScrollPane1.setViewportView(Ttabla);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(530, 100, 330, 210);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/log.png"))); // NOI18N
        jPanel2.add(jLabel8);
        jLabel8.setBounds(10, 10, 240, 120);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tecmini.png"))); // NOI18N
        jPanel2.add(jLabel9);
        jLabel9.setBounds(690, 10, 180, 70);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setText("INTERPOLACION LINEAL  Y AJUSTE POLINOMIAL");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(250, 10, 690, 60);

        btnpuntos.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                btnpuntosStateChanged(evt);
            }
        });
        jPanel2.add(btnpuntos);
        btnpuntos.setBounds(210, 140, 80, 30);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 10, 870, 320);

        txtaresultado.setEditable(false);
        txtaresultado.setColumns(20);
        txtaresultado.setRows(5);
        jScrollPane2.setViewportView(txtaresultado);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(590, 370, 290, 120);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Proceso");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(180, 320, 210, 60);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Resultado");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(680, 330, 210, 60);

        txtadatos.setEditable(false);
        txtadatos.setColumns(20);
        txtadatos.setRows(5);
        jScrollPane3.setViewportView(txtadatos);

        jPanel1.add(jScrollPane3);
        jScrollPane3.setBounds(10, 360, 540, 200);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 890, 570);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void metodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_metodosActionPerformed
        int metodo = metodos.getSelectedIndex();
        if(metodo==0){
            btnpuntos.setEnabled(false);
            txtorden.setEnabled(false);
            txtorden.setText("");
        }
        if(metodo==1){
            btnpuntos.setEnabled(true);
            txtorden.setEnabled(false);
            txtorden.setText("");
        }

        if(metodo==2){
            btnpuntos.setEnabled(true);
            txtorden.setEnabled(false);
            txtorden.setText("");
        }
        if(metodo==3){
            btnpuntos.setEnabled(true);
            txtorden.setEnabled(true);
        }

    }//GEN-LAST:event_metodosActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        txtx.setText("");
        txtorden.setText("");
        txtaresultado.setText("");
        txtadatos.setText("");
        btnpuntos.setToolTipText("0");
        metodos.setSelectedIndex(0);
        
         Object Col [] = new Object [2];
         Col[0]="X";
         Col[1]= "Y";  
         modelo = new DefaultTableModel(Col, 0);
         Ttabla.setModel(modelo);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void btncalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncalcularActionPerformed
       int pun=metodos.getSelectedIndex();
       txtadatos.setText("");
       txtaresultado.setText("");
       if(pun == 0)
       {
           JOptionPane.showMessageDialog(null, "Seleccione un Método");
       }
       if(pun ==3 && txtorden.getText().isEmpty()){
           JOptionPane.showMessageDialog(null, "Defina el orden de la función");
       }
       if( txtx.getText().isEmpty()){
             JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos.");
             return;
        }
       switch(pun){
           case 1:
                Internewton();
           break;
           case 2:
               Lagrange();
           break;
           case 3:
               MinimosC();
           break;
       }
    }//GEN-LAST:event_btncalcularActionPerformed

    private void btnpuntosStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_btnpuntosStateChanged
       ((DefaultTableModel) this.Ttabla.getModel()).setRowCount(Integer.parseInt(btnpuntos.getValue().toString()));
    }//GEN-LAST:event_btnpuntosStateChanged
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interpolacion().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Ttabla;
    private javax.swing.JButton btncalcular;
    private javax.swing.JSpinner btnpuntos;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JComboBox metodos;
    private javax.swing.JTextArea txtadatos;
    private javax.swing.JTextArea txtaresultado;
    private javax.swing.JTextField txtorden;
    private javax.swing.JTextField txtx;
    // End of variables declaration//GEN-END:variables
    public void Internewton(){	
        try{
            
            double xi[], yi[][];
            int m=Integer.parseInt(btnpuntos.getValue().toString());
            xi=new double [m];
            yi=new double [m][m];
            for(int i=0; i<m;i++){
                xi[i]=Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, 0)));
                for(int j=0; j<2;j++){
                    if(j==1)
                        yi[i][0]=Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, j)));
                }
            }
            for(int j=1;j<m-1;j++)
                for(int i=0;i<m-j;i++){
                    yi[i][j]=yi[i+1][j-1]-yi[i][j-1];
                }		
            for(int i=0;i<m+1;i++){
                if(i==0){
                    txtadatos.append(blancos("x"));
                continue;
                }
                if(i==1){
                    txtadatos.append(blancos("y"));
                continue;
                }
                txtadatos.append(blancos("D^"+(i-1)+"y"));
            }
            txtadatos.append("\n");
            String cadena="";
            for(int i=0;i<m;i++){
                cadena=blancos(xi[i]+"");
                txtadatos.append(cadena);
                for(int j=0;j<m;j++){       
                    cadena=blancos(yi[i][j]+"");
                    txtadatos.append(cadena);
                }	
                txtadatos.append("\n");
            }
            double x=Double.parseDouble( txtx.getText());
            double k=((x-xi[0])/(xi[2]-xi[1]));
            double y=0;
            for(int i=1;i<m;i++){
                double num=1;
                double j=0;
                while(j<=(i-2)){
                    num=num*(k-j);
                    j++;
                }
                y=y+num/factorial(i-1) * yi[0][i-1];
            }
            txtaresultado.append("El valor de y\n para x="+x+" \nes: "+y);
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
    }
    public  void Lagrange(){	
        try{
            double xi[], yi[];
            int m=Integer.parseInt(btnpuntos.getValue().toString());
            xi=new double [m];
            yi=new double [m];
            for(int i=0; i<m;i++){
                xi[i]=Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, 0)));
                yi[i]=Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, 1)));
            }
            double x=Double.parseDouble( txtx.getText());
            double y=0;
            for(int i=0;i<m;i++){
                double num=1;
                double den=1;
                for(int j=0; j<m;j++){
                    if(i!=j){
                        num=num*(x-xi[j]);
                        den=den*(xi[i]-xi[j]);
                    }
                }
                y=y+(num/den) * yi[i];
            }
            txtaresultado.append("PARA EL VALOR DE X="+x+"\n EL VALOR DE Y ES IGUAL A: "+y);
        }
        catch (Exception e){
            JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
        }
}
    public  double factorial(double number) {
        if (number <= 1)
            return 1;
        return number * factorial(number - 1);
    }
    public void MinimosC(){
        try{
            txtadatos.append("*****MINIMOS CUADRADOS*****");
            txtadatos.append("\n");
            double [] a, b,x,y;
            int n = Integer.parseInt(btnpuntos.getValue().toString());
            int m=Integer.parseInt(txtorden.getText());
            double Mat[][]=new double [n][(m*2)+1+m];
            a=new double[(m*2)+1];
            b=new double [m+1];
            x=new double [n];
            y=new double [n];
            a[0]=n;
            b[0]=0;
            for(int i=0;i<n;i++ ){
                x[i]=Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, 0)));
                Mat[i][0]=x[i];
                y[i]=Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, 1)));
                Mat[i][1]=y[i];
                b[0]=b[0]+y[i]; 
                for(int j=1; j<=(m*2);j++){
                    a[j]= a[j]+ Math.pow(x[i], j);
                    if(j!=1)
                        Mat[i][j]=Math.pow(x[i], j);
                }
                for(int j=1;j<=m;j++){
                    b[j]=b[j] + y[i]*Math.pow(x[i], j);
                    if(j==1){
                        Mat[i][m*2+1]=y[i]*Math.pow(x[i], j);
                    }
                    if(j==2){
                        Mat[i][m*2+2]=y[i]*Math.pow(x[i], j);
                    }
                    if(j==3){
                        Mat[i][m*2+3]=y[i]*Math.pow(x[i], j);  
                    }
                }   	  
            }  
            int cont=0;
            for(int i=0;i<((m*2)+m);i++){
                if(i==1)
                    txtadatos.append(blancos("y"+""));
                if(i<m*2)
                    txtadatos.append(blancos("x^"+(i+1)+""));
                if(i>m*2-1){
                    cont++;
                    txtadatos.append(blancos("x^"+cont+"y"+""));
                }	  
            }           
            txtadatos.append("\n");
            for(int i=0;i<n;i++){
                for(int j=0;j<((m*2)+1+m);j++){
                    txtadatos.append(blancos(Mat[i][j]+""));
                }
                txtadatos.append("\n");		
            }
            double c[][]=new double [m+1][m+2];
            for(int i=1; i<=m+1; i++){
                for(int j=1;j<=m+1;j++){
                    c[i-1][j-1]=a[i+j-2];
                }
                c[i-1][m+1]=b[i-1];
            }
            txtadatos.append("SISTEMA RESULTANTE DE LAS OPERACIONES ES:");
            txtadatos.append("\n");
            for(int i=0;i<m+1;i++){
                for(int j=0; j<m+2;j++){
                    txtadatos.append(blancos(c[i][j]+"a"+(j+1)+"")); 
                }
                txtadatos.append("\n");
            }
            double [] res=NewCuadrados(c,m);
            txtadatos.append("\n**RESULTADOS**");
            txtadatos.append("\n");
            for(int i=0; i<m+1;i++){
                txtadatos.append("a["+(i+1)+"] ="+res[i]);
                txtadatos.append("\n");
            } 
            txtadatos.append("La Funcion f(x)= ");
            int cn=m;
            String Funcion="";
            for(int i=m+1;i>0;i--){
                if(i>1){
                    txtadatos.append(res[i-1]+"x^"+cn);
                    Funcion=Funcion+res[i-1]+"x^"+cn;
                    txtadatos.append("+");
                    Funcion=Funcion+"+";
                }else{
                    txtadatos.append(res[i-1]+"");
                    Funcion=Funcion+res[i-1];
                }
                cn--;
            }
            Funcion CFun=new Funcion(Funcion);
            double Val=Double.parseDouble( txtx.getText());
            txtaresultado.append("EL VALOR DE  f("+Val+")= "+CFun.eval( Val));
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");
            }	  
    }  
    public static double [] NewCuadrados(double [][] Matriz, int m){
        double resultado[]=new double [m+1];
        double pivote, cero;	
        for(int i=0;i<Matriz.length;i++){
            pivote=Matriz[i][i];
                for(int j=0;j<Matriz[i].length;j++){
                    Matriz[i][j]=Matriz[i][j]/pivote;	
                }
            for(int k=0;k<Matriz.length;k++){
                if(k!=i){
                    cero= Matriz[k][i];
                    for(int j=0; j<Matriz[i].length; j++){
                        Matriz[k][j]=Matriz[k][j]-cero*Matriz[i][j];
                    }
                }
            }  
        }
        for(int i=0; i<m+1;i++){
            resultado[i]= Matriz[i][m+1];
        } 
        return resultado;
    }
    public static String blancos (String exp){
        int dif=20-exp.length();
        for(int i=0; i<dif; i++)
            exp=exp+" ";
        return exp;
    }
}
