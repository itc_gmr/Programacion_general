import Componentes.MyRandom;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
public class MJacobi extends javax.swing.JFrame {
 private DefaultTableModel Model;
    public MJacobi() {
        initComponents();
        setSize(960,500);
        setLocationRelativeTo(null);
        btnresolver.setEnabled(false);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtIncognitas = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txterror = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtni = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        btnresolver = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtLimpiar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Ttabla = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtaSolucion = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtLimpiar1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Metodo de Jacobi");
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel1.setText("Incognitas");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(10, 180, 80, 24);

        txtIncognitas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIncognitasKeyTyped(evt);
            }
        });
        jPanel1.add(txtIncognitas);
        txtIncognitas.setBounds(80, 180, 100, 30);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel4.setText("Error Permitido");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 230, 130, 15);

        txterror.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txterrorKeyTyped(evt);
            }
        });
        jPanel1.add(txterror);
        txterror.setBounds(100, 220, 130, 30);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setText("Numero de Iteraciones Max");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 260, 180, 30);

        txtni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtniKeyTyped(evt);
            }
        });
        jPanel1.add(txtni);
        txtni.setBounds(180, 260, 80, 30);

        jButton2.setText("Generar Matriz Aumentada");
        jButton2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jButton2FocusLost(evt);
            }
        });
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton2MousePressed(evt);
            }
        });
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(10, 310, 180, 30);

        btnresolver.setText("Resolver el Sistema");
        btnresolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnresolverActionPerformed(evt);
            }
        });
        jPanel1.add(btnresolver);
        btnresolver.setBounds(10, 360, 150, 30);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/log.png"))); // NOI18N
        jPanel1.add(jLabel2);
        jLabel2.setBounds(20, 30, 240, 120);

        txtLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/limpiar.png"))); // NOI18N
        txtLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLimpiarActionPerformed(evt);
            }
        });
        jPanel1.add(txtLimpiar);
        txtLimpiar.setBounds(220, 300, 40, 40);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 20, 280, 490);

        Ttabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "x"
            }
        ));
        Ttabla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TtablaKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(Ttabla);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(300, 210, 660, 300);

        txtaSolucion.setEditable(false);
        txtaSolucion.setColumns(20);
        txtaSolucion.setRows(5);
        jScrollPane3.setViewportView(txtaSolucion);

        getContentPane().add(jScrollPane3);
        jScrollPane3.setBounds(540, 10, 420, 190);

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel3.setText("Procedimiento y Solucion");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(300, 90, 280, 40);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tecmini.png"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(320, 20, 180, 70);

        txtLimpiar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/limpiar.png"))); // NOI18N
        txtLimpiar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLimpiar1ActionPerformed(evt);
            }
        });
        getContentPane().add(txtLimpiar1);
        txtLimpiar1.setBounds(490, 160, 40, 40);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnresolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnresolverActionPerformed

        int m;
	int iteraciones;
	int it=1;
	m=Integer.parseInt(txtIncognitas.getText());
	iteraciones=Integer.parseInt(txtni.getText());
	double e=Double.parseDouble(txterror.getText());
	double a[][] = new double[m][m+1];
	double x[]=new double[m];
	double y[]=new double[100];
	boolean fin;
	double delta;
	for (int i=0;i<m;i++)
	{
            for (int j=0;j<m+1;j++)
            {
                if(Ttabla.getValueAt(i, j).equals("")){
                     JOptionPane.showMessageDialog(null, "Campo vacio", "No puede haber campos vacios verifique", JOptionPane.WARNING_MESSAGE);
                    return;
                }
                a[i][j]=Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, j))); 
            }
	}
        txtaSolucion.setText("");
        txtaSolucion.append("i\t"+"X1\t"+"X2\t"+"X3\t"+"X1\t"+"X2\t"+"X3\t");	
	for(int i=0;i<m;i++)
	{
            x[i]=0;
	}
	do
        {
            fin=false;
            for(int i=0;i<m;i++)
            {
                y[i]=a[i][m];
                for(int j=0;j<m;j++)
                {
                    if(i!=j)
                    {
                        y[i]=y[i]-a[i][j]*x[j];
                    }
		}
		y[i]=y[i]/a[i][i];	
		delta=Math.abs(x[i]-y[i]);
		if(delta>e)
                    {
			fin=true;
                    }
		}
                txtaSolucion.append("\n");
		txtaSolucion.append(MyRandom.Redondear(it, 5)+"\t");
		for(int i =0; i<m;i++)
                    txtaSolucion.append(MyRandom.Redondear(x[i], 5)+"\t");
		for(int i=0;i<m;i++)
                {
                    x[i]=y[i];
                    txtaSolucion.append(MyRandom.Redondear(x[i], 5)+"\t");
		}
		txtaSolucion.append("\n");
		it++;
	}while(fin && iteraciones>=it);
        txtaSolucion.append("\n\n");
	for(int i=0;i<m;i++)
	{
            txtaSolucion.append("El valor de X"+(i+1)+" es: "+x[i] + "   " + y[i] + "\n");
	}
    }//GEN-LAST:event_btnresolverActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try{
            btnresolver.setEnabled(true);
            boolean ban=false;
            int incognitas=Integer.parseInt(String.valueOf(txtIncognitas.getText()));
            if(incognitas<=1){
                throw new Exception("El numero de incognitas es invalido");
            }
            Object col[]= new Object[incognitas+1];
            for(int i=0; i<incognitas+1; i++){
                if(i<incognitas){
                    col[i]="X"+(i+1);
                }else{
                    col[i]="b";
                }
            }
            Model=new  DefaultTableModel(col,incognitas);
            Ttabla.setModel(Model);
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(this, e.getMessage());

        }        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLimpiarActionPerformed
        txtIncognitas.setText("");
        txterror.setText("");
        txtni.setText("");
    }//GEN-LAST:event_txtLimpiarActionPerformed

    private void txtIncognitasKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIncognitasKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)
                                && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtIncognitasKeyTyped

    private void txterrorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txterrorKeyTyped
    char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)
                                && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txterrorKeyTyped

    private void txtniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtniKeyTyped
    char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)
                                && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }    }//GEN-LAST:event_txtniKeyTyped

    private void TtablaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TtablaKeyTyped
        char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)&& (caracter !='.')&& (caracter !='-')&& (caracter !='-'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_TtablaKeyTyped

    private void txtLimpiar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLimpiar1ActionPerformed
       txtaSolucion.setText("");
    }//GEN-LAST:event_txtLimpiar1ActionPerformed

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked

    }//GEN-LAST:event_jButton2MouseClicked

    private void jButton2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MousePressed

    }//GEN-LAST:event_jButton2MousePressed

    private void jButton2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jButton2FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2FocusLost

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2KeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MJacobi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MJacobi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MJacobi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MJacobi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MJacobi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Ttabla;
    private javax.swing.JButton btnresolver;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField txtIncognitas;
    private javax.swing.JButton txtLimpiar;
    private javax.swing.JButton txtLimpiar1;
    private javax.swing.JTextArea txtaSolucion;
    private javax.swing.JTextField txterror;
    private javax.swing.JTextField txtni;
    // End of variables declaration//GEN-END:variables
}
