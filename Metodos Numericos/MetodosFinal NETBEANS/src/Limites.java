/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Dimension;
import java.util.Vector;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class Limites {

    Funcion f;
    double error;
    double valor;
    JTable table;
   
    Vector Columnas;//si
    Vector Renglones;//si
    Vector Datos;//si
    DefaultTableModel modelo;//si

    public Limites(Funcion f, double error, double valor, JTable table) {
        this.f = f;
        this.error = error;
        this.valor = valor;
        this.table = table;
    }

    public double solve() {
        Columnas = new Vector();
        Renglones = new Vector();

        Columnas.addElement("Iteraciones");
        Columnas.addElement("h");
        Columnas.addElement("k");
        Columnas.addElement("x+h");
        Columnas.addElement("f(x+h)");
        Columnas.addElement("f(x)");
        Columnas.addElement("f(x+h)-f(x)");
        Columnas.addElement("f'(x)");
        Columnas.addElement("Error");

        modelo = new DefaultTableModel(Renglones, Columnas);
        table.setModel(modelo);

        for (int i = modelo.getRowCount() - 1; i > -1; i--) {
            modelo.removeRow(i);
        }

        double x, e, da, d, delta, n;
        double potencia, k;

        //Grafica = new Graficar("Grafica Diferenciacion");
        //f = this.f
        x = valor;
        e = error;
        n = 1;
        da = Math.pow(10, 10);
        da = 1 * (Math.pow(10, 10));
        do {
            potencia = Math.pow(0.1, n);
            k = 1 / potencia;
            
            delta = 0;
            d = (f.calc(x + potencia) - f.calc(x)) * k;
            
            k = (float) (1 / (Math.pow(0.1, n)));
            delta = Math.abs(da - d);
            Renglones = new Vector();
            Renglones.add(n);//Iteraciones
            Renglones.add(Formatea.alinder("############.############", potencia));//esto corresponde a la h
            Renglones.add(k);//esto corresponde a la k
            Renglones.add(x + potencia);//esto corrsponde a x+h
            Renglones.add(f.calc(x + potencia));//esto corresponde a f(x+h)
            Renglones.add(f.calc(x));//esto es f(x)
            //aquí falta (f(x+h)-f(x))
            Renglones.add(Formatea.alinder("############.############",f.calc(x + (Math.pow(0.1, n))) - f.calc(x)));
            //Renglones.add(Formatea.alinder(("############.############", (f.calc(x+(Math.pow(0.1, n))) - f.calc(x)))));
            //aquí va la derivada
            Renglones.add((f.calc(x + Math.pow(0.1, n)) - f.calc(x)) * k);
            if (n == 1) {
                Renglones.add("");
            } else {
                Renglones.add(Formatea.alinder("############.############", delta));
            }
            modelo.addRow(Renglones);
            n = n + 1;
            da = d;

        } while (delta > error);
        //table.setModel(modelo);
        //Grafica.AgregaDatos(f, x);
        //Grafica.MuestraGrafica("Grafica Falsa Posición");
        //Respuesta.setText("Solución= " + d);

        //add(Respuesta);
        return d;
    }

}
