

import java.io.*;
import org.nfunk.jep.JEP;

public class Leer
{
	static JEP j = new JEP();
	
	public static String datoString()
	{
		String sdato = "";
		try
		{
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader nose = new BufferedReader(isr);
            sdato =nose.readLine();
		}
		catch(IOException e)
		{
			System.err.println("Error:"+ e.getMessage());
		}
		return sdato;
	}
	
	public static short datoShort()
	{
	
		try
		{
			String sd=datoString();
			if (sd==null)
			{
				System.out.println();
   			    return Short.MIN_VALUE;
			}
			else
			   return Short.parseShort(sd); 
		}
		catch(NumberFormatException e)
		{
			System.out.print("Dato invalido, volver a teclear el dato: ");
			return datoShort();
		}
	}
	
	public static int datoInt()
	{
		try
		{
			return Integer.parseInt(datoString());
		}
		catch(NumberFormatException e)
		{
			System.out.print("Dato invalido, volver a teclear el dato: ");
			return datoInt();
		}
	}
	public static long datoLong()
	{
		try
		{
			return Long.parseLong(datoString());
		}
		catch(NumberFormatException e)
		{
			System.out.print("Dato invalido, volver a teclear el dato: ");
			return datoLong();
		}
	}
	public static float datoFloat()
	{
		try
		{
			return Float.parseFloat(datoString());
		}
		catch(NumberFormatException e)
		{
			System.out.print("Dato invalido, volver a teclear el dato: ");
			return datoFloat();
		}
	}
	public static double datoDouble()
	{
		String cadena=datoString();
		try
		{
			return Double.parseDouble(cadena);
		}
		catch(NumberFormatException e)
		{
			
			if(cadena.length()==0)
			   return Double.NaN;
			else
				return 0;
		}
	}
	
	public static char datoChar()
	{
		char sdato=' ';
		try
		{
			sdato=(char)System.in.read();
			System.in.skip(System.in.available());
		}
		catch(IOException e)
		{
			System.out.print("Dato invalido, volver a teclear el dato: ");
			return datoChar();
		}
		return sdato;
	}

	public static String funcion (String Funcion)
	{       String fx=Funcion;
		j.addStandardConstants();
		j.addStandardFunctions();
		j.setImplicitMul(true); 
		j.addVariable("x", 0);
		j.parseExpression(fx);
		
		if(j.hasError())
                    return "error"; //Hay error.
                
                j.parseExpression("");
		return fx;
	}

	public static double eval (String funcion, double x) 
	{
		j.parseExpression(funcion);
		j.addVariable("x", x);
		if(j.hasError())
                    j.getErrorInfo();
		return j.getValue();
	}

	public static double [] eval (String funcion, double [] x)
	{
		int n = x.length;
		double [] r = new double [n];
		
		for(int i=0; i<n; i++)
			r[i] = eval(funcion, x[i]);
		
		return r;
	}
}