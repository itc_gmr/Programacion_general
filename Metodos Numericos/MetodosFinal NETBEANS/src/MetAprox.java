import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.math.plot.Plot2DPanel;
import org.nfunk.jep.JEP;
public class MetAprox extends javax.swing.JInternalFrame {
    Plot2DPanel grafica = new Plot2DPanel();
    JEP funcion = new JEP();
    MGrafica Graficacion;
    private DefaultTableModel Model;
    int con=0;
    public MetAprox() {
        initComponents();
        setSize(960,500); 
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtfun = new javax.swing.JTextField();
        txtgx = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtxi = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txte = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtni = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtraiz = new javax.swing.JTextField();
        btncal = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setTitle("Metodo de Aproximaciones Sucesivas");
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setName(""); // NOI18N
        jPanel1.setLayout(null);

        jLabel7.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel7.setText("Funcion");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 130, 60, 24);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel1.setText("G(x)");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(10, 170, 60, 24);
        jPanel1.add(txtfun);
        txtfun.setBounds(60, 130, 200, 30);
        jPanel1.add(txtgx);
        txtgx.setBounds(40, 170, 220, 30);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel2.setText("Valor de Xi");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 220, 90, 15);

        txtxi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtxiKeyTyped(evt);
            }
        });
        jPanel1.add(txtxi);
        txtxi.setBounds(80, 210, 130, 30);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel4.setText("Error Permitido");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 260, 130, 15);

        txte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txteKeyTyped(evt);
            }
        });
        jPanel1.add(txte);
        txte.setBounds(100, 250, 150, 30);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setText("Numero de Iteraciones Max");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 290, 180, 30);

        txtni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtniKeyTyped(evt);
            }
        });
        jPanel1.add(txtni);
        txtni.setBounds(170, 290, 150, 30);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setText("Raiz");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 330, 40, 20);

        txtraiz.setEnabled(false);
        jPanel1.add(txtraiz);
        txtraiz.setBounds(40, 330, 270, 30);

        btncal.setText("Calcular la Raiz");
        btncal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncalActionPerformed(evt);
            }
        });
        jPanel1.add(btncal);
        btncal.setBounds(180, 400, 130, 30);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/log.png"))); // NOI18N
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 0, 240, 120);

        jButton1.setText("Limpiar Campos");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(30, 400, 130, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 10, 330, 440);

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabla.setEnabled(false);
        jScrollPane2.setViewportView(tabla);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(350, 70, 620, 390);

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel8.setText("Tabla de Procedimiento y Solucion");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(510, 10, 260, 30);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tecmini.png"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(760, 0, 180, 70);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncalActionPerformed
        String data[][]={};
        String col[]={"i","Xi","g(Xi)","Error","f(Xi)"};
        Model=new DefaultTableModel(data,col);
        tabla.setModel(Model);
        try{
            String fun, gx;
            JEP j = new JEP();
            j.addStandardConstants();
            j.addStandardFunctions();
            fun = txtfun.getText();
            gx = txtgx.getText();
            Funcion CFun =new Funcion(fun);
            j.parseExpression(txtxi.getText());// pasamos a convertr la expresion ingresada en la casilla x0
            double xi = j.getValue();
            j.parseExpression(txte.getText());// pasamos a convertr la expresion ingresada en la casilla  de tolerancia
            double e = j.getValue();
            int ni = Integer.parseInt(txtni.getText());// pasamos a convertr la expresion ingresada en la casilla  de numero maximo de iteraciones
            double ex;
            int i=0;
            do{
                ex=f(gx,xi)-xi;
        	i++;
                Model.insertRow(con, new Object[]{});
                Model.setValueAt(i, con, 0);
                Model.setValueAt(xi, con, 1);
                Model.setValueAt(f(gx,xi), con, 2);
                Model.setValueAt(ex, con, 3);
                Model.setValueAt(f(fun,xi), con, 4); 
                con++;
                xi=f(gx,xi);
                ex=Math.abs(ex); 
            }while(i<ni & ex>e);
            Funcion Cfun=new Funcion(fun);
            Funcion Cgx=new Funcion(gx);
            txtraiz.setText(Double.toString(xi));
            Graficacion = new MGrafica("Metodo de Aproximaciones Sucesivas");
            Graficacion.AgregaDatos(Cfun,xi );
            Graficacion.Agregagx(Cgx,xi );
            Graficacion.Agrega(xi,ni);
            Graficacion.MuestraGrafica("Grafica Aproximaciones Sucesivas");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "ERROR EN LA LECTURA DE DATOS", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }     
    }//GEN-LAST:event_btncalActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        limpiar();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtxiKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtxiKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtxiKeyTyped

    private void txteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txteKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txteKeyTyped

    private void txtniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtniKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtniKeyTyped
      public void limpiar(){
        txtfun.setText("");
        txtgx.setText("");
        txtxi.setText("");
        txte.setText("");
        txtni.setText("");
        txtraiz.setText("");
        Model=new DefaultTableModel();
        tabla.setModel(Model);   
    }
    private double f(String funcion, double x){
        JEP f = new JEP();
        f.addStandardConstants();
        f.addStandardFunctions();
        f.addVariable("x", x);
        f.parseExpression(funcion); 
        return f.getValue();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btncal;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txte;
    private javax.swing.JTextField txtfun;
    private javax.swing.JTextField txtgx;
    private javax.swing.JTextField txtni;
    private javax.swing.JTextField txtraiz;
    private javax.swing.JTextField txtxi;
    // End of variables declaration//GEN-END:variables
}
