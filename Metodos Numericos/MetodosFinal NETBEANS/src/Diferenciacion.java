
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Componentes.MyRandom;
import javax.swing.JScrollPane;
public class Diferenciacion extends javax.swing.JFrame {
    public Diferenciacion() {
        initComponents();
        setSize(900,600); 
        setLocationRelativeTo(null);
        String [] nomcolumnas = {"X","Y"};
        DefaultTableModel modelo = new DefaultTableModel(null, nomcolumnas);
        Ttabla.setModel(modelo);
        modelo.setColumnCount(2);
        Ttabla.setEnabled(true); 
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        btnpuntos = new javax.swing.JSpinner();
        TxtFuncion = new javax.swing.JTextField();
        TxtE = new javax.swing.JTextField();
        TxtInferior = new javax.swing.JTextField();
        TxtSuperior = new javax.swing.JTextField();
        TxtX = new javax.swing.JTextField();
        TxtIntervalos = new javax.swing.JTextField();
        TxtH = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        metodo = new javax.swing.JComboBox<>();
        jLabel14 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Ttabla = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        funcionRadio = new javax.swing.JRadioButton();
        tablaRadio = new javax.swing.JRadioButton();
        resultado = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Diferenciacion e Integracion", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 18))); // NOI18N
        jPanel2.setLayout(null);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/log.png"))); // NOI18N
        jPanel2.add(jLabel8);
        jLabel8.setBounds(10, 30, 240, 120);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel2.setText("Limites");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(380, 120, 100, 20);

        jLabel3.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel3.setText("Error Permitido");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(330, 240, 120, 40);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel4.setText("Inferior");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(280, 150, 70, 30);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setText("Superior");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(410, 150, 70, 30);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setText("Numero de Puntos");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(10, 190, 120, 40);

        jLabel7.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel7.setText("Numero de Intervalos");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(200, 190, 140, 40);

        jLabel10.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel10.setText("Valor de X");
        jPanel2.add(jLabel10);
        jLabel10.setBounds(10, 240, 70, 40);

        jLabel11.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel11.setText("Valor de H");
        jPanel2.add(jLabel11);
        jLabel11.setBounds(170, 240, 80, 40);

        btnpuntos.setEnabled(false);
        btnpuntos.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                btnpuntosStateChanged(evt);
            }
        });
        jPanel2.add(btnpuntos);
        btnpuntos.setBounds(120, 200, 70, 30);

        TxtFuncion.setEnabled(false);
        jPanel2.add(TxtFuncion);
        TxtFuncion.setBounds(60, 150, 190, 30);

        TxtE.setEnabled(false);
        jPanel2.add(TxtE);
        TxtE.setBounds(430, 240, 70, 30);

        TxtInferior.setEnabled(false);
        jPanel2.add(TxtInferior);
        TxtInferior.setBounds(330, 150, 70, 30);

        TxtSuperior.setEnabled(false);
        jPanel2.add(TxtSuperior);
        TxtSuperior.setBounds(470, 150, 70, 30);

        TxtX.setEnabled(false);
        jPanel2.add(TxtX);
        TxtX.setBounds(80, 240, 70, 30);

        TxtIntervalos.setEnabled(false);
        jPanel2.add(TxtIntervalos);
        TxtIntervalos.setBounds(330, 200, 70, 30);

        TxtH.setEnabled(false);
        jPanel2.add(TxtH);
        TxtH.setBounds(240, 240, 70, 30);

        jLabel12.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel12.setText("Funcion");
        jPanel2.add(jLabel12);
        jLabel12.setBounds(10, 150, 60, 30);

        metodo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un Metodo", "Limites ", "Trapecio", "Simpson" }));
        metodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                metodoActionPerformed(evt);
            }
        });
        jPanel2.add(metodo);
        metodo.setBounds(250, 60, 150, 30);

        jLabel14.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel14.setText("Seleccione el Metodo");
        jPanel2.add(jLabel14);
        jLabel14.setBounds(250, 20, 140, 50);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tecmini.png"))); // NOI18N
        jPanel2.add(jLabel9);
        jLabel9.setBounds(640, 20, 180, 70);

        Ttabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        Ttabla.setCellSelectionEnabled(true);
        jScrollPane1.setViewportView(Ttabla);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(680, 90, 150, 430);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/limpiar.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(10, 280, 40, 40);

        jButton2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jButton2.setText("Calcular");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2);
        jButton2.setBounds(60, 280, 90, 40);

        jToolBar1.setRollover(true);
        jPanel2.add(jToolBar1);
        jToolBar1.setBounds(640, 80, 13, 2);

        buttonGroup1.add(funcionRadio);
        funcionRadio.setText("Por Función");
        funcionRadio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcionRadioActionPerformed(evt);
            }
        });
        jPanel2.add(funcionRadio);
        funcionRadio.setBounds(450, 50, 150, 23);

        buttonGroup1.add(tablaRadio);
        tablaRadio.setText("Por Tabla");
        tablaRadio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tablaRadioActionPerformed(evt);
            }
        });
        jPanel2.add(tablaRadio);
        tablaRadio.setBounds(450, 70, 130, 23);

        resultado.setText("Resultado...");
        resultado.setToolTipText("");
        resultado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resultadoActionPerformed(evt);
            }
        });
        jPanel2.add(resultado);
        resultado.setBounds(10, 500, 160, 30);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jScrollPane2.setViewportView(jTable1);

        jPanel2.add(jScrollPane2);
        jScrollPane2.setBounds(10, 320, 660, 170);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 860, 560);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 10, 860, 540);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnpuntosStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_btnpuntosStateChanged
        ((DefaultTableModel) this.Ttabla.getModel()).setRowCount(Integer.parseInt(btnpuntos.getValue().toString()));
    }//GEN-LAST:event_btnpuntosStateChanged

    private void funcionRadioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcionRadioActionPerformed
        if(metodo.getSelectedIndex() == 2 || metodo.getSelectedIndex() == 3){
            TxtFuncion.setEnabled(true);
            TxtInferior.setEnabled(true);
            TxtSuperior.setEnabled(true);
            TxtIntervalos.setEnabled(true);
             btnpuntos.setEnabled(false);
            TxtH.setEnabled(false);
        }
    }//GEN-LAST:event_funcionRadioActionPerformed

    private void metodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_metodoActionPerformed
       if(metodo.getSelectedIndex() == 1){
           funcionRadio.setEnabled(false);
           tablaRadio.setEnabled(false);
           TxtFuncion.setEnabled(true);
           TxtInferior.setEnabled(false);
           TxtSuperior.setEnabled(false);
           btnpuntos.setEnabled(false);
           TxtIntervalos.setEnabled(false);
           TxtX.setEnabled(true);
           TxtH.setEnabled(false);
           TxtE.setEnabled(true);
       }
       if(metodo.getSelectedIndex() == 2){
            funcionRadio.setEnabled(true);
           tablaRadio.setEnabled(true);
            TxtFuncion.setEnabled(false);
           TxtInferior.setEnabled(false);
           TxtSuperior.setEnabled(false);
           btnpuntos.setEnabled(false);
           TxtIntervalos.setEnabled(false);
           TxtX.setEnabled(false);
           TxtH.setEnabled(false);
           TxtE.setEnabled(false);
       }
       if(metodo.getSelectedIndex() == 3){
            funcionRadio.setEnabled(true);
            tablaRadio.setEnabled(true);
            TxtFuncion.setEnabled(false);
            TxtInferior.setEnabled(false);
            TxtSuperior.setEnabled(false);
            btnpuntos.setEnabled(false);
            TxtIntervalos.setEnabled(false);
            TxtX.setEnabled(false);
            TxtH.setEnabled(false);
            TxtE.setEnabled(false);
       }
    }//GEN-LAST:event_metodoActionPerformed

    private void tablaRadioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tablaRadioActionPerformed
        if(metodo.getSelectedIndex() == 2 || metodo.getSelectedIndex() == 3){
            btnpuntos.setEnabled(true);
            TxtH.setEnabled(true);
            TxtFuncion.setEnabled(false);
            TxtInferior.setEnabled(false);
            TxtSuperior.setEnabled(false);
            TxtIntervalos.setEnabled(false);
        }
    }//GEN-LAST:event_tablaRadioActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
         funcionRadio.setEnabled(false);
           tablaRadio.setEnabled(false);
           TxtFuncion.setText("");
           TxtInferior.setText("");
           TxtSuperior.setText("");
           TxtIntervalos.setText("");
           TxtX.setText("");
           TxtH.setText("");
           TxtE.setText("");
           metodo.setSelectedIndex(0);
           TxtFuncion.setEnabled(false);
           TxtInferior.setEnabled(false);
           TxtSuperior.setEnabled(false);
           btnpuntos.setEnabled(false);
           TxtIntervalos.setEnabled(false);
           TxtX.setEnabled(false);
           TxtH.setEnabled(false);
           TxtE.setEnabled(false);
           resultado.setText("");
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       double y[];
       jTable1.setEnabled(true);
       Ttabla.setEnabled(true);
        if(metodo.getSelectedIndex() == 1){
            Limites lim = new Limites(new Funcion(TxtFuncion.getText()), Double.parseDouble(TxtE.getText()), Double.parseDouble(TxtX.getText()), jTable1);
            double d = lim.solve();
            System.out.println(d);
            resultado.setText(d + "");
            return;
        }
        if(metodo.getSelectedIndex() == 2 && funcionRadio.isSelected()){
            TrapecioFuncion trapeciof = new TrapecioFuncion(TxtFuncion.getText(), Double.parseDouble(TxtInferior.getText()), Double.parseDouble(TxtSuperior.getText()), Double.parseDouble(TxtIntervalos.getText()), jTable1);
            double ans = trapeciof.solve();
            resultado.setText(ans + "");
            return;
        }
        if(metodo.getSelectedIndex() == 2 && tablaRadio.isSelected()){
             y = new double[Integer.parseInt(btnpuntos.getValue().toString()) + 1];

            for (int i = 0; i < Ttabla.getRowCount(); i++) {
                y[i] = Double.parseDouble(Ttabla.getValueAt(i, Ttabla.getColumnCount() - 1) + "");
            }
            TrapecioPuntos tp = new TrapecioPuntos(y, jTable1, Double.parseDouble(TxtH.getText()), Integer.parseInt(btnpuntos.getValue().toString()));
            double ans = tp.solve();
            resultado.setText(ans + "");
        }
        if(metodo.getSelectedIndex() == 3 && funcionRadio.isSelected()){
             SimpsonFuncion simpsonf = new SimpsonFuncion(TxtFuncion.getText(), Double.parseDouble(TxtInferior.getText()), Double.parseDouble(TxtSuperior.getText()), Double.parseDouble(TxtIntervalos.getText()), jTable1);
            double ans = simpsonf.solve();
            resultado.setText(ans + "");
        }
        if(metodo.getSelectedIndex() == 3 && tablaRadio.isSelected()){
             y = new double[Integer.parseInt(btnpuntos.getValue().toString()) + 1];

            for (int i = 0; i < Ttabla.getRowCount(); i++) {
                y[i] = Double.parseDouble(Ttabla.getValueAt(i, Ttabla.getColumnCount() - 1) + "");
            }
            
            SimpsonPuntos sp = new SimpsonPuntos(y, jTable1, Integer.parseInt(btnpuntos.getValue().toString()), Double.parseDouble(TxtH.getText()));
            double ans = sp.solve();
            resultado.setText(ans + "");
        }
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void resultadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resultadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_resultadoActionPerformed
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Diferenciacion().setVisible(true);
            }
        });
        
    }
    
    /*public void Limites()
    {
       // try{
        
        String f = Leer.funcion(TxtFuncion.getText().toString());
        double x = Double.parseDouble(TxtX.getText().toString());
        double e = Double.parseDouble(TxtE.getText().toString());
	
        resultado.setText("n\th\tk\tx+h\tf(x+h)\tf(x)\tf(x+h)-f(x)\tf'(x)\te\n");
	
	int n = 1;
	double k, d , delta = 0, h;
	double da = 1 * Math.pow(10, 10);
		
	do
        {
            h = Math.pow(0.1, n);
            k=1 / h;
            d = (Leer.eval(f, x+h) - Leer.eval(f, x)) * k;
            delta = Math.abs(da - d);
            resultado.append("\n"+n+"\t"+MyRandom.Redondear(h, 4)+"\t"+MyRandom.Redondear(k,4)+"\t"+MyRandom.Redondear((x+h),4)+"\t"+ MyRandom.Redondear(Leer.eval(f, x+h),4)+"\t"+MyRandom.Redondear(Leer.eval(f, x),4)+"\t"+MyRandom.Redondear((Leer.eval(f, x+h)-Leer.eval(f, x)),4)+"\t"+MyRandom.Redondear((Leer.eval(f, x+h)-Leer.eval(f, x))*k,4)+"\t"+MyRandom.Redondear((da-d),4));
            da = d;
            n++;
        }while(delta > e);
        
      //  }catch(Exception e){
      //      JOptionPane.showMessageDialog(null, "Ocurrió un error. Es posible que existan campos vacíos\n o los datos introducidos no sean válidos.");

      // }
    }*/
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Ttabla;
    private javax.swing.JTextField TxtE;
    private javax.swing.JTextField TxtFuncion;
    private javax.swing.JTextField TxtH;
    private javax.swing.JTextField TxtInferior;
    private javax.swing.JTextField TxtIntervalos;
    private javax.swing.JTextField TxtSuperior;
    private javax.swing.JTextField TxtX;
    private javax.swing.JSpinner btnpuntos;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.JRadioButton funcionRadio;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JComboBox<String> metodo;
    private javax.swing.JTextField resultado;
    private javax.swing.JRadioButton tablaRadio;
    // End of variables declaration//GEN-END:variables
}
