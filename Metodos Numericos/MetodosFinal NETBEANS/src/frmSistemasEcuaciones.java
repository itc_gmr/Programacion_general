import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
public class frmSistemasEcuaciones extends javax.swing.JFrame {
     private DefaultTableModel Model=new  DefaultTableModel();
     int incognitas;
    public int getIncognitas() {
        return incognitas;
    }
    public void setIncognitas(int incognitas) {
        this.incognitas = incognitas;
    }
    public frmSistemasEcuaciones() {
        initComponents();
        setSize(900,600); 
        setLocationRelativeTo(null);
        txtError.disable();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtResolver = new javax.swing.JButton();
        btngenerar = new javax.swing.JButton();
        txtincognitas = new javax.swing.JTextField();
        cmbmetodo = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtError = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtIteraciones = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        Ttabla = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtaSolucion = new javax.swing.JTextArea();
        txtLimpiar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Solucion de Sistemas de Ecuaciones");
        getContentPane().setLayout(null);

        jPanel1.setLayout(null);

        jLabel1.setText("Seleccione el  metodo para resolver el sistema: ");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(10, 120, 270, 40);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/log.png"))); // NOI18N
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 0, 240, 120);

        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jPanel2.setLayout(null);

        jLabel4.setText("Error");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(10, 260, 50, 20);

        txtResolver.setText("Resolver el Sistema");
        txtResolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtResolverActionPerformed(evt);
            }
        });
        jPanel2.add(txtResolver);
        txtResolver.setBounds(10, 480, 190, 40);

        btngenerar.setText("Generar la Matriz Aumentada");
        btngenerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngenerarActionPerformed(evt);
            }
        });
        jPanel2.add(btngenerar);
        btngenerar.setBounds(10, 430, 190, 40);

        txtincognitas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtincognitasKeyTyped(evt);
            }
        });
        jPanel2.add(txtincognitas);
        txtincognitas.setBounds(10, 230, 100, 30);

        cmbmetodo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Metodo Gauss", "Metodo Gauss Jordan", "Metodo Montante", "Metodo Cramer", "Metodo Jacobi", "Metodo Gauss Seidel" }));
        cmbmetodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbmetodoActionPerformed(evt);
            }
        });
        jPanel2.add(cmbmetodo);
        cmbmetodo.setBounds(10, 160, 180, 30);

        jLabel2.setText("Numero de incognitas");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(10, 200, 150, 30);

        txtError.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtErrorKeyTyped(evt);
            }
        });
        jPanel2.add(txtError);
        txtError.setBounds(10, 280, 130, 30);

        jLabel6.setText("Iteraciones");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(10, 320, 70, 20);
        jPanel2.add(txtIteraciones);
        txtIteraciones.setBounds(10, 340, 100, 30);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 280, 530);

        Ttabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "x"
            }
        ));
        jScrollPane1.setViewportView(Ttabla);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(290, 220, 580, 310);

        txtaSolucion.setEditable(false);
        txtaSolucion.setColumns(20);
        txtaSolucion.setRows(5);
        jScrollPane2.setViewportView(txtaSolucion);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(510, 10, 360, 200);

        txtLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/limpiar.png"))); // NOI18N
        txtLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLimpiarActionPerformed(evt);
            }
        });
        jPanel1.add(txtLimpiar);
        txtLimpiar.setBounds(460, 160, 40, 50);

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel3.setText("Procedimiento y Solucion");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(290, 80, 280, 40);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tecmini.png"))); // NOI18N
        jPanel1.add(jLabel9);
        jLabel9.setBounds(290, 10, 180, 70);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 10, 880, 540);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void txtResolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtResolverActionPerformed
        txtaSolucion.setText("");
        String Met = (String)cmbmetodo.getSelectedItem();
        switch(Met){
            case "Metodo Gauss":
                Gauss();
            break;
            case "Metodo Gauss Jordan":
                GaussJ();
            break;
            case "Metodo Montante":
                Montante();
            break;
            case "Metodo Cramer":
                Cramer();
            break;
            case "Metodo Jacobi":
                Jacobi();
            break;
            case "Metodo Gauss Seidel":
                GaussS();
            break;
        }
    }//GEN-LAST:event_txtResolverActionPerformed
    private void txtLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLimpiarActionPerformed
        txtaSolucion.setText("");
        Ttabla.setModel(new DefaultTableModel());
    }//GEN-LAST:event_txtLimpiarActionPerformed
    private void btngenerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngenerarActionPerformed
        try{
            boolean ban=false;
            this.setIncognitas(Integer.parseInt(txtincognitas.getText()));
            incognitas=this.getIncognitas();
            if(incognitas<=1){
                throw new Exception("El numero de incognitas es invalido");
            }
            Object col[]= new Object[incognitas+1];
            for(int i=0; i<incognitas+1; i++){
                if(i<incognitas){
                    col[i]="X"+(i+1);
                }else{
                    col[i]="b";
                }
            }
            Model=new  DefaultTableModel(col,incognitas);
            Ttabla.setModel(Model);
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }//GEN-LAST:event_btngenerarActionPerformed
    private void cmbmetodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbmetodoActionPerformed
        txtaSolucion.setText("");
        String Met = (String)cmbmetodo.getSelectedItem();
        switch(Met){
            case "Metodo Jacobi":
                txtError.disable();
                Jacobi();
            break;
            case "Metodo Gauss Seidel":
                txtError.enable();
            break;
            case "Metodo Gauss":
                txtError.disable();
            break;
            case "Metodo Gauss Jordan":
                txtError.disable();
            break;
            case "Metodo Montante":
                txtError.disable();
            break;
            case "Metodo Cramer":
                txtError.disable();
            break;
        }
    }//GEN-LAST:event_cmbmetodoActionPerformed

    private void txtincognitasKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtincognitasKeyTyped
        char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }         // TODO add your handling code here:
    }//GEN-LAST:event_txtincognitasKeyTyped

    private void txtErrorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtErrorKeyTyped
        char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          } 
    }//GEN-LAST:event_txtErrorKeyTyped
        public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new frmSistemasEcuaciones().setVisible(true);
            }
        }
        );
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Ttabla;
    private javax.swing.JButton btngenerar;
    private javax.swing.JComboBox<String> cmbmetodo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField txtError;
    private javax.swing.JTextField txtIteraciones;
    private javax.swing.JButton txtLimpiar;
    private javax.swing.JButton txtResolver;
    private javax.swing.JTextArea txtaSolucion;
    private javax.swing.JTextField txtincognitas;
    // End of variables declaration//GEN-END:variables
//******************METODOS ADICIONALES PARA RESOLVER LOS SISTEMAS********************* 
    //******************METODO RESOLVER EL SISTEMA POR EL METODO DE GAUSS********************* 
    private void Gauss(){
        int n=0,m=0; 
	double matriz[][],t,suma,k[]; 
	m=this.getIncognitas();
	n=m+1;
	matriz=new double[m][n]; 
	k=new  double[m];
	for(int x=0;x<m;x++) 
	{ 
            for(int y=0;y<n;y++) 
            { 
                matriz[x][y] = Double.parseDouble(String.valueOf(Ttabla.getValueAt(x, y)));	                 
            } 
	}
	txtaSolucion.append("\n****************Matriz original****************\n");
	for(int x=0;x<m;x++) 
	{ 
            for(int y=0;y<n;y++) 
                txtaSolucion.append(matriz[x][y]+"\t"); 
            txtaSolucion.append("\n");
	} 
	for(int x=0;x<m-1;x++) 
	{ 
            for(int y=x+1;y<m;y++) 
            {
                t=matriz[y][x]/matriz[x][x];
		for(int j=x;j<n;j++) 
		{
                    matriz[y][j]=matriz[y][j]-t*matriz[x][j];
		}
            }
	}
	txtaSolucion.append("\n***************Matriz de Solucion***************\n"); 
	for(int x=0;x<m;x++) 
	{ 
            for(int y=0;y<n;y++) 
                txtaSolucion.append(" "+matriz[x][y]+"\t"); 
            txtaSolucion.append("\n");
	} 
	k[m-1]=matriz[m-1][n-1]/matriz[m-1][m-1];
	for(int x=m-1;x>=0;x--) 
        {
            suma=0;
            for(int y=x+1;y<m;y++) 
            {
		suma=suma+matriz[x][y]*k[y];
            }
            k[x]=(matriz[x][n-1]-suma)/matriz[x][x];
	}
	txtaSolucion.append("***************Soluciones***************\n"); 
	for(int x=0;x<m;x++) 
	{ 
            txtaSolucion.append("solucion de X "+(x+1)+" es: "+k[x]); 
            txtaSolucion.append("\n");
	}
    }
    //******************METODO RESOLVER EL SISTEMA POR EL METODO DE GAUSS JORDAN********************* 
    private void GaussJ(){
        try {  
            int n = this.getIncognitas();
            double m[][] = new double[n][n];
            double r[] = new double[n];
            for (int i = 0; i < n; i++) {
                for (int k = 0; k < n; k++) {
                    m[i][k] = Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, k)));
                }
                r[i] = Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, n)));
            }
            txtaSolucion.append("\n****************Matriz original****************\n");
            for(int x=0;x<n;x++) 
            { 
                for(int y=0;y<n;y++) 
                    txtaSolucion.append(m[x][y]+"\t"); 
                    txtaSolucion.append(r[x]+"\t"); 
                    txtaSolucion.append("\n");
            }
            r = this.ResolverGJ(m, r);
            txtaSolucion.append("\n***************Matriz de Solucion***************\n"); 
            for(int x=0;x<n;x++) 
            { 
            for(int y=0;y<n;y++) 
                txtaSolucion.append(" "+m[x][y]+"\t"); 
                txtaSolucion.append("\n");
            }
            txtaSolucion.append("Solucion de las incognitas");
            for(int i=0; i<n;i++){
                txtaSolucion.append("X"+(i+1)+" = "+r[i]+"\n");
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error en la lectura de datos"); 
        }      
    }
    //******************METODO RESOLVER EL SISTEMA POR EL METODO DE MONTANTE********************* 
    private void Montante(){
        int n,m=0;
	double deter;
	double mat[][];
	double pivoteant=1;
	m=this.getIncognitas();
	n=m+1;
	mat=new double[m][n];
	for(int i=0;i<mat.length;i++){
            for(int j=0;j<mat[0].length;j++){
                mat[i][j] = Double.parseDouble(String.valueOf(Ttabla.getValueAt(i, j)));
            }
	}		
	txtaSolucion.append("*******Matriz Inicial*******\n");
	Imprime(mat);
        txtaSolucion.append("\n");
	pivoteant=1;
	for(int k=0; k<m;k++){
            for(int i=0;i<m;i++){ 
		if(i!=k){
                    for(int j=m;j>=0; j--)
			mat[i][j]=(mat[k][k]*mat[i][j]-mat[i][k]*mat[k][j])/pivoteant;
		}
            }
            pivoteant=mat[k][k];
	}
	txtaSolucion.append("*******Matriz Final*******\n");
	Imprime(mat);
        txtaSolucion.append("\n");
	deter=mat[0][0];
	for(int i=0; i<m; i++){
            for(int j=0; j<m; j++){
		if(i==j){
                    mat[i][j]=mat[i][j]/deter;
                    mat[i][m]=mat[i][m]/deter;
		}
            }
	}
	txtaSolucion.append("\nLa solución es: \n");
	for(int i=0; i<m; i++){
            txtaSolucion.append("x "+i+"= "+mat[i][m]);
            txtaSolucion.append("\n");
	}	
    }
    public void Imprime(double mat[][]){
        for(int i=0;i<mat.length;i++){
            for(int j=0;j<mat[0].length;j++){          
                txtaSolucion.append(" "+mat[i][j]+" ");
            }
            txtaSolucion.append("\n");
        }
    }
    //******************METODO RESOLVER EL SISTEMA POR EL METODO DE CRAMER********************* 
    private void Cramer(){
        int tam = 0;
	tam=this.getIncognitas();
	int[][] a = new int[tam][tam];
	int[]  b = new int[tam];
	float[] cram = new float[tam];
	CrainsOr(a);
	CrainsRes(b);
	txtaSolucion.append("El determinante de la matriz es: "+ determinante(a) + " ");
	cram=Crasolucion(a,b);
	txtaSolucion.append("Los valores de solucion de X son: \n");
	Cramostrar(cram);
        txtaSolucion.append("\n");
        txtaSolucion.append("La Matriz Original es: \n");
        txtaSolucion.append("\n");
	for (int x = 0; x < tam; x++) {
            for (int y = 0; y < tam; y++){
                txtaSolucion.append("     " + a[x][y]);
                 
            }
           txtaSolucion.append("     "+b[x]+" \n");
	}
    }
    
    public void CrainsOr(int a[][]){
        for(int i=0; i<a.length; i++){
            for (int j=0; j<a[i].length;j++){
               a[i][j]=Integer.parseInt(String.valueOf(Ttabla.getValueAt(i, j)));
            }
	}	
    }
    public  void CrainsRes(int a[]){
	for(int i=0; i<a.length; i++){		
            a[i]=Integer.parseInt(String.valueOf(Ttabla.getValueAt(i, this.getIncognitas())));
	}	
    }
    
    public static int determinante(int[][] matriz)
{
    int det;
    if(matriz.length==2)
    {
        det=(matriz[0][0]*matriz[1][1])-(matriz[1][0]*matriz[0][1]);
        return det;
    }
    int suma=0;
    for(int i=0; i<matriz.length; i++){
    int[][] nm=new int[matriz.length-1][matriz.length-1];
        for(int j=0; j<matriz.length; j++){
            if(j!=i){
                for(int k=1; k<matriz.length; k++){
                int indice=-1;
                if(j<i)
                indice=j;
                else if(j>i)
                indice=j-1;
                nm[indice][k-1]=matriz[j][k];
                }
            }
        }
        if(i%2==0)
        suma+=matriz[i][0] * determinante(nm);
        else
        suma-=matriz[i][0] * determinante(nm);
    }
    return suma;
}
    
    public  int [][] Crasustituye(int a[][], int b[], int pos){
        int c[][] =new int[a.length][a.length];
        for(int i=0;i<a.length;i++){
            for(int j=0; j<a[i].length; j++){
		if(j==pos){
                    c[i][j]=b[i];
		}
		else{
                    c[i][j]=a[i][j];	
                }
            }	
	}		
        return c;		
    }
    public  int Crasuma(int a[]){
	int result=0;
	for(int i=0; i<a.length; i++){
            result=result+a[i];
	}
	return result;				
    }
    public  float[] Crasolucion(int a[][], int b[]){
	float Rcramer[]=new float[b.length];
	int det=determinante(a);
	if(det==0){
            txtaSolucion.append("No tiene solucion\n");			
	return Rcramer;
	}
	int detTemp;
	int c[][]=new int[a.length][a.length];
	for(int i=0; i<a.length; i++){
            c=Crasustituye(a,b,i);
            detTemp=determinante(c);		
            Rcramer[i]=(float)detTemp/(float)det;			
	}            
	return Rcramer;
    }
    public  void Cramostrar(float a[]){
	for(int i=0; i<a.length; i++){
            txtaSolucion.append("El resultado de X_"+i+" es: "+a[i]);
            txtaSolucion.append("\n");
	}
    }
    //******************METODO RESOLVER EL SISTEMA POR EL METODO DE JACOBI********************* 
    private void Jacobi(){
        MJacobi Obj=new MJacobi();
        Obj.show();
    }
    //******************METODO RESOLVER EL SISTEMA POR EL METODO DE GAUSS SEIDEL********************* 
    private void GaussS(){
    	int m=this.getIncognitas();
    	int n=m+1;
    	double error;
        error=Double.parseDouble(String.valueOf(txtError.getText()));
    	double a[][]=new double[m][n];
    	for(int  i=0;i<m;i++)
    	{
    		for(int j=0;j<n;j++)
    		{
    			a[i][j]=Integer.parseInt(String.valueOf(Ttabla.getValueAt(i,j)));
    		}
    	}
		 m=a.length;
    	double x[] = new double[m];
    	double y;
    	double delta=0;
    	boolean fin;
	for(int i=0; i<m;i++){
            x[i]=0;
	}
	do{
            fin=true;
            for(int i=0;i<m;i++){
                y=a[i][m];	
                for(int j=0;j<m;j++){
                    if(i!=j){
                        y=y-a[i][j]*x[j];
                    }
                }
                y=y/a[i][i];
                delta=Math.abs(x[i]-y);
                if(delta>error){
                    fin=false;
                }
		x[i]=y;
            }
	}while(!fin);		
        txtaSolucion.append("Soluciones: ");
	for(int i=0;i<m;i++){
            txtaSolucion.append("X1= "+x[i]+"\n");
	}
    }
    //******************METODO RESOLVER EL SISTEMA POR EL METODO DE GAUSS JORDAN********************* 
private double[] ResolverGJ(double[][] m, double[] r) {
       int n =this.getIncognitas();
        for (int i = 0; i <n; i++) {
            double d, c = 0;
            d = m[i][i];
            for (int j = 0; j <n; j++) {
                m[i][j] = ((m[i][j]) / d);
            }
            r[i] = ((r[i]) / d);
            for (int x = 0; x <n; x++) {
                if (i != x) {
                    c = m[x][i];
                    for (int y = 0; y <n; y++) {
                        m[x][y] = m[x][y] - c * m[i][y];
                    }
                    r[x] = r[x] - c * r[i];
                }
            }
        }
        return r;
  }
}