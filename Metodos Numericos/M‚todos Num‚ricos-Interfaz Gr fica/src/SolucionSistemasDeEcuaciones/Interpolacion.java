package SolucionSistemasDeEcuaciones;
import MetNum.MetodosNumericos;
import Tablas.TablaInterpolacion;
import javax.swing.JOptionPane;

public class Interpolacion extends javax.swing.JFrame {
    public Interpolacion() {
        super("Interpolacion");
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new MetNum.JPanelConFondo();
        jLabel1 = new javax.swing.JLabel();
        Ecuaciones = new javax.swing.JTextField();
        Crear = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        Panel.setImagen("/ima/5.jpg");

        jLabel1.setText("Numero de puntos");

        Ecuaciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                EcuacionesKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                EcuacionesKeyTyped(evt);
            }
        });

        Crear.setText("Crear Matriz");
        Crear.setEnabled(false);
        Crear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CrearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                .addContainerGap(46, Short.MAX_VALUE)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(Ecuaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(Crear, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(Ecuaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Crear, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void EcuacionesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EcuacionesKeyTyped

    }//GEN-LAST:event_EcuacionesKeyTyped

    private void EcuacionesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EcuacionesKeyReleased
        if(Ecuaciones.getText().length()>0)
            Crear.setEnabled(true);
        else
            Crear.setEnabled(false);
    }//GEN-LAST:event_EcuacionesKeyReleased

    private void CrearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CrearActionPerformed
        try{
            int n = Integer.parseInt(Ecuaciones.getText()); 
            if(n<2)
            {
                JOptionPane.showMessageDialog(null, "Minimo Dos Puntos", "Error ", 0);
                Ecuaciones.setText("");
                return;
            }
                JOptionPane.showMessageDialog(null, "Llene toda la tabla y de enter para terminar", "Instrucciones", 2);
            MetodosNumericos.TI = new TablaInterpolacion(); // crear tabla
//agregar (X, Y)  columnas             
            MetodosNumericos.TI.AgregarColumna("X"); 
            MetodosNumericos.TI.AgregarColumna("Y"); 
//borrar todos los renglones Default
            for(int i=0; i<=n; i++)
                MetodosNumericos.TI.BorrarRenglon();
//agregar (n) renglones 
            for(int i=0; i<n; i++)
                MetodosNumericos.TI.AgregarRenglon();
            if(n==1)
                MetodosNumericos.TI.BorrarRenglon();
//---------------Dar Tamaño a Ventana------------------------------------------------
            if(n<5)
                MetodosNumericos.TI.setSize(150,  ((int)((n-1)*(13.5))) + ((103-(n-1)) + ((n-1)*(n-1))));
            else
                MetodosNumericos.TI.setSize(150, ((int)((n-1)*(13.5))) + ((98-(n-1)) + ((n-1)*(n-1))));
//-----------------------------------------------------------------------------------
            // borrar tabla------
            for(int i=0; i<TablaInterpolacion.jTable1.getRowCount(); i++)
                for(int j=0; j<TablaInterpolacion.jTable1.getColumnCount(); j++)
                    TablaInterpolacion.jTable1.setValueAt("", i, j);
            //-------------------
            
            if (!MetodosNumericos.TI.isVisible()) {
                MetodosNumericos.TI.setVisible(true);
            }
            
        }
        catch(NumberFormatException e)
        {
            JOptionPane.showMessageDialog(null, "Debe teclear un numero entero", "Error ", 0);
            Ecuaciones.setText("");
        }
    }//GEN-LAST:event_CrearActionPerformed
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interpolacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interpolacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interpolacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interpolacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new Interpolacion().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Crear;
    public static javax.swing.JTextField Ecuaciones;
    public static MetNum.JPanelConFondo Panel;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}