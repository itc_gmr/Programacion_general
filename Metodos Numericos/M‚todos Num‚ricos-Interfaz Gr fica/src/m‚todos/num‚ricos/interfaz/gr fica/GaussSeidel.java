/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LuisFernando
 */
public class GaussSeidel {

    int m;
    JTable table;
    double matriz[][];
    double vss[];
    double error;
    int iteraciones;

    public GaussSeidel(int m, JTable table, double error, int iteraciones) {
        this.m = m;
        this.table = table;
        this.error = error;
        this.iteraciones = iteraciones;
        matriz = new double[m][m + 1];
        vss = new double[m];
    }

    public double[] solve(JTable table) {
        String columnas[] = {"Iteraciones", "X1", "X2", "X3", "X1", "X2", "X3"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        String valores = "";
        String masc = "##0.0####";
        for (int i = 0; i < m; i++) {
            for (int j = 0; j <= m; j++) {
                matriz[i][j] = Double.parseDouble(this.table.getValueAt(i, j) + "");
            }
        }
        vss = valoresiniciales();

        @SuppressWarnings("unused")
        double pivote, delta, cero = 0;
        boolean fin;
        int itera;
        double[] y = new double[m];
        itera = 1;
        double[] x = vss;
        //System.out.println("I    \t    X1    \t    X2    \t    X3    \t    X1    \t    X2    \t    X3    ");
        do {
            fin = true;

            //System.out.print(Formatea.alinder("0", itera));
            valores += Formatea.alinder("0", itera) + ",";
            for (int k = 0; k <= m - 1; k++) {
                //System.out.print("\t" + (Formatea.alinder(masc, x[k])));
                valores += (Formatea.alinder(masc, x[k])) + ",";
            }
            for (int i = 0; i <= m - 1; i++) {
                y[i] = matriz[i][m];
                for (int j = 0; j <= m - 1; j++) {
                    if (i != j) {
                        y[i] = y[i] - matriz[i][j] * x[j];
                    }
                }
                y[i] = y[i] / matriz[i][i];
                delta = Math.abs(x[i] - y[i]);
                if (delta <= this.error) {
                    fin = false;
                }
                x[i] = y[i];
            }

            for (int i = 0; i <= m - 1; i++) {
                //System.out.print("\t" + (Formatea.alinder(masc, x[i])));
                if (i != m) {
                    valores += (Formatea.alinder(masc, x[i])) + ",";
                }
                if (i == m) {
                    valores += (Formatea.alinder(masc, x[i])) + "";
                }
            }
            //System.out.println();
            String filas[] = valores.split(",");
            dtm.addRow(filas);
            valores = "";
            itera++;
        } while (fin == true && iteraciones > itera);
        table.setModel(dtm);
        return x;

    }

    public double[] valoresiniciales() {
        double[] x = new double[m];
        for (int i = 0; i < m - 1; i++) {
            x[i] = 0;
        }
        return x;
    }

    public static double Determinante(double[][] matriz) {
        double det;
        if (matriz.length == 2) {
            det = (matriz[0][0] * matriz[1][1]) - (matriz[1][0] * matriz[0][1]);
            return det;
        }
        double suma = 0;
        for (int i = 0; i < matriz.length; i++) {
            double[][] nm = new double[matriz.length - 1][matriz.length - 1];
            for (int j = 0; j < matriz.length; j++) {
                if (j != i) {
                    for (int k = 1; k < matriz.length; k++) {
                        int indice = -1;
                        if (j < i) {
                            indice = j;
                        } else if (j > i) {
                            indice = j - 1;
                        }
                        nm[indice][k - 1] = matriz[j][k];
                    }
                }
            }
            if (i % 2 == 0) {
                suma += matriz[i][0] * Determinante(nm);
            } else {
                suma -= matriz[i][0] * Determinante(nm);
            }
        }

        return suma;
    }

}
