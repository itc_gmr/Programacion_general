/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import org.nfunk.jep.*;
//import org.lsmp.djep.djep.DJep;
import org.nfunk.jep.Node;
import org.nfunk.jep.ParseException;

public class Funcion {

    JEP j = new JEP();
    String funcion;
    Funcion derivada;

    public Funcion(String funcion) {
        this.funcion = funcion;
        j.addVariable("x", 0);
        j.addStandardConstants();
        j.addStandardFunctions();
        j.parseExpression(funcion);
        if (j.hasError()) {
            System.out.println(j.getErrorInfo());
        }

    }

    public double calc(double x) {
        double r;
        j.addVariable("x", x);
        r = j.getValue();
        if (j.hasError()) {
            System.out.println(j.getErrorInfo());
        }
        return r;
    }
    /**
    public double valorDerivada(double x) {
        double r;

        DJep k = new DJep();//DJep es la clase encargada de la derivacion en su escencia

        k.addStandardConstants();//agrega constantes estandares, pi, e, etc
        k.addStandardFunctions();//agrega funciones estandares cos(x), sin(x)
        k.addComplex();//por si existe algun numero complejo
        k.setAllowUndeclared(true);//permite variables no declarables
        k.setAllowAssignment(true);//permite asignaciones
        k.setImplicitMul(true);//regla de multiplicacion o para sustraccion y sumas
        k.addStandardDiffRules();
        String derivada = null;

        try {
            Node node = k.parse(funcion);//coloca el nodo con una funcion preestablecida
            Node diff = k.differentiate(node, "x");//deriva la funcion con respecto a x
            Node simp = k.simplify(diff);//Simplificamos la funcion con respecto a x
            derivada = k.toString(simp); //Convertimos el valor simplificado en un String

        } catch (ParseException e) {
            e.getStackTrace();
        }
        this.derivada = new Funcion(derivada);
        r = this.derivada.calc(x);
        /*k.addVariable("x", x); //evaluamos la derivada en el punto x
        r = k.getValue();// contenemos el valor de f'(x)
        if (k.hasError()) {
            JOptionPane.showMessageDialog(null, " error  al convertir la funcion");
        }*/
       // return r;// retornamos f'(x)
    //}
}
