package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;

/**
 *
 * @author LuisFernando
 */
public class Crammer {

    int m, n;
    JTable table;
    public Crammer(int Cm, JTable table) {
        m = Cm;
        n = m + 1;
        this.table=table;
    }

    public double[][] LeerMatriz() {// NO TE OCUPO
        @SuppressWarnings("resource")
        //Scanner Leer = new Scanner(System.in);
        // double [] solucion = new double[m];
        // double [][] duplimatr = new double[m][m];
        double[][] matr = new double[m][m];
        for (int i = 0; i <= m - 1; i++) {
            //System.out.println("Teclee los coeficientes de la ecuación " + (i + 1));
            for (int j = 0; j <= m - 1; j++) {
                //System.out.println("Teclee el elemento a [" + (i + 1) + "," + (j + 1) + "]: ");
                matr[i][j] = Double.parseDouble(table.getValueAt(i, j)+"");
                // duplimatr[i][j]= matr[i][j];
            }
        }
        return matr;
    }

    public double[] LeerVectorR() {// NO TE OCUPO
        @SuppressWarnings("resource")
        //Scanner Leer = new Scanner(System.in);
        double[] solucion = new double[m];

        //System.out.println("Teclee los valores del vector de valores independientes: ");
        for (int i = 0; i <= m - 1; i++) {
            //System.out.println("Teclee el elemento a [" + (i + 1) + "]: ");
            solucion[i] = Double.parseDouble(table.getValueAt(i, m)+"");
        }
        return solucion;
    }

    public double[][] duplicarMatriz(double[][] matriz) {// YA TE PASÉ

        double[][] duplimatr = new double[m][m];
        for (int i = 0; i <= m - 1; i++) {
            for (int j = 0; j <= m - 1; j++) {
                duplimatr[i][j] = matriz[i][j];
            }
        }
        return duplimatr;
    }

    public void desplegamatriz(double[][] matriz, double[] solucion) {// POSIBLEMENTE
        // TE
        // OCUPE
        String masc = "##0.0####";
        System.out.println();
        for (int i = 0; i <= m - 1; i++) {
            for (int j = 0; j <= m - 1; j++) {
                System.out.print(Formatea.alinder(masc, matriz[i][j]) + "	");
            }
            System.out.print(Formatea.alinder(masc, solucion[i]) + "	");
            System.out.println();
        }

    }

    public void desplegamatriz(double[][] matriz) {// NO TE OCUPO
        String masc = "##0.0####";
        System.out.println();
        for (int i = 0; i <= m - 1; i++) {
            for (int j = 0; j <= m - 1; j++) {
                System.out.print(Formatea.alinder(masc, matriz[i][j]) + "	");
            }
            System.out.println();
        }

    }

    public void desplegamatriz(double[] matriz) {// POSIBLEMENTE TE OCUPE
        String masc = "##0.0####";
        System.out.println();
        for (int i = 0; i <= m - 1; i++) {
            System.out.println("Valor de X" + (i + 1) + ": " + Formatea.alinder(masc, matriz[i]) + "	");
        }
    }

    public static double Determinante(double[][] matriz) {
        double det;
        if (matriz.length == 2) {
            det = (matriz[0][0] * matriz[1][1]) - (matriz[1][0] * matriz[0][1]);
            return det;
        }
        double suma = 0;
        for (int i = 0; i < matriz.length; i++) {
            double[][] nm = new double[matriz.length - 1][matriz.length - 1];
            for (int j = 0; j < matriz.length; j++) {
                if (j != i) {
                    for (int k = 1; k < matriz.length; k++) {
                        int indice = -1;
                        if (j < i) {
                            indice = j;
                        } else if (j > i) {
                            indice = j - 1;
                        }
                        nm[indice][k - 1] = matriz[j][k];
                    }
                }
            }
            if (i % 2 == 0) {
                suma += matriz[i][0] * Determinante(nm);
            } else {
                suma -= matriz[i][0] * Determinante(nm);
            }
        }

        return suma;
    }

    @SuppressWarnings("unused")
    public double ResolverMatriz(double[][] matriz, double[] solucion, int vn, double deterA) {
        double[] resol = new double[m];
        double deter, resolucion;

        for (int j = 0; j <= m - 1; j++) {
            matriz[j][vn] = solucion[j];
        }
        System.out.println("Matriz A" + (vn + 1) + ": ");
        desplegamatriz(matriz);

        deter = Determinante(matriz);
        System.out.println("Determinante A" + (vn + 1) + ": " + deter);

        return deter / deterA;
    }
}
