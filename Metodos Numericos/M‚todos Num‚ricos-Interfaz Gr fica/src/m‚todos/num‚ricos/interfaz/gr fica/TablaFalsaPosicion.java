/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author LuisFernando
 */
public class TablaFalsaPosicion {

    String funcion;
    double x1;
    double x2;
    int iteraciones;
    double error;
    JTable table;
    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");

    public TablaFalsaPosicion(String funcion, double x1, double x2, int iteraciones, double error, JTable table) {
        this.funcion = funcion;
        this.x1 = x1;
        this.x2 = x2;
        this.iteraciones = iteraciones;
        this.error = error;
        this.table = table;
    }

    public double pasarValores(JTable table) {
        String columnas[] = {"Iteraciones", "Xi", "F(Xi)", "Xi-X1", "F(Xi)-F(X1)", "X(I+1)", "Error", "F(Xi+1)"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        String masc = "##0.0####";
        String nulo = "-";
        iteraciones = 30;
        double Xi, fxi, x1 = this.x1, fx1 = Expr(x1), Xi1 = x1;
        int i = 0; // numero de iteraciones
        double eps = 1;

        do {
            i++;

            if (i == 1) {
                Xi = x1;
                fxi = Expr(Xi);
                //System.out.println(Formatea.alinder("0", i) + "\t"
                //        + (Formatea.alinder(masc, Xi) + "\t" + Formatea.alinder(masc, fxi) + "\t"
                //        + Formatea.alinder(masc, (Xi - x1)) + "\t" + Formatea.alinder(masc, (fxi - fx1))
                //        + "\t\t" + nulo + "\t\t" + nulo + "\t\t" + nulo));
                String[] filas = {Formatea.alinder("0", i) + "", Formatea.alinder(masc, Xi) + "", Formatea.alinder(masc, fxi) + "", Formatea.alinder(masc, (Xi - x1)) + "", Formatea.alinder(masc, (fxi - fx1)) + "", nulo + "", nulo + "", nulo + ""};
                dtm.addRow(filas);

            } else {
                Xi = x2;
                fxi = Expr(Xi);

                Xi1 = x2 - ((Expr(x2) * (x2 - x1)) / (Expr(x2) - Expr(x1)));
                eps = Math.abs(Xi1 - x2);

                //System.out.println(Formatea.alinder("0", i) + "\t"
                //      + (Formatea.alinder(masc, Xi) + "\t" + Formatea.alinder(masc, fxi) + "\t"
                //    + Formatea.alinder(masc, (Xi - x1)) + "\t" + Formatea.alinder(masc, (fxi - fx1)) + "\t"
                //  + Formatea.alinder(masc, (Xi1)) + "\t" + Formatea.alinder(masc, eps))
                //+ "\t" + Formatea.alinder(masc, Expr(Xi1)));
                String[] filas = {Formatea.alinder("0", i) + "", Formatea.alinder(masc, Xi) + "", Formatea.alinder(masc, fxi) + "", Formatea.alinder(masc, (Xi - x1)) + "", Formatea.alinder(masc, (fxi - fx1)) + "", Formatea.alinder(masc, (Xi1)) + "", Formatea.alinder(masc, eps) + "", Formatea.alinder(masc, Expr(Xi1)) + ""};
                dtm.addRow(filas);
                x2 = Xi1;

            }
        } while (Expr(x2) != 0 && i <= 30 && eps > 0.001);
        System.out.println(Expr(x2));
        System.out.println(i);
        System.out.println(eps);
        table.setModel(dtm);
        return Xi;

        //for(int i=0; i<30; i++) {
        //  String filas [] = {"","","","","","","",""};
        //dtm.addRow(filas);
        //}
        //table.setModel(dtm);
    }

    private double Expr(double x, String f) {
        engine.put("x", x);
        Object aux;
        try {
            aux = engine.eval(f);
            return Double.parseDouble(aux.toString());
        } catch (ScriptException | NumberFormatException e) {
        }
        return 0;
    }

    private double Expr(double x) {
        engine.put("x", x);
        Object aux;
        try {
            aux = engine.eval(funcion);
            return Double.parseDouble(aux.toString());
        } catch (ScriptException | NumberFormatException e) {
        }
        return 0;
    }
}
