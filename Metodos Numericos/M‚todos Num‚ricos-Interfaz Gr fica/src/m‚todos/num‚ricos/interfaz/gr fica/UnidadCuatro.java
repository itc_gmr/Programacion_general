package métodos.numéricos.interfaz.gráfica;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 * @author LuisFernando
 */
public class UnidadCuatro extends javax.swing.JFrame {

    Limites lim = null;
    TrapecioFuncion trapeciof = null;
    SimpsonFuncion simpsonf = null;
    double y[];

    public UnidadCuatro() {
        initComponents();
        addFoco();
    }
    private void addFoco() {
        funcion.addFocusListener(new FullSelectorListener());
        punto1.addFocusListener(new FullSelectorListener());
        punto2.addFocusListener(new FullSelectorListener());
        n.addFocusListener(new FullSelectorListener());
        valor.addFocusListener(new FullSelectorListener());
        error.addFocusListener(new FullSelectorListener());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        combo = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        funcion = new javax.swing.JTextField();
        punto1 = new javax.swing.JTextField();
        jScrollPane4 = new javax.swing.JScrollPane();
        table1 = new javax.swing.JTable();
        punto2 = new javax.swing.JTextField();
        n = new javax.swing.JTextField();
        value = new javax.swing.JLabel();
        valor = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        result = new javax.swing.JTextField();
        resolver = new javax.swing.JButton();
        home = new javax.swing.JButton();
        clear = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        error = new javax.swing.JTextField();
        jScrollPane6 = new javax.swing.JScrollPane();
        table2 = new javax.swing.JTable();
        generara = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(jTable5);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Diferenciación e integración");

        combo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione método", "Límites", "Trapecio función", "Trapecio por tabla", "Simpson por función", "Simpson por tabla" }));
        combo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboItemStateChanged(evt);
            }
        });

        jLabel2.setText("Función:");

        jLabel3.setText("Punto 1:");

        jLabel4.setText("Punto 2:");

        jLabel5.setText("N:");

        funcion.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        punto1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        punto1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                punto1ActionPerformed(evt);
            }
        });

        table1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "X", "Y"
            }
        ));
        jScrollPane4.setViewportView(table1);

        punto2.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        n.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        n.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nActionPerformed(evt);
            }
        });
        n.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nKeyReleased(evt);
            }
        });

        value.setText("Valor:");

        valor.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel7.setText("Resultado:");

        result.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        result.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resultActionPerformed(evt);
            }
        });

        resolver.setText("Resolver");
        resolver.setToolTipText("Resolver tarea.");
        resolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resolverActionPerformed(evt);
            }
        });

        home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imágenes/menu.png"))); // NOI18N
        home.setToolTipText("Regresar al menú principal");
        home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeActionPerformed(evt);
            }
        });

        clear.setText("Limpiar pantalla");
        clear.setToolTipText("Deja vacías las cajas de texto.");
        clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearActionPerformed(evt);
            }
        });

        jLabel8.setText("Errror permisible");

        error.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        table2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Iteracion", "Title 2", "Title 3", "Title 4", "Title 5"
            }
        ));
        jScrollPane6.setViewportView(table2);

        generara.setText("Generar");
        generara.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generaraActionPerformed(evt);
            }
        });

        jLabel6.setText("Dé ENTER para continuar");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(clear)
                                .addGap(95, 95, 95)
                                .addComponent(resolver))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel3)
                                                .addGap(18, 18, 18)
                                                .addComponent(punto1))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel2)
                                                .addGap(18, 18, 18)
                                                .addComponent(funcion))
                                            .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(jLabel5)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, Short.MAX_VALUE)
                                                .addComponent(n, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(value)
                                                .addGap(26, 26, 26)
                                                .addComponent(valor, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel8)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                                                .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(generara)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jLabel4)
                                                    .addGap(18, 18, 18)
                                                    .addComponent(punto2, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(32, 32, 32)
                                        .addComponent(jLabel7)
                                        .addGap(18, 18, 18)
                                        .addComponent(result, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE))
                                .addGap(9, 9, 9))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(143, 143, 143)
                        .addComponent(jLabel1)
                        .addGap(0, 375, Short.MAX_VALUE))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(resolver)
                    .addComponent(clear)
                    .addComponent(generara))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(value)
                            .addComponent(valor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(punto1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(punto2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(n, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(result, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(39, 39, 39)))
                .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void punto1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_punto1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_punto1ActionPerformed

    private void nActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nActionPerformed

    private void resultActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resultActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_resultActionPerformed

    private void resolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resolverActionPerformed
       
        if (combo.getSelectedItem().toString().equals("Límites")) {
            lim = new Limites(new Funcion(funcion.getText()), Double.parseDouble(error.getText()), Double.parseDouble(valor.getText()), table2);
            double d = lim.solve();
            System.out.println(d);
            result.setText(d + "");
            return;
        }
        if (combo.getSelectedItem().toString().equals("Trapecio función")) {
            trapeciof = new TrapecioFuncion(funcion.getText(), Double.parseDouble(punto1.getText()), Double.parseDouble(punto2.getText()), Double.parseDouble(n.getText()), table2);
            double ans = trapeciof.solve();
            result.setText(ans + "");
            return;
        }
        if (combo.getSelectedItem().toString().equals("Simpson por función")) {
            if (combo.getSelectedItem().toString().equals("Simpson por función")) {
                if(Integer.parseInt(n.getText())%2==0) {
                    JOptionPane.showMessageDialog(null, "Número de puntos debe se impar.");
                    n.setText("");
                    n.requestFocus();
                    return;
                }
            }
            simpsonf = new SimpsonFuncion(funcion.getText(), Double.parseDouble(punto1.getText()), Double.parseDouble(punto2.getText()), Double.parseDouble(n.getText()), table2);
            double ans = simpsonf.solve();
            result.setText(ans + "");
        }
        if (combo.getSelectedItem().toString().equals("Simpson por tabla")) {
            y = new double[Integer.parseInt(n.getText()) + 1];

            for (int i = 0; i < table1.getRowCount(); i++) {
                y[i] = Double.parseDouble(table1.getValueAt(i, table1.getColumnCount() - 1) + "");
            }
            //for (int i = 0; i < y.length; i++) {
            //System.out.println(y[i]);
            //}
            SimpsonPuntos sp = new SimpsonPuntos(y, table2, Integer.parseInt(n.getText()), Double.parseDouble(valor.getText()));
            double ans = sp.solve();
            result.setText(ans + "");
        }
        if (combo.getSelectedItem().toString().equals("Trapecio por tabla")) {
            y = new double[Integer.parseInt(n.getText()) + 1];

            for (int i = 0; i < table1.getRowCount(); i++) {
                y[i] = Double.parseDouble(table1.getValueAt(i, table1.getColumnCount() - 1) + "");
            }
            TrapecioPuntos tp = new TrapecioPuntos(y, table2, Double.parseDouble(valor.getText()), Integer.parseInt(n.getText()));
            double ans = tp.solve();
            result.setText(ans + "");
        }
    }//GEN-LAST:event_resolverActionPerformed

    private void homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeActionPerformed
        // TODO add your handling code here:
        Main main = new Main();
        main.setVisible(true);
        dispose();
    }//GEN-LAST:event_homeActionPerformed

    private void clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearActionPerformed
        // TODO add your handling code here:
        funcion.setText("");
        valor.setText("");
        punto1.setText("");
        punto2.setText("");
        n.setText("");
        error.setText("");
        result.setText("");
        
        for(int i=0; i<table1.getRowCount(); i++) {
            for(int j=0; j<table1.getColumnCount(); j++) {
                table1.setValueAt("", i, j);
            }
        }
        for(int i=0; i<table2.getRowCount(); i++) {
            for(int j=0; j<table2.getColumnCount(); j++) {
                table2.setValueAt("", i, j);
            }
        }
    }//GEN-LAST:event_clearActionPerformed

    private void comboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboItemStateChanged
        // TODO add your handling code here:
        JComboBox combo = (JComboBox) evt.getSource();
        if (combo.getSelectedItem().toString().equals("Límites")) {
            punto1.setText("0");
            punto1.setEnabled(false);
            punto2.setText("0");
            punto2.setEnabled(false);
            n.setText("0");
            n.setEnabled(false);
            table1.setEnabled(false);
            value.setText("Valor:");
            return;
        }
        if (combo.getSelectedItem().toString().equals("Trapecio función") || combo.getSelectedItem().toString().equals("Simpson por función")) {
            punto1.setEnabled(true);
            punto2.setEnabled(true);
            n.setEnabled(true);
            table1.setEnabled(false);
            valor.setText("0");
            valor.setEnabled(false);
            error.setText("0");
            error.setEnabled(false);
            value.setText("Valor:");
            funcion.setEnabled(true);
            return;
        }
        if (combo.getSelectedItem().toString().equals("Simpson por tabla")) {
            table1.setEnabled(true);
            n.setEnabled(true);
            value.setText("Presición:");
            valor.setEnabled(true);
            funcion.setText("0");
            funcion.setEnabled(false);
            punto1.setText("0");
            punto1.setEnabled(false);
            punto2.setText("0");
            punto2.setEnabled(false);
            error.setText("0");
            error.setEnabled(false);
            return;
        }
        if (combo.getSelectedItem().toString().equals("Trapecio por tabla")) {
            table1.setEnabled(true);
            n.setEnabled(true);
            value.setText("Presición:");
            valor.setEnabled(true);
            funcion.setText("0");
            funcion.setEnabled(false);
            punto1.setText("0");
            punto1.setEnabled(false);
            punto2.setText("0");
            punto2.setEnabled(false);
            error.setText("0");
            error.setEnabled(false);
            return;
        }
        combo.requestFocus();
    }//GEN-LAST:event_comboItemStateChanged

    private void generaraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generaraActionPerformed
        // TODO add your handling code here:
        TableXY xy = new TableXY(Integer.parseInt(n.getText()), table1);
        xy.generate();
    }//GEN-LAST:event_generaraActionPerformed

    private void nKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_nKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UnidadCuatro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UnidadCuatro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UnidadCuatro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UnidadCuatro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UnidadCuatro().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clear;
    private javax.swing.JComboBox<String> combo;
    private javax.swing.JTextField error;
    private javax.swing.JTextField funcion;
    private javax.swing.JButton generara;
    private javax.swing.JButton home;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable5;
    private javax.swing.JTextField n;
    private javax.swing.JTextField punto1;
    private javax.swing.JTextField punto2;
    private javax.swing.JButton resolver;
    private javax.swing.JTextField result;
    private javax.swing.JTable table1;
    private javax.swing.JTable table2;
    private javax.swing.JTextField valor;
    private javax.swing.JLabel value;
    // End of variables declaration//GEN-END:variables
}
