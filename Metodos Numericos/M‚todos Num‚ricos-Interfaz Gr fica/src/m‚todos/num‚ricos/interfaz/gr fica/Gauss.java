/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;

/**
 *
 * @author LuisFernando
 */
public class Gauss {

    double matriz[][];
    JTable table;
    int n;

    public Gauss(int n, JTable table) {
        this.table = table;
        matriz = new double[n][n + 1];
        this.n = n;

    }

    public float[] solve(JTable table) {
        String masc = "##0.0####";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= n; j++) {
                matriz[i][j] = Double.parseDouble(this.table.getValueAt(i, j) + "");
            }
        }
        //con esto ya hemos leído la matríz
        //Vamos a imprimirla a ver qué sale :v
        for (int u = 0; u < n; u++) {
            for (int y = 0; y <= n; y++) {
                System.out.print((matriz[u][y]) + "	");
            }
            System.out.println();
        }
        //por lo visto sí funciona :v
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j <= n; j++) {
                double m = (-matriz[j][i]) / (matriz[i][i]);
                for (int k = i; k < n + 1; k++) {
                    matriz[j][k] = ((matriz[i][k]) * m) + matriz[j][k];
                }
            }
        }
        //ahora vamos a mandar la matriz resultante
        for (int d = 0; d < n; d++) {
            for (int p = 0; p <= n; p++) {
                table.setValueAt(Formatea.alinder(masc,(matriz[d][p])) + "", d, p);
            }
        }
        float array[] = new float[3];
        array[2] = (float) (matriz[n - 1][n] / matriz[n - 1][n - 1]);
        array[1] = (float) ((matriz[n - 2][n] - array[2] * matriz[n - 2][n - 1]) / matriz[n - 2][n - 2]);
        array[0] = (float) ((matriz[n - 3][n] - array[1] * matriz[n - 3][n - 2] - array[2] * matriz[n - 3][n - 1]) / matriz[n - 3][n - 3]);

        System.out.println("Estoy en la solución.");
        return array;
    }

    public double[] gauss(JTable table) {
        String masc = "##0.0####";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= n; j++) {
                matriz[i][j] = Double.parseDouble(this.table.getValueAt(i, j) + "");
            }
        }
        //en esta parte resolvemos el sistema
        for (int pivote = 0; pivote < n; pivote++)//renglon pivote
        {
            for (int i = (pivote + 1); i < n; i++)//renglon a eliminar
            {
                double m1 = -matriz[pivote][pivote];
                double m2 = matriz[i][pivote];
                for (int j = 0; j < (n + 1); j++)//elemento a eliminar
                {
                    matriz[i][j] = m1 * matriz[i][j] + m2 * matriz[pivote][j];
                }
            }
        }
        for (int d = 0; d < n; d++) {
            for (int p = 0; p <= n; p++) {
                table.setValueAt(Formatea.alinder(masc, Math.abs(matriz[d][p])) + "", d, p);
            }
        }

        //------Despeja incognitas-------
        double[] x = new double[n];
        for (int i = (n - 1); i >= 0; i--) {
            double sumatoria = 0;
            for (int j = (n - 1); j > i; j--) {
                sumatoria = sumatoria + matriz[i][j] * x[j];
            }
            x[i] = (matriz[i][n] - sumatoria) / matriz[i][i];
        }
        return x;
    }
}
