/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LuisFernando
 */
public class GenerateTable {

    public GenerateTable(int n, JTable table) {
        String valores[] = new String[n + 1];
        for (int i = 0; i < valores.length - 1; i++) {
            valores[i] = "X" + (i + 1);
        }
        valores[valores.length - 1] = "B";
        DefaultTableModel dtm = new DefaultTableModel(null, valores);
        String filas[] = new String[n + 1];
        for (int i = 0; i < n; i++) {
            filas[i] = "";
        }
        for (int i = 0; i < n; i++) {
            dtm.addRow(filas);
        }
        table.setModel(dtm);
    }
}
