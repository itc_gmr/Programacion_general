/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JFrame;

/**
 *
 * @author LuisFernando
 */
public class Unidad5Prueba extends JFrame{
    
    public Unidad5Prueba () {
        setSize(500,500);
        add(new Uni5Diferenciacion());
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    public static void main (String [] args) {
        new Unidad5Prueba ();
    }
    
    
}
