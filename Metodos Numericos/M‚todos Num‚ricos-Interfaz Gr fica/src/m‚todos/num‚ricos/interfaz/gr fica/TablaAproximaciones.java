package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.text.DecimalFormat;

/**
 *
 * @author LuisFernando
 */
public class TablaAproximaciones {

    String funcion;
    double x1;
    double error;
    int iteraciones;
    JTable table;

    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");
    ScriptEngine engine2 = manager.getEngineByName("js");
    DecimalFormat format = new DecimalFormat("0.000");

    public TablaAproximaciones(String funcion, double x1, double error, int iteraciones, JTable table) {
        this.funcion = funcion;
        this.error = error;
        this.x1 = x1;
        this.table = table;
        this.iteraciones = iteraciones;
    }

    public double pasarValores(JTable table) throws Exception {
        String columnas[] = {"Iteraciones", "X(i)", "X(i+1)", "F(Xi+1)", "Error"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        double X = x1, delta, Xn, e;
        Double funcionG, funcionXi;
        int it = 0;
        String fG = funcion + "+x";

        do {
            it += 1;
            engine.put("x", X);
            funcionG = (Double) engine.eval(fG);
            Xn = funcionG;
            delta = Math.abs(Xn - X);

            engine2.put("x", X);
            funcionXi = (Double) engine2.eval(funcion);

            e = Xn - X;

            //System.out.println(it + "\t" + format.format(X) + "\t\t" + format.format(funcionG) + "\t\t"
            //      + format.format(funcionXi) + "\t\t" + format.format(e));
            String filas [] = {it+"", format.format(X)+"", format.format(funcionG)+"", format.format(funcionXi)+"", format.format(Math.abs(e))};
              
            dtm.addRow(filas);

            X = Xn;
        } while (delta > error);
        table.setModel(dtm);
        return Xn;
        //for(int i=0; i<30; i++) {
        //  String filas [] = {"","","","",""};
        //            dtm.addRow(filas);
        //      }
        //    table.setModel(dtm);
    }
}