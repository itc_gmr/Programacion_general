/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.text.DecimalFormat;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 *
 * @author LuisFernando
 */
public class TablaRaphson {

    String funcion;
    String derivada;
    double x1;
    double error;
    int iteraciones;
    JTable table;

    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");
    DecimalFormat df = new DecimalFormat("0.0000");
    double xi, delta;
    Double funcionX, funcionPrimaX;
    int it = 0;

    public TablaRaphson(String funcion, String derivada, double x1, double error, int iteraciones, JTable table) {
        this.funcion = funcion;
        this.derivada = derivada;
        this.x1 = x1;
        this.error = error;
        this.iteraciones = iteraciones;
        this.table=table;
    }

    public double pasarValores(JTable table) throws Exception {
        String columnas[] = {"Iteraciones", "Xi", "F(Xi)", "F'(Xi)", "Xi+1", "Error"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        do {
            it += 1;
            engine.put("x", x1);
            funcionX = (Double) engine.eval(funcion);
            funcionPrimaX = (Double) engine.eval(derivada);
            xi = x1 - funcionX / funcionPrimaX;
            delta = Math.abs(x1 - xi);
            //System.out.println(it + "\t" + df.format(x1) + "\t\t" + df.format(funcionX) + "\t\t"
            //        + df.format(funcionPrimaX) + "\t\t\t" + df.format(xi) + "\t\t" + df.format((xi - x1)));
            String filas [] = {it+"", df.format(x1)+"", df.format(funcionX)+"", df.format(funcionPrimaX), df.format(xi), df.format(Math.abs(xi-x1))};
            dtm.addRow(filas);
            x1 = xi;
        } while (delta > error);
        table.setModel(dtm);
        //for(int i=0; i<30; i++) {
        //  String filas [] = {"","","","","",""};
        //dtm.addRow(filas);
        //}
        //table.setModel(dtm);
        return xi;
    }
}
