package métodos.numéricos.interfaz.gráfica;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Uni5Diferenciacion extends JPanel implements ActionListener {

    Funcion f;

    JLabel Titulo;//no

    JLabel Funcion;//no
    JLabel x;//no
    JLabel Error;//no

    JLabel Respuesta;//no

    JTextField txtfun;//no
    JTextField txtx;//no
    JTextField txterror;//no

    JButton Calcular;//no
    JButton Limpiar;//no

    // ----------------------------------------->TABLA
    JScrollPane Tabla;//maybe
    JTable tbl;//maybe
    Vector Columnas;//si
    Vector Renglones;//si
    Vector Datos;//si
    DefaultTableModel modelo;//si

    public Uni5Diferenciacion() {
        HazInterfaz();//
        HazEscuchas();//
    }

    public void HazEscuchas() {
        Calcular.addActionListener(this);//
        Limpiar.addActionListener(this);//
    }

    public void HazInterfaz() {

        Titulo = new JLabel("Diferenciación por medio de limites", SwingConstants.CENTER);//No
        Titulo.setFont(new Font("Arial", Font.BOLD, 22));//No

        Funcion = new JLabel("Funcion: ", SwingConstants.CENTER);//no
        x = new JLabel("x = ", SwingConstants.CENTER);//no
        Error = new JLabel("Error Permitido: ", SwingConstants.CENTER);//no

        Respuesta = new JLabel("", SwingConstants.LEFT);//no
        Respuesta.setFont(new Font("Arial", Font.BOLD, 18));//No

        txtfun = new JTextField(30);//no
        txtx = new JTextField(30);//no
        txterror = new JTextField(30);//no

        Calcular = new JButton("Calcular");//no
        Limpiar = new JButton("Limpiar");//no

        Titulo.setPreferredSize(new Dimension(600, 30));//no

        Respuesta.setPreferredSize(new Dimension(600, 50));//no

        Funcion.setPreferredSize(new Dimension(240, 20));//no
        x.setPreferredSize(new Dimension(240, 20));//no
        Error.setPreferredSize(new Dimension(240, 20));//no

        Calcular.setPreferredSize(new Dimension(150, 30));//no
        Limpiar.setPreferredSize(new Dimension(150, 30));//No

        // ----------------------------------->TABLA
        Columnas = new Vector();
        Renglones = new Vector();

        Columnas.addElement("i");
        Columnas.addElement("h");
        Columnas.addElement("k");
        Columnas.addElement("x+h");
        Columnas.addElement("f(x+h)");
        Columnas.addElement("f'(x)");
        Columnas.addElement("Error");

        modelo = new DefaultTableModel(Renglones, Columnas);
        tbl = new JTable(modelo);
        tbl.setEnabled(false);

        Tabla = new JScrollPane(tbl);
        Tabla.setPreferredSize(new Dimension(600, 183));

        add(Titulo);

        add(Funcion);
        add(txtfun);
        add(x);
        add(txtx);
        add(Error);
        add(txterror);

        add(Limpiar);
        add(Calcular);

        add(Tabla);

    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == Calcular) {

            for (int i = modelo.getRowCount() - 1; i > -1; i--) {
                modelo.removeRow(i);
            }

            Diferenciacion();
            return;
        }
        if (e.getSource() == Limpiar) {
            for (int i = modelo.getRowCount() - 1; i > -1; i--) {
                modelo.removeRow(i);
            }

            txtfun.setText(null);
            txtx.setText(null);
            txterror.setText(null);

            Respuesta.setText("");
            add(Respuesta);

            validate();
        }
    }

    public void Diferenciacion() {
        double x, e, da, d, delta, n;
        double potencia, k;

        //Grafica = new Graficar("Grafica Diferenciacion");
        f = new Funcion(txtfun.getText());
        x = Double.parseDouble(txtx.getText());
        e = Double.parseDouble(txterror.getText());
        n = 1;
        da = Math.pow(10, 10);

        do {
            potencia = Math.pow(0.1, n);
            k = 1 / potencia;
            d = (f.calc(x + potencia) - f.calc(x)) * k;
            delta = Math.abs(da - d);
            da = d;
            Renglones = new Vector();
            Renglones.add(n);
            Renglones.add(Formatea.alinder("0.#############", potencia));
            Renglones.add(Formatea.alinder("#,###,###,##0.00", k));
            Renglones.add(x + potencia);
            Renglones.add(f.calc(x + potencia));
            Renglones.add(f.calc(x));
            Renglones.add(e);
            modelo.addRow(Renglones);
            n = n + 1;

        } while (delta > e);

        //Grafica.AgregaDatos(f, x);
        //Grafica.MuestraGrafica("Grafica Falsa Posición");
        Respuesta.setText("Solución= " + d);

        add(Respuesta);
        validate();
    }
}
