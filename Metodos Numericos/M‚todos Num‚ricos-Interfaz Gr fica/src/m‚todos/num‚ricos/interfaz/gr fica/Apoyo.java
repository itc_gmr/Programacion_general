/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

/**
 *
 * @author LuisFernando
 */
public class Apoyo {

    public static String Ceros(int Valor, int Ancho) {
        String B = Valor + "";
        while (B.length() < Ancho) {
            B = "0" + B;
        }
        return B;
    }

    public static String Blancos(String Valor, int Ancho) {
        String B = Valor + "";
        while (B.length() < Ancho) {
            B = B + " ";
        }
        if (B.length() > Ancho) {
            B = B.substring(0, Ancho);
        }
        return B;
    }
}
