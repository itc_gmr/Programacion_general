/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LuisFernando
 */
public class TablaPuntos {

    public TablaPuntos(int n, JTable table) {
        String columns [] = new String [2];
        columns[0]="X";
        columns[1]="Y";
        DefaultTableModel dtm = new DefaultTableModel(null, columns);

        String filas[] = new String[2];
        filas[0]="";
        filas[1]="";
        //for (int i = 0; i < filas.length; i++) {
          //  filas[i] = "";
        //}
        for (int i = 0; i < n; i++) {
            dtm.addRow(filas);
        }
        table.setModel(dtm);
    }
}
