/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;

/**
 *
 * @author LuisFernando
 */
public class UtilizaCrammer {

    int n;
    JTable table;
    double matr [][];
    public UtilizaCrammer(int n, JTable table) {
        this.n = n;
        this.table = table;
    }

    public String solve() {
        String str="";
        Crammer g1 = new Crammer(n, table);
        double[] Vecsol = new double[n];
        // double[] recsol = new double[m1];
        double[][] duplimatr = new double[n][n];
        double[][] matriz = new double[n][n];
        matriz = g1.LeerMatriz();
        matr=matriz;
        if (Crammer.Determinante(matriz) == 0) {
            System.out.println("No tiene solución");
        } else {
            duplimatr = g1.duplicarMatriz(matriz);
            Vecsol = g1.LeerVectorR();
            double deter = Crammer.Determinante(matriz);
            //System.out.println("Matriz original");
            //g1.desplegamatriz(matriz, Vecsol);
            //System.out.println("Determinante de la matriz: " + deter);
            for (int i = 0; i <= n - 1; i++) {
                //System.out.println("X" + (i + 1) + ": " + g1.ResolverMatriz(duplimatr, Vecsol, i, deter));
                str+="X" + (i + 1) + ": " + g1.ResolverMatriz(duplimatr, Vecsol, i, deter)+"\n";
                duplimatr = g1.duplicarMatriz(matriz);
            }
        }
        return str;

    }
    public double determinante () {
        return Crammer.Determinante(matr);
    }

}
