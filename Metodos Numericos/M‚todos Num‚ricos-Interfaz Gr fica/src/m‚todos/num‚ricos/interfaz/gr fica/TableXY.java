/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LuisFernando
 */
public class TableXY {
    JTable table;
    int n;
    public TableXY (int n, JTable table) {
        this.n=n;
        this.table=table;
    }
    
    public void generate () {
        String values [] = {"X", "Y"};
        DefaultTableModel dtm = new DefaultTableModel(null, values);
        
        String rows [] = new String [n];
        
        for(int i=0; i<rows.length; i++) {
            rows[i]="";
        }
        for(int i=0; i<n; i++) {
            dtm.addRow(rows);
        }
        table.setModel(dtm);
        
    }
}
