/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import java.util.Scanner;

/**
 *
 * @author LuisFernando
 */
public class Cramer {

    public static void resuelveCrammer(double[][] matriz, double[] b) {
        int m = matriz.length;
        double[][] mCopia = new double[matriz.length][matriz.length];
        mCopia = copiaMatriz(matriz, mCopia);
        double x[] = new double[matriz.length];
        double det = determinante(matriz);
        if (det == 0) {
            System.out.println("Sistema sin solución.");
        } else {
            matriz = copiaMatriz(mCopia, matriz);
            for (int j = 0; j < m; j++) {
                double detx[] = new double[matriz.length];
                for (int i = 0; i < m; i++) {
                    mCopia[i][j] = b[i];
                }
                detx[j] = determinante(mCopia);
                mCopia = copiaMatriz(matriz, mCopia);
                x[j] = detx[j] / det;
            }
        }

        //Imprimir original
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(Apoyo.Blancos(String.valueOf(matriz[i][j]), 4) + "\t");
            }
            System.out.print(b[i]);
            System.out.println("");
        }

        for (int i = 0; i < m; i++) {
            System.out.println("x" + (i + 1) + "= " + x[i]);
        }
    }

    public static double[] resuelveSistemaCrammer(double[][] matriz, double[] b) {
        int m = matriz.length;
        double[][] mCopia = new double[matriz.length][matriz.length];
        mCopia = copiaMatriz(matriz, mCopia);
        double x[] = new double[matriz.length];
        double det = determinante(matriz);
        if (det == 0) {
            System.out.println("Sistema sin solución");
        } else {
            matriz = copiaMatriz(mCopia, matriz);
            for (int j = 0; j < m; j++) {
                double detx[] = new double[matriz.length];
                for (int i = 0; i < m; i++) {
                    mCopia[i][j] = b[i];
                }
                detx[j] = determinante(mCopia);
                mCopia = copiaMatriz(matriz, mCopia);
                x[j] = detx[j] / det;
            }
        }

        //Imprimir original
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.print(Apoyo.Blancos(String.valueOf(matriz[i][j]), 4) + "\t");
            }
            System.out.print(b[i]);
            System.out.println("");
        }

        for (int i = 0; i < m; i++) {
            System.out.println("x" + (i + 1) + "= " + x[i]);
        }
        return x;
    }

    public static double[][] copiaMatriz(double[][] matriz, double[][] mCopia) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                mCopia[i][j] = matriz[i][j];
            }
        }
        return mCopia;
    }

    public static double determinante(double[][] matriz) {
        assert matriz != null;
        assert matriz.length > 0;
        assert matriz.length == matriz[0].length;

        double determinante = 0.0;

        int filas = matriz.length;
        int columnas = matriz[0].length;

        // Si la matriz es 1x1, el determinante es el elemento de la matriz
        if ((filas == 1) && (columnas == 1)) {
            return matriz[0][0];
        }

        int signo = 1;

        for (int columna = 0; columna < columnas; columna++) {
            // Obtiene el adjunto de fila=0, columna=columna, pero sin el signo.
            double[][] submatriz = getSubmatriz(matriz, filas, columnas,
                    columna);
            determinante = determinante + signo * matriz[0][columna] * determinante(submatriz);
            signo *= -1;
        }

        return determinante;
    }

    /**
     * Obtiene la matriz que resulta de eliminar la primera fila y la columna
     * que se pasa como parámetro.
     *
     * @param matriz Matriz original
     * @param filas Numero de filas de la matriz original
     * @param columnas Numero de columnas de la matriz original
     * @param columna Columna que se quiere eliminar, junto con la fila=0
     * @return Una matriz de N-1 x N-1 elementos
     */
    public static double[][] getSubmatriz(double[][] matriz,
            int filas,
            int columnas,
            int columna) {
        double[][] submatriz = new double[filas - 1][columnas - 1];
        int contador = 0;
        for (int j = 0; j < columnas; j++) {
            if (j == columna) {
                continue;
            }
            for (int i = 1; i < filas; i++) {
                submatriz[i - 1][contador] = matriz[i][j];
            }
            contador++;
        }
        return submatriz;
    }

    public static void main(String[] args) {
        @SuppressWarnings("resource")
        Scanner input = new Scanner(System.in);
        int var = 0;
        double matriz[][];
        System.out.println("\nNúmero de ecuaciones: ");
        var = input.nextInt();
        double b[] = new double[var];
        matriz = new double[var][var];
        double[][] mCopia = new double[matriz.length][matriz.length];
        for (int x = 0; x < var; x++) {
            for (int y = 0; y < (var); y++) {
                System.out.println("Ingresa la constante de la posicion: A[" + (x + 1) + "][" + (y + 1) + "]");
                matriz[x][y] = input.nextDouble();
            }
        }
        System.out.println("Vector de Resultados:");
        for (int i = 0; i < var; i++) {
            System.out.println("Ingresa B[" + (i + 1) + "]");
            b[i] = input.nextDouble();
        }
        mCopia = copiaMatriz(matriz, mCopia);
        double det = determinante(mCopia);
        if (det == 0) {
            System.out.println("Sistema sin solución");
            return;
        }
        resuelveCrammer(matriz, b);
    }

}
