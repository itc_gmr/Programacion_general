/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author LuisFernando
 */
public class TablaBiseccion {

    String funcion;
    double x1;
    double x2;
    int iteraciones;
    JTable table;
    double xm;
    Double funcionXm = null, funcionA = null;
    double error;

    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");
    ScriptEngine engine2 = manager.getEngineByName("js");
    DecimalFormat df = new DecimalFormat("0.0000");

    public TablaBiseccion(String funcion, int iteraciones, double error, double x1, double x2, JTable table) {
        this.funcion = funcion;
        this.x1 = x1;
        this.x2 = x2;
        this.iteraciones = iteraciones;
        this.table = table;
        this.error = error;
    }

    public double pasarValores(JTable table) throws Exception {
        String columnas[] = {"Iteraciones", "X1", "X2", "X2-X1", "Xm", "F(X1)", "F(Xm)"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        //System.out.println("Aquí estoy en el método solve");
        int auxIter = iteraciones;
        iteraciones = 0;
        do {
            iteraciones += 1;
            if(iteraciones>=auxIter) {
                JOptionPane.showMessageDialog(null, "No se encontró raíz en 30 iteraciones");
                return 0;
            }
            xm = (x1 + x2) / 2;
            engine.put("x", xm);
            funcionXm = (Double) engine.eval(funcion);

            if (funcionXm == 0) {
                return xm;
            } else {
                engine2.put("x", x1);
                funcionA = (Double) engine2.eval(funcion);
                String[] filas = {iteraciones + "", x1 + "", x2 + "", x2 - x1 + "", xm + "", df.format(funcionA) + "", df.format(funcionXm) + ""};
                dtm.addRow(filas);
                if ((funcionA * funcionXm) < 0) {
                    x2 = xm;
                } else {
                    x1 = xm;
                }
            }
        } while (Math.abs(x2 - x1) > error);
        table.setModel(dtm);
        return xm;
    }
}
