/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LuisFernando
 */
public class SolDifFin {

    public String difFin(double[] xin, double[] yin) {
        List<List<Double>> ay = new ArrayList<List<Double>>();
        boolean constantes = false;

        // Pasar xin y yin a la lista
        List<Double> yxin = new ArrayList<Double>();
        List<Double> yyin = new ArrayList<Double>();
        for (int i = 0; i < xin.length; i++) {
            yxin.add(xin[i]);
            yyin.add(yin[i]);
        }
        ay.add(yxin);
        ay.add(yyin);

        // Generar Ayi: listas y hacer restas
        while (!constantes) {
            List<Double> yi = new ArrayList<Double>();
            List<Double> aux = ay.get(ay.size() - 1);
            for (int i = (aux.size() - 1); i > 0; i--) {
                double valor = aux.get(i) - aux.get(i - 1);
                yi.add(0, valor);
            }

            // agregar Ayi a la "tabla de valores"
            ay.add(yi);

            // verificar si son constantes iguales
            double rep = 1;
            for (int i = 1; i < yi.size(); i++) {
                if (yi.get(0).equals(yi.get(i))) {
                    rep++;
                }
            }
            if (rep == yi.size()) {
                constantes = true;
            }

        }

        // Generar sistema de ecuaciones
        int n = ay.size() - 1;
        double[] b = new double[n];
        double[][] matriz = new double[n][n];

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                double x = ay.get(0).get(i);
                matriz[i][j] = Math.pow(x, n - (j + 1));
            }
        }

        for (int i = 0; i < b.length; i++) {
            b[i] = ay.get(1).get(i);
        }

        // Resolver el sistema de ecuaciones
        double[] x = Cramer.resuelveSistemaCrammer(matriz, b);
        int mayor = 0;
        String str = "";
        for (int i = 0; i < x.length - 1; i++) {
            if (x[i] == 0.0) {
                continue;
            }
            //System.out.print((int) x[i]);
            str += (int) x[i] + "";
            if (x.length - (i + 1) > mayor) {
                mayor = x.length - (i + 1);
            }
            for (int j = 0; j < x.length - (i + 1); j++) {
                //System.out.print("*x");
                str += "*x";
            }
            // System.out.print((int) x[i]+"x^"+(x.length-(i+1))+" + ");
            //System.out.print("+");
            str += "+";
        }
        //System.out.print((int) x[x.length - 1]);
        str += (int) x[x.length - 1] + "";
        return str;
    }
}
