/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.text.DecimalFormat;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author LuisFernando
 */
public class TablaNewtonSegundo {

    String funcion;
    String derivadaUno;
    String derivadaDos;
    double x1;
    int iteraciones;
    double error;
    JTable table;

    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");
    ScriptEngine engine2 = manager.getEngineByName("js");
    DecimalFormat df = new DecimalFormat("0.0000");
    double xi, delta;
    Double funcionX, funcionPrimaX, funcionBiprimaX, funcionXi;
    int it = 0;

    public TablaNewtonSegundo(String funcion, String derivadaUno, String derivadaDos, double x1, int iteraciones, double error, JTable table) {
        this.funcion = funcion;
        this.derivadaUno = derivadaUno;
        this.derivadaDos = derivadaDos;
        this.x1 = x1;
        this.iteraciones = iteraciones;
        this.error = error;
        this.table = table;
    }

    public double pasarValores(JTable table) throws Exception {
        String columnas[] = {"Iteraciones", "Xi", "F(Xi)", "F'(Xi)", "F''(Xi)", "Xi+1", "F(Xi+1)", "Error"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);

        do {
            it += 1;
            engine.put("x", x1);

            funcionX = xExist(funcion) ? (Double) engine.eval(funcion) : Double.parseDouble(funcion);
            funcionPrimaX = xExist(derivadaUno) ? (Double) engine.eval(derivadaUno)
                    : Double.parseDouble(derivadaUno);
            funcionBiprimaX = xExist(derivadaDos) ? (Double) engine.eval(derivadaDos)
                    : Double.parseDouble(derivadaDos);

            xi = x1 - funcionX / (funcionPrimaX - ((funcionX * funcionBiprimaX) / (2 * funcionPrimaX)));

            engine2.put("x", xi);
            funcionXi = xExist(funcion) ? (Double) engine2.eval(funcion) : Double.parseDouble(funcion);

            delta = Math.abs(x1 - xi);

            //System.out.println(it + "\t" + df.format(x1) + "\t\t" + df.format(funcionX) + "\t\t"
            //  + df.format(funcionPrimaX) + "\t\t\t" + df.format(funcionBiprimaX) + "\t\t\t" + df.format(xi) + "\t\t"
            //  + df.format(funcionXi) + "\t\t\t" + df.format((xi - x1)));
            
            String filas [] = {it+"", df.format(x1)+"", df.format(funcionX)+"", df.format(funcionPrimaX)+"", df.format(funcionBiprimaX)+"", df.format(xi)+"", df.format(funcionXi)+"", df.format(xi-x1)+""};
            dtm.addRow(filas);
            x1 = xi;

        } while (delta > error);
        table.setModel(dtm);
        return xi;
        //for(int i=0; i<30; i++) {
        //  String filas [] = {"","","","","","","",""};
        //dtm.addRow(filas);
        //}
        //table.setModel(dtm);
    }

    public boolean xExist(String v) {
        for (int x = 0; x < v.length(); x++) {
            if (v.charAt(x) == 'x') {
                return true;
            }
        }
        return false;
    }
}
