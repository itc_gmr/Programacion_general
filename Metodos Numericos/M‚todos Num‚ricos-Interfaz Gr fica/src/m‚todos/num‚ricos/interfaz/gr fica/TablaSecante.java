package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.text.DecimalFormat;

/**
 *
 * @author LuisFernando
 */
public class TablaSecante {

    String funcion;
    double x1;
    double x2;
    int iteraciones;
    double error;
    JTable table;

    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");
    ScriptEngine engine2 = manager.getEngineByName("js");
    ScriptEngine engine3 = manager.getEngineByName("js");
    ScriptEngine engine4 = manager.getEngineByName("js");
    DecimalFormat f = new DecimalFormat("0.0000");

    public TablaSecante(String funcion, double x1, double x2, int iteraciones, double error, JTable table) {
        this.funcion = funcion;
        this.x1 = x1;
        this.x2 = x2;
        this.iteraciones = iteraciones;
        this.error = error;
        this.table = table;
    }

    public double pasarValores(JTable table) throws Exception {
        String columnas[] = {"Iteraciones", "X(I)", "X(I+1)", "F(Xi)", "F(Xi+1)", "Xi+2", "Error", "F(Xi+2)"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        double xiMas1, delta, xiMas2, e;
        Double funcionXiMenos1, funcionXi, funcionXiMas1, funcionXiMas2;
        int it = 0;
        do {
            it += 1;
            engine.put("x", x2);
            funcionXi = (Double) engine.eval(funcion);
            engine2.put("x", x1);
            funcionXiMenos1 = (Double) engine2.eval(funcion);
            xiMas1 = x2 - (((funcionXi) * (x2 - x1)) / (funcionXi - funcionXiMenos1));

            engine3.put("x", xiMas1);
            funcionXiMas1 = (Double) engine3.eval(funcion);

            // xiMas2 = (xiMas1 - (((funcionXiMas1) * (xiMas1 - xi)) /
            // (funcionXiMas1 - funcionXi)));
            xiMas2 = (x2 - (((funcionXi) * (x2 - x1)) / (funcionXi - funcionXiMenos1)));
            engine4.put("x", xiMas2);
            funcionXiMas2 = (Double) engine4.eval(funcion);

            delta = Math.abs(xiMas1 - x2);
            if (it == 1) {
                e = xiMas2 - x2;
            } else {
                e = xiMas2 - x2;
            }
            // funcionXi
            xiMas2 = (x2 - (((funcionXi) * (x2 - x1)) / (funcionXi - funcionXiMenos1)));
            //System.out.println(it + "\t" + f.format(x1) + "\t\t" + f.format(x2) + "\t\t"
            //                   + f.format(funcionXiMenos1) + "\t\t\t" + f.format(funcionXi) + "\t\t" + f.format(xiMas2) + "\t\t"
            //                 + f.format(e) + "\t\t" + f.format(funcionXiMas2));

            String filas[] = {it + "", f.format(x1) + "", f.format(x2) + "", f.format(funcionXiMenos1) + "", f.format(funcionXi) + "", f.format(xiMas2) + "" , f.format(e) + "", f.format(funcionXiMas2) + ""};
            dtm.addRow(filas);
            x1 = x2;
            x2 = xiMas1;
        } while (delta > error);
        table.setModel(dtm);
        return xiMas1;
        //for (int i = 0; i < 30; i++) {
        //String filas[] = {"", "", "", "", "", "", "", ""};
        //dtm.addRow(filas);
        //}
        //table.setModel(dtm);
    }
}
