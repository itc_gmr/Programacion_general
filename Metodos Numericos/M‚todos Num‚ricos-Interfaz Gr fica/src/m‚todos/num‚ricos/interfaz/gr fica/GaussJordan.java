package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;

/**
 *
 * @author LuisFernando
 */
public class GaussJordan {

    int n;
    JTable table;
    double[][] matriz;

    public GaussJordan(int n, JTable table) {
        this.n = n;
        this.table = table;
        matriz = new double[n][n + 1];
    }

    public double[] gaussJordan(JTable table) {
        String masc = "##,##0.00";
        //pasamos matriz de la tabla a una matriz double

        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j <= this.n; j++) {
                matriz[i][j] = Double.parseDouble(this.table.getValueAt(i, j) + "");
            }
        }

        for (int cont = 0; cont < (n); cont++)//renglon pivote
        {
            if (matriz[cont][cont] != 1) {
                double aux = matriz[cont][cont];
                for (int j = 0; j < (n + 1); j++)//hacer 1 el elemento pivote
                {
                    matriz[cont][j] = matriz[cont][j] / aux;
                }
            }
            for (int i = 0; i < n; i++)//renglon a eliminar
            {
                if (i != cont) {
                    double n = -matriz[i][cont];
                    for (int j = 0; j < (this.n + 1); j++)//elemento a eliminar
                    {
                        matriz[i][j] = n * matriz[cont][j] + matriz[i][j];
                    }
                }
            }
        }
        //pasamos valores a la table nueva local
        for (int d = 0; d < n; d++) {
            for (int p = 0; p <= n; p++) {
                table.setValueAt(Formatea.alinder(masc, (matriz[d][p])) + "", d, p);
            }
        }

        //----imprimir incognitas
        //mandamos incógnitas a un arreglo
        double array[] = new double[this.n];
        for (int cont = 0; cont < n; cont++) {
            //System.out.println("X" + (cont + 1) + "= " + (Formatea.alinder(masc, matriz[cont][n])));
            array[cont] = Double.parseDouble(Formatea.alinder(masc, matriz[cont][n]) + "");
        }
        return array;
    }

}
