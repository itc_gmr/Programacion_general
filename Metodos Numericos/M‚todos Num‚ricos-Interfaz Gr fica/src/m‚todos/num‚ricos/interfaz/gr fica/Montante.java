package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;

/**
 * @author LuisFernando
 */
public class Montante {

    int m;
    JTable table;

    public Montante(int m, JTable table) {
        this.m = m;
        this.table = table;
    }

    public double[] solve(JTable table) {
        String masc = "##0.0####";
        int n = m + 1;
        double matriz[][] = new double[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matriz[i][j] = Double.parseDouble(this.table.getValueAt(i, j) + "");
            }
        }
        double pivote = 1;
        for (int k = 0; k <= m - 1; k++) {
            for (int i = 0; i <= m - 1; i++) {
                if (i != k) {
                    for (int j = m; j >= 0; j--) {
                        matriz[i][j] = (matriz[k][k] * matriz[i][j] - matriz[i][k] * matriz[k][j]) / pivote;
                    }
                }
            }
            pivote = matriz[k][k];
        }

        double deter = matriz[0][0];
        // ciclo
        //aquí se imprime la matriz resultante
        for (int i = 0; i <= m - 1; i++) {
            for (int j = 0; j <= m; j++) {
                table.setValueAt(Formatea.alinder(masc, (matriz[i][j])) + "", i, j);
            }
        }
        //aquí sacamos las soluciones
        for (int i = 0; i <= m - 1; i++) {
            for (int j = 0; j <= m - 1; j++) {
                if (i == j) {
                    matriz[i][j] = matriz[i][j] / deter;
                    matriz[i][m] = matriz[i][m] / deter;
                }
            }
        }
        double array[] = new double[matriz.length];
        for (int i = 0; i < matriz.length; i++) {
            array[i] = Double.parseDouble(Formatea.alinder(masc, (matriz[i][matriz[i].length - 1])) + "");
        }
        return array;
    }
}
