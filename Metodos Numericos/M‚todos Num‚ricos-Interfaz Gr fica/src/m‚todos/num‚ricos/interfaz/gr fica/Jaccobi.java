package métodos.numéricos.interfaz.gráfica;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * @author LuisFernando
 */
public class Jaccobi {

    int m;
    JTable table;
    double matriz[][];
    double vs[];
    double error;
    int iteraciones;

    public Jaccobi(int m, JTable table, double error, int iteraciones) {
        this.m = m;
        this.table = table;
        matriz = new double[m][m + 1];
        vs = new double[m];
        this.error = error;
        this.iteraciones = iteraciones;
    }

    public double[] solve(JTable table) {
        String columnas[] = {"Iteraciones", "X1", "X2", "X3", "X1", "X2", "X3"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        String valores = "";
        for (int i = 0; i < m; i++) {
            for (int j = 0; j <= m; j++) {
                matriz[i][j] = Double.parseDouble(this.table.getValueAt(i, j) + "");
            }
        }
        vs = valoresiniciales();
        @SuppressWarnings("unused")
        double pivote, delta, cero = 0;
        boolean fin;
        int itera;
        String masc = "##0.0####";
        double[] y = new double[m];
        itera = 1;
        double x[] = vs;
        //System.out.println("I    \t    X1    \t    X2    \t    X3    \t    X1    \t    X2    \t    X3    ");
        do {
            fin = true;

            //System.out.print(Formatea.alinder("0", itera));
            valores += Formatea.alinder("0", itera) + ",";
            for (int i = 0; i <= m - 1; i++) {
                y[i] = matriz[i][m];
                for (int j = 0; j <= m - 1; j++) {
                    if (i != j) {
                        y[i] = y[i] - matriz[i][j] * x[j];
                    }
                }
                y[i] = y[i] / matriz[i][i];
                delta = Math.abs(x[i] - y[i]);
                if (delta <= error) {
                    fin = false;
                }
            }
            for (int i = 0; i <= m - 1; i++) {
                //System.out.print("\t" + (Formatea.alinder(masc, x[i])));
                valores += (Formatea.alinder(masc, x[i])) + ",";
            }
            for (int i = 0; i <= m - 1; i++) {
                x[i] = y[i];
                //System.out.print("\t" + (Formatea.alinder(masc, x[i])));
                if (i != m) {
                    valores += (Formatea.alinder(masc, x[i])) + ",";
                }
                if (i == m) {
                    valores += (Formatea.alinder(masc, x[i])) + "";
                }
            }
            String filas[] = valores.split(",");
            dtm.addRow(filas);
            valores = "";
            //System.out.println();
            itera++;
        } while (fin == true && iteraciones > itera);
        table.setModel(dtm);
        return x;

    }

    public double[] valoresiniciales() {
        double[] x = new double[m];
        for (int i = 0; i < m - 1; i++) {
            x[i] = 0;
        }
        return x;
    }
}
