package métodos.numéricos.interfaz.gráfica;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * @author LuisFernando
 */
public class SimpsonPuntos {

    double y[];
    JTable table;
    int n;
    double h;
    double integral = 0.0;
    double i = 1;

    public SimpsonPuntos(double[] y, JTable table, int n, double h) {
        this.y = y;
        this.table = table;
        this.n = n;
        this.h = h;
    }

    public double solve() {
        String columnas[] = {"Iteraciones", "Y", "FM", "Resultado"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        String masc = "##0.0####";
        integral = y[0] + y[ n];
        double sum = 0.0;
        double aux = 0, aux1 = 0;
        int fm;
        for (int k = 1; k < n; k++) {
            if (k % 2 != 0) {
                aux = y[k] * 4 + aux;
            }
            if (k % 2 == 0) {
                aux = y[k] * 2 + aux;
            }
        }
        //System.out.println("I   \t	Y  \t	FM  \t Resultado");
        //System.out.println(i + "\t" + Formatea.alinder(masc, y[0]) + "\t" + Formatea.alinder(masc, 1) + "\t"
        //+ Formatea.alinder(masc, y[0]));
        String rows[] = {i + "", Formatea.alinder(masc, y[0]) + "", Formatea.alinder(masc, 1) + "", Formatea.alinder(masc, y[0]) + ""};
        dtm.addRow(rows);

        sum += Double.parseDouble(Formatea.alinder(masc, y[0]) + "");
        for (int k = 1; k < n - 1; k++) {
            i++;
            if (k % 2 != 0) {
                fm = 4;
                aux1 = (double) y[k] * fm;
                //System.out.println(i + "\t" + Formatea.alinder(masc, y[k]) + "\t" + Formatea.alinder(masc, fm) + "\t"
                //+ Formatea.alinder(masc, aux1));
                sum += Double.parseDouble(Formatea.alinder(masc, aux1) + "");
                String row[] = {i + "", Formatea.alinder(masc, y[k]) + "", Formatea.alinder(masc, fm) + "", Formatea.alinder(masc, aux1) + ""};
                dtm.addRow(row);
            }
            if (k % 2 == 0) {
                fm = 2;
                aux1 = (double) y[k] * fm;
                //System.out.println(i + "\t" + Formatea.alinder(masc, y[k]) + "\t" + Formatea.alinder(masc, fm) + "\t"
                //+ Formatea.alinder(masc, aux1));
                sum += Double.parseDouble(Formatea.alinder(masc, aux1) + "");
                String row[] = {i + "", Formatea.alinder(masc, y[k]) + "", Formatea.alinder(masc, fm) + "", Formatea.alinder(masc, aux1) + ""};
                dtm.addRow(row);
            }

        }
        //System.out.println(n + "\t" + Formatea.alinder(masc, y[n - 1]) + "\t" + Formatea.alinder(masc, 1) + "\t"
        //+ Formatea.alinder(masc, y[n - 1]));
        sum += Double.parseDouble(Formatea.alinder(masc, y[(int) n - 1]) + "");
        String row[] = {(int) n + "", Formatea.alinder(masc, y[(int) n - 1]) + "", Formatea.alinder(masc, 1) + "", Formatea.alinder(masc, y[(int) n - 1]) + ""};
        dtm.addRow(row);
        integral = integral + aux;
        integral = (h / 3) * integral;
        //System.out.println();
        integral = (h / 3) * sum;
        String[] fin = {"Toal:", "", "", sum + ""};
        dtm.addRow(fin);
        //System.out.println("El area de la integral es: " + integral);
        table.setModel(dtm);
        return integral;
    }

}
