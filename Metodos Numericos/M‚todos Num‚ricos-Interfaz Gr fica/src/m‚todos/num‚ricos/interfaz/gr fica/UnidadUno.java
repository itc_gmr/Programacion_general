package métodos.numéricos.interfaz.gráfica;

import Graficadora.Graficadora;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JOptionPane;
/**
 *
 * @author LuisFernando
 */
public class UnidadUno extends javax.swing.JFrame implements ActionListener, KeyListener {
    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");
    ScriptEngine engine2 = manager.getEngineByName("js");
    DecimalFormat df = new DecimalFormat("0.0000");
    TablaFalsaPosicion tf = null;
    TablaBiseccion tb = null;
    TablaAproximaciones ta = null;
    TablaSecante ts = null;
    TablaRaphson tr = null;
    TablaNewtonSegundo tn = null;
    double raiz;

    public UnidadUno() {
        initComponents();
        hazEscuchas();
        addFoco();
        meth.requestFocus();
    }

    private void addFoco() {
        funcion.addFocusListener(new FullSelectorListener());
        iteraciones.addFocusListener(new FullSelectorListener());
        derivadaUno.addFocusListener(new FullSelectorListener());
        derivadaDos.addFocusListener(new FullSelectorListener());
        x1.addFocusListener(new FullSelectorListener());
        x2.addFocusListener(new FullSelectorListener());
        error.addFocusListener(new FullSelectorListener());
    }
    private void hazEscuchas() {
        funcion.addKeyListener(this);
        iteraciones.addKeyListener(this);
        derivadaUno.addKeyListener(this);
        derivadaDos.addKeyListener(this);
        x1.addKeyListener(this);
        x2.addKeyListener(this);
        error.addKeyListener(this);
        solve.setEnabled(false);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        clear = new javax.swing.JButton();
        solve = new javax.swing.JButton();
        graph = new javax.swing.JButton();
        meth = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        funcion = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        derivadaUno = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        x1 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        error = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        iteraciones = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        x2 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        derivadaDos = new javax.swing.JTextField();
        home = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        root = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Métodos de solución de ecuaciones");

        clear.setText("Limpiar pantalla");
        clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearActionPerformed(evt);
            }
        });

        solve.setText("Resolver ecuación");
        solve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                solveActionPerformed(evt);
            }
        });

        graph.setText("Graficar");
        graph.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                graphActionPerformed(evt);
            }
        });

        meth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione método:", "Bisección", "Falsa Posición", "Secante", "Aproximaciones Sucesivas", "Newton Raphson", "Newton de Segundo Orden" }));
        meth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                methActionPerformed(evt);
            }
        });

        jLabel1.setText("Función f(x):");

        funcion.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        funcion.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        funcion.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        funcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcionActionPerformed(evt);
            }
        });

        jLabel2.setText("X1: ");

        derivadaUno.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel3.setText("X2: ");

        x1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        x1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setText("Error Permisible: ");

        error.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        error.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));

        jLabel5.setText("Iteraciones: ");

        iteraciones.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        iteraciones.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        table.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Iteraciones", "X1", "X2", "X2-X1", "Xm", "F(X1)", "F(Xm)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class, java.lang.Long.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        table.setAlignmentX(1.0F);
        table.setColumnSelectionAllowed(true);
        table.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        table.setDoubleBuffered(true);
        table.setOpaque(false);
        table.setSelectionBackground(new java.awt.Color(153, 255, 255));
        jScrollPane1.setViewportView(table);
        table.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jLabel6.setText("Derivada: ");

        x2.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel7.setText("Segunda derivada: ");

        derivadaDos.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        derivadaDos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                derivadaDosActionPerformed(evt);
            }
        });

        home.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imágenes/menu.png"))); // NOI18N
        home.setToolTipText("Volver al menú.");
        home.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeActionPerformed(evt);
            }
        });

        jLabel8.setText("Raíz de la función:");

        root.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(x1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(x2, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(root))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(derivadaUno, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel7)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(derivadaDos, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(clear)
                        .addGap(18, 18, 18)
                        .addComponent(solve)
                        .addGap(18, 18, 18)
                        .addComponent(graph)
                        .addGap(18, 18, 18)
                        .addComponent(meth, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(home)))
                .addContainerGap(46, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(clear)
                    .addComponent(solve)
                    .addComponent(graph)
                    .addComponent(meth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(x2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(x1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(derivadaUno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(derivadaDos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(home, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(root, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void funcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_funcionActionPerformed

    private void derivadaDosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_derivadaDosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_derivadaDosActionPerformed

    private void homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeActionPerformed
        if (evt.getSource() == home) {
            Main main = new Main();
            main.setVisible(true);
            setVisible(false);
        }
    }//GEN-LAST:event_homeActionPerformed

    private void methActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_methActionPerformed
        javax.swing.JComboBox combo = (javax.swing.JComboBox) evt.getSource();
        switch (combo.getSelectedItem().toString()) {
            case "Bisección":
                enabledAll();
                derivadaUno.setText("0");
                derivadaDos.setText("0");
                derivadaUno.setEnabled(false);
                derivadaDos.setEnabled(false);
                break;
            case "Falsa Posición":
                enabledAll();
                derivadaUno.setText("0");
                derivadaDos.setText("0");
                derivadaUno.setEnabled(false);
                derivadaDos.setEnabled(false);
                break;
            case "Secante":
                enabledAll();
                derivadaUno.setText("0");
                derivadaDos.setText("0");
                derivadaUno.setEnabled(false);
                derivadaDos.setEnabled(false);
                break;
            case "Aproximaciones Sucesivas":
                enabledAll();
                derivadaUno.setText("0");
                derivadaDos.setText("0");
                x2.setText("0");
                derivadaUno.setEnabled(false);
                derivadaDos.setEnabled(false);
                x2.setEnabled(false);
                break;
            case "Newton Raphson":
                enabledAll();
                derivadaDos.setText("0");
                x2.setText("0");
                iteraciones.setText("0");
                x2.setEnabled(false);
                derivadaDos.setEnabled(false);
                break;
            case "Newton de Segundo Orden":
                enabledAll();
                x2.setText("0");
                x2.setEnabled(false);
                break;
        }
    }//GEN-LAST:event_methActionPerformed

    private void solveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_solveActionPerformed
        switch (meth.getSelectedItem().toString()) {
            case "Bisección":
                try {
                    if(!bolzano(funcion.getText(), Double.parseDouble(x1.getText()), Double.parseDouble(x2.getText()))) {
                        JOptionPane.showMessageDialog(null, "Teorema de Bolzano no cumplido.");
                        x1.setText("");
                        x2.setText("");
                        x1.requestFocus();
                        return;
                    }
                }catch (Exception e) {}
                
                tb = new TablaBiseccion(funcion.getText(), Integer.parseInt(iteraciones.getText()), Double.parseDouble(error.getText()), Double.parseDouble(x1.getText()), Double.parseDouble(x2.getText()), table);
                try {
                    raiz = tb.pasarValores(table);
                    root.setText(raiz + "");
                } catch (Exception e) {
                }
                break;
            case "Falsa Posición":
                try {
                    if(!bolzano(funcion.getText(), Double.parseDouble(x1.getText()), Double.parseDouble(x2.getText()))) {
                        JOptionPane.showMessageDialog(null, "Teorema de Bolzano no cumplido.");
                        x1.setText("");
                        x2.setText("");
                        x1.requestFocus();
                        return;
                    }
                }catch (Exception e) {}
                tf = new TablaFalsaPosicion(funcion.getText(), Double.parseDouble(x1.getText()), Double.parseDouble(x2.getText()), Integer.parseInt(iteraciones.getText()), Double.parseDouble(error.getText()), table);
                try {
                    raiz = tf.pasarValores(table);
                    root.setText(raiz + "");
                } catch (Exception e) {
                }
                break;
            case "Secante":
                ts = new TablaSecante(funcion.getText(), Double.parseDouble(x1.getText()), Double.parseDouble(x2.getText()), Integer.parseInt(iteraciones.getText()), Double.parseDouble(error.getText()), table);
                try {
                    raiz = ts.pasarValores(table);
                    root.setText(raiz + "");
                } catch (Exception e) {
                }
                break;
            case "Aproximaciones Sucesivas":
                System.out.println("Entré");
                ta = new TablaAproximaciones(funcion.getText(), Double.parseDouble(x1.getText()), Double.parseDouble(error.getText()), Integer.parseInt(iteraciones.getText()), table);
                try {
                    raiz = ta.pasarValores(table);
                    root.setText(raiz + "");
                } catch (Exception e) {
                }
                break;
            case "Newton Raphson":
                tr = new TablaRaphson(funcion.getText(), derivadaUno.getText(), Double.parseDouble(x1.getText()), Double.parseDouble(error.getText()), Integer.parseInt(iteraciones.getText()), table);
                try {
                    raiz = tr.pasarValores(table);
                    root.setText(raiz + "");
                } catch (Exception e) {
                }
                break;
            case "Newton de Segundo Orden":
                tn = new TablaNewtonSegundo(funcion.getText(), derivadaUno.getText(), derivadaDos.getText(), Double.parseDouble(x1.getText()), Integer.parseInt(iteraciones.getText()), Double.parseDouble(error.getText()), table);
                try {
                    raiz = tn.pasarValores(table);
                    root.setText(raiz + "");
                } catch (Exception e) {
                }
                break;
            default:
                System.out.println(meth.getSelectedItem().toString());
        }


    }//GEN-LAST:event_solveActionPerformed

    private void graphActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_graphActionPerformed
        // TODO add your handling code here:
        new Graficadora (funcion.getText());
    }//GEN-LAST:event_graphActionPerformed

    private void clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearActionPerformed
        // TODO add your handling code here:
        funcion.setText("");
        iteraciones.setText("");
        error.setText("");
        derivadaUno.setText("");
        derivadaDos.setText("");
        x1.setText("");
        x2.setText("");
        root.setText("");
    }//GEN-LAST:event_clearActionPerformed
    public void enabledAll() {
        funcion.setEnabled(true);
        x1.setEnabled(true);
        x2.setEnabled(true);
        iteraciones.setEnabled(true);
        derivadaUno.setEnabled(true);
        derivadaDos.setEnabled(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UnidadUno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UnidadUno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UnidadUno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UnidadUno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UnidadUno().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clear;
    private javax.swing.JTextField derivadaDos;
    private javax.swing.JTextField derivadaUno;
    private javax.swing.JTextField error;
    private javax.swing.JTextField funcion;
    private javax.swing.JButton graph;
    private javax.swing.JButton home;
    private javax.swing.JTextField iteraciones;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> meth;
    private javax.swing.JTextField root;
    private javax.swing.JButton solve;
    private javax.swing.JTable table;
    private javax.swing.JTextField x1;
    private javax.swing.JTextField x2;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (check()) {
            solve.setEnabled(true);
        }
    }

    public boolean bolzano(String funcion, double a, double b) throws ScriptException {
        engine.put("x", a);
        engine2.put("x", b);
        return (((double) engine.eval(funcion) * (double) engine2.eval(funcion) < 0));
    }

    public boolean check() {
        if (funcion.getText().equals("")) {
            return false;
        }
        if (iteraciones.getText().equals("")) {
            return false;
        }
        if (x1.getText().equals("")) {
            return false;
        }
        if (x2.getText().equals("")) {
            return false;
        }
        if (derivadaUno.getText().equals("")) {
            return false;
        }
        if (derivadaDos.getText().equals("")) {
            return false;
        }
        if (error.getText().equals("")) {
            return false;
        }
        if (meth.getSelectedItem().toString().equals("Seleccione método:")) {
            return false;
        }
        return true;
    }
}