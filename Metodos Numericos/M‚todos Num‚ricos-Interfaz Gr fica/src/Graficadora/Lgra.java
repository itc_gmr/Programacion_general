package Graficadora;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JButton;
import javax.swing.JPanel;

public class Lgra extends JPanel implements ActionListener, MouseListener {
	private static final long serialVersionUID = 4438673352570454165L;
	static ScriptEngineManager manager = new ScriptEngineManager();
	static ScriptEngine engine = manager.getEngineByName("js");
	String Funcion = "";
	JButton ZMas, ZMenos;
	int densiPix;

	Lgra(String func) {
		Funcion = func;
		densiPix = 10;
		ZMas = new JButton("+");
		ZMenos = new JButton("-");
		JPanel caja = new JPanel();
		caja.setLayout(new GridLayout());
		this.add(caja, BorderLayout.SOUTH);
		caja.add(ZMas);
		caja.add(ZMenos);
		ZMas.addActionListener(this);
		ZMenos.addActionListener(this);

	}

	public void paint(Graphics g) {
		Graphics2D G2 = (Graphics2D) g;
		G2.clearRect(0, 0, getWidth(), getHeight());
		G2.setStroke(new BasicStroke(2));
		G2.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
		G2.drawLine(getWidth() / 2, 0, getWidth() / 2, getHeight());
		G2.setStroke(new BasicStroke(1));
		G2.translate(getWidth() / 2, getHeight() / 2);
		for (int x = 0; x < getWidth() / 2; x += densiPix) {
			G2.drawLine(x, -5, x, 5);
		}
		for (int x = 0; x < getWidth() / 2; x += densiPix) {
			G2.drawLine(-x, -5, -x, 5);
		}
		for (int x = 0; x < getHeight() / 2; x += densiPix) {
			G2.drawLine(-5, x, 5, x);
		}
		for (int x = 0; x < getHeight() / 2; x += densiPix) {
			G2.drawLine(-5, -x, 5, -x);
		}
		for (double x = 0; x < getWidth() / 2; x += .1) {
			engine.put("x", (double) x / densiPix);
			try {
				Object aux = engine.eval(Funcion);
				double aux2 = Double.parseDouble(aux.toString()) * densiPix;
				G2.fillOval((int) (x - 2), -((int) aux2) - 2, 4, 4);
			} catch (ScriptException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for (double x = 0; x > -getWidth() / 2; x -= .1) {
			engine.put("x", x / (double) densiPix);
			try {
				Object aux = engine.eval(Funcion);
				double aux2 = Double.parseDouble(aux.toString()) * densiPix;
				G2.fillOval((int) (x - 2), -((int) aux2) - 2, 4, 4);
			} catch (ScriptException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ZMas.grabFocus();
		ZMenos.grabFocus();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == ZMas)
			densiPix += 10;
		else
			densiPix -= 10;
		this.repaint();
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
