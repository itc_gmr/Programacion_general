package Graficadora;

import javax.swing.JFrame;

public class Graficadora extends JFrame {
	private static final long serialVersionUID = -5515587893511850214L;
	Lgra lienzo;

	public Graficadora(String func) {
		super("f(x)="+func);
		this.add(lienzo = new Lgra(func));
		this.setSize(600, 500);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

}
