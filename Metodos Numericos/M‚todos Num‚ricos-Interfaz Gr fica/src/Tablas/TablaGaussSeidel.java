package Tablas;
import javax.swing.JOptionPane;
public class TablaGaussSeidel extends javax.swing.JFrame {
    public TablaGaussSeidel() {
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    double a[][];
    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        TablaDesplegarMatriz t = new TablaDesplegarMatriz(""); // crear tabla
        t.setVisible(false);
        boolean vacio = false;
        double M[][];
        for(int i=0; i<jTable1.getRowCount(); i++)
            for(int j=0; j<jTable1.getColumnCount(); j++)
            {
                if(jTable1.getValueAt(i, j).equals(""))
                {
                    vacio = true;
                    break;
                }
            }
        if(!vacio)
        {
            if(!TodosDouble())
            {
                JOptionPane.showMessageDialog(null, "Las celdas solo pueden tener valor double");
                return;
            }
            else                
            {
                M = getMatrix();
                double []x= new double[M.length];
		double []x2= new double[M.length];
		double []Y= new double[M.length];
		double Error,Delta;
		boolean Fin;
		int d=1;
		if(!MatrizDominante(M))
		{
                    JOptionPane.showMessageDialog(null, "La diagonal no es dominante");
                    for(int j=0; j<jTable1.getRowCount(); j++)
                        BorrarRenglon();
                    for(int i=0; i<jTable1.getColumnCount(); i++)                        
                        BorrarColumna();
                    this.dispose();
		return;
		}
                else{
                    do{
                        try{
                            Error=Double.parseDouble(JOptionPane.showInputDialog("Teclee el Error"));
                            break;
                        }
                        catch(NumberFormatException e){
                            JOptionPane.showMessageDialog(null, "Dato invalido");
                        }
                    }while(true);
                    a = new double[M.length][M.length]; 

                    for(int j=0; j<t.jTable1.getRowCount(); j++)
                        t.BorrarRenglon();
                    for(int i=0; i<t.jTable1.getColumnCount(); i++)
                        t.BorrarColumna();
                    for(int i=0;i<M.length*2;i++)
                    {
                            if(i<=M.length-1)
                                t.AgregarColumna("x"+(i+1));                          
                            else
                                t.AgregarColumna("x"+(i-(M.length-1)));            
                    }
                    for(int j=0; j<t.jTable1.getRowCount(); j++)
                        t.BorrarRenglon();
                    t.setVisible(true);
                    int b=0;
                        do
                        {
                            t.AgregarRenglon();
                            Fin=true;
                            for(int i=0;i<M.length;i++)
                            {
                                    Y[i]=M[i][M.length];
                                    for(int j=0; j<M.length; j++)
                                        if(j!=i)
                                            Y[i]=Y[i]-M[i][j]*x[j];
                                    Y[i]=Y[i]/M[i][i];
                                    if(Error<Math.abs(x[i]-Y[i]))
                                        Fin=false;
                                    x2[i]=x[i];
                                    x[i]=Y[i];
                             }
                            for(int j=0;j<M.length*2;j++)
                            {
                                if(j<M.length)
                                {
                                    //a[b][j]=x2[j];
                                    t.jTable1.setValueAt(x2[j], b, j);
                                }
                                else
                                {
                                    //a[b][j]=Y[j-M.length];
                                    t.jTable1.setValueAt(Y[j-M.length], b, j);
                                }
                            }
                            b++;
                        }while(!Fin);
                        t.BorrarRenglon();
                        
                }
             TablaDesplegarMatriz tr = new TablaDesplegarMatriz("");
            for(int i=0; i<x.length; i++)
                tr.AgregarColumna("x"+(i+1));
            tr.AgregarRenglon();            
            for(int j=0; j<tr.jTable1.getColumnCount(); j++)
                tr.jTable1.setValueAt(x[j], 0, j);
            int r=tr.jTable1.getRowCount();
            for(int j=0; j<r-1; j++)
                tr.BorrarRenglon();
            int n = 1;
            tr.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50),  ((int)((n-1)*(13.5))) + ((103-(n-1)) + ((n-1)*(n-1))));
            
            tr.setVisible(true);
            }
           
        }
    }//GEN-LAST:event_jTable1KeyReleased
    public boolean MatrizDominante(double [][]m)
	{
		double suma=0;
		for(int i=0;i<m.length;i++)
		{
			for(int j=0;j<m[i].length-1;j++)
				if(i!=j)
					suma=suma+Math.abs(m[i][j]);
			if(Math.abs(suma)>Math.abs(m[i][i]))
				return false;
			suma=0;	
		}	
		return true;
		
	}
    private void crearVentanaMatrizResultante(int n, TablaDesplegarMatriz TDM) {
//agregar (n + 1)  columnas
        for(int i=0; i<=n; i++)
        {
            if(i<n)
                TDM.AgregarColumna("X"+(i+1)); 
            else
                TDM.AgregarColumna("B"); 
        }
//borrar todos los renglones Default
        for(int i=0; i<=n; i++)
            TDM.BorrarRenglon();
//agregar (n) renglones 
        for(int i=0; i<n; i++)
            TDM.AgregarRenglon();
        if(n==1)
            TDM.BorrarRenglon();

//---------------Dar Tamaño a Ventana------------------------------------------------
        if(n<5)
            TDM.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50),  ((int)((n-1)*(13.5))) + ((103-(n-1)) + ((n-1)*(n-1))));
        else
            TDM.jTable1.setSize(((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1))), ((int)((n-1)*(13.5))) + ((98-(n-1)) + ((n-1)*(n-1))));
//-----------------------------------------------------------------------------------
        // borrar tabla------
        for(int i=0; i<TablaGaussSeidel.jTable1.getRowCount(); i++)
            for(int j=0; j<TablaGaussSeidel.jTable1.getColumnCount(); j++)
                TDM.jTable1.setValueAt("", i, j);
    }
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaGaussSeidel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaGaussSeidel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaGaussSeidel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaGaussSeidel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TablaGaussSeidel().setVisible(true);
            }
        });
    }
    private double[][] getMatrix() {
        int b=jTable1.getRowCount();
        int c=jTable1.getColumnCount();
        double a[][] = new double [b][c];
         for(int i=0; i<b; i++)
            for(int j=0; j<c; j++)
            {
                    if(jTable1.getValueAt(i, j)!=null)
                        a[i][j] = Double.parseDouble(jTable1.getValueAt(i, j).toString());
                    else
                        a[i][j]=0;
            }
         return a;
    }

    private boolean TodosDouble() {
        boolean respuesta = true;
        int i = 0,j = 0;
        try{
            for(i=0; i<jTable1.getRowCount(); i++)
                for(j=0; j<jTable1.getColumnCount(); j++)
                     Double.parseDouble(jTable1.getValueAt(i, j).toString());
        }catch(NumberFormatException e)
        {
            jTable1.setValueAt("", i, j);
            respuesta = false;
        }
        return respuesta;
    }
     public void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    public void AgregarColumna(String i)
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo [] = {temp.getColumnCount()+1,"",""};
            temp.addColumn(i,nuevo);
        }catch(NullPointerException e){}
    }
    public void BorrarColumna()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getColumnCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }

    public void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}