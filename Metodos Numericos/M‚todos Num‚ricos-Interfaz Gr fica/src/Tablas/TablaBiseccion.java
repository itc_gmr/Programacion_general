package Tablas;
import MetNum.MetodosNumericos;
import MetodosIterativos.Biseccion;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
public class TablaBiseccion extends javax.swing.JFrame {
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        Resultado = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Iteración", "X1", "X2", "X2-X1", "Xm", "F(X1)", "F(Xm)", "Error"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);

        Resultado.setText("Resultado");
        Resultado.setVisible(false);

        jButton1.setText("Cerrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Resultado)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Resultado)
                    .addComponent(jButton1))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    public static boolean NoFound = false;
    public TablaBiseccion() 
    {       
        initComponents();
        try
        {
            String a, b;
            int itr=0;
            a = Biseccion.PuntoInicial.getText();
            b = Biseccion.PuntoFinal.getText();
            itr = Integer.parseInt(Biseccion.Iteraciones.getText()+"");
            if(itr<=0)
            {
                JOptionPane.showMessageDialog(null, "Error Iteraciones debe ser positiva");
                NoFound = true;
                return;
            }
            else
            {
                NoFound = false;
                double x1, x2, t, e;
                for(int i=0; i<itr; i++)
                {
                    AgregarRenglon();
                    if(i==1)
                        break;
                }
                x1 = Double.parseDouble(a);
                x2 = Double.parseDouble(b);
                if(x1>x2)
                {
                    t = x1;
                    x1 = x2;
                    x2 = t;
                }
                jTable1.setValueAt(x1, 0, 1);
                jTable1.setValueAt(x2, 0, 2);
                jTable1.setValueAt(x2-x1, 0, 3);
                jTable1.setValueAt(((x2+x1)/2), 0, 4);
                jTable1.setValueAt(MetodosNumericos.F(x1), 0, 5); // f(x1)
                jTable1.setValueAt(MetodosNumericos.F((Double)(jTable1.getValueAt(0, 4))), 0, 6); // f(xm)
                e = Math.abs(x2-x1);
                jTable1.setValueAt(e, 0, 7);
                for(int i=1; i<itr; i++)
                {

                    if(Double.parseDouble(jTable1.getValueAt(i-1, 5)+"")*Double.parseDouble(jTable1.getValueAt(i-1, 6)+"") <=  0)
                    {
                        jTable1.setValueAt(jTable1.getValueAt((i-1), 1), i, 1);
                        jTable1.setValueAt(jTable1.getValueAt((i-1), 4), i, 2); // f(xm)-->x2
                    }
                    else
                    {
                        jTable1.setValueAt(jTable1.getValueAt((i-1), 4), i, 1);
                        jTable1.setValueAt(jTable1.getValueAt((i-1), 2), i, 2);
                    }
                    x1 = Double.parseDouble(jTable1.getValueAt(i, 1)+"");
                    x2 = Double.parseDouble(jTable1.getValueAt(i, 2)+"");
                    jTable1.setValueAt(x2-x1, i, 3);
                    jTable1.setValueAt(((x2+x1)/2), i, 4);
                    jTable1.setValueAt(MetodosNumericos.F((Double)jTable1.getValueAt(i, 1)), i, 5); // f(x1)
                    jTable1.setValueAt(MetodosNumericos.F((Double)jTable1.getValueAt(i, 4)), i, 6); // f(xm)
                    e = Math.abs(x2-x1);
                    jTable1.setValueAt(e, i, 7);
                    if(e<Double.parseDouble(Biseccion.Error.getText()+""))
                    {
                        if(i < itr)
                        {
                            JOptionPane.showMessageDialog(null, "Se encontro la raiz en x = "+Double.parseDouble(jTable1.getValueAt(i, 4)+""));
                            Resultado.setText("Se encontro la raiz en x = "+Double.parseDouble(jTable1.getValueAt(i, 4)+""));
                            Resultado.setVisible(true);
                            NoFound = false;
                        }
                        break;
                    }
                    AgregarRenglon();
                    if(i==itr-1)
                    {
                        JOptionPane.showMessageDialog(null, "NO Se encontro la raiz en "+itr+" intentos");
                        Resultado.setVisible(false);
                        NoFound = true;
                    }
                }
            }
        }
        catch(NumberFormatException o){}
        catch(ArrayIndexOutOfBoundsException g){}
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel Resultado;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    private void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    private void BorrarTabla() {                                         
    int a = Integer.parseInt(Biseccion.Iteraciones.getText()+"");
        while(a>0)
        {
            BorrarRenglon();
            a--;
        }        
    }

    private void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
}