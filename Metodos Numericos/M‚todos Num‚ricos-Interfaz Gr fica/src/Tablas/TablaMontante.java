package Tablas;

import javax.swing.JOptionPane;

public class TablaMontante extends javax.swing.JFrame {
    public TablaMontante() {
        initComponents();
        
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
    TablaDesplegarMatriz TDM = new TablaDesplegarMatriz(); // crear tabla
        TDM.setVisible(false);
        boolean vacio = false;
        double M[][];
        for(int i=0; i<jTable1.getRowCount(); i++)
            for(int j=0; j<jTable1.getColumnCount(); j++)
            {
                if(jTable1.getValueAt(i, j).equals(""))
                {
                    vacio = true;
                    break;
                }
            }
        if(!vacio)
        {
            if(!TodosDouble())
            {
                JOptionPane.showMessageDialog(null, "Las celdas solo pueden tener valor double");
                return;
            }
            else                
            {                
                M = getMatrix();double piv_ant = 1;
	        // La iteracin "k" proviene del nmero de pasos a realizar
	        int k;
	    	for(k = 0; k < M.length; k++) 
	    	{
                    for (int i = 0; i < M.length; i++) 
                    {
                        for (int j = M[0].length-1; j>=0 ; j--) 
                        {
                            if (i != k) // Las columnas hasta la k ...
                            {
                                if (j <= k) // estarn formadas por 0's...
                                {
                                    if (i != j) 
                                            M[i][j] = 0;  // ...excepto la diagonal, que tendr valores iguales al pivote.
                                    else 
                                            M[i][j] = M[k][k];//El resto de los datos...
                                } 
                                else 
                                {
                                    // ...se obtiene mediante el determinante de ese punto y el pivote, dividido por el pivote anterior...
                                    M[i][j] = DetMontante(M[k][k], M[k][j], M[i][k], M[i][j]) / piv_ant;
                                    // ...y son negativos si estn sobre el rengln del pivote...
                                    if (i < k ) 
                                        M[i][j] = - M[i][j]; 
                                }
                            }
                        }
                    }
                    piv_ant = M[k][k];
	    	}
	    	// Se divide la matriz entre el valor del pivote actual para obtener la matriz identidad.
	    	System.out.println("Dererminante " + M[0][0]);
              crearVentanaMatrizResultante(M.length, TDM);
            }
            // Escribir datos de la matriz a la tabla de resultados
            for(int i=0; i<TDM.jTable1.getRowCount(); i++)
                for(int j=0; j<TDM.jTable1.getColumnCount(); j++)
                    TDM.jTable1.setValueAt(M[i][j], i, j);
            TDM.setVisible(true);
            despliegaValores(M);
        }
    }//GEN-LAST:event_jTable1KeyReleased
    static void despliegaValores(double d[][]) 
    {
        TablaDesplegarMatriz VARIABLES = new TablaDesplegarMatriz("VALORES DE LAS INCOGNITAS\n");
        crearVentanaVariables(d.length, VARIABLES);

        double b = d[0][0];
        for(int i=0; i<d.length; i++)
        {
            for(int j=0; j<d[i].length; j++)
                d[i][j]/=b;// dividir toda la matriz entre el determinante
            VARIABLES.jTable1.setValueAt(d[i][ d[i].length-1 ], 0 , i);
        }
        VARIABLES.setVisible(true);
    }
    private static double DetMontante(double d, double e, double f, double g) 
    {
        return d*g-e*f;
    }
    private double[][] getMatrix() {
        double a[][] = new double [jTable1.getRowCount()][jTable1.getColumnCount()];
        for(int i=0; i<jTable1.getRowCount(); i++)
        for(int j=0; j<jTable1.getColumnCount(); j++)
            a[i][j] = Double.parseDouble(jTable1.getValueAt(i, j).toString());
        return a;
    }

    private boolean TodosDouble() {
        boolean respuesta = true;
        int i = 0,j = 0;
        try{
            for(i=0; i<jTable1.getRowCount(); i++)
                for(j=0; j<jTable1.getColumnCount(); j++)
                     Double.parseDouble(jTable1.getValueAt(i, j).toString());
        }catch(NumberFormatException e)
        {
            jTable1.setValueAt("", i, j);
            respuesta = false;
        }
        return respuesta;
    }

    private void crearVentanaMatrizResultante(int n, TablaDesplegarMatriz TDM) {
//agregar (n + 1)  columnas
        for(int i=0; i<=n; i++)
        {
            if(i<n)
                TDM.AgregarColumna("X"+(i+1)); 
            else
                TDM.AgregarColumna("B"); 
        }
//borrar todos los renglones Default
        for(int i=0; i<=n; i++)
            TDM.BorrarRenglon();
//agregar (n) renglones 
        for(int i=0; i<n; i++)
            TDM.AgregarRenglon();
        if(n==1)
            TDM.BorrarRenglon();

//---------------Dar Tamaño a Ventana------------------------------------------------
        if(n<5)
            TDM.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50),  ((int)((n-1)*(13.5))) + ((103-(n-1)) + ((n-1)*(n-1))));
        else
            TDM.jTable1.setSize(((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1))), ((int)((n-1)*(13.5))) + ((98-(n-1)) + ((n-1)*(n-1))));
//-----------------------------------------------------------------------------------
        // borrar tabla------
        for(int i=0; i<TablaMontante.jTable1.getRowCount(); i++)
            for(int j=0; j<TablaMontante.jTable1.getColumnCount(); j++)
                TDM.jTable1.setValueAt("", i, j);
    }

    private static void crearVentanaVariables(int n, TablaDesplegarMatriz VARIABLES) 
    {
    //agregar (n + 1)  columnas
        for(int i=0; i<n; i++)
        {
            VARIABLES.AgregarColumna("X"+(i+1));  
        }
    //borrar todos los renglones Default
        for(int i=0; i<=n; i++)
            VARIABLES.BorrarRenglon();
    //agregar 1 renglones 
        VARIABLES.AgregarRenglon();

    //---------------Dar Tamaño a Ventana------------------------------------------------
        VARIABLES.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50), 100);
    //-----------------------------------------------------------------------------------
    }
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaMontante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaMontante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaMontante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaMontante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TablaMontante().setVisible(true);
            }
        });
    }
     public void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    public void AgregarColumna(String i)
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo [] = {temp.getColumnCount()+1,"",""};
            temp.addColumn(i,nuevo);
        }catch(NullPointerException e){}
    }
    public void BorrarColumna()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getColumnCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }

    public void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
