package Tablas;
import MetNum.MetodosNumericos;
import MetodosIterativos.Secante;
import javax.swing.JOptionPane;

public class TablaSecante extends javax.swing.JFrame {
    public static boolean NoFound = false;
    public TablaSecante() {
        initComponents();
        try
        {
            double x1 = Double.parseDouble(Secante.PuntoInicial.getText()),
                   x2 = Double.parseDouble(Secante.PuntoFinal.getText()), 
                   error = Double.parseDouble(Secante.Error.getText()), 
                   x3 = 0;
	    int itera = Integer.parseInt(Secante.Iteraciones.getText()), i = 0;
            if(Math.abs(x1)==Math.abs(x2))
            {
                JOptionPane.showMessageDialog(null, "Error de Rango");
                NoFound = true;
                return;
            }
            else
            {
                AgregarRenglon();
                while(i<itera)
                {
                    if(i==0)
                    {
                        x3=x1-((MetodosNumericos.F(x1)*(x1-x2))/(MetodosNumericos.F(x1)-MetodosNumericos.F(x2)));
                        jTable1.setValueAt(i, i, 0);
                        jTable1.setValueAt(x1, i, 1);
                        jTable1.setValueAt(x2, i, 2);
                        jTable1.setValueAt(MetodosNumericos.F(x1), i, 3);
                        jTable1.setValueAt(MetodosNumericos.F(x2), i, 4);
                        jTable1.setValueAt(x3, i, 5);
                        jTable1.setValueAt((Math.abs(Math.abs(x3)-Math.abs(x2))), i, 6);
                        jTable1.setValueAt(MetodosNumericos.F(x3), i, 7);
                    }
                    else
                    {
                        AgregarRenglon();
                        x1=x2;
                        x2=x3;
                        x3=x1-((MetodosNumericos.F(x1)*(x1-x2))/(MetodosNumericos.F(x1)-MetodosNumericos.F(x2)));
                        jTable1.setValueAt(i, i, 0);
                        jTable1.setValueAt(x1, i, 1);
                        jTable1.setValueAt(x2, i, 2);
                        jTable1.setValueAt(MetodosNumericos.F(x1), i, 3);
                        jTable1.setValueAt(MetodosNumericos.F(x2), i, 4);
                        jTable1.setValueAt(x3, i, 5);
                        jTable1.setValueAt((Math.abs(Math.abs(x3)-Math.abs(x2))), i, 6);//x2 y x1
                        jTable1.setValueAt(MetodosNumericos.F(x3), i, 7);
                    }
                    i++;
                    if((Math.abs(Math.abs(x2)-Math.abs(x1)))<error)
                    {
                        JOptionPane.showMessageDialog(null, "Solucion encontrada en: "+x1);
                        Resultado1.setText("Solucion encontrada en x: "+x1);
                        break;
                    }
                    if((i+1)==itera)
                    {
                        JOptionPane.showMessageDialog(null, "No se encontro la solucion en "+(i+1)+" intentos");
                        NoFound = true;
                    }
                    else
                        NoFound = false;
                }
            }
        }
        catch(NumberFormatException o){}
        catch(ArrayIndexOutOfBoundsException g){}
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        Resultado1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Iteración", "Xi", "X(i+1)", "F(Xi)", "F(Xi+1)", "Xi+2", "Error", "F(Xi+2)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);

        Resultado1.setText("Resultado");

        jButton2.setText("Cerrar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 712, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Resultado1, javax.swing.GroupLayout.PREFERRED_SIZE, 347, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Resultado1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    private void BorrarTabla() {                                         
    int a = Integer.parseInt(Secante.Iteraciones.getText()+"");
        while(a>0)
        {
            BorrarRenglon();
            a--;
        }        
    }

    private void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaSecante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaSecante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaSecante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaSecante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TablaSecante().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel Resultado1;
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}