package Tablas;
import MetNum.MetodosNumericos;
import MetodosIterativos.FalsaPosicion;
import javax.swing.JOptionPane;

public class TablaFalsaPosicion extends javax.swing.JFrame {
    public static boolean NoFound = false;
    public TablaFalsaPosicion() {
        initComponents();
        try
        {
            String a, b;
            
	    int itera = Integer.parseInt(FalsaPosicion.Iteraciones.getText()), i=0;
            a = FalsaPosicion.PuntoInicial.getText();
            b = FalsaPosicion.PuntoFinal.getText();
            if(Double.parseDouble(a)>Double.parseDouble(b))
            {
                double c = Double.parseDouble(a);
                a = b;
                b = c + "";
            }
            if(itera<=0)
            {
                JOptionPane.showMessageDialog(null, "Error Iteraciones debe ser positiva");
                NoFound = true;
                return;
            }
            else
            {
               NoFound = false;
               double xi, x2 = 0, x3, Error = Double.parseDouble(FalsaPosicion.Error.getText());
               x3 = Double.parseDouble(b);
               do{
                    if(i==itera-1)
                    {
                        JOptionPane.showMessageDialog(null, "NO Se encontro la raiz en "+itera+" intentos");
                        NoFound = true;
                        break;
                    }
                    else
                    {
                        AgregarRenglon();
                        NoFound = false;
                    }
                    xi = x3;
                    jTable1.setValueAt(i, i, 0);
                    jTable1.setValueAt(xi, i, 1);
                    jTable1.setValueAt(MetodosNumericos.F(xi), i, 2);
                    if(i==0)
                    {
                        jTable1.setValueAt(i, i, 3);
                        jTable1.setValueAt(i, i, 4);
                        jTable1.setValueAt(i, i, 5);
                        jTable1.setValueAt(i, i, 6);
                        jTable1.setValueAt(i, i, 7);
                        x2=Double.parseDouble(a);
                    }
                    else
                    {
                         jTable1.setValueAt((xi-Double.parseDouble(b)), i, 3);//B
                         jTable1.setValueAt(MetodosNumericos.F(xi)-MetodosNumericos.F(Double.parseDouble(b)), i, 4);//b
                         x2 = xi-(MetodosNumericos.F(xi)*(xi-Double.parseDouble(b))/((MetodosNumericos.F(xi)-MetodosNumericos.F(Double.parseDouble(b)))));//b
                         jTable1.setValueAt(x2, i, 5);
                         jTable1.setValueAt((x2-xi), i, 6);
                         jTable1.setValueAt(MetodosNumericos.F(x2), i, 7);
                     }
                    if(Math.abs(x2-xi)<=Error)
                    {
                        JOptionPane.showMessageDialog(null, "Solucion encontrada en x: "+x2);
                        Resultado1.setText("Solucion encontrada en x: "+x2);
                        Resultado1.setVisible(true);
                        break;
                    }
                    x3 = x2;
                    i++;
               }while(i < itera);
            }
        }
        catch(NumberFormatException o){}
        catch(ArrayIndexOutOfBoundsException g){}
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Resultado = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        Resultado1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        Resultado.setText("Resultado");
        Resultado.setVisible(false);

        jButton1.setText("Cerrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        Resultado1.setText("Resultado");
        Resultado1.setVisible(false);

        jButton2.setText("Cerrar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Iteración", "X(i)", "F(Xi)", "Xi-X1", "F(Xi)-F(X1)", "Xi+1", "Error", "F(Xi+1)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Resultado1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 523, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Resultado1)
                    .addComponent(jButton2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    private void BorrarTabla() {                                         
    int a = Integer.parseInt(FalsaPosicion.Iteraciones.getText()+"");
        while(a>0)
        {
            BorrarRenglon();
            a--;
        }        
    }

    private void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel Resultado;
    public static javax.swing.JLabel Resultado1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}