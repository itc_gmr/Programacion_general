package Tablas;
import javax.swing.JOptionPane;
public class TablaCramer extends javax.swing.JFrame {
    public TablaCramer() {
        super("Capturar Datos");
        initComponents();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public static double Determinante(double M[][])
    {
        double A[][] = new double [M.length][M[0].length-1];
        for (int i=0; i<M.length;i++)
            for (int j=0; j<M[i].length-1;j++)            
                A[i][j]=M[i][j];
        return DeterminanteInterno(A);
    }
     public static double DeterminanteInterno (double M[][])
    {
        double D=0;
        double A[][]= new double [M.length-1][M[0].length-1];
        if (M.length!=1)
        {
            int f,c;
            for (int k=0; k<M.length;k++)
            {
                f=0;
                for (int i=1;i<M.length;i++)
                {
                    c=0;
                    for (int j=0; j<M[i].length; j++)
                    {
                        if (k == j)
                            continue;
                        A[f][c]=M[i][j];
                        c++;
                    }
                f++;   
                }
                D+=Math.pow(-1,(k+1)+1)*M[0][k]*DeterminanteInterno (A);
            }
            return D;
        }
        return M[0][0];
    }
     static double []VecDetermiantes;
    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        TablaDesplegarMatriz TDM = new TablaDesplegarMatriz(); // crear tabla
        TDM.setVisible(false);
        boolean vacio = false;
        double M[][];
        for(int i=0; i<jTable1.getRowCount(); i++)
            for(int j=0; j<jTable1.getColumnCount(); j++)
            {
                if(jTable1.getValueAt(i, j).equals(""))
                {
                    vacio = true;
                    break;
                }
            }
        if(!vacio)
        {
            if(!TodosDouble())
            {
                JOptionPane.showMessageDialog(null, "Las celdas solo pueden tener valor double");
                return;
            }
            else                
            {
                M = getMatrix();
                double m[][] = getMatrix();
                double [][]mc = new double [m.length][m[0].length-1];	 
		copiaMcoeficientes(m,mc);
		Determinante p4=new Determinante();
                VecDetermiantes=new double [m.length];	
		for (int j=0;j<mc[0].length;j++)
		{
			for(int i=0;i<mc.length;i++)
				mc[i][j]=m[i][m[0].length-1];
			VecDetermiantes[j]=p4.Determinante(mc);
		    copiaMcoeficientes(m,mc);
		}              
            }
            // Escribir datos de la matriz a la tabla de resultados
            for(int i=0; i<TDM.jTable1.getRowCount(); i++)
                for(int j=0; j<TDM.jTable1.getColumnCount(); j++)
                    TDM.jTable1.setValueAt(M[i][j], i, j);
            //TDM.setVisible(true);
            
            //Regresion
	    int num = M.length;
            double y[]= new double[M.length];
            TablaDesplegarMatriz VARIABLES = new TablaDesplegarMatriz("VALORES DE LAS INCOGNITAS\n");
            crearVentanaVariables(M.length, VARIABLES);
            
            for(int i = num-1; i >= 0; i--)
            {
                VARIABLES.jTable1.setValueAt(VecDetermiantes[i]/60, 0, i);
            }
            VARIABLES.setVisible(true);
        }
    }//GEN-LAST:event_jTable1KeyReleased
    public static void copiaMcoeficientes(double a[][],double b[][] )
    {
        for(int  i=0;i<a.length;i++)
            for(int j=0;j<a[i].length-1;j++)
                    b[i][j]=a[i][j];
    }
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaCramer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaCramer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaCramer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaCramer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TablaCramer().setVisible(true);
            }
        });
    }
    public void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    public void AgregarColumna(String i)
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo [] = {temp.getColumnCount()+1,"",""};
            temp.addColumn(i,nuevo);
        }catch(NullPointerException e){}
    }
    public void BorrarColumna()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getColumnCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }

    public void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
     private double[][] getMatrix() {
        double a[][] = new double [jTable1.getRowCount()][jTable1.getColumnCount()];
         for(int i=0; i<jTable1.getRowCount(); i++)
            for(int j=0; j<jTable1.getColumnCount(); j++)
                    a[i][j] = Double.parseDouble(jTable1.getValueAt(i, j).toString());
         return a;
    }

    private boolean TodosDouble() {
        boolean respuesta = true;
        int i = 0,j = 0;
        try{
            for(i=0; i<jTable1.getRowCount(); i++)
                for(j=0; j<jTable1.getColumnCount(); j++)
                     Double.parseDouble(jTable1.getValueAt(i, j).toString());
        }catch(NumberFormatException e)
        {
            jTable1.setValueAt("", i, j);
            respuesta = false;
        }
        return respuesta;
    }

    private void crearVentanaMatrizResultante(int n, TablaDesplegarMatriz TDM) {
                
    //agregar (n + 1)  columnas
                for(int i=0; i<=n; i++)
                {
                    if(i<n)
                        TDM.AgregarColumna("X"+(i+1)); 
                    else
                        TDM.AgregarColumna("B"); 
                }
    //borrar todos los renglones Default
                for(int i=0; i<=n; i++)
                    TDM.BorrarRenglon();
    //agregar (n) renglones 
                for(int i=0; i<n; i++)
                    TDM.AgregarRenglon();
                if(n==1)
                    TDM.BorrarRenglon();

    //---------------Dar Tamaño a Ventana------------------------------------------------
                if(n<5)
                    TDM.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50),  ((int)((n-1)*(13.5))) + ((103-(n-1)) + ((n-1)*(n-1))));
                else
                    TDM.jTable1.setSize(((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1))), ((int)((n-1)*(13.5))) + ((98-(n-1)) + ((n-1)*(n-1))));
    //-----------------------------------------------------------------------------------
                // borrar tabla------
                for(int i=0; i<TablaGauss.jTable1.getRowCount(); i++)
                    for(int j=0; j<TablaGauss.jTable1.getColumnCount(); j++)
                        TDM.jTable1.setValueAt("", i, j);
    }

    private void crearVentanaVariables(int n, TablaDesplegarMatriz VARIABLES) 
    {
    //agregar (n + 1)  columnas
        for(int i=0; i<n; i++)
        {
            VARIABLES.AgregarColumna("X"+(i+1));  
        }
    //borrar todos los renglones Default
        for(int i=0; i<=n; i++)
            VARIABLES.BorrarRenglon();
    //agregar 1 renglones 
        VARIABLES.AgregarRenglon();

    //---------------Dar Tamaño a Ventana------------------------------------------------
        VARIABLES.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50), 100);
    //-----------------------------------------------------------------------------------
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}