package Tablas;

import SolucionSistemasDeEcuaciones.Interpolacion;
import javax.swing.JOptionPane;

public class TablaInterpolacion extends javax.swing.JFrame {

    public TablaInterpolacion() {
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        String L = "abcdefghijklmnopqrstuvwxyz";
        TablaDesplegarMatriz TDM = new TablaDesplegarMatriz("SISTEMA DE ECUACIONES"); // crear tabla
        TDM.setVisible(false);
        boolean vacio = false;
        String M[][];
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            for (int j = 0; j < jTable1.getColumnCount(); j++) {
                {
                    if (jTable1.getValueAt(i, j) == null) {
                        jTable1.setValueAt("", i, j);
                    }
                    if (jTable1.getValueAt(i, j).equals("")) {
                        vacio = true;
                        break;
                    }
                }
            }
        }
        if (!vacio) {
            if (!TodosDouble()) {
                JOptionPane.showMessageDialog(null, "Las celdas solo pueden tener valor double");
            } else {
                int n = Integer.parseInt(Interpolacion.Ecuaciones.getText());
                AgregarColumna("Δy");
                if (n > 2) {
                    for (int i = 3; i < n + 1; i++) {
                        AgregarColumna("Δ" + (i - 1) + "y");
                    }
                }
                this.setSize(n * 80, this.getSize().height);
                M = getMatrix();
                boolean err;
                // -- llenar tabla
                for (int i = 2; i < M[0].length; i++)//->
                {
                    for (int j = 0; j < M.length - (i - 1); j++)// v
                    {
                        try {
                            jTable1.setValueAt(Double.parseDouble(jTable1.getValueAt(j + 1, i - 1).toString()) - Double.parseDouble(jTable1.getValueAt(j, i - 1).toString()), j, i);
                        } catch (NumberFormatException e) {
                            System.out.println("Error en i: " + i + " j: " + j + " valor1: " + (M[j + 1][i - 1]) + " valor2: " + (M[j][i - 1]));
                        }
                    }
                }
                M = getMatrix();
                err = false;
                double suma;
                int a, orden = 0;
                //i= n-1
                for (int i = n; i >= 2; i--) {
                    if (!M[(n - i)][(n - 1)].equals(""))// i de 6 a 2
                    {
                        if (Double.parseDouble(M[(n - i)][(n - 1)]) != 0) {
                            orden = i - 1;
                            err = true;
                        }
                    }
                    suma = 0;
                    a = 0;
                    for (int j = (n - i); j >= 0; j--)// 
                    {
                        if (!M[j][i].equals(""))// i de 6 a 2
                        {
                            suma += Double.parseDouble(M[j][i]);
                            a = j;
                        }
                    }
                    System.out.println("Suma: " + suma);
                    if (suma == 0) {
                        if (i > 3) {
                            orden = Integer.parseInt(jTable1.getColumnName(i - 1).charAt(1) + "");
                        } else {
                            if (i == 2) {
                                orden = 1;
                            }
                            if (i == 3) {
                                orden = 2;
                            }
                        }
                    }
                    if (err) {
                        break;
                    }
                }
                String F = " f(x) = ";
                int r = 0;
                for (int i = orden; i >= 1; i--) {
                    if (i != 1) {
                        F += L.charAt(r) + "x^" + i + " + ";
                    } else {
                        F += L.charAt(r) + "x + " + L.charAt(r + 1);
                    }
                    r++;
                }
                JOptionPane.showMessageDialog(null, F, "El orden es: " + orden, 1);// hasta aqui todo bien
                M = getMatrix();
                for (int i = 0; i < M.length; i++) {
                    for (int j = 0; j < M.length; j++) {
                        System.out.print(M[i][j] + "\t");
                    }
                    System.out.println();
                }
                double z[][] = ValoresDeConstantes(M, orden);
                System.out.println("Sistema de ecuaciones ");
                r = 0;
                for (int i = orden; i >= 0; i--) {
                    if (i != 0) {
                        System.out.print(L.charAt(r) + "\t");
                    } else {
                        System.out.print("B" + "\n");
                    }
                    r++;
                }
                desp(z);
                crearVentanaMatrizResultante(orden, TDM, z);//CREAR Y DESPLEGAR SISTEMA DE ECUACIONES
                double FM, Piv;
                for (int i = 0; i < orden; i++) {
                    Piv = z[i][i];
                    for (int j = i; j < orden + 1; j++) {
                        z[i][j] = z[i][j] / Piv;
                    }
                    for (int j = 0; j < orden; j++) {
                        if (i != j) {
                            FM = z[j][i];
                            for (int k = i; k < orden + 1; k++) {
                                z[j][k] -= z[i][k] * FM;
                            }
                        }
                    }
                }
                TablaDesplegarMatriz TDG = new TablaDesplegarMatriz("Solucion del Sistema por Gauss-Jordan");
                crearVentanaMatrizResultante(orden, TDG, z);
                r = 0;
                //-----------------crear f(x)
                F = " f(x) = ";
                for (int i = orden; i >= 1; i--) {
                    if (i != 1) {
                        if (z[r][orden] != 0) {
                            if (z[r][orden] == 1) {
                                F += "x^" + i;
                            } else if (z[r][orden] == (-1)) {
                                F = F.substring(0, F.length() - 2);
                                F += "- x^" + i;
                            } else {
                                F += z[r][orden] + "x^" + i;
                            }
                        }
                    } else if (z[r][orden] != 0) {
                        if (F.charAt(F.length() - 2) != '=') {
                            F += " + ";
                        }
                        if (z[r][orden] == 1) {
                            F += "x";
                            if (Y(0, orden, M) != 0) {
                                F += Y(0, orden, M);
                            }
                        }
                        if (z[r][orden] == (-1)) {
                            F = F.substring(0, F.length() - 2);
                            F += "- x";
                            if (Y(0, orden, M) != 0) {
                                F += " + " + Y(0, orden, M);
                            }
                        } else {
                            F += z[r][orden] + "x";
                            if (Y(0, orden, M) != 0) {
                                F += " + " + Y(0, orden, M);
                            }
                        }
                    }
                    r++;
                }
                JOptionPane.showMessageDialog(null, F, "La Funcion es", 1);
                double h = Double.parseDouble(M[2][0]) - Double.parseDouble(M[1][0]);
                double Xk;
                do {
                    try {
                        Xk = Double.parseDouble(JOptionPane.showInputDialog("Valor a interpolar??"));
                        break;
                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "SOLO NUMEROS REALES", "Error", 2);
                    }
                } while (true);
                System.out.println("x: " + Xk);
                System.out.println("Y: " + Y(Xk, orden, M));
                System.out.println("Y0: " + Y(0, orden, M));
                System.out.print("Teclea el punto a evaluar: ");
                System.out.println();

                System.out.println("Xo: " + Double.parseDouble(M[1][0]));
                System.out.println("Xk: " + Xk);
                System.out.println("h: " + h);

                double k = (Xk - Double.parseDouble(M[0][0])) / h;
                System.out.println("k: " + k + "\n");

                double Yk = Y(k, orden, M);

                JOptionPane.showMessageDialog(null, "Yk = " + Yk, "", 2);
            }
        }
    }//GEN-LAST:event_jTable1KeyReleased
    private static void desp(double M[][]) {
        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M[i].length; j++) {
                System.out.print(M[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static double[][] ValoresDeConstantes(String[][] m, int orden) {
        double constante = Y(0, orden, m), S[][] = new double[orden][orden + 1];
        int i = 0;
        int j = 0;
        do {
            if (i >= m.length) {
                break;
            }
            if (Double.parseDouble(m[i][0]) != 0)// si x != 0 guardar j
            {
                int l = 0;
                for (int k = orden; k >= 1; k--) {
                    if (j >= orden) {
                        break;
                    }
                    S[j][l] = Math.pow(Double.parseDouble(m[i][0]), k);
                    l++;
                    if (l == orden) {
                        S[j][orden] = Double.parseDouble(m[i][1]) - constante;
                        break;
                    }
                }
                j++;
            }
            i++;
        } while (true);
        return S;
    }

    private static double Y(double k, int orden, String M[][]) {
        double Yk = Double.parseDouble(M[0][1]); // orden 0
        if (orden > 0) {
            Yk += (k * Double.parseDouble(M[0][2])); // si orden es mayor que cero calcular orden 1
        }
        double Or2;

        if (orden > 1) {
            Or2 = (k * (k - 1)) / Factorial(2); // si el orden es mayor que 1 calcular orden 2
            Or2 *= Double.parseDouble(M[0][3]);
            Yk += Or2;
        }
        if (orden > 2) {
            double Orx = 0, tmp; // orden x
            for (int i = 3; i <= orden; i++)// calcular todos los ordenes
            {
                //cada valor Orx
                tmp = k;
                for (int j = 1; j < i; j++)// numero de terminos (k-j) por orden
                {
                    tmp *= (k - j);
                }
                Orx = tmp / Factorial(i);
                Orx *= Double.parseDouble(M[0][i + 1]);
                Yk += Orx;
            }
        }
        return Yk;
    }

    private static double Factorial(int numero) {
        double factorial = 1;
        while (numero != 0) {
            factorial = factorial * numero;
            numero--;
        }
        return factorial;
    }

    public void AgregarRenglon() {
        try {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[] = {temp.getRowCount() + 1, "", ""};
            temp.addRow(nuevo);
        } catch (NullPointerException e) {
        }
    }

    public void AgregarColumna(String i) {
        try {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[] = {temp.getColumnCount() + 1, "", ""};
            temp.addColumn(i, nuevo);
        } catch (NullPointerException e) {
        }
    }

    public void BorrarColumna() {
        try {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getColumnCount() - 1);
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    public void BorrarRenglon() {
        try {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount() - 1);
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaInterpolacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaInterpolacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaInterpolacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaInterpolacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TablaInterpolacion().setVisible(true);
            }
        });
    }

    private String[][] getMatrix() {
        int b = jTable1.getRowCount();
        int c = jTable1.getColumnCount();
        String a[][] = new String[b][c];
        for (int i = 0; i < b; i++) {
            for (int j = 0; j < c; j++) {
                if (jTable1.getValueAt(i, j) != null) {
                    a[i][j] = jTable1.getValueAt(i, j).toString();
                } else {
                    a[i][j] = "";
                }
            }
        }
        return a;
    }

    private boolean TodosDouble() {
        boolean respuesta = true;
        int i = 0, j = 0;
        try {
            for (i = 0; i < jTable1.getRowCount(); i++) {
                for (j = 0; j < jTable1.getColumnCount(); j++) {
                    Double.parseDouble(jTable1.getValueAt(i, j).toString());
                }
            }
        } catch (NumberFormatException e) {
            jTable1.setValueAt("", i, j);
            respuesta = false;
        }
        return respuesta;
    }

    private void crearVentanaMatrizResultante(int n, TablaDesplegarMatriz TDM, double M[][]) {

        //agregar (n + 1)  columnas
        for (int i = 0; i <= n; i++) {
            if (i < n) {
                TDM.AgregarColumna("X" + (i + 1));
            } else {
                TDM.AgregarColumna("B");
            }
        }
        //borrar todos los renglones Default
        for (int i = 0; i <= n; i++) {
            TDM.BorrarRenglon();
        }
        //agregar (n) renglones 
        for (int i = 0; i < n; i++) {
            TDM.AgregarRenglon();
        }
        if (n == 1) {
            TDM.BorrarRenglon();
        }

        //---------------Dar Tamaño a Ventana------------------------------------------------
        if (n < 5) {
            TDM.setSize((((int) ((n - 1) * (50))) + ((100 + n) + ((n - 1) * (n - 1))) + 50), ((int) ((n - 1) * (13.5))) + ((103 - (n - 1)) + ((n - 1) * (n - 1))));
        } else {
            TDM.jTable1.setSize(((int) ((n - 1) * (50))) + ((100 + n) + ((n - 1) * (n - 1))), ((int) ((n - 1) * (13.5))) + ((98 - (n - 1)) + ((n - 1) * (n - 1))));
        }
        //-----------------------------------------------------------------------------------
        // borrar tabla------
        //  for(int i=0; i<jTable1.getRowCount(); i++)
        //    for(int j=0; j<jTable1.getColumnCount(); j++)
        //      TDM.jTable1.setValueAt("", i, j);
        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M[i].length; j++) {
                TDM.jTable1.setValueAt(M[i][j], i, j);
            }
        }
        TDM.setVisible(true);
    }

    private void crearVentanaVariables(int n, TablaDesplegarMatriz VARIABLES) {
        //agregar (n + 1)  columnas
        for (int i = 0; i < n; i++) {
            VARIABLES.AgregarColumna("X" + (i + 1));
        }
        //borrar todos los renglones Default
        for (int i = 0; i <= n; i++) {
            VARIABLES.BorrarRenglon();
        }
        //agregar 1 renglones 
        VARIABLES.AgregarRenglon();

        //---------------Dar Tamaño a Ventana------------------------------------------------
        VARIABLES.setSize((((int) ((n - 1) * (50))) + ((100 + n) + ((n - 1) * (n - 1))) + 50), 100);
        //-----------------------------------------------------------------------------------
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
