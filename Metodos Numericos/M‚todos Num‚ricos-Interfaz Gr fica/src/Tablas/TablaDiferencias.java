package Tablas;

import MetNum.MetodosNumericos;
import SolucionSistemasDeEcuaciones.DiferenciasFinitas;
import javax.swing.JOptionPane;

public class TablaDiferencias extends javax.swing.JFrame {

    public TablaDiferencias() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        String L = "abcdefghijklmnopqrstuvwxyz";
        TablaDesplegarMatriz TDM = new TablaDesplegarMatriz("SISTEMA DE ECUACIONES"); // crear tabla
        TDM.setVisible(false);
        boolean vacio = false;
        String M[][];
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            for (int j = 0; j < jTable1.getColumnCount(); j++) {
                {
                    if (jTable1.getValueAt(i, j) == null) {
                        jTable1.setValueAt("", i, j);
                    }
                    if (jTable1.getValueAt(i, j).equals("")) {
                        vacio = true;
                        break;
                    }
                }
            }
        }
        if (!vacio) {
            if (!TodosDouble()) {
                JOptionPane.showMessageDialog(null, "Las celdas solo pueden tener valor double");
            } else {
                int n = Integer.parseInt(DiferenciasFinitas.Ecuaciones.getText());
                AgregarColumna("Δy");
                if (n > 2) {
                    for (int i = 3; i < n + 1; i++) {
                        AgregarColumna("Δ" + (i - 1) + "y");
                    }
                }
                this.setSize(n * 80, this.getSize().height);
                M = getMatrix();
                boolean err;
                // -- llenar tabla
                for (int i = 2; i < M[0].length; i++)//->
                {
                    for (int j = 0; j < M.length - (i - 1); j++)// v
                    {
                        try {
                            jTable1.setValueAt(Double.parseDouble(jTable1.getValueAt(j + 1, i - 1).toString()) - Double.parseDouble(jTable1.getValueAt(j, i - 1).toString()), j, i);
                        } catch (NumberFormatException e) {

                        }
                    }
                }
                M = getMatrix();
                err = false;

                for (int i = 0; i < M.length; i++) {
                    for (int j = 0; j < M.length; j++) {
                        System.out.print(M[i][j] + "\t");
                    }
                    System.out.println();
                }
                int num = Integer.parseInt(DiferenciasFinitas.Ecuaciones.getText()), Limite, Grado = 0;
                boolean Existe = false;
                double h, Suma = 0, Punto, K, K2;
                double[] x;
                String[] EleK = {"1", "2k-1", "3k^2-6k+2", "4k^3-18k^2+22k-6"};
                double[][] m;
                x = new double[num];
                m = new double[num][num];

                for (int i = 0; i < num; i++) {   //x
                    x[i] = Double.parseDouble(jTable1.getValueAt(i, 0) + "");
                    //y
                    m[i][0] = Double.parseDouble(jTable1.getValueAt(i, 1) + "");
                }
                h = x[1] - x[0];
                Limite = m.length - 2;
                do {
                    try {
                        Punto = Double.parseDouble(JOptionPane.showInputDialog("Valor a interpolar??"));
                        break;
                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "SOLO NUMEROS REALES", "Error", 2);
                    }
                } while (true);
                for (int j = 0; j <= m[0].length - 2; j++) {
                    for (int i = 0; i <= Limite; i++) {
                        m[i][j + 1] = m[i + 1][j] - m[i][j];
                    }
                    Limite--;
                }

                //Se busca el grado
                for (int j = m[0].length - 1; j >= 1; j--) {
                    if (m[0][j] != 0) {
                        Grado = j;
                        Existe = true;
                        break;
                    }
                }
                JOptionPane.showMessageDialog(null, "El grado es: " + Grado);
                K = (Punto - x[0]) / h;
                K2 = 1;
                //Obtenesf(Xk)
                System.out.println("Imprimimos la K");
                for (int j = 1; j <= (Grado) && j <= 3; j++) {
                    MetodosNumericos.F = EleK[j - 1];
                    Suma = Suma + ((MetodosNumericos.Evaluar(K, "k") * m[0][j]) / factorial(j));
                    K2 = K;
                }
                        
                Suma = Suma * (1 / h);

                JOptionPane.showMessageDialog(null, "\n F'(" + Punto + ") = " + Suma);

            }
        }
    }//GEN-LAST:event_jTable1KeyReleased
    public boolean Comprobar(double[][] m, int Grado) {
        int j = Grado;
        for (int i = 0; i < m.length - j; i++) {
            if (m[i][j] != m[0][j]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaDiferencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaDiferencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaDiferencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaDiferencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TablaDiferencias().setVisible(true);
            }
        });
    }

    public void AgregarRenglon() {
        try {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[] = {temp.getRowCount() + 1, "", ""};
            temp.addRow(nuevo);
        } catch (NullPointerException e) {
        }
    }

    public void AgregarColumna(String i) {
        try {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[] = {temp.getColumnCount() + 1, "", ""};
            temp.addColumn(i, nuevo);
        } catch (NullPointerException e) {
        }
    }

    public void BorrarColumna() {
        try {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getColumnCount() - 1);
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    public int factorial(int x) {
        int factorial = 1;
        if (x == 0 || x == 1) {
            return 1;
        }
        for (int i = x; i > 1; i--) {
            factorial = factorial * i;
        }
        return factorial;
    }

    public void BorrarRenglon() {
        try {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount() - 1);
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    private boolean TodosDouble() {
        boolean respuesta = true;
        int i = 0, j = 0;
        try {
            for (i = 0; i < jTable1.getRowCount(); i++) {
                for (j = 0; j < jTable1.getColumnCount(); j++) {
                    Double.parseDouble(jTable1.getValueAt(i, j).toString());
                }
            }
        } catch (NumberFormatException e) {
            jTable1.setValueAt("", i, j);
            respuesta = false;
        }
        return respuesta;
    }

    private String[][] getMatrix() {
        int b = jTable1.getRowCount();
        int c = jTable1.getColumnCount();
        String a[][] = new String[b][c];
        for (int i = 0; i < b; i++) {
            for (int j = 0; j < c; j++) {
                if (jTable1.getValueAt(i, j) != null) {
                    a[i][j] = jTable1.getValueAt(i, j).toString();
                } else {
                    a[i][j] = "";
                }
            }
        }
        return a;
    }

    private double[][] getMatrixD() {
        int b = jTable1.getRowCount();
        int c = jTable1.getColumnCount();
        double a[][] = new double[b][c];
        for (int i = 0; i < b; i++) {
            for (int j = 0; j < c; j++) {
                try {
                    if (jTable1.getValueAt(i, j) != null) {
                        a[i][j] = Double.parseDouble(jTable1.getValueAt(i, j).toString());
                    } else {
                        a[i][j] = 0;
                    }
                } catch (Exception e) {
                }
            }
        }
        return a;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
