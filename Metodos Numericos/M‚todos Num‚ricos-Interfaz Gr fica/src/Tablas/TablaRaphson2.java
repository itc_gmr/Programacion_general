package Tablas;
import MetNum.MetodosNumericos;
import MetodosIterativos.Raphson2;
import javax.swing.JOptionPane;
public class TablaRaphson2 extends javax.swing.JFrame {
    public static boolean NoFound = false;
    public TablaRaphson2() {
        initComponents();
        try
        {
            double x = Double.parseDouble(Raphson2.X.getText()), xi,
                   Error = Double.parseDouble(Raphson2.Error.getText());
	    int itera = Integer.parseInt(Raphson2.Iteraciones.getText());
            int i = 0;
            double x2=x;
            do			 
            {
                if(i==itera)
                {
                    JOptionPane.showMessageDialog(null, "No se encontro la solucion en "+(i+1)+" intentos");
                    NoFound = true;
                    break;
                }
                else
                {
                    NoFound = false;
                    xi=x2;
                    AgregarRenglon();
                    jTable1.setValueAt(i, i, 0);
                    jTable1.setValueAt(xi, i, 1);
                    jTable1.setValueAt(MetodosNumericos.F(xi), i, 2);
                    jTable1.setValueAt(MetodosNumericos.dF(xi), i, 3);
                    jTable1.setValueAt(MetodosNumericos.d2F(xi), i, 4);
                    x2 = x2(xi);
                    jTable1.setValueAt(x2, i, 5);
                    jTable1.setValueAt(MetodosNumericos.F(x2), i, 6);
                    jTable1.setValueAt(x2-xi, i, 7);
                }

                if(MetodosNumericos.F(xi)==0)
                {
                    JOptionPane.showMessageDialog(null, "Solucion exacta: "+xi);
                    NoFound = false;
                    break;
                }
                i++;
            }while(Math.abs(x2-xi)>Error);
            if(i<itera)
            {
                JOptionPane.showMessageDialog(null, "Solucion encontrada en: "+x2);
                Resultado1.setText("Solucion encontrada en x: "+x2);
                NoFound = false;
            }
        }
        catch(NumberFormatException o){}
        catch(ArrayIndexOutOfBoundsException g){}
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Resultado1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        Resultado2 = new javax.swing.JLabel();
        Cerrar = new javax.swing.JButton();

        Resultado1.setText("Resultado");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Iteración", "Xi", "F(Xi)", "F'(Xi)", "F''(Xi)", "Xi+1", "F(Xi+1)", "Error"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);

        Resultado2.setText("Resultado");

        Cerrar.setText("Cerrar");
        Cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(Resultado2, javax.swing.GroupLayout.PREFERRED_SIZE, 347, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Cerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 710, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 522, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Resultado2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Cerrar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void CerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CerrarActionPerformed
        dispose();
    }//GEN-LAST:event_CerrarActionPerformed
    public static double xi_1(double x)
    {
        return x - (MetodosNumericos.F(x)/MetodosNumericos.dF(x));
    }
    private void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    private void BorrarTabla() {                                         
    int a = Integer.parseInt(Raphson2.Iteraciones.getText()+"");
        while(a>0)
        {
            BorrarRenglon();
            a--;
        }        
    }

    private void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
       public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaRaphson2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaRaphson2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaRaphson2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaRaphson2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TablaRaphson2().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Cerrar;
    public static javax.swing.JLabel Resultado1;
    public static javax.swing.JLabel Resultado2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    private double x2(double xi) {
        return xi-MetodosNumericos.F(xi)/(MetodosNumericos.dF(xi)-MetodosNumericos.F(xi)*MetodosNumericos.d2F(xi)/(2*MetodosNumericos.dF(xi)));
    }
}