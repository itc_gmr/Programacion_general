package Tablas;
public class TablaDesplegarMatriz extends javax.swing.JFrame {
    public TablaDesplegarMatriz() 
    {
        super("MATRIZ RESULTANTE");
        initComponents();
    }
    public TablaDesplegarMatriz(String a) 
    {
        super(a);
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 311, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public static double[][] getMatrix() {
        int b=jTable1.getRowCount();
        int c=jTable1.getColumnCount();
        double a[][] = new double [b][c];
         for(int i=0; i<b; i++)
            for(int j=0; j<c; j++)
            {
                    if(jTable1.getValueAt(i, j)!=null)
                        a[i][j] = Double.parseDouble(jTable1.getValueAt(i, j).toString());
                    else
                        a[i][j]=0;
            }
         return a;
    }
    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
   
    }//GEN-LAST:event_jTable1KeyReleased
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaDesplegarMatriz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaDesplegarMatriz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaDesplegarMatriz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaDesplegarMatriz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TablaDesplegarMatriz().setVisible(true);
            }
        });
    }
     public void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    public void AgregarColumna(String i)
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo [] = {temp.getColumnCount()+1,"",""};
            temp.addColumn(i,nuevo);
        }catch(NullPointerException e){}
    }
    public void BorrarColumna()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getColumnCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }

    public void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
