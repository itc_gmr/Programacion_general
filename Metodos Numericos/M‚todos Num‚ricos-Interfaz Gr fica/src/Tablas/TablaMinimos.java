package Tablas;
import SolucionSistemasDeEcuaciones.Minimos;
import javax.swing.JOptionPane;
public class TablaMinimos extends javax.swing.JFrame {
    public TablaMinimos() {
        super("Capturar Datos");
        initComponents();       
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
public void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    public void AgregarColumna(String i)
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo [] = {temp.getColumnCount()+1,"",""};
            temp.addColumn(i,nuevo);
        }catch(NullPointerException e){}
    }
    public void BorrarColumna()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getColumnCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }

    public void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        TablaDesplegarMatriz TDM = new TablaDesplegarMatriz(); // crear tabla
        TDM.setVisible(false);
        boolean vacio = false;
        double M[][] = null;
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            for (int j = 0; j < jTable1.getColumnCount(); j++) {
                if (jTable1.getValueAt(i, j).equals("")) {
                    vacio = true;
                    break;
                }
            }
        }
        if (!vacio) {
            if (!TodosDouble()) {
                JOptionPane.showMessageDialog(null, "Las celdas solo pueden tener valor double");
                return;
            } else
            {
                int n,m;
		double x[],xy[];
		double mat[][],Sist[][];
		double Xk;
                String m1;
                
		//Leer datos
                n = Integer.parseInt(Minimos.Ecuaciones.getText());
		mat=new double [n][2];
		do
		{
                    m1 = JOptionPane.showInputDialog("Teclee m");
                    m=Integer.parseInt(m1);
		}while(m<=0);
		x=new double[m+3];
		xy=new double[m+1];
		Sist=new double[m+1][m+2];
		for(int i=0;i<n;i++)
		{
			mat[i][0]=Double.parseDouble(jTable1.getValueAt(i, 0)+"");
			mat[i][1]=Double.parseDouble(jTable1.getValueAt(i, 1)+"");
		}
                m1 = JOptionPane.showInputDialog("Teclea el valor de x a evaluar: ");
		Xk=Double.parseDouble(m1);
		//Desplegar encabezado
                for(int i=0; i<TDM.jTable1.getColumnCount(); i++)
                    TDM.BorrarColumna();
                TDM.AgregarColumna("x");
                TDM.AgregarColumna("y");
		for(int i=0;i<m+1;i++)
                    TDM.AgregarColumna("x^"+(i+2));
		for(int i=0;i<m;i++)
                {
                    if(i==0)
                        TDM.AgregarColumna("x^y");
                    else
                        TDM.AgregarColumna("x^("+(i+1)+")y");
                }		
		//Desplegar Tabla
		x[0]=n;
                int r=TDM.jTable1.getRowCount();
                for(int i=0; i<r; i++)
                    TDM.BorrarRenglon();
		for(int i=0;i<n;i++)
		{
                    TDM.AgregarRenglon();
                    TDM.jTable1.setValueAt(mat[i][0], i, 0);
                    x[1]=x[1]+mat[i][0];    
                    TDM.jTable1.setValueAt(mat[i][1], i, 1);
                    xy[0]=xy[0]+mat[i][1];
                    for(int j=2;j<x.length;j++)
                    {
                        TDM.jTable1.setValueAt(Math.pow(mat[i][0], j), i, j);
                        x[j]=x[j]+Math.pow(mat[i][0], j);
                    }
                    for(int j=(2*m+1);j<(2*m+1)+xy.length-1;j++)
                    {
                        TDM.jTable1.setValueAt(Math.pow(mat[i][0], j-2*m)*mat[i][1], i, j);
                        xy[j-2*m]=xy[j-2*m]+Math.pow(mat[i][0], j-2*m)*mat[i][1];
                    }
                    System.out.println();	
		}
		//Crear sistemas
		for(int i=0;i<m+1;i++)
		{
			for(int j=0;j<m+1;j++)
				Sist[i][j]=x[j+i];
			Sist[i][Sist[i].length-1]=xy[i];
		}
		//Desplegar Sistema
		TablaDesplegarMatriz a = new TablaDesplegarMatriz("Sistema Resultante");
		crearVentanaMatrizResultante(Sist.length, a);
              // Escribir datos de la matriz a la tabla de resultados
            for (int i = 0; i < a.jTable1.getRowCount(); i++) {
                for (int j = 0; j < a.jTable1.getColumnCount(); j++) {
                    a.jTable1.setValueAt(Sist[i][j], i, j);
                }
            }
            a.setVisible(true);
		//Resolver Sistema
                if(!GaussJordan(Sist)) 
		{
			System.out.println("Error No se puede resolver el sistema ");
			return;
		}
		//Desplegar funcion
		String Funcion="F(x)=";
		double Func[]=new double  [m+1];
		double Xk2=0;
		for(int i=0;i<Sist.length;i++)
		{
                    if(i==0)
                    Funcion=Funcion+Sist[i][Sist[i].length-1];
                    if(Sist[i][Sist[i].length-1]>0&&i!=0)
                            Funcion=Funcion+"+"+Sist[i][Sist[i].length-1]+"x^("+i+")";
                    if(Sist[i][Sist[i].length-1]<0&&i!=0)
                            Funcion=Funcion+Sist[i][Sist[i].length-1]+"x^("+i+")";
                    Func[i]=Sist[i][Sist[i].length-1];
		}
                
                JOptionPane.showMessageDialog(null,Funcion);
		//Desplegar f(xk)
		for(int i=0;i<Sist.length;i++)
			Xk2=Xk2+Math.pow(Xk, i)*Sist[i][Sist[i].length-1];
                JOptionPane.showMessageDialog(null,"F("+Xk+")= "+Xk2);
                
            }
            TDM.setVisible(true);
        }
    }//GEN-LAST:event_jTable1KeyReleased
    public static boolean IntercambiarRenglones(double m[][],int k)
	{
		double n;
		for(int i=k+1;i<m.length;i++)
			if(m[i][k]!=0)
			{
				for(int j=0;j<m[i].length;j++)
				{
					n=m[k][j]; //pasar los elementos del reglon pivpote cero a una variable
					m[k][j]=m[i][j];//Pasar los elementos del renglon con pivote difernte de cero al renglon con pivote cero 
					m[i][j]=n;//pasar los elmentos de la variable al reglon que tenia pivote diferente de cero
				}
				return true;
			}
		return false;
	}
    public static boolean GaussJordan(double [][] m)
	{
		double Mul;
		double pivote;
		double dert;
		Determinante p= new Determinante();
		dert=p.Determinante(m);
		if(dert==0)
			return false;
		for (int k=0;k<m.length;k++)
		{
			if(m[k][k]==0)
				if(!IntercambiarRenglones(m,k))//cambiar los renglones para quitar el cero
					return false;
			pivote=m[k][k];
			for(int j=k;j<m[k].length;j++)
				m[k][j]=m[k][j]/pivote;
			for(int i=0;i<m.length;i++)
				if(k!=i)
				{
					Mul=m[i][k];
					for(int j=k;j<m[i].length;j++)
						m[i][j]=m[i][j]-Mul*m[k][j];
				}
		}
		return true;
	}
    private void crearVentanaMatrizResultante(int n, TablaDesplegarMatriz TDM) {
//agregar (n + 1)  columnas
        for(int i=0; i<=n; i++)
        {
            if(i<n)
                TDM.AgregarColumna("X"+(i+1)); 
            else
                TDM.AgregarColumna("B"); 
        }
//borrar todos los renglones Default
        for(int i=0; i<=n; i++)
            TDM.BorrarRenglon();
//agregar (n) renglones 
        for(int i=0; i<n; i++)
            TDM.AgregarRenglon();
        if(n==1)
            TDM.BorrarRenglon();

//---------------Dar Tamaño a Ventana------------------------------------------------
        if(n<5)
            TDM.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50),  ((int)((n-1)*(13.5))) + ((103-(n-1)) + ((n-1)*(n-1))));
        else
            TDM.setSize(((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1))), ((int)((n-1)*(13.5))) + ((98-(n-1)) + ((n-1)*(n-1))));
//-----------------------------------------------------------------------------------
        // borrar tabla------
        for(int i=0; i<TDM.jTable1.getRowCount(); i++)
            for(int j=0; j<TDM.jTable1.getColumnCount(); j++)
                TDM.jTable1.setValueAt("", i, j);
    }

    private void crearVentanaVariables(int n, TablaDesplegarMatriz VARIABLES) 
    {
    //agregar (n + 1)  columnas
        for(int i=0; i<n; i++)
        {
            VARIABLES.AgregarColumna("X"+(i+1));  
        }
    //borrar todos los renglones Default
        for(int i=0; i<=n; i++)
            VARIABLES.BorrarRenglon();
    //agregar 1 renglones 
        VARIABLES.AgregarRenglon();

    //---------------Dar Tamaño a Ventana------------------------------------------------
        VARIABLES.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50), 100);
    //-----------------------------------------------------------------------------------
    }
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaMinimos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaMinimos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaMinimos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaMinimos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TablaMinimos().setVisible(true);
            }
        });
    }
     private double[][] getMatrix() {
        double a[][] = new double [jTable1.getRowCount()][jTable1.getColumnCount()];
        for(int i=0; i<jTable1.getRowCount(); i++)
        for(int j=0; j<jTable1.getColumnCount(); j++)
            a[i][j] = Double.parseDouble(jTable1.getValueAt(i, j).toString());
        return a;
    }
    public static void des_mat(double m[][])
    {
        for(int  i=0;i<m.length;i++)
        {
            for(int j=0;j<m[i].length;j++)
                System.out.print(m[i][j]);
            System.out.println();
        }
    }
    private boolean TodosDouble() {
        boolean respuesta = true;
        int i = 0,j = 0;
        try{
            for(i=0; i<jTable1.getRowCount(); i++)
                for(j=0; j<jTable1.getColumnCount(); j++)
                     Double.parseDouble(jTable1.getValueAt(i, j).toString());
        }catch(NumberFormatException e)
        {
            jTable1.setValueAt("", i, j);
            respuesta = false;
        }
        return respuesta;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}