package Tablas;
import javax.swing.JOptionPane;
public class TablaGauss extends javax.swing.JFrame {
public static boolean NoFound = false;
    public TablaGauss() 
    {
        super("MATRIZ ORIGINAL");
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 105, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        TablaDesplegarMatriz TDM = new TablaDesplegarMatriz(); // crear tabla
        TDM.setVisible(false);
        boolean vacio = false;
        double M[][];
        for(int i=0; i<jTable1.getRowCount(); i++)
            for(int j=0; j<jTable1.getColumnCount(); j++)
            {
                if(jTable1.getValueAt(i, j).equals(""))
                {
                    vacio = true;
                    break;
                }
            }
        if(!vacio)
        {
            if(!TodosDouble())
            {
                JOptionPane.showMessageDialog(null, "Las celdas solo pueden tener valor double");
                return;
            }
            else                
            {
                double FM,Piv;
                M = getMatrix();
                for (int i=0; i<M.length; i++)
                {
                    Piv=M[i][i];
                    for (int j=i; j<M.length; j++)
                    {
                        if (i!=j)
                        {
                            FM=M[j][i]/Piv;
                            for (int k=i; k<M[0].length; k++)
                                M[j][k]-=M[i][k]*FM;
                        }
                    }
                }
              crearVentanaMatrizResultante(M.length, TDM);
            }
            // Escribir datos de la matriz a la tabla de resultados
            for(int i=0; i<TDM.jTable1.getRowCount(); i++)
                for(int j=0; j<TDM.jTable1.getColumnCount(); j++)
                    TDM.jTable1.setValueAt(M[i][j], i, j);
            TDM.setVisible(true);
            
            //Regresion
	    int num = M.length;
            double y[]= new double[M.length];
            y[num-1] = M[num-1][num]/M[num-1][num-1];
            double suma;
            for(int i = (num - 2); i >= 0; i--)
            {
                suma = 0;
                for(int j = i+1; j<num; j++)
                {
                    suma = suma + M[i][j]* y[j];
                }
                y[i] = (M[i][num] - suma)/M[i][i];
            }
            TablaDesplegarMatriz VARIABLES = new TablaDesplegarMatriz("VALORES DE LAS INCOGNITAS\n");
            crearVentanaVariables(M.length, VARIABLES);
            
            for(int i = num-1; i >= 0; i--)
            {
                VARIABLES.jTable1.setValueAt(y[i], 0, i);
            }
            VARIABLES.setVisible(true);
        }
    }//GEN-LAST:event_jTable1KeyReleased
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TablaGauss.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TablaGauss.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TablaGauss.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TablaGauss.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TablaGauss().setVisible(true);
            }
        });
    }
    public void AgregarRenglon()
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo[]= {temp.getRowCount()+1,"",""};
            temp.addRow(nuevo);
        }catch(NullPointerException e){}
    }
    public void AgregarColumna(String i)
    {
        try{
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            Object nuevo [] = {temp.getColumnCount()+1,"",""};
            temp.addColumn(i,nuevo);
        }catch(NullPointerException e){}
    }
    public void BorrarColumna()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getColumnCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }

    public void BorrarRenglon()
    {
        try
        {
            javax.swing.table.DefaultTableModel temp = (javax.swing.table.DefaultTableModel) jTable1.getModel();
            temp.removeRow(temp.getRowCount()-1);
        }
        catch(ArrayIndexOutOfBoundsException e){}
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables

    private double[][] getMatrix() {
        double a[][] = new double [jTable1.getRowCount()][jTable1.getColumnCount()];
         for(int i=0; i<jTable1.getRowCount(); i++)
            for(int j=0; j<jTable1.getColumnCount(); j++)
                    a[i][j] = Double.parseDouble(jTable1.getValueAt(i, j).toString());
         return a;
    }

    private boolean TodosDouble() {
        boolean respuesta = true;
        int i = 0,j = 0;
        try{
            for(i=0; i<jTable1.getRowCount(); i++)
                for(j=0; j<jTable1.getColumnCount(); j++)
                     Double.parseDouble(jTable1.getValueAt(i, j).toString());
        }catch(NumberFormatException e)
        {
            jTable1.setValueAt("", i, j);
            respuesta = false;
        }
        return respuesta;
    }

    private void crearVentanaMatrizResultante(int n, TablaDesplegarMatriz TDM) {
                
    //agregar (n + 1)  columnas
                for(int i=0; i<=n; i++)
                {
                    if(i<n)
                        TDM.AgregarColumna("X"+(i+1)); 
                    else
                        TDM.AgregarColumna("B"); 
                }
    //borrar todos los renglones Default
                for(int i=0; i<=n; i++)
                    TDM.BorrarRenglon();
    //agregar (n) renglones 
                for(int i=0; i<n; i++)
                    TDM.AgregarRenglon();
                if(n==1)
                    TDM.BorrarRenglon();

    //---------------Dar Tamaño a Ventana------------------------------------------------
                if(n<5)
                    TDM.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50),  ((int)((n-1)*(13.5))) + ((103-(n-1)) + ((n-1)*(n-1))));
                else
                    TDM.jTable1.setSize(((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1))), ((int)((n-1)*(13.5))) + ((98-(n-1)) + ((n-1)*(n-1))));
    //-----------------------------------------------------------------------------------
                // borrar tabla------
                for(int i=0; i<TablaGauss.jTable1.getRowCount(); i++)
                    for(int j=0; j<TablaGauss.jTable1.getColumnCount(); j++)
                        TDM.jTable1.setValueAt("", i, j);
    }

    private void crearVentanaVariables(int n, TablaDesplegarMatriz VARIABLES) 
    {
    //agregar (n + 1)  columnas
        for(int i=0; i<n; i++)
        {
            VARIABLES.AgregarColumna("X"+(i+1));  
        }
    //borrar todos los renglones Default
        for(int i=0; i<=n; i++)
            VARIABLES.BorrarRenglon();
    //agregar 1 renglones 
        VARIABLES.AgregarRenglon();

    //---------------Dar Tamaño a Ventana------------------------------------------------
        VARIABLES.setSize((((int)((n-1)*(50))) + ((100 + n) + ((n-1)*(n-1)))+ 50), 100);
    //-----------------------------------------------------------------------------------
    }
}