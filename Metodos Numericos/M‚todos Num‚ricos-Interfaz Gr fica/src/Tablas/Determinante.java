package Tablas;
public class Determinante
{
	public double Determinante(double [][] m)
	{
		double Determinantes=1;
		double Mul;
		double [][]m2=new double [m.length][m.length+1];
		copia(m,m2);
		for (int k=0;k<m.length-1;k++)
		{
			if(m2[k][k]==0)
				if(IntercambiarRenglones(m2,k))//cambiar los renglones para quitar el cero
					Determinantes=Determinantes*-1;
				else
					return Determinantes=0;   
			for(int i=k+1;i<m.length;i++)
			{
				Mul=m2[i][k]/m2[k][k];
				for(int j=k;j<m[i].length;j++)
					m2[i][j]=m2[i][j]-Mul*m2[k][j];
			}
			Determinantes=Determinantes*m2[k][k];	//Se multiplica por que falto el ultimo pivote de la matriz  en el for  
		}
		Determinantes=Determinantes*m2[m2.length-1][m2.length-1];
		return Determinantes;

	}
	public boolean IntercambiarRenglones(double m[][],int k)
	{
		double []x;
		int n=0;
		for(int i=k+1;i<m.length;i++)
			if(m[i][k]!=0)
			{
				
				x=new double [m[i].length-k];//Se crea el vector desde el pivote  para evitar intercambiar los ceros con  ceros  
				for(int j=k;j<m[i].length;j++)
				{
					x[n]=m[k][j]; //pasar los elementos del reglon pivpote cero a un vector
					m[k][j]=m[i][j];//Pasar los elementos del renglon con pivote difernte de cero al renglon con pivote cero 
					m[i][j]=x[n];//pasar los elmentos del vector al reglon que tenia pivote diferente de cero
					n++;
				}
				return true;
			}
		return false;
	}
	
	public static void copia(double a[][],double b[][] )
	{
		for(int  i=0;i<a.length;i++)
			for(int j=0;j<a[i].length;j++)//No se ocupan copiar los terminos independientes
				b[i][j]=a[i][j];		
	}
}
