/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imágenes;

import java.awt.GridLayout;
import javax.swing.*;

/**
 *
 * @author LuisFernando
 */
public class Resultado extends JFrame {

    Persona[] people;

    public Resultado(Persona[] people) {
        this.people = people;
        interfaz();
    }

    public void interfaz() {
        setSize(300, 300);
        setLocationRelativeTo(null);
        setLayout(new GridLayout(0, 4));
        setResizable(false);
        add(new JLabel("Nombre"));
        add(new JLabel("Edad"));
        add(new JLabel("Dirección"));
        add(new JLabel("Sexo"));

        for (Persona people1 : people) {
            if (people1 == null) {
                break;
            }
            add(new JLabel(people1.nombre + ""));
            add(new JLabel(people1.edad + ""));
            add(new JLabel(people1.adress + ""));
            add(new JLabel(people1.sexo + ""));
        }
        setVisible(true);
    }
}
