/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imágenes;

import java.util.Arrays;
import java.util.Scanner;

import javax.swing.JLabel;


public class Main {
	private int numPuntos=0;
	private double[] points;
	private double[][] matriz;
	private Scanner leer=new Scanner(System.in);
	
	public Main() {
		Start();
	}

	private void Start() {
		System.out.println("Ingresa Numero Puntos: ");
		
		numPuntos=Integer.parseInt(leer.next());
		matriz=new double[numPuntos][numPuntos];
		
		for(int i=0;i<numPuntos;i++){
			System.out.println("Ingresa X "+(i+1)+": ");
			matriz[i][0]=Double.parseDouble(leer.next());
			System.out.println("Ingresa Y "+(i+1)+": ");
			matriz[i][1]=Double.parseDouble(leer.next());
		}
		int grado=solDiferencias(matriz);
		genMatrizSis(matriz,grado);
	}
	
	public int solDiferencias(double[][] matx){//Regresa el grado de la funcion
		double[][] temp=matx;
		int m=this.numPuntos;
		double[][] deltaI = new double[m][m];
		int gradoFuncion=0;//Grado de la funcion
		
		for(int j = 0; j < 1; j++)
			for(int i=0;i<m;i++)
				System.out.println(temp[i][j]+"  "+temp[i][j+1]);
		
		
		for (int j = 1; j < (m - 1); j++) {
			for (int i = 0; i < (m - j); i++) {
				temp[i][j+1] =(temp[i + 1][j] - temp[i][j]);
			}

			gradoFuncion++;
		}	
				
		return gradoFuncion;
	}
	
	public void genMatrizSis(double[][] mat,int grado){//Genera la Matriz de solucion del Sistema
		//Solo se requiere 'X' y 'Y' mat[i][0] y mat[i][1]
		double matSol[][] = new double[grado+1][grado+2];
		int auxGrado=grado;//Auxiliar de Grado que ira decrementando
		
		System.out.println("Tamaño: "+matSol.length);
		
		for(int i=0;i<mat.length-1;i++){//Llena la matriz sin aumentar
			for(int j=0;j<mat.length-1;j++){
				matSol[i][j]=Math.pow(mat[i][0],auxGrado);
				//System.out.println(i+"  "+j+" = "+matSol[i][j]);
				auxGrado--;
			}
			auxGrado=grado;
		}
		
		for(int i=0;i<mat.length-1;i++){//Llena el aumentado de la matriz
			matSol[i][mat.length-1]=mat[i][1];
			//System.out.println(matSol[i][mat.length-1]);
		}		
		this.Cramer(matSol, matSol.length);
	}
	
	public void Cramer(double[][] mat,int m/*Tamaño*/)
	  {
		double[] incognitas = new double[m];
		double[][] aux = new double[m][m + 1];
		double deter = determinante(mat, m);

		for (int i = 0; i < m; i++)// meter datos a auxiliar
			System.arraycopy(mat[i], 0, aux[i], 0, m);

		for (int j = 0; j < m; j++)// escoger columnas
		{
			for (int i = 0; i < m; i++)// sustituir columna
				aux[i][j] = mat[i][m];
			
			incognitas[j] = determinante(aux, m) / deter;

			for (int i = 0; i < m; i++)// restaurar valores
										// a auxiliar
				aux[i][j] = mat[i][j];
		}

		// ---Imprimir incognitas
		for (int cont = 0; cont < m; cont++) {
			System.out.println("X " + (cont + 1) + " = "+ incognitas[cont]);
		}
	      
	  }		

	// Sacar Detarminante
	public double determinante(double[][] matriz, int n) {
		double resultado = 0;

		if (n < 2)
			return 0;
		if (n == 2) {
			resultado = matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0];
			return resultado;
		}

		int i_prohibida = 0;
		int j_prohibida = 0;
		int i_aux_sig = 0;
		int j_aux_sig = 0;
		boolean sumar = true;

		for (int cont = 0; cont < n; cont++)// numero que multiplica a la
											// matriz menor
		{
			double aux[][] = new double[n - 1][n - 1];
			j_prohibida = cont;
			for (int i = 0; i < n; i++)// renglon
			{
				for (int j = 0; j < n; j++)// columna
				{
					if (j != j_prohibida && i != i_prohibida) {
						aux[i_aux_sig][j_aux_sig] = matriz[i][j];

						if (j_aux_sig >= n - 2) {
							j_aux_sig = 0;
							i_aux_sig++;
						} else
							j_aux_sig++;
					} // for j
				} // for i
			}
			if (sumar) {
				resultado = resultado + matriz[i_prohibida][j_prohibida] * determinante(aux, n - 1);
				sumar = false;
			} else {
				resultado = resultado - matriz[i_prohibida][j_prohibida] * determinante(aux, n - 1);
				sumar = true;
			}
			j_aux_sig = 0;
			i_aux_sig = 0;
		}
		return resultado;
	}	
	
	public static void main(String[] args) {
		new Main();
		/*Formatea f=new Formatea();
		double x=.0001;
		System.out.println(f.person("############.############", x));*/
                new Main ();
	}

}