package MetNum;
import java.awt.*;
@SuppressWarnings("serial")
class Graf extends Canvas {
  /* VARIABLES PARA EL GRAFICADOR */
  static final  int   M  = 720;
  static final String TITULO = "Metodos Numericos";
  static final String EJEX   = "Y"; // etiquetas de la grafica
  static final String EJEY   = "X";// error en los labels estan intercambiados a proposito
  static double x[] = new double[M];
  static double y[] = new double[M];
  static int xi[] = new int[M];
  static int yi[] = new int[M];
  static double ymax,ymin,yrango;
  static double xmax,xmin,xrango;
  int  fx;
  public  void setFun(int funcion)// en esta clase tambien estan declarados las funciones y estan indiciadas con fx 
  { 
	  fx = funcion; 
  }
  public  int getFun() { return  fx; }
  public void paint(Graphics lg)
  {
	  int i,domi,ydiv;
	
	  /*area rectangular donde se va a graficar la funci�n */
	  int x1=0, y1=20, x2=610, y2=400;
	
	  /*para determinar el origen de los ejes x,y para las coordenadas normalizadas*/
	  int xorigen,yorigen;
	
	  /* para obtener el indice del arreglo para los valores del origen */
	  int indxor=0, indyor=0;
	
	  double ymax, ymin, yrango, xmax, xmin, xrango;
	  Font f;
	  FontMetrics fm;

	  lg.drawRect(x1,y1,x2,y2);   /* dibuja borde */
	  lg.setColor(new Color(0,0,0));   /* color de la grafica */
	
	  lg.fillRect(x1+1,y1+1,x2-1,y2-1);    /* dibuja borde */
	  lg.setColor(Color.blue);   /* establece color */
	  domi=-360;
	  for (i=0; i<M; i++)    //se maneja localidades del 0 a M para el arreglo
	  {
        x[i]=i + domi;  
        if (i + domi == 0) indxor = i;  /* para obtener la cordenada x cuando es 0 u origen */
          y[i]= MetodosNumericos.F(x[i]);//--------------------------------------------<<<<<
        if ( y[i] <= 0.0 ) indyor = i;    /* cuando la yi es negativa atraviesa el eje x */
	  }   /* for */
    f  = new Font("Courier", Font.BOLD, 18);
    fm = getFontMetrics(f);
    lg.setFont(f);
    int xtit = ( getSize().width  - fm.stringWidth(TITULO)) / 2;
    lg.drawString(TITULO, xtit ,20);
    int xejx =( getSize().width  - fm.stringWidth(EJEX)) / 2;
    lg.drawString(EJEX, xejx ,getSize().height - 20);
    int yejy =( getSize().height - EJEY.length()*fm.getHeight()) / 2;
    for ( ydiv = EJEY.length();  ydiv < EJEY.length(); ydiv++)
     {
        lg.drawString(""+EJEY.charAt(ydiv), 20, yejy + fm.getHeight()*ydiv);
     }
    ymax= y[0];
    ymin= y[0];
    xmax= x[0];
    xmin= x[0];
  for (i=0; i<M; i++)
  {
    if (y[i]>ymax) ymax = y[i];
    if (y[i]<ymin) ymin = y[i];
    if (x[i]>xmax) xmax = x[i];
    if (x[i]<xmin) xmin = x[i];
  }
  yrango =  ymax - ymin;
  xrango =  xmax - xmin;

  /* coordenada x para el origen ya normalizada */
   xorigen = (int) ( x1 + Math.floor( ((x[indxor]-xmin) / xrango) * (x2-x1)) );
  /* coordenada y para el origen ya normalizada */
   yorigen = (int) ( y2 - Math.floor( ((y[indyor]-ymin) * (y2-y1)) / yrango) );

   	 for (i=0; i<M; i++)
     {  /* conversi�n de los datos para graficarse por pantalla */
        yi[i] = (int) ( y2 - Math.floor( ((y[i]-ymin) * (y2-y1)) / yrango) );
        xi[i] = (int) ( x1 + Math.floor( ((x[i]-xmin) / xrango) * (x2-x1)) );
     }  
      /* dibuja lineas del eje y o ordenadas y el eje x absisas */
      lg.setColor(Color.WHITE);
      lg.drawLine(xorigen,y1,xorigen,y1+y2);  /* eje de las ordenadas */
      lg.drawLine(x1,yorigen,x1+x2,yorigen);  /* eje de las absisas */
      lg.setColor(new Color(255,255,0));    /* establece color amarillo */
      int pts = xi.length;
      lg.drawPolyline(xi, yi, pts);
    }//fin del paint()
}/* fin de MyCanvas */