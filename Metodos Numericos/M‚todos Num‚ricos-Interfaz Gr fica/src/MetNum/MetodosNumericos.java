package MetNum;
import MetodosIterativos.*;
import SistemasEcuaciones.*;
import SolucionSistemasDeEcuaciones.*;
import Tablas.*;
import org.nfunk.jep.JEP;
public class MetodosNumericos 
{
    public static Biseccion b = new Biseccion();
    public static Secante s = new Secante();
    public static Aproximaciones a = new Aproximaciones();
    public static FalsaPosicion f = new FalsaPosicion();
    public static NewtonRaphson nr = new NewtonRaphson();
    public static Raphson2 r2 = new Raphson2();
    public static Cramer c = new Cramer();
    public static Montante m = new Montante();
    public static Gauss g = new Gauss();
    public static GaussJordan j = new GaussJordan();
    public static Interpolacion i =  new Interpolacion();
    public static DerivacionPorLimites L = new DerivacionPorLimites();
    public static Trapecio Tr =  new Trapecio();
    public static GaussSeidel Gs =  new GaussSeidel();
    public static Jaccobi jc = new Jaccobi();
    public static DiferenciasFinitas d = new DiferenciasFinitas();
    public static Simpson si = new Simpson();
    public static Minimos M = new Minimos();
    public static String F; // FUNCION
    public static String dF; // DERIVADA FUNCION
    public static String G; // FUNCION + X
    public static String d2F; // FUNCION
    
    public static TablaBiseccion TB;
    public static TablaFalsaPosicion FP;
    public static TablaSecante TS;
    public static TablaAproximaciones TA;
    public static TablaNewtonRaphson TNR;
    public static TablaRaphson2 TR2;
    public static TablaJaccobi TJ;
    public static TablaGauss TG;
    public static TablaGaussJordan TGJ;
    public static TablaGaussSeidel TGS;
    public static TablaMontante TM;
    public static TablaInterpolacion TI;
    public static TablaCramer TC;
    public static TablaLimites TL = new TablaLimites();
    public static TablaLagrange TLar = new TablaLagrange();
    public static TablaDiferencias Td = new TablaDiferencias();
    public static TablaMinimos Tm = new TablaMinimos();
    public static void main(String[] args) 
    {
        Main m = new Main();
        m.setSize(500,400);
        m.setVisible(true); 
        m.setLocationRelativeTo(null);
    }
    public static double F(double x)
    {
       JEP funcion = new JEP();
       funcion.addStandardFunctions();
       funcion.addStandardConstants();
       funcion.addVariable("x",x);
       funcion.parseExpression(F);
       return funcion.getValue();
    }
    public static double dF(double d) 
    {
       JEP funcion = new JEP();
       funcion.addStandardFunctions();
       funcion.addStandardConstants();
       funcion.addVariable("x", d);
       funcion.parseExpression(dF);
       return funcion.getValue();
    }
    public static double G(double d) 
    {
        JEP funcion = new JEP();
        funcion.addStandardFunctions();
        funcion.addStandardConstants();
        funcion.addVariable("x", d);
        funcion.parseExpression(G);
        return funcion.getValue();
    }
    public static double d2F(double d) 
    {
        JEP funcion = new JEP();
        funcion.addStandardFunctions();
        funcion.addStandardConstants();
        funcion.addVariable("x", d);
        funcion.parseExpression(d2F);
        return funcion.getValue();
    }
    public static JEP ejem;
    public static double Evaluar(double x,String k)
	{
            JEP funcion = new JEP();
            funcion.addStandardFunctions();
            funcion.addStandardConstants();
            funcion.addVariable(k, x);
            funcion.parseExpression(F);
            return funcion.getValue();
	}
}