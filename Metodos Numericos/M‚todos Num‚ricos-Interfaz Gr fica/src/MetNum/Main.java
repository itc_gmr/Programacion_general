package MetNum;

import Tablas.TablaLagrange;
import javax.swing.JOptionPane;

public class Main extends javax.swing.JFrame {
    public Main() {
        super("METODOS NUMERICOS");
        initComponents();
        HidedMI();
        HidedSE();
        HidedDI();
        HidedIN();
        setLocationRelativeTo(null);
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        panel = new MetNum.JPanelConFondo();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        Seidel = new javax.swing.JButton();
        Jaccobi = new javax.swing.JButton();
        Interpolacion = new javax.swing.JButton();
        Gauss = new javax.swing.JButton();
        GaussJordan = new javax.swing.JButton();
        Montante = new javax.swing.JButton();
        Determinante = new javax.swing.JButton();
        Biseccion = new javax.swing.JButton();
        Posicion = new javax.swing.JButton();
        Secante = new javax.swing.JButton();
        Raphson = new javax.swing.JButton();
        Aprox = new javax.swing.JButton();
        Raphson2 = new javax.swing.JButton();
        Gauss8 = new javax.swing.JButton();
        Larange = new javax.swing.JButton();
        Minimos = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        Limites = new javax.swing.JButton();
        Diferencias = new javax.swing.JButton();
        Trapecio = new javax.swing.JButton();
        Simpson = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();

        jMenu2.setText("jMenu2");

        jMenuItem4.setText("jMenuItem4");

        jMenuItem6.setText("jMenuItem6");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        panel.setImagen("/ima/2.jpg");

        jLabel1.setText("INTERPOLACION Y AJUSTE POL.");

        jLabel2.setText("SOLCUION DE ECUACIONES");

        jLabel3.setText("METODOS ITERATIVOS");

        Seidel.setText("Gauss - Seidel");
        Seidel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SeidelActionPerformed(evt);
            }
        });

        Jaccobi.setText("Jaccobi");
        Jaccobi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JaccobiActionPerformed(evt);
            }
        });

        Interpolacion.setText("Interpolacion Newton");
        Interpolacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InterpolacionActionPerformed(evt);
            }
        });

        Gauss.setText("Gauss");
        Gauss.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GaussActionPerformed(evt);
            }
        });

        GaussJordan.setText("Gauss- Jordan");
        GaussJordan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GaussJordanActionPerformed(evt);
            }
        });

        Montante.setText("Montante");
        Montante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MontanteActionPerformed(evt);
            }
        });

        Determinante.setText("Cramer");
        Determinante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeterminanteActionPerformed(evt);
            }
        });

        Biseccion.setText("Biseccion");
        Biseccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BiseccionActionPerformed(evt);
            }
        });

        Posicion.setText("Falsa Posicion");
        Posicion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PosicionActionPerformed(evt);
            }
        });

        Secante.setText("Secante");
        Secante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SecanteActionPerformed(evt);
            }
        });

        Raphson.setText("Newton Raphson");
        Raphson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RaphsonActionPerformed(evt);
            }
        });

        Aprox.setText("Aproximaciones");
        Aprox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AproxActionPerformed(evt);
            }
        });

        Raphson2.setText("Newton 2° Orden");
        Raphson2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Raphson2ActionPerformed(evt);
            }
        });

        Gauss8.setText("Salir");
        Gauss8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Gauss8ActionPerformed(evt);
            }
        });

        Larange.setText("Interpolacion Larange");
        Larange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LarangeActionPerformed(evt);
            }
        });

        Minimos.setText("Minimos Cuadrados");
        Minimos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MinimosActionPerformed(evt);
            }
        });

        jLabel4.setText("DIFERENCIACION E INTEGRACION");

        Limites.setText("Diferenciacion por limites");
        Limites.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimitesActionPerformed(evt);
            }
        });

        Diferencias.setText("Dif. por diferencias finitas");
        Diferencias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DiferenciasActionPerformed(evt);
            }
        });

        Trapecio.setText("Integracion por Trapecio");
        Trapecio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TrapecioActionPerformed(evt);
            }
        });

        Simpson.setText("Integracion por Simpson");
        Simpson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SimpsonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addContainerGap(247, Short.MAX_VALUE)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addGap(13, 13, 13))
                    .addComponent(Gauss8, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Seidel, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(Trapecio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Limites, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Simpson, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Diferencias, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(Minimos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Larange, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Interpolacion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(Montante, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(GaussJordan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Gauss, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Determinante, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                                    .addComponent(Jaccobi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(Biseccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Posicion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Secante, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Raphson, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Raphson2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Aprox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(9, 9, 9)))
                .addGap(42, 42, 42))
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Gauss)
                            .addComponent(Biseccion))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(GaussJordan)
                            .addComponent(Posicion))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Montante)
                            .addComponent(Secante))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Determinante)
                            .addComponent(Raphson))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Aprox, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Jaccobi))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Raphson2)
                            .addComponent(Seidel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                        .addComponent(Gauss8)
                        .addGap(19, 19, 19))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Limites)
                            .addComponent(Interpolacion))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addComponent(Diferencias)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Trapecio)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Simpson))
                            .addComponent(Larange)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addComponent(Minimos)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jMenuBar1.setForeground(new java.awt.Color(255, 255, 255));

        jMenu1.setBackground(new java.awt.Color(255, 255, 255));
        jMenu1.setText("Programas");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Metodos Iterativos");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Solcucion de Ecuaciones");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Interpolacion y Ajuste Polinomial");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Diferenciacion e Integracion");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        if(!jLabel3.isVisible())
        {
            if(Gauss.isVisible())            
                HidedSE();
            if(Limites.isVisible())
                HidedDI();
            if(Interpolacion.isVisible())
                HidedIN();
            
            ShowedMI();
        }
        else
        {
            HidedMI();
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        if(!jLabel2.isVisible())
        {
            if(Biseccion.isVisible())
                 HidedMI();
            if(Interpolacion.isVisible())
                HidedIN();
            if(Limites.isVisible())
                 HidedDI();
           ShowedSE();
        }
        else
        {
           HidedSE();
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        if(!jLabel1.isVisible())
        {
            if(Gauss.isVisible())
                HidedSE();//gauss
            if(Biseccion.isVisible())
                HidedMI();
            if(Limites.isVisible())
                HidedDI();
            ShowedIN();//biseccion
        }
        else
        {
            HidedIN();
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    
    private void Gauss8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Gauss8ActionPerformed
        dispose();
        System.exit(0);
    }//GEN-LAST:event_Gauss8ActionPerformed

    private void SeidelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SeidelActionPerformed
        if(!MetodosNumericos.Gs.isVisible())
            MetodosNumericos.Gs.setVisible(true);
        else
            MetodosNumericos.Gs.setVisible(false);
    }//GEN-LAST:event_SeidelActionPerformed

    private void BiseccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BiseccionActionPerformed
        if (!MetodosNumericos.b.isVisible()) {
            MetodosNumericos.b.setVisible(true);
        } else {
            MetodosNumericos.b.setVisible(false);
        }
    }//GEN-LAST:event_BiseccionActionPerformed

    private void PosicionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PosicionActionPerformed
        if (!MetodosNumericos.f.isVisible()) {
            MetodosNumericos.f.setVisible(true);
        } else {
            MetodosNumericos.f.setVisible(false);
        }
    }//GEN-LAST:event_PosicionActionPerformed

    private void SecanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SecanteActionPerformed
        if (!MetodosNumericos.s.isVisible()) {
            MetodosNumericos.s.setVisible(true);
        } else {
            MetodosNumericos.s.setVisible(false);
        }
    }//GEN-LAST:event_SecanteActionPerformed

    private void RaphsonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RaphsonActionPerformed
        if (!MetodosNumericos.nr.isVisible()) {
            MetodosNumericos.nr.setVisible(true);
        } else {
            MetodosNumericos.nr.setVisible(false);
        }
    }//GEN-LAST:event_RaphsonActionPerformed

    private void AproxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AproxActionPerformed
        if (!MetodosNumericos.a.isVisible()) {
            MetodosNumericos.a.setVisible(true);
        } else {
            MetodosNumericos.a.setVisible(false);
        }
    }//GEN-LAST:event_AproxActionPerformed

    private void Raphson2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Raphson2ActionPerformed
        if (!MetodosNumericos.r2.isVisible()) {
            MetodosNumericos.r2.setVisible(true);
        } else {
            MetodosNumericos.r2.setVisible(false);
        }
    }//GEN-LAST:event_Raphson2ActionPerformed

    private void GaussActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GaussActionPerformed
        if(!MetodosNumericos.g.isVisible())
            MetodosNumericos.g.setVisible(true);
        else
            MetodosNumericos.g.setVisible(false);
    }//GEN-LAST:event_GaussActionPerformed

    private void GaussJordanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GaussJordanActionPerformed
        if(!MetodosNumericos.j.isVisible())
            MetodosNumericos.j.setVisible(true);
        else
            MetodosNumericos.j.setVisible(false);
    }//GEN-LAST:event_GaussJordanActionPerformed

    private void MontanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MontanteActionPerformed
        if(!MetodosNumericos.m.isVisible())
            MetodosNumericos.m.setVisible(true);
        else
            MetodosNumericos.m.setVisible(false);
    }//GEN-LAST:event_MontanteActionPerformed

    private void DeterminanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeterminanteActionPerformed
        if(!MetodosNumericos.c.isVisible())
            MetodosNumericos.c.setVisible(true);
        else
            MetodosNumericos.c.setVisible(false);
    }//GEN-LAST:event_DeterminanteActionPerformed

    private void JaccobiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JaccobiActionPerformed
        if(!MetodosNumericos.jc.isVisible())
            MetodosNumericos.jc.setVisible(true);
        else
            MetodosNumericos.jc.setVisible(false);
    }//GEN-LAST:event_JaccobiActionPerformed

    private void InterpolacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InterpolacionActionPerformed
        if(!MetodosNumericos.i.isVisible())
            MetodosNumericos.i.setVisible(true);
        else
            MetodosNumericos.i.setVisible(false);
    }//GEN-LAST:event_InterpolacionActionPerformed

    private void LarangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LarangeActionPerformed
        boolean r;
        int a=0;
            try{
                a=Integer.parseInt(JOptionPane.showInputDialog("Numero de Puntos"));
                r = true;
            }catch(NumberFormatException e){
                r = false;
                JOptionPane.showMessageDialog(null, "Solo valor entero", "Error" , 3);
            }
        //TablaLagrange.jTable1.getRowCount()
        for(int i=0; i<TablaLagrange.jTable1.getRowCount(); i++)
            TablaLagrange.BorrarRenglon();
        for(int i=0; i<a; i++)
            TablaLagrange.AgregarRenglon();
        for(int i=0; i<a; i++)
            TablaLagrange.jTable1.setValueAt("", i, 0);
        if(!MetodosNumericos.TLar.isVisible())
            MetodosNumericos.TLar.setVisible(true);
        else
            MetodosNumericos.TLar.setVisible(false);
    }//GEN-LAST:event_LarangeActionPerformed

    private void MinimosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MinimosActionPerformed
        if(!MetodosNumericos.M.isVisible())
            MetodosNumericos.M.setVisible(true);
        else
            MetodosNumericos.M.setVisible(false);
    }//GEN-LAST:event_MinimosActionPerformed

    private void LimitesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimitesActionPerformed
        if(!MetodosNumericos.L.isVisible())
            MetodosNumericos.L.setVisible(true);
        else
            MetodosNumericos.L.setVisible(false);
    }//GEN-LAST:event_LimitesActionPerformed

    private void DiferenciasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DiferenciasActionPerformed
        if(!MetodosNumericos.d.isVisible())
            MetodosNumericos.d.setVisible(true);
        else
            MetodosNumericos.d.setVisible(false);
    }//GEN-LAST:event_DiferenciasActionPerformed

    private void TrapecioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TrapecioActionPerformed
        if(!MetodosNumericos.Tr.isVisible())
            MetodosNumericos.Tr.setVisible(true);
        else
            MetodosNumericos.Tr.setVisible(false);        
    }//GEN-LAST:event_TrapecioActionPerformed

    private void SimpsonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SimpsonActionPerformed
       if(!MetodosNumericos.si.isVisible())
            MetodosNumericos.si.setVisible(true);
        else
            MetodosNumericos.si.setVisible(false);
    }//GEN-LAST:event_SimpsonActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        if(!jLabel4.isVisible())
        {
            if(Gauss.isVisible())
                HidedSE();//gauss
            if(Biseccion.isVisible())
                HidedMI();
            if(Interpolacion.isVisible())
                HidedIN();
            ShowedDI();//biseccion
        }
        else
        {
            HidedDI();//biseccion
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed
    public final void ShowedMI(){
        jLabel3.setVisible(true);
        Biseccion.setVisible(true);
        Secante.setVisible(true);
        Aprox.setVisible(true);
        Posicion.setVisible(true);
        Raphson.setVisible(true);
        Raphson2.setVisible(true);
    }
    public final void HidedMI(){
        jLabel3.setVisible(false);
        Biseccion.setVisible(false);
        Secante.setVisible(false);
        Aprox.setVisible(false);
        Posicion.setVisible(false);
        Raphson.setVisible(false);
        Raphson2.setVisible(false);
    }
    public final void ShowedSE(){
        jLabel2.setVisible(true);
        Determinante.setVisible(true);
        Montante.setVisible(true);
        Gauss.setVisible(true);
        GaussJordan.setVisible(true);        
        Seidel.setVisible(true);
        Jaccobi.setVisible(true);
    }
    public final void HidedSE(){
        jLabel2.setVisible(false);
        Determinante.setVisible(false);
        Montante.setVisible(false);
        Gauss.setVisible(false);
        GaussJordan.setVisible(false);
        Seidel.setVisible(false);
        Jaccobi.setVisible(false);
    }
    public final void ShowedIN(){
        jLabel1.setVisible(true);
        Interpolacion.setVisible(true);
        Larange.setVisible(true);
        Minimos.setVisible(true);
    }
    public final void HidedIN(){
        jLabel1.setVisible(false);
        Interpolacion.setVisible(false);
        Larange.setVisible(false);
        Minimos.setVisible(false);
    }
    public final void ShowedDI(){
        jLabel4.setVisible(true);
        Limites.setVisible(true);
        Diferencias.setVisible(true);
        Trapecio.setVisible(true);
        Simpson.setVisible(true);
    }
    public final void HidedDI(){
        jLabel4.setVisible(false);
        Limites.setVisible(false);
        Diferencias.setVisible(false);
        Trapecio.setVisible(false);
        Simpson.setVisible(false);
    }
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Aprox;
    public static javax.swing.JButton Biseccion;
    public static javax.swing.JButton Determinante;
    public static javax.swing.JButton Diferencias;
    public static javax.swing.JButton Gauss;
    public static javax.swing.JButton Gauss8;
    public static javax.swing.JButton GaussJordan;
    public static javax.swing.JButton Interpolacion;
    public static javax.swing.JButton Jaccobi;
    public static javax.swing.JButton Larange;
    public static javax.swing.JButton Limites;
    public static javax.swing.JButton Minimos;
    public static javax.swing.JButton Montante;
    public static javax.swing.JButton Posicion;
    public static javax.swing.JButton Raphson;
    public static javax.swing.JButton Raphson2;
    public static javax.swing.JButton Secante;
    public static javax.swing.JButton Seidel;
    public static javax.swing.JButton Simpson;
    public static javax.swing.JButton Trapecio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    public static MetNum.JPanelConFondo panel;
    // End of variables declaration//GEN-END:variables
}