package MetNum;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
public class ProyGraf{
  /* VARIABLES PARA LA INTERFACE */
  Font fuente;
  static Graf c = new Graf();//--------------------------crear grafica +
  public static Frame f;
  private Panel pa;
    public ProyGraf(String title) 
    {
        f = new Frame(title);
        pa = new Panel();
    }
    public static void main(String args[]) 
    {
        ProyGraf guiWindow = new ProyGraf("GRAFICA "+MetodosNumericos.F);
        f.setResizable(false);
        guiWindow.launchFrame();
        c.setFun(1);
        c.repaint();	  
    }
    public void launchFrame() 
    {
        f.setSize(627,500);
        f.setBackground(new Color(255, 255, 255)); //combinaci�n (rojo,verde,azul) RGB fondo blanco
        pa.setLayout(new GridLayout(1,2));
        fuente = new java.awt.Font("Arial", Font.BOLD, 12);
        pa.setFont(fuente);
        pa.add(c);//------------------------------ incertar grafica
        f.add(pa,BorderLayout.NORTH);
        f.addWindowListener( new WindowAdapter ()
        {
            @Override
                public void windowClosing(WindowEvent e)
                {
                    ProyGraf.f.setVisible(false);
                }
        });        
        f.add("Center",pa);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}