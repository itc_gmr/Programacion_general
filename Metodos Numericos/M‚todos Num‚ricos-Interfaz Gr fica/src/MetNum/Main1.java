package MetNum;
public class Main1 extends javax.swing.JFrame {
    public Main1() {
        super("Metodos Numericos");
        initComponents();
        HidedMI();
        HidedSE();
        HidedSSE();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(4, 4), new java.awt.Dimension(4, 4), new java.awt.Dimension(4, 4));
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        panel = new MetNum.JPanelConFondo();
        panel.setImagen("/ima/2.jpg");
        Biseccion = new javax.swing.JButton();
        Secante = new javax.swing.JButton();
        Posicion = new javax.swing.JButton();
        Raphson = new javax.swing.JButton();
        Aprox = new javax.swing.JButton();
        Raphson2 = new javax.swing.JButton();
        Misses = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 32767));
        Determinante = new javax.swing.JButton();
        Montante = new javax.swing.JButton();
        Gauss = new javax.swing.JButton();
        GaussJordan = new javax.swing.JButton();
        MILavel = new javax.swing.JLabel();
        SELavel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        Seidel = new javax.swing.JButton();
        Jaccobi = new javax.swing.JButton();
        Interpolacion = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();

        jMenuItem3.setText("jMenuItem3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        Biseccion.setText("Biseccion");
        Biseccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BiseccionActionPerformed(evt);
            }
        });

        Secante.setText("Secante");
        Secante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SecanteActionPerformed(evt);
            }
        });

        Posicion.setText("Falsa Posicion");
        Posicion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PosicionActionPerformed(evt);
            }
        });

        Raphson.setText("Newton Raphson");
        Raphson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RaphsonActionPerformed(evt);
            }
        });

        Aprox.setText("Aproximaciones");
        Aprox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AproxActionPerformed(evt);
            }
        });

        Raphson2.setText("Newton 2° Orden");
        Raphson2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Raphson2ActionPerformed(evt);
            }
        });

        Misses.setText("Von Misses");
        Misses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MissesActionPerformed(evt);
            }
        });

        salir.setText("Salir");
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });

        Determinante.setText("Cramer");
        Determinante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeterminanteActionPerformed(evt);
            }
        });

        Montante.setText("Montante");
        Montante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MontanteActionPerformed(evt);
            }
        });

        Gauss.setText("Gauss");
        Gauss.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GaussActionPerformed(evt);
            }
        });

        GaussJordan.setText("Gauss-Jordan");
        GaussJordan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GaussJordanActionPerformed(evt);
            }
        });

        MILavel.setText("METODOS ITERATIVOS");
        MILavel.setVisible(false);

        SELavel.setText("SOLUCION DE ECUACIONES");
        SELavel.setVisible(false);

        jLabel1.setText("SISTEMAS DE ECUACIONES");
        jLabel1.setVisible(false);

        Seidel.setText("Gauss - Seidel");

        Jaccobi.setText("Jaccobi");

        Interpolacion.setText("Interpolacion");

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(salir, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(Seidel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(Jaccobi, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                            .addComponent(Interpolacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addGap(146, 146, 146)
                                .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(SELavel)
                                        .addComponent(Determinante, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(Montante, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(GaussJordan, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Gauss, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(MILavel)
                                    .addComponent(Biseccion, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Posicion, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Raphson, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Raphson2, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Secante, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Aprox, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(Misses, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addGap(26, 26, 26))
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(SELavel)
                            .addComponent(MILavel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Biseccion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelLayout.createSequentialGroup()
                                .addGap(65, 65, 65)
                                .addComponent(Determinante)
                                .addGap(85, 85, 85)
                                .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(salir))
                            .addGroup(panelLayout.createSequentialGroup()
                                .addComponent(Posicion)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Secante)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Raphson)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Aprox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Raphson2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Misses)))
                        .addGap(35, 35, 35))
                    .addGroup(panelLayout.createSequentialGroup()
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Seidel)
                            .addComponent(Gauss))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Jaccobi)
                            .addComponent(GaussJordan))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Interpolacion)
                            .addComponent(Montante))
                        .addGap(200, 200, 200))))
        );

        jMenuBar1.setBackground(new java.awt.Color(102, 102, 102));

        jMenu1.setBorder(null);
        jMenu1.setText("Programas");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Metodos Iterativos");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Solucion de Ecuaciones");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Solucion de Sistemas de Ecuaciones");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem4);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        dispose();
        System.exit(0);
    }//GEN-LAST:event_salirActionPerformed
    public final void ShowedMI(){
        MILavel.setVisible(true);
        Biseccion.setVisible(true);
        Secante.setVisible(true);
        Aprox.setVisible(true);
        Misses.setVisible(true);
        Posicion.setVisible(true);
        Raphson.setVisible(true);
        Raphson2.setVisible(true);
    }
    public final void HidedMI(){
        MILavel.setVisible(false);
        Biseccion.setVisible(false);
        Secante.setVisible(false);
        Aprox.setVisible(false);
        Misses.setVisible(false);
        Posicion.setVisible(false);
        Raphson.setVisible(false);
        Raphson2.setVisible(false);
    }
    public final void HidedSSE(){
        jLabel1.setVisible(false);
        Seidel.setVisible(false);
        Jaccobi.setVisible(false);
        Interpolacion.setVisible(false);
    }
    public final void ShowedSSE(){
        jLabel1.setVisible(true);
        Seidel.setVisible(true);
        Jaccobi.setVisible(true);
        Interpolacion.setVisible(true);
    }
    public final void ShowedSE(){
        SELavel.setVisible(true);
        Determinante.setVisible(true);
        Montante.setVisible(true);
        Gauss.setVisible(true);
        GaussJordan.setVisible(true);        
    }
    public final void HidedSE(){
        SELavel.setVisible(false);
        Determinante.setVisible(false);
        Montante.setVisible(false);
        Gauss.setVisible(false);
        GaussJordan.setVisible(false);        
    }
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        if(!Biseccion.isVisible())
        {
            if(Gauss.isVisible())
                HidedSE();
            if(jLabel1.isVisible())
                HidedSSE();
            ShowedMI();
        }
        else
        {
            HidedMI();
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        if(!Gauss.isVisible())
        {
            if(Biseccion.isVisible())
                 HidedMI();
            if(jLabel1.isVisible())
                 HidedSSE();
           ShowedSE();
        }
        else
        {
           HidedSE();
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void DeterminanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeterminanteActionPerformed
        if(!MetodosNumericos.c.isVisible())
            MetodosNumericos.c.setVisible(true);
        else
            MetodosNumericos.c.setVisible(false);
    }//GEN-LAST:event_DeterminanteActionPerformed

    private void MontanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MontanteActionPerformed
        if(!MetodosNumericos.m.isVisible())
            MetodosNumericos.m.setVisible(true);
        else
            MetodosNumericos.m.setVisible(false);
    }//GEN-LAST:event_MontanteActionPerformed

    private void GaussActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GaussActionPerformed
        if(!MetodosNumericos.g.isVisible())
            MetodosNumericos.g.setVisible(true);
        else
            MetodosNumericos.g.setVisible(false);
    }//GEN-LAST:event_GaussActionPerformed

    private void GaussJordanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GaussJordanActionPerformed
        if(!MetodosNumericos.j.isVisible())
            MetodosNumericos.j.setVisible(true);
        else
            MetodosNumericos.j.setVisible(false);
    }//GEN-LAST:event_GaussJordanActionPerformed

    private void Raphson2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Raphson2ActionPerformed
        if (!MetodosNumericos.r2.isVisible()) {
            MetodosNumericos.r2.setVisible(true);
        } else {
            MetodosNumericos.r2.setVisible(false);
        }
    }//GEN-LAST:event_Raphson2ActionPerformed

    private void MissesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MissesActionPerformed
        
    }//GEN-LAST:event_MissesActionPerformed

    private void SecanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SecanteActionPerformed
        if (!MetodosNumericos.s.isVisible()) {
            MetodosNumericos.s.setVisible(true);
        } else {
            MetodosNumericos.s.setVisible(false);
        }
    }//GEN-LAST:event_SecanteActionPerformed

    private void PosicionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PosicionActionPerformed
        if (!MetodosNumericos.f.isVisible()) {
            MetodosNumericos.f.setVisible(true);
        } else {
            MetodosNumericos.f.setVisible(false);
        }
    }//GEN-LAST:event_PosicionActionPerformed

    private void RaphsonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RaphsonActionPerformed
        if (!MetodosNumericos.nr.isVisible()) {
            MetodosNumericos.nr.setVisible(true);
        } else {
            MetodosNumericos.nr.setVisible(false);
        }
    }//GEN-LAST:event_RaphsonActionPerformed

    private void AproxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AproxActionPerformed
        if (!MetodosNumericos.a.isVisible()) {
            MetodosNumericos.a.setVisible(true);
        } else {
            MetodosNumericos.a.setVisible(false);
        }
    }//GEN-LAST:event_AproxActionPerformed

    private void BiseccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BiseccionActionPerformed
        if (!MetodosNumericos.b.isVisible()) {
            MetodosNumericos.b.setVisible(true);
        } else {
            MetodosNumericos.b.setVisible(false);
        }
    }//GEN-LAST:event_BiseccionActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        if(!jLabel1.isVisible())
        {
            if(Gauss.isVisible())
                HidedSE();//gauss
            if(Biseccion.isVisible())
                HidedMI();
            ShowedSSE();//biseccion
        }
        else
        {
            HidedSSE();
        }
    }//GEN-LAST:event_jMenuItem4ActionPerformed
    public static void panel (String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, spanel       * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new RunnableImpl());
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Aprox;
    public static javax.swing.JButton Biseccion;
    public static javax.swing.JButton Determinante;
    public static javax.swing.JButton Gauss;
    public static javax.swing.JButton GaussJordan;
    public static javax.swing.JButton Interpolacion;
    public static javax.swing.JButton Jaccobi;
    public static javax.swing.JLabel MILavel;
    public static javax.swing.JButton Misses;
    public static javax.swing.JButton Montante;
    public static javax.swing.JButton Posicion;
    public static javax.swing.JButton Raphson;
    public static javax.swing.JButton Raphson2;
    public static javax.swing.JLabel SELavel;
    public static javax.swing.JButton Secante;
    public static javax.swing.JButton Seidel;
    private javax.swing.Box.Filler filler1;
    private javax.swing.Box.Filler filler2;
    public static javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPopupMenu jPopupMenu1;
    public static MetNum.JPanelConFondo panel;
    public static javax.swing.JButton salir;
    // End of variables declaration//GEN-END:variables

    private static class RunnableImpl implements Runnable {
        public RunnableImpl() {
        }
        @Override
        public void run() {
            new Main1().setVisible(true);
        }
    }
}