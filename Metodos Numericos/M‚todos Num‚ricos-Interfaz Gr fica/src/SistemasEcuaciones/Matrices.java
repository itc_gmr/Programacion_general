package SistemasEcuaciones;
import java.io.PrintStream;
public class Matrices 
{
	static PrintStream pr = System.out;
	static double [][] copia(double original[][])
	{
		double copia[][]= new double[original.length][original[0].length];
		for(int i=0; i<original.length; i++)
			for(int j=0; j<original[i].length; j++)
				copia[i][j] = original[i][j];
		return copia;
	}
	private static void depPos(int a, int b, int f) 
	{
		String s[][]= new String[f][f + 1];
		for(int i=0; i<s.length; i++)
			for(int j=0; j<s[i].length; j++)
				s[i][j]="_ ";
		s[a][b]="x ";
		System.out.print("\n\t");
		for(int i=0; i<s.length; i++)
		{
			for(int j=0; j<s[i].length; j++)
				System.out.print(s[i][j]);
			System.out.print("\n\t");
		}
		System.out.println();
	}
	static void imp_M(double x[][]) {
		for(int i=0; i<x.length; i++) {
			for (int j=0; j<x[0].length; j++) {
				System.out.print("\t"+x[i][j]+"  ");
			}
			System.out.println();
		}
	}
	static double determinate(double a[][]) 
	{
		double x[][]=Coef(a);
		for(int k=0; k<x.length-1; k++)
			for(int i=k+1; i<x[0].length; i++)
				for(int j=k+1; j<x.length; j++)
					x[i][j]-=x[i][k]*x[k][j]/x[k][k];
		double det = 1.0;
		
		for(int i=0; i<x.length; i++)
            det*=x[i][i];
		
        return det;
	}
	private static double[][] Coef(double[][] a) 
	{
		double x[][] = new double [a.length][a.length];
		for(int i=0; i<x.length; i++)
			for(int j=0; j<x.length; j++)
				x[i][j]=a[i][j];
		return x;
	}
	public static void GaussJordan(double[][] c1) 
	{
		int n=c1.length;
        double FM,Piv;
        for (int i=0; i<c1.length; i++)
        {
            Piv=c1[i][i];
            for (int j=i; j<c1[i].length; j++)
                c1[i][j]=(int) (c1[i][j]/Piv);
            for (int j=0; j<n; j++)
            {
                if (i!=j)
                {
                    FM=c1[j][i];
                    for (int k=i; k<n+1; k++)
                        c1[j][k]-=c1[i][k]*FM;
                }
            }
        }
	}
	 public static double[][] Montante(double m[][]) 
	 {
	    	double piv_ant = 1;
	        // La iteracin "k" proviene del nmero de pasos a realizar
	        int k;
	    	for(k = 0; k < m.length; k++) 
	    	{
//	    		pr.println("Paso " + (k+1));
//	    		pr.println("Pivote actual: " + m[k][k]);
//	    		pr.println("Pivote anterior: " + piv_ant);
	    		for (int i = 0; i < m.length; i++) 
	    		{
	    			for (int j = m[0].length-1; j>=0 ; j--) 
	    			{
			    		if (i != k) // Las columnas hasta la k ...
			    		{
			    			if (j <= k) // estarn formadas por 0's...
			    			{
			    				if (i != j) 
			    					m[i][j] = 0;  // ...excepto la diagonal, que tendr valores iguales al pivote.
		    					else 
		    						m[i][j] = m[k][k];//El resto de los datos...
			    			} 
			    			else 
			    			{
				    			// ...se obtiene mediante el determinante de ese punto y el pivote, dividido por el pivote anterior...
				    			m[i][j] = DetMontante(m[k][k], m[k][j], m[i][k], m[i][j]) / piv_ant;
				    			// ...y son negativos si estn sobre el rengln del pivote...
				    			if (i < k ) 
				    				m[i][j] = - m[i][j]; 
				    		}
			    		}
					}
				}
	    		piv_ant = m[k][k];
	    	}
	    	// Se divide la matriz entre el valor del pivote actual para obtener la matriz identidad.
	    	System.out.println("Dererminante " + m[0][0]);
	    	return m;
	    }
	 public static double[][] Gauss(double M[][])
	 {
		 double FM,Piv;
	     for (int i=0; i<M.length; i++)
	     {
	         Piv=M[i][i];
	         for (int j=i; j<M.length; j++)
	         {
	             if (i!=j)
	             {
	                 FM=M[j][i]/Piv;
	                 for (int k=i; k<M[0].length; k++)
	                     M[j][k]-=M[i][k]*FM;
	             }
	         }
	     }
	     return M;
	 }
	private static double DetMontante(double d, double e, double f, double g) 
	{
		return d*g-e*f;
	}
	static void despliegaValores(double d[][]) 
	{   
		double b = d[0][0];
		for(int i=0; i<d.length; i++)
		{
			for(int j=0; j<d[i].length; j++)
					d[i][j]/=b;// dividir toda la matriz entre el determinante
			System.out.println("x"+(i+1)+" = "+d[i][ d[i].length-1 ]);
		}
	}
}