package SistemasEcuaciones;
import MetNum.MetodosNumericos;
import Tablas.TablaGaussJordan;
import javax.swing.JOptionPane;
public class GaussJordan extends javax.swing.JFrame {
    public GaussJordan() {
        super("Gauss Jordan");
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new MetNum.JPanelConFondo();
        Panel.setImagen("/ima/5.jpg");
        Ecuaciones = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        Crear = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        Ecuaciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                EcuacionesKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                EcuacionesKeyTyped(evt);
            }
        });

        jLabel1.setText("Numero de Ecuaciones");

        Crear.setText("Crear Matriz");
        Crear.setEnabled(false);
        Crear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CrearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(Ecuaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(Crear, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(Ecuaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Crear, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void EcuacionesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EcuacionesKeyReleased
        if (Ecuaciones.getText().length() > 0) {
            Crear.setEnabled(true);
        } else {
            Crear.setEnabled(false);
        }
    }//GEN-LAST:event_EcuacionesKeyReleased

    private void EcuacionesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_EcuacionesKeyTyped

   }//GEN-LAST:event_EcuacionesKeyTyped

    private void CrearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CrearActionPerformed
        try {
            int n = Integer.parseInt(Ecuaciones.getText());
            if (n < 2) {
                JOptionPane.showMessageDialog(null, "Minimo Dos Ecuaciones", "Error ", 0);
                Ecuaciones.setText("");
                return;
            }
            JOptionPane.showMessageDialog(null, "Llene la tabla completa\nUse las teclas de direccion"
                    + "\nPara moverse en la tabla\nY tecle el valor\n\nEn la ultima celda llenada"
                    + "\nDe enter para terminar", "Instrucciones", 2);
            MetodosNumericos.TGJ = new TablaGaussJordan(); // crear tabla
//agregar (n + 1)  columnas
            for (int i = 0; i <= n; i++) {
                if (i < n) {
                    MetodosNumericos.TGJ.AgregarColumna("X" + (i + 1));
                } else {
                    MetodosNumericos.TGJ.AgregarColumna("B");
                }
            }
//borrar todos los renglones Default
            for (int i = 0; i <= n; i++) {
                MetodosNumericos.TGJ.BorrarRenglon();
            }
//agregar (n) renglones 
            for (int i = 0; i < n; i++) {
                MetodosNumericos.TGJ.AgregarRenglon();
            }
            if (n == 1) {
                MetodosNumericos.TGJ.BorrarRenglon();
            }
//---------------Dar Tamaño a Ventana------------------------------------------------
            if (n < 5) {
                MetodosNumericos.TGJ.setSize((((int) ((n - 1) * (50))) + ((100 + n) + ((n - 1) * (n - 1))) + 50), ((int) ((n - 1) * (13.5))) + ((103 - (n - 1)) + ((n - 1) * (n - 1))));
            } else {
                MetodosNumericos.TGJ.setSize(((int) ((n - 1) * (50))) + ((100 + n) + ((n - 1) * (n - 1))), ((int) ((n - 1) * (13.5))) + ((98 - (n - 1)) + ((n - 1) * (n - 1))));
            }
//-----------------------------------------------------------------------------------
            // borrar tabla------
            for (int i = 0; i < TablaGaussJordan.jTable1.getRowCount(); i++) {
                for (int j = 0; j < TablaGaussJordan.jTable1.getColumnCount(); j++) {
                    TablaGaussJordan.jTable1.setValueAt("", i, j);
                }
            }
            //-------------------

            if (!MetodosNumericos.TGJ.isVisible()) {
                MetodosNumericos.TGJ.setVisible(true);
            }

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Debe teclear un numero entero", "Error ", 0);
            Ecuaciones.setText("");
        }
    }//GEN-LAST:event_CrearActionPerformed
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GaussJordan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GaussJordan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GaussJordan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GaussJordan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GaussJordan().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Crear;
    public static javax.swing.JTextField Ecuaciones;
    public static MetNum.JPanelConFondo Panel;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}