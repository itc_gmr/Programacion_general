package MetodosIterativos;

import MetNum.MetodosNumericos;
import MetNum.ProyGraf;
import Tablas.TablaSecante;

public class Secante extends javax.swing.JFrame {
    public Secante() {
        super("Secante");
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        funcion = new javax.swing.JTextField();
        Panel = new MetNum.JPanelConFondo();
        Panel.setImagen("/ima/5.jpg");
        jLabel2 = new javax.swing.JLabel();
        funcion1 = new javax.swing.JTextField();
        Graficar = new javax.swing.JToggleButton();
        jLabel3 = new javax.swing.JLabel();
        PuntoInicial = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        PuntoFinal = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        Iteraciones = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        Error = new javax.swing.JTextField();
        Reiniciar = new javax.swing.JButton();
        Aceptar = new javax.swing.JButton();

        jLabel1.setText("FUNCIÓN A EVALUAR");

        funcion.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                funcionFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                funcionFocusLost(evt);
            }
        });
        funcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                funcionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                funcionKeyReleased(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel2.setText("FUNCIÓN A EVALUAR");

        funcion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcion1ActionPerformed(evt);
            }
        });
        funcion1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                funcion1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                funcion1FocusLost(evt);
            }
        });
        funcion1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                funcion1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                funcion1KeyReleased(evt);
            }
        });

        Graficar.setText("Graficar");
        Graficar.setEnabled(false);
        Graficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GraficarActionPerformed(evt);
            }
        });

        jLabel3.setText("Punto Inicial");

        PuntoInicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                PuntoInicialKeyReleased(evt);
            }
        });

        jLabel4.setText("Punto Final");

        PuntoFinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                PuntoFinalKeyReleased(evt);
            }
        });

        jLabel5.setText("Iteraciones");

        Iteraciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                IteracionesKeyReleased(evt);
            }
        });

        jLabel6.setText("Error");

        Error.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ErrorKeyReleased(evt);
            }
        });

        Reiniciar.setText("Reiniciar");
        Reiniciar.setEnabled(false);
        Reiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReiniciarActionPerformed(evt);
            }
        });

        Aceptar.setText("Aceptar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });
        Aceptar.setEnabled(false);

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Graficar)
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel6)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(funcion1)
                            .addComponent(PuntoInicial)
                            .addComponent(PuntoFinal)
                            .addComponent(Iteraciones)
                            .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(Reiniciar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Aceptar)))
                .addContainerGap(40, Short.MAX_VALUE))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(funcion1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Graficar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(PuntoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(PuntoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(Iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Reiniciar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Aceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(31, 31, 31))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void funcionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcionFocusGained

   }//GEN-LAST:event_funcionFocusGained

    private void funcionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcionFocusLost

   }//GEN-LAST:event_funcionFocusLost

    private void funcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionKeyPressed
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcionKeyPressed

    private void funcionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionKeyReleased
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcionKeyReleased

    private void funcion1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcion1FocusGained

   }//GEN-LAST:event_funcion1FocusGained

    private void funcion1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcion1FocusLost

   }//GEN-LAST:event_funcion1FocusLost

    private void funcion1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcion1KeyPressed
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcion1KeyPressed

    private void funcion1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcion1KeyReleased
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcion1KeyReleased

    private void GraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GraficarActionPerformed
        MetodosNumericos.F = funcion1.getText().toString().toLowerCase();
        ProyGraf.main(null);
        funcion1.setEnabled(false);
        Graficar.setEnabled(false);
        Reiniciar.setEnabled(true);
        if (!ProyGraf.f.isVisible()) {
            ProyGraf.f.setVisible(true);
        }
    }//GEN-LAST:event_GraficarActionPerformed

    private void PuntoInicialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PuntoInicialKeyReleased
        if (!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_PuntoInicialKeyReleased

    private void PuntoFinalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PuntoFinalKeyReleased
        if (!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_PuntoFinalKeyReleased

    private void IteracionesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IteracionesKeyReleased
        if (!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_IteracionesKeyReleased

    private void ReiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReiniciarActionPerformed
        if (MetodosNumericos.TS != null) {
            if (MetodosNumericos.TS.isVisible()) {
                MetodosNumericos.TS.setVisible(false);
            }
        }
        funcion1.setText("");
        funcion1.setEnabled(true);
        PuntoInicial.setText("");
        PuntoFinal.setText("");
        Iteraciones.setText("");
        Error.setText("");
        Aceptar.setEnabled(false);
        ProyGraf.f.setVisible(false);
        Reiniciar.setEnabled(false);
    }//GEN-LAST:event_ReiniciarActionPerformed

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        MetodosNumericos.TS = new TablaSecante();
        if (!MetodosNumericos.TS.isVisible() && !MetodosNumericos.TS.NoFound) {
            MetodosNumericos.TS.setVisible(true);
        }
    }//GEN-LAST:event_AceptarActionPerformed

    private void funcion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcion1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_funcion1ActionPerformed

    private void ErrorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ErrorKeyReleased
        if (!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_ErrorKeyReleased
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Secante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Secante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Secante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Secante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Secante().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Aceptar;
    public static javax.swing.JTextField Error;
    public static javax.swing.JToggleButton Graficar;
    public static javax.swing.JTextField Iteraciones;
    public static MetNum.JPanelConFondo Panel;
    public static javax.swing.JTextField PuntoFinal;
    public static javax.swing.JTextField PuntoInicial;
    public static javax.swing.JButton Reiniciar;
    public static javax.swing.JTextField funcion;
    public static javax.swing.JTextField funcion1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    // End of variables declaration//GEN-END:variables
}