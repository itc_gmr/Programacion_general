package MetodosIterativos;

import MetNum.MetodosNumericos;
import MetNum.ProyGraf;
import Tablas.TablaFalsaPosicion;

public class FalsaPosicion extends javax.swing.JFrame {
    public FalsaPosicion() {
        super("Falsa Posicion");
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new MetNum.JPanelConFondo();
        Panel.setImagen("/ima/5.jpg");
        Reiniciar = new javax.swing.JButton();
        Error = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        PuntoInicial = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        Graficar = new javax.swing.JToggleButton();
        Iteraciones = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        PuntoFinal = new javax.swing.JTextField();
        Aceptar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        funcion = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        Reiniciar.setText("Reiniciar");
        Reiniciar.setEnabled(false);
        Reiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReiniciarActionPerformed(evt);
            }
        });

        Error.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ErrorKeyReleased(evt);
            }
        });

        jLabel5.setText("Error");

        PuntoInicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                PuntoInicialKeyReleased(evt);
            }
        });

        jLabel4.setText("Iteraciones");

        Graficar.setText("Graficar");
        Graficar.setEnabled(false);
        Graficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GraficarActionPerformed(evt);
            }
        });

        Iteraciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                IteracionesKeyReleased(evt);
            }
        });

        jLabel3.setText("Punto Final");

        jLabel2.setText("Punto Inicial");

        PuntoFinal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PuntoFinalActionPerformed(evt);
            }
        });
        PuntoFinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                PuntoFinalKeyReleased(evt);
            }
        });

        Aceptar.setText("Aceptar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });
        Aceptar.setEnabled(false);

        jLabel1.setText("FUNCION A EVALUAR");

        funcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcionActionPerformed(evt);
            }
        });
        funcion.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                funcionFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                funcionFocusLost(evt);
            }
        });
        funcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                funcionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                funcionKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(PanelLayout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(PuntoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, PanelLayout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel4))
                                .addGap(18, 18, 18)
                                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(Iteraciones)
                                    .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(PanelLayout.createSequentialGroup()
                            .addGap(46, 46, 46)
                            .addComponent(jLabel2)
                            .addGap(18, 18, 18)
                            .addComponent(PuntoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(Reiniciar)
                        .addGap(18, 18, 18)
                        .addComponent(Aceptar))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PanelLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(Graficar))
                            .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(52, Short.MAX_VALUE))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Graficar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(PuntoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(PuntoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Reiniciar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Aceptar, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addGap(54, 54, 54))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void funcionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcionFocusGained

   }//GEN-LAST:event_funcionFocusGained

    private void funcionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcionFocusLost

   }//GEN-LAST:event_funcionFocusLost

    private void funcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionKeyPressed
        if (funcion.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcionKeyPressed

    private void funcionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionKeyReleased
        if (funcion.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcionKeyReleased

    private void GraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GraficarActionPerformed
        MetodosNumericos.F = funcion.getText().toString().toLowerCase();
        ProyGraf.main(null);
        funcion.setEnabled(false);
        Graficar.setEnabled(false);
        Reiniciar.setEnabled(true);
        if (!ProyGraf.f.isVisible()) {
            ProyGraf.f.setVisible(true);
        }
    }//GEN-LAST:event_GraficarActionPerformed

    private void PuntoInicialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PuntoInicialKeyReleased
        if (!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_PuntoInicialKeyReleased

    private void PuntoFinalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PuntoFinalKeyReleased
        if (!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_PuntoFinalKeyReleased

    private void IteracionesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IteracionesKeyReleased
        if (!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_IteracionesKeyReleased

    private void ReiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReiniciarActionPerformed
        if (MetodosNumericos.FP != null) {
            if (MetodosNumericos.FP.isVisible()) {
                MetodosNumericos.FP.setVisible(false);
            }
        }
        funcion.setText("");
        funcion.setEnabled(true);
        PuntoInicial.setText("");
        PuntoFinal.setText("");
        Iteraciones.setText("");
        Error.setText("");
        Aceptar.setEnabled(false);
        ProyGraf.f.setVisible(false);
        Reiniciar.setEnabled(false);
    }//GEN-LAST:event_ReiniciarActionPerformed

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        MetodosNumericos.FP = new TablaFalsaPosicion();
        if (!MetodosNumericos.FP.isVisible() && !MetodosNumericos.FP.NoFound) {
            MetodosNumericos.FP.setVisible(true);
        }
    }//GEN-LAST:event_AceptarActionPerformed

    private void funcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcionActionPerformed

    }//GEN-LAST:event_funcionActionPerformed

    private void PuntoFinalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PuntoFinalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_PuntoFinalActionPerformed

    private void ErrorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ErrorKeyReleased
        if (!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_ErrorKeyReleased
    public static void main(String args[]) {
         try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FalsaPosicion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FalsaPosicion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FalsaPosicion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FalsaPosicion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FalsaPosicion().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Aceptar;
    public static javax.swing.JTextField Error;
    public static javax.swing.JToggleButton Graficar;
    public static javax.swing.JTextField Iteraciones;
    public static MetNum.JPanelConFondo Panel;
    public static javax.swing.JTextField PuntoFinal;
    public static javax.swing.JTextField PuntoInicial;
    public static javax.swing.JButton Reiniciar;
    public static javax.swing.JTextField funcion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}