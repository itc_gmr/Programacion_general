package MetodosIterativos;

import MetNum.MetodosNumericos;
import MetNum.ProyGraf;
import Tablas.TablaRaphson2;

public class Raphson2 extends javax.swing.JFrame {
    public Raphson2() {
        super("Newton Raphson Segundo Orden");
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new MetNum.JPanelConFondo();
        Panel.setImagen("/ima/5.jpg");
        jLabel4 = new javax.swing.JLabel();
        X = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        Derivada = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Iteraciones = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        Aceptar = new javax.swing.JButton();
        Graficar = new javax.swing.JToggleButton();
        Reiniciar = new javax.swing.JButton();
        funcion1 = new javax.swing.JTextField();
        Error = new javax.swing.JTextField();
        SegundaDerivada = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel4.setText("X");

        X.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                XKeyReleased(evt);
            }
        });

        jLabel3.setText("Derivada");

        Derivada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                DerivadaKeyReleased(evt);
            }
        });

        jLabel6.setText("Error");

        jLabel5.setText("Iteraciones");

        Iteraciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                IteracionesKeyReleased(evt);
            }
        });

        jLabel2.setText("FUNCIÓN A EVALUAR");

        Aceptar.setText("Aceptar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });
        Aceptar.setEnabled(false);

        Graficar.setText("Graficar");
        Graficar.setEnabled(false);
        Graficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GraficarActionPerformed(evt);
            }
        });

        Reiniciar.setText("Reiniciar");
        Reiniciar.setEnabled(false);
        Reiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReiniciarActionPerformed(evt);
            }
        });

        funcion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcion1ActionPerformed(evt);
            }
        });
        funcion1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                funcion1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                funcion1FocusLost(evt);
            }
        });
        funcion1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                funcion1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                funcion1KeyReleased(evt);
            }
        });

        Error.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ErrorKeyReleased(evt);
            }
        });

        SegundaDerivada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                SegundaDerivadaKeyReleased(evt);
            }
        });

        jLabel7.setText("Segunda Derivada");

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Graficar)
                            .addComponent(funcion1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(Error)
                                .addComponent(Iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(X, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(Reiniciar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Aceptar))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SegundaDerivada, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Derivada, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(funcion1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Graficar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Derivada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SegundaDerivada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(X, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(29, 29, 29)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Aceptar)
                    .addComponent(Reiniciar))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void XKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_XKeyReleased
        if (!Derivada.getText().equals("") && !X.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_XKeyReleased

    private void DerivadaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DerivadaKeyReleased
        if (!Derivada.getText().equals("") && !X.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_DerivadaKeyReleased

    private void IteracionesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IteracionesKeyReleased
        if (!Derivada.getText().equals("") && !X.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_IteracionesKeyReleased

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        MetodosNumericos.dF = Derivada.getText().toString().toLowerCase();
        MetodosNumericos.d2F = SegundaDerivada.getText().toLowerCase();
        MetodosNumericos.TR2 = new TablaRaphson2();
        if (!MetodosNumericos.TR2.isVisible() && !MetodosNumericos.TR2.NoFound) {
            MetodosNumericos.TR2.setVisible(true);
        }
    }//GEN-LAST:event_AceptarActionPerformed

    private void GraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GraficarActionPerformed
        MetodosNumericos.F = funcion1.getText().toString().toLowerCase();
        ProyGraf.main(null);
        funcion1.setEnabled(false);
        Graficar.setEnabled(false);
        Reiniciar.setEnabled(true);
        if (!ProyGraf.f.isVisible()) {
            ProyGraf.f.setVisible(true);
        }
    }//GEN-LAST:event_GraficarActionPerformed

    private void ReiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReiniciarActionPerformed
        if (MetodosNumericos.TR2 != null) {
            if (MetodosNumericos.TR2.isVisible()) {
                MetodosNumericos.TR2.setVisible(false);
            }
        }
        funcion1.setText("");
        funcion1.setEnabled(true);
        Derivada.setText("");
        X.setText("");
        Iteraciones.setText("");
        Error.setText("");
        Aceptar.setEnabled(false);
        ProyGraf.f.setVisible(false);
        Reiniciar.setEnabled(false);
    }//GEN-LAST:event_ReiniciarActionPerformed

    private void funcion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcion1ActionPerformed
       
    }//GEN-LAST:event_funcion1ActionPerformed

    private void funcion1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcion1FocusGained

   }//GEN-LAST:event_funcion1FocusGained

    private void funcion1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcion1FocusLost

   }//GEN-LAST:event_funcion1FocusLost

    private void funcion1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcion1KeyPressed
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcion1KeyPressed

    private void funcion1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcion1KeyReleased
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcion1KeyReleased

    private void ErrorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ErrorKeyReleased
        if (!Derivada.getText().equals("") && !X.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_ErrorKeyReleased

    private void SegundaDerivadaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_SegundaDerivadaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_SegundaDerivadaKeyReleased
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Raphson2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Raphson2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Raphson2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Raphson2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Raphson2().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Aceptar;
    public static javax.swing.JTextField Derivada;
    public static javax.swing.JTextField Error;
    public static javax.swing.JToggleButton Graficar;
    public static javax.swing.JTextField Iteraciones;
    public static MetNum.JPanelConFondo Panel;
    public static javax.swing.JButton Reiniciar;
    public static javax.swing.JTextField SegundaDerivada;
    public static javax.swing.JTextField X;
    public static javax.swing.JTextField funcion1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    // End of variables declaration//GEN-END:variables
}