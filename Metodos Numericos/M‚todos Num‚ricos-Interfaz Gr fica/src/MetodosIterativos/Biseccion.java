package MetodosIterativos;
import MetNum.MetodosNumericos;
import MetNum.ProyGraf;
import Tablas.TablaBiseccion;
public class Biseccion extends javax.swing.JFrame {
    public Biseccion() {
        super("Biseccion");
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new MetNum.JPanelConFondo();
        Panel.setImagen("/ima/5.jpg");
        jLabel1 = new javax.swing.JLabel();
        funcion = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        PuntoInicial = new javax.swing.JTextField();
        PuntoFinal = new javax.swing.JTextField();
        Iteraciones = new javax.swing.JTextField();
        Error = new javax.swing.JTextField();
        Aceptar = new javax.swing.JButton();
        Graficar = new javax.swing.JToggleButton();
        Reiniciar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        Panel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                PanelKeyTyped(evt);
            }
        });

        jLabel1.setText("FUNCIÓN A EVALUAR");

        funcion.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                funcionFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                funcionFocusLost(evt);
            }
        });
        funcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                funcionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                funcionKeyReleased(evt);
            }
        });

        jLabel2.setText("Punto Inicial");

        jLabel3.setText("Punto Final");

        jLabel4.setText("Iteraciones");

        jLabel5.setText("Error");

        PuntoInicial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                PuntoInicialKeyReleased(evt);
            }
        });

        PuntoFinal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                PuntoFinalKeyReleased(evt);
            }
        });

        Iteraciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                IteracionesKeyReleased(evt);
            }
        });

        Error.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ErrorKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ErrorKeyTyped(evt);
            }
        });

        Aceptar.setText("Aceptar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });
        Aceptar.setEnabled(false);

        Graficar.setText("Graficar");
        Graficar.setEnabled(false);
        Graficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GraficarActionPerformed(evt);
            }
        });

        Reiniciar.setText("Reiniciar");
        Reiniciar.setEnabled(false);
        Reiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReiniciarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(Reiniciar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Aceptar))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3))
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PuntoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(PuntoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Graficar)
                            .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(59, Short.MAX_VALUE))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(funcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(Graficar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(PuntoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(PuntoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Reiniciar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Aceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(59, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void funcionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcionFocusGained
    }//GEN-LAST:event_funcionFocusGained

    private void funcionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcionFocusLost
    }//GEN-LAST:event_funcionFocusLost

    private void funcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionKeyPressed
        if(funcion.getText().length()>0)
            Graficar.setEnabled(true);
        else
            Graficar.setEnabled(false);
    }//GEN-LAST:event_funcionKeyPressed
    public static int cerrar(){
        if(ProyGraf.f!=null)
            ProyGraf.f.setVisible(false);
        if(MetodosNumericos.b!=null)
            MetodosNumericos.b.setVisible(false);
        return 0;
    }
    private void funcionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionKeyReleased
        if(funcion.getText().length()>0)
            Graficar.setEnabled(true);
        else
            Graficar.setEnabled(false);
    }//GEN-LAST:event_funcionKeyReleased

    private void GraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GraficarActionPerformed
        MetodosNumericos.F = funcion.getText().toString().toLowerCase();
        ProyGraf.main(null);
        funcion.setEnabled(false);
        Graficar.setEnabled(false);
        Reiniciar.setEnabled(true);
        if(!ProyGraf.f.isVisible())
            ProyGraf.f.setVisible(true);
    }//GEN-LAST:event_GraficarActionPerformed

    private void ReiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReiniciarActionPerformed
        if(MetodosNumericos.TB != null)
            if(MetodosNumericos.TB.isVisible())
                MetodosNumericos.TB.setVisible(false);
        funcion.setText("");
        funcion.setEnabled(true);
        PuntoInicial.setText("");
        PuntoFinal.setText("");
        Iteraciones.setText("");
        Error.setText("");
        Aceptar.setEnabled(false);
        ProyGraf.f.setVisible(false);
        Reiniciar.setEnabled(false);
    }//GEN-LAST:event_ReiniciarActionPerformed

    private void PanelKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PanelKeyTyped
        
    }//GEN-LAST:event_PanelKeyTyped

    private void ErrorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ErrorKeyTyped
       
    }//GEN-LAST:event_ErrorKeyTyped

    private void ErrorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ErrorKeyReleased
        if(!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("") && 
           !Iteraciones.getText().equals("") && !Error.getText().equals(""))//PARTE QUE SE TIENE QUE MEJORAR
            Aceptar.setEnabled(true);
        else
            Aceptar.setEnabled(false);
    }//GEN-LAST:event_ErrorKeyReleased

    private void IteracionesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IteracionesKeyReleased
        if(!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("") && 
           !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled())
            Aceptar.setEnabled(true);
        else
            Aceptar.setEnabled(false);
    }//GEN-LAST:event_IteracionesKeyReleased

    private void PuntoFinalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PuntoFinalKeyReleased
        if(!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("") && 
           !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled())
            Aceptar.setEnabled(true);
        else
            Aceptar.setEnabled(false);
    }//GEN-LAST:event_PuntoFinalKeyReleased

    private void PuntoInicialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PuntoInicialKeyReleased
        if(!PuntoInicial.getText().equals("") && !PuntoFinal.getText().equals("") && 
           !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled())
            Aceptar.setEnabled(true);
        else
            Aceptar.setEnabled(false);
    }//GEN-LAST:event_PuntoInicialKeyReleased

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        MetodosNumericos.TB =  new TablaBiseccion();
        if(!MetodosNumericos.TB.isVisible() && !MetodosNumericos.TB.NoFound)
            MetodosNumericos.TB.setVisible(true);
    }//GEN-LAST:event_AceptarActionPerformed
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Biseccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Biseccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Biseccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Biseccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Biseccion().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Aceptar;
    public static javax.swing.JTextField Error;
    public static javax.swing.JToggleButton Graficar;
    public static javax.swing.JTextField Iteraciones;
    public static MetNum.JPanelConFondo Panel;
    public static javax.swing.JTextField PuntoFinal;
    public static javax.swing.JTextField PuntoInicial;
    public static javax.swing.JButton Reiniciar;
    public static javax.swing.JTextField funcion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}