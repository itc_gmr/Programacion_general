package MetodosIterativos;

import MetNum.MetodosNumericos;
import MetNum.ProyGraf;
import Tablas.TablaNewtonRaphson;

public class NewtonRaphson extends javax.swing.JFrame {
    public NewtonRaphson() {
        super("Newton Raphson");
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new MetNum.JPanelConFondo();
        Panel.setImagen("/ima/5.jpg");
        jLabel2 = new javax.swing.JLabel();
        funcion1 = new javax.swing.JTextField();
        Graficar = new javax.swing.JToggleButton();
        jLabel3 = new javax.swing.JLabel();
        Derivada = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        X = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        Iteraciones = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        Error = new javax.swing.JTextField();
        Reiniciar = new javax.swing.JButton();
        Aceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel2.setText("FUNCIÓN A EVALUAR");

        funcion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcion1ActionPerformed(evt);
            }
        });
        funcion1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                funcion1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                funcion1FocusLost(evt);
            }
        });
        funcion1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                funcion1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                funcion1KeyReleased(evt);
            }
        });

        Graficar.setText("Graficar");
        Graficar.setEnabled(false);
        Graficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GraficarActionPerformed(evt);
            }
        });

        jLabel3.setText("Derivada");

        Derivada.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                DerivadaKeyReleased(evt);
            }
        });

        jLabel4.setText("X");

        X.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                XKeyReleased(evt);
            }
        });

        jLabel5.setText("Iteraciones");

        Iteraciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                IteracionesKeyReleased(evt);
            }
        });

        jLabel6.setText("Error");

        Error.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ErrorKeyReleased(evt);
            }
        });

        Reiniciar.setText("Reiniciar");
        Reiniciar.setEnabled(false);
        Reiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReiniciarActionPerformed(evt);
            }
        });

        Aceptar.setText("Aceptar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });
        Aceptar.setEnabled(false);

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(Error)
                                .addComponent(Iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(Derivada, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(X, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(PanelLayout.createSequentialGroup()
                                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(Graficar)
                                    .addComponent(funcion1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(2, 2, 2))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                        .addComponent(Reiniciar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Aceptar)))
                .addContainerGap(33, Short.MAX_VALUE))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                .addContainerGap(28, Short.MAX_VALUE)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(funcion1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Graficar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Derivada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(X, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(29, 29, 29)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Aceptar)
                    .addComponent(Reiniciar))
                .addGap(26, 26, 26))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void funcion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcion1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_funcion1ActionPerformed

    private void funcion1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcion1FocusGained

   }//GEN-LAST:event_funcion1FocusGained

    private void funcion1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcion1FocusLost

   }//GEN-LAST:event_funcion1FocusLost

    private void funcion1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcion1KeyPressed
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcion1KeyPressed

    private void funcion1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcion1KeyReleased
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcion1KeyReleased

    private void GraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GraficarActionPerformed
        MetodosNumericos.F = funcion1.getText().toString().toLowerCase();
        ProyGraf.main(null);
        funcion1.setEnabled(false);
        Graficar.setEnabled(false);
        Reiniciar.setEnabled(true);
        if (!ProyGraf.f.isVisible()) {
            ProyGraf.f.setVisible(true);
        }
    }//GEN-LAST:event_GraficarActionPerformed

    private void DerivadaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DerivadaKeyReleased
        if (!Derivada.getText().equals("") && !X.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_DerivadaKeyReleased

    private void XKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_XKeyReleased
        if (!Derivada.getText().equals("") && !X.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_XKeyReleased

    private void IteracionesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IteracionesKeyReleased
        if (!Derivada.getText().equals("") && !X.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_IteracionesKeyReleased

    private void ErrorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ErrorKeyReleased
        if (!Derivada.getText().equals("") && !X.getText().equals("")
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_ErrorKeyReleased

    private void ReiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReiniciarActionPerformed
        if (MetodosNumericos.TNR != null) {
            if (MetodosNumericos.TNR.isVisible()) {
                MetodosNumericos.TNR.setVisible(false);
            }
        }
        funcion1.setText("");
        funcion1.setEnabled(true);
        Derivada.setText("");
        X.setText("");
        Iteraciones.setText("");
        Error.setText("");
        Aceptar.setEnabled(false);
        ProyGraf.f.setVisible(false);
        Reiniciar.setEnabled(false);
    }//GEN-LAST:event_ReiniciarActionPerformed

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        MetodosNumericos.dF = Derivada.getText().toLowerCase();
        MetodosNumericos.TNR = new TablaNewtonRaphson();
        if (!MetodosNumericos.TNR.isVisible() && !MetodosNumericos.TNR.NoFound) {
            MetodosNumericos.TNR.setVisible(true);
        }
    }//GEN-LAST:event_AceptarActionPerformed
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewtonRaphson.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewtonRaphson.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewtonRaphson.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewtonRaphson.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewtonRaphson().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Aceptar;
    public static javax.swing.JTextField Derivada;
    public static javax.swing.JTextField Error;
    public static javax.swing.JToggleButton Graficar;
    public static javax.swing.JTextField Iteraciones;
    public static MetNum.JPanelConFondo Panel;
    public static javax.swing.JButton Reiniciar;
    public static javax.swing.JTextField X;
    public static javax.swing.JTextField funcion1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    // End of variables declaration//GEN-END:variables
}