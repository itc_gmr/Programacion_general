package MetodosIterativos;
import MetNum.MetodosNumericos;
import MetNum.ProyGraf;
import Tablas.TablaAproximaciones;

public class Aproximaciones extends javax.swing.JFrame {
    public Aproximaciones() {
        super("Aproximaciones Consecutivas");
        initComponents();
        setLocationRelativeTo(null);
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new MetNum.JPanelConFondo();
        Panel.setImagen("/ima/5.jpg");
        funcion1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        Graficar = new javax.swing.JToggleButton();
        x = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Iteraciones = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        Error = new javax.swing.JTextField();
        Reiniciar = new javax.swing.JButton();
        Aceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        funcion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcion1ActionPerformed(evt);
            }
        });
        funcion1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                funcion1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                funcion1FocusLost(evt);
            }
        });
        funcion1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                funcion1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                funcion1KeyReleased(evt);
            }
        });

        jLabel2.setText("FUNCIÓN A EVALUAR");

        Graficar.setText("Graficar");
        Graficar.setEnabled(false);
        Graficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GraficarActionPerformed(evt);
            }
        });

        x.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                xKeyReleased(evt);
            }
        });

        jLabel3.setText("X0");

        jLabel5.setText("Iteraciones");

        Iteraciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                IteracionesKeyReleased(evt);
            }
        });

        jLabel6.setText("Error");

        Error.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                ErrorKeyReleased(evt);
            }
        });

        Reiniciar.setText("Reiniciar");
        Reiniciar.setEnabled(false);
        Reiniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReiniciarActionPerformed(evt);
            }
        });

        Aceptar.setText("Aceptar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });
        Aceptar.setEnabled(false);

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(Reiniciar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Aceptar))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(Iteraciones, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                            .addComponent(x, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Graficar)
                            .addComponent(funcion1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Error))))
                .addContainerGap(33, Short.MAX_VALUE))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(funcion1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(11, 11, 11)
                .addComponent(Graficar, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(x, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(Iteraciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(Error, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Reiniciar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Aceptar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(29, 29, 29))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void funcion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcion1ActionPerformed

    }//GEN-LAST:event_funcion1ActionPerformed

    private void funcion1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcion1FocusGained

   }//GEN-LAST:event_funcion1FocusGained

    private void funcion1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_funcion1FocusLost

   }//GEN-LAST:event_funcion1FocusLost

    private void funcion1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcion1KeyPressed
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcion1KeyPressed

    private void funcion1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcion1KeyReleased
        if (funcion1.getText().length() > 0) {
            Graficar.setEnabled(true);
        } else {
            Graficar.setEnabled(false);
        }
    }//GEN-LAST:event_funcion1KeyReleased

    private void GraficarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GraficarActionPerformed
        MetodosNumericos.F = funcion1.getText().toString().toLowerCase();
        MetodosNumericos.G = Aproximaciones.funcion1.getText() + "+x";
        ProyGraf.main(null);
        funcion1.setEnabled(false);
        Graficar.setEnabled(false);
        Reiniciar.setEnabled(true);
        if (!ProyGraf.f.isVisible()) {
            ProyGraf.f.setVisible(true);
        }
    }//GEN-LAST:event_GraficarActionPerformed

    private void xKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_xKeyReleased
        if (!x.getText().equals("") 
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_xKeyReleased

    private void IteracionesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_IteracionesKeyReleased
        if (!x.getText().equals("") && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()) {
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_IteracionesKeyReleased

    private void ErrorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ErrorKeyReleased
        if (!x.getText().equals("") 
                && !Iteraciones.getText().equals("") && !Error.getText().equals("") && Reiniciar.isEnabled()==false) {//PARTE A VERIFICAR
            Aceptar.setEnabled(true);
        } else {
            Aceptar.setEnabled(false);
        }
    }//GEN-LAST:event_ErrorKeyReleased

    private void ReiniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReiniciarActionPerformed
        if (MetodosNumericos.TA != null) {
            if (MetodosNumericos.TA.isVisible()) {
                MetodosNumericos.TA.setVisible(false);
            }
        }
        funcion1.setText("");
        funcion1.setEnabled(true);
        x.setText("");
        Iteraciones.setText("");
        Error.setText("");
        Aceptar.setEnabled(false);
        ProyGraf.f.setVisible(false);
        Reiniciar.setEnabled(false);
    }//GEN-LAST:event_ReiniciarActionPerformed

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        MetodosNumericos.TA = new TablaAproximaciones();
        if (!MetodosNumericos.TA.isVisible() && !TablaAproximaciones.NoFound) {
            MetodosNumericos.TA.setVisible(true);
        }
    }//GEN-LAST:event_AceptarActionPerformed
    public static void main(String args[]) { 
       try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Aproximaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Aproximaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Aproximaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Aproximaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Aproximaciones().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton Aceptar;
    public static javax.swing.JTextField Error;
    public static javax.swing.JToggleButton Graficar;
    public static javax.swing.JTextField Iteraciones;
    public static MetNum.JPanelConFondo Panel;
    public static javax.swing.JButton Reiniciar;
    public static javax.swing.JTextField funcion1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    public static javax.swing.JTextField x;
    // End of variables declaration//GEN-END:variables
}
