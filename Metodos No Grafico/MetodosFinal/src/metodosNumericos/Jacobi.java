package metodosNumericos;


public class Jacobi {
	public static void Met() 
	{
		int m;
		int iteraciones;
		int it=1;
		do{
			System.out.print("Numero de ecuaciones: ");
			m=Leer.datoInt();
		}while(m<=0);
		System.out.print("Iteraciones: ");
		iteraciones=Leer.datoInt();
		System.out.print("Error Permitido: ");
		double e=Leer.datoDouble();
		double a[][] = new double[m][m+1];
		double x[]=new double[m];
		double y[]=new double[100];
		boolean fin;
		double delta;
		for (int i=0;i<m;i++)
		{
			for (int j=0;j<m+1;j++)
			{
				System.out.print("M["+(i+1)+"]["+(j+1)+"]: ");
				a[i][j]=Leer.datoDouble();     
			}
		}
		System.out.println(Pon.Blancos("i", 5)+Pon.Blancos("X1", 25)+Pon.Blancos("X2", 25)+Pon.Blancos("X3", 25)+Pon.Blancos("X1", 25)+Pon.Blancos("X2", 25)+Pon.Blancos("X3", 25));
		for(int i=0;i<m;i++)
		{
			x[i]=0;
		}
		do{
			fin=false;
			for(int i=0;i<m;i++)
			{
				y[i]=a[i][m];
				for(int j=0;j<m;j++)
				{
					if(i!=j)
					{
						y[i]=y[i]-a[i][j]*x[j];
					}
				}
				y[i]=y[i]/a[i][i];	
				delta=Math.abs(x[i]-y[i]);
				if(delta>e)
				{
					fin=true;
				}
			}
			System.out.print(Pon.Blancos(it, 5));
			for(int i =0; i<m;i++)
				System.out.print(Pon.Blancos(x[i], 25));
			for(int i=0;i<m;i++)
			{
				x[i]=y[i];
				System.out.print(Pon.Blancos(x[i], 25));
			}
			System.out.println();
			it++;
		}while(fin && iteraciones>=it);
		System.out.println();
		System.out.println();
		
		for(int i=0;i<m;i++)
		{
			System.out.println("El valor de X"+(i+1)+" es: "+x[i]);
		}
	}

}
