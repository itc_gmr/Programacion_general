package metodosNumericos;

import org.nfunk.jep.*;
public class MetNewtonR {
	MGrafica Grafica;
	public MetNewtonR(){
		NewtonRaphson();
	}
	public void NewtonRaphson(){	
		String fun, der;
		double x, e;
		int ni;
		Grafica = new MGrafica("Grafica Newton Raphson");
		System.out.println();
		System.out.println("Ingrese Valores");
		System.out.println();
		System.out.print("Funci�n: ");
		fun=Leer.DString();
		System.out.print("Derivada: ");
		der=Leer.DString();
		System.out.println("Intersecci�n: ");
		System.out.print("a= ");
		x=Leer.datoDouble();
		System.out.print("Error permitido: ");
		e=Leer.datoDouble();
		System.out.print("N�mero de Iteraciones m�ximas: ");
		ni=Leer.datoInt();
		double s, delta;
        int i=1;
        System.out.println(Pon.Blancos("i",5)+Pon.Blancos("Xi",25)+Pon.Blancos("f(Xi)",25)+Pon.Blancos("f'(Xi)",25)+Pon.Blancos("Xi+1",25)+"Error");
        do{
            s = x - (f(fun,x)/f(der,x));
            delta = Math.abs(x-s);
            System.out.println(Pon.Blancos(i,5)+Pon.Blancos(x,25)+Pon.Blancos(f(fun,x),25)+Pon.Blancos(f(der,x),25)+Pon.Blancos(s,25)+delta);
            x = s;
            i++;
        }
        while(i<=ni && delta>e);
        Funcion CFun=new Funcion(fun);
        Grafica.AgregaDatos(CFun,x);
        System.out.println("\nSoluci�n= " + s);
        Grafica.Agrega(s, ni);
        Grafica.MuestraGrafica("Grafica Newton Raphson");
    }
	private double f(String funcion, double x){
        JEP f = new JEP();
        f.addStandardConstants();
        f.addStandardFunctions();
        f.addVariable("x", x);
        f.parseExpression(funcion);
        return f.getValue();
    }
}


