package metodosNumericos;

public class GaussJordan {
	public static void Met() 
	{ 
		int n=0,m=0; 
		float matriz[][],pivote,cero;  
		System.out.print("\nTamano de la Matriz: "); 
		m=Leer.datoInt();
		n=m+1;
		matriz=new float[m][n]; 
		for(int x=0;x<m;x++) 
		{ 
			for(int y=0;y<n;y++) 
			{ 
				System.out.print("A["+(x)+"]["+(y)+"]: "); 
				matriz[x][y]=Leer.datoFloat(); 
			} 
		}
		System.out.print(" -----------Matriz original----------- "); 
		System.out.println(); 
		for(int x=0;x<m;x++) 
		{ 
			for(int y=0;y<(n);y++) 
				System.out.print(" "+matriz[x][y]+" |"); 
			System.out.println(); 
		} 
		for(int x=0;x<m;x++) 
		{ 
			pivote = matriz[x][x];
			for(int y=0;y<n;y++) 
			{
				matriz[x][y]=matriz[x][y]/pivote;
			}
			for(int k=0;k<m;k++) 
			{
				if(k!=x)
				{
					cero=matriz[k][x];
					for(int y=0;y<n;y++) 
					{
						matriz[k][y]=matriz[k][y]-cero*matriz[x][y];
					}	
				}
			}
		}
		System.out.print(" -----------Matriz final----------- "); 
		System.out.println(); 
		for(int x=0;x<m;x++) 
		{ 
			for(int y=0;y<(n);y++) 
				System.out.print(" "+matriz[x][y]+" |"); 
				System.out.println(); 
		} 
		System.out.print("***Solucion***"); 
		System.out.println(); 
		for(int x=0;x<m;x++) 
		{
			System.out.println("el valor de x "+(x+1)+" es: "+matriz[x][m]);
		}
	}
}
