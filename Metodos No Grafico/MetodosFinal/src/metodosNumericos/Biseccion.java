package metodosNumericos;

import org.nfunk.jep.*;
public class Biseccion {
	JEP funcion = new JEP();
	MGrafica Graficacion;
	public Biseccion(){
		Bisec();
	}
	public void Bisec(){
		String fun;
		double a, b, e;
		int ni;
		Graficacion = new MGrafica("Metodo de Biseccion");
		System.out.println("Teclee los Datos");
		System.out.print("Funci�n: ");
		fun=Leer.DString();
		Funcion CFun=new Funcion(fun);
		System.out.println("Intersecci�n: ");
		do{
			System.out.print("a= ");
			a=Leer.datoDouble();
			System.out.print("b= ");
			b=Leer.datoDouble();
			if(CFun.calc(a)*CFun.calc(b)>0){
				System.out.println("Ingrese un intervalo valido");
			}
		}while(f(a)*f(b)>0);
		System.out.print("Error permitido: ");
		e=Leer.datoDouble();
		System.out.print("N�mero de Iteraciones m�ximas: ");
		ni=Leer.datoInt();
		double p=a,eps; 
        int i=1, cont=0;
        System.out.println(Pon.Blancos("i",5)+Pon.Blancos("a",20)+Pon.Blancos("b",20)+Pon.Blancos("Error",20)+Pon.Blancos("Xm",20)+Pon.Blancos("f(a)",25)+"f(Xm)");
        System.out.println();
		do{
            eps = Math.abs(b-a);
            p = (a+b)/2;
            System.out.println(Pon.Blancos(i, 5)+Pon.Blancos(a, 20)+Pon.Blancos(b, 20)+Pon.Blancos(eps,20)+Pon.Blancos(p, 20)+Pon.Blancos(CFun.calc(a), 25)+CFun.calc(p));
            if(CFun.calc(p)*CFun.calc(a)>0)
               a=p;
            else 
               if(CFun.calc(p)*CFun.calc(b)>0)
                  b=p;
           
            i++;
            cont++;
         }while(CFun.calc(p)!=0 & i<=ni & eps>e);
		Graficacion.AgregaDatos(CFun,a);
		Graficacion.Agrega(p,ni);
		System.out.println("\nSoluci�n= " + p);
		Graficacion.MuestraGrafica("Metodo de Biseccion");
	}
	public double f(double x){
		 funcion.addVariable("x",x);
		 return funcion.getValue();
	}
}
