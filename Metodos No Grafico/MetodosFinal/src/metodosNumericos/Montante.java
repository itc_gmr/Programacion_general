package metodosNumericos;

public class Montante {
	public static void Met()
	{
		int n,m=0;
		double deter;
		double mat[][];
		double pivoteant=1;
		do{
			System.out.print("NR: ");
			m=Leer.datoInt();
			n=m+1;
		}while(m<=0);
		mat=new double[m][n];
		for(int i=0;i<mat.length;i++)
		{
			for(int j=0;j<mat[0].length;j++)
			{
				System.out.print("M["+(i+1)+"]["+(j+1)+"]:");
				mat[i][j]=Leer.datoDouble();
			}
		}		
		System.out.println("---------Matriz Inicial--------\n");
		Imprime(mat);
		pivoteant=1;
		for(int k=0; k<m;k++)
		{
			for(int i=0;i<m;i++)
			{ 
				if(i!=k)
				{
					for(int j=m;j>=0; j--)
						mat[i][j]=(mat[k][k]*mat[i][j]-mat[i][k]*mat[k][j])/pivoteant;
				}
			}
			pivoteant=mat[k][k];
		}
		System.out.println("---------Matriz Final--------\n");
		Imprime(mat);
		
		deter=mat[0][0];
		for(int i=0; i<m; i++)
		{
			for(int j=0; j<m; j++)
			{
				if(i==j)
				{
					mat[i][j]=mat[i][j]/deter;
					mat[i][m]=mat[i][m]/deter;
				}
			}
		}
		System.out.println("\nLa soluci�n es: ");
		for(int i=0; i<m; i++){
			System.out.println("x "+i+"= "+mat[i][m]);
		}	
	}
	public static void Imprime(double mat[][]){
		for(int i=0;i<mat.length;i++)
		{
			for(int j=0;j<mat[0].length;j++)
				System.out.print(" | "+mat[i][j]+" | ");	
			System.out.println();
		}
	}
}
