package metodosNumericos;

import org.nfunk.jep.*;
public class MetodoSecante {	
	JEP funcion = new JEP();
	MGrafica Grafica;
	public MetodoSecante(){
		Secante();
	}
	public void Secante(){	
		String fun;
		double a,b,e;
		int ni;
		Grafica = new MGrafica("Grafica Secante");
		System.out.println();
		System.out.println("Ingrese Valores");
		System.out.println();
		System.out.print("Funci�n: ");
		fun=Leer.DString();
		System.out.println("Intersecci�n: ");
		System.out.print("a= ");
		a=Leer.datoDouble();
		System.out.print("b= ");
		b=Leer.datoDouble();
		System.out.print("Error permitido: ");
		e=Leer.datoDouble();
		System.out.print("N�mero de Iteraciones m�ximas: ");
		ni=Leer.datoInt();
		System.out.println();
		System.out.println();
		double xi2=0,xi1=b,xi=a, eps;
		int i=0;
		System.out.println(Pon.Blancos("i",5)+Pon.Blancos("Xi",25)+Pon.Blancos("Xi+1",25)+Pon.Blancos("f(Xi)",25)+Pon.Blancos("f(Xi+1)",25)+Pon.Blancos("Xi+2",25)+Pon.Blancos("Error",25)+"f(Xi+2)");
		do{
			i++;
			xi2 = xi1 - ((f(fun,xi1)*(xi1-xi))/(f(fun,xi1)-f(fun,xi)));
			eps= Math.abs(xi2-xi1);
			System.out.println(Pon.Blancos(i,5)+Pon.Blancos(xi,25)+Pon.Blancos(xi1,25)+Pon.Blancos(f(fun,xi),25)+Pon.Blancos(f(fun,xi1), 25)+Pon.Blancos(xi2,25)+Pon.Blancos(eps, 25)+f(fun,xi2));
			xi=xi1;
			xi1=xi2;
		}while(i<ni && eps>e);
		System.out.println("\nSoluci�n= " + xi2);
		Funcion CFun=new Funcion(fun);
		Grafica.AgregaDatos(CFun, a);
		Grafica.Agrega(xi2, ni);
		Grafica.MuestraGrafica("Grafica de la Secante");
	}
	private double f(String funcion, double x){
        JEP f = new JEP();
        f.addStandardConstants();
        f.addStandardFunctions();
        f.addVariable("x", x);
        f.parseExpression(funcion); 
        return f.getValue();
    }
	
}
