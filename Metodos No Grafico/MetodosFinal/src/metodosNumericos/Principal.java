package metodosNumericos;

/*
 Funciones para probar el programa:
 	Metodo de Biseccion: 3*x*x-5 intervalo 1-2
 	Metodo de Falsa Posicion: 3*x*x-5 intervalo 1-2
 	Metodo de Secante: x*x*-4: intervalo: 4-3
 	Metodo Newton Raphson: 3*x*x-5 intervalo: 1-2
 	Metodo de Aproximaciones Sucesivas: x*x-.5 intervalo: -.6 g(x): x*x+x-.5
 	Metodo de Newton de 2do Orden: 3*x*x-5 intervalo: 1 
 */
public class Principal {
	public static void main(String[] args) {
		int op;
		System.out.println("**********M�TODOS NUM�RICOS**********\n");
		while(true){
			System.out.println("*****MEN� DE OPCIONES*****");
			System.out.println();
			System.out.println("1.-M�todo de Soluci�n de Ecuaciones ");
			System.out.println("2.-M�todo de Soluci�n de Sistemas de Ecuaciones ");
			System.out.println("3.-Salir ");
			System.out.print("Teclee una opcion: ");
			op=Leer.datoInt();
			switch(op){
			case 1:
				 Menu_Ecuaciones();
				break;
			case 2: 
				Menu_SisEcuaciones();
			break;
			case 3: 
					System.exit(0);
				break;
			default: System.out.println("**********Opci�n No Aceptada**********");
					break;
			}
		}
	}
	public static void Menu_Ecuaciones(){
	int op;
		while(true){
			System.out.println("\n\n**********Men� De Opciones**********");
			System.out.println();
			System.out.println("1.-M�todo Bisecci�n ");
			System.out.println("2.-M�todo de Falsa Posici�n ");
			System.out.println("3.-M�todo de Secante ");
			System.out.println("4.-M�todo de Newton Raphson ");
			System.out.println("5.-M�todo de Aproximaci�n Sucesiva ");
			System.out.println("6.-M�todo de Newton de 2do Orden ");
			System.out.println("7.-Volver ");
			System.out.print("Opci�n: ");
			op=Leer.datoInt();		
			System.out.println();
			System.out.println();
			switch(op){
			case 1: System.out.println("*****M�todo de Bisecci�n*****");
					new Biseccion();
				break;
			case 2: System.out.println("*****M�todo de Falsa Posici�n*****");
					new MetodoFalsaPosicion();
				break;
			case 3: System.out.println("*****M�todo Secante*****");
					new MetodoSecante();
				break;
			case 4: System.out.println("*****M�todo Newton Raphson*****");
					new MetNewtonR();
				break;
			case 5: System.out.println("*****M�todo de Aproximaci�n Sucesiva*****");
					new MetAproxSusesivas();
				break;
			case 6: System.out.println("*****M�todo de Newton de 2do Orden*****");
					new Newton2();
				break;
			case 7: 
					return;
			default: System.out.println("**********Opci�n No Aceptada**********");
					break;
			}
		}
	}
	
	
	public static void Menu_SisEcuaciones(){
		int op=0;
			while(true){
				System.out.println("\n\n*******Men� M�todos de Soluci�n de Sistemas de Ecuaciones*****");
				System.out.println();
				System.out.println("1.- M�todo Gauss ");
				System.out.println("2.- M�todo Gauss-Jordan ");
				System.out.println("3.- M�todo Montante ");
				System.out.println("4.- M�todo Cramer ");
				System.out.println("5.- M�todo Jacobi ");
				System.out.println("6.- M�todo Gauss-Seidel ");
				System.out.println("7.- Volver");
				System.out.print("Teclee una opcion: ");
				op=Leer.datoInt();
				System.out.println();
				System.out.println();
				switch(op){
				case 1: System.out.println("----------M�todo Gauss----------");
						Gauss.Met();
					break;
				case 2: System.out.println("--------M�todo Gauss-Jordan--------");
						GaussJordan.Met();
					break;
				case 3: System.out.println("-------------M�todo Montante-------------");
						Montante.Met();
					break;
				case 4: System.out.println("---------M�todo Cramer---------");
						Cramer.Met();
					break;
				case 5: System.out.println("-----M�todo Jacobi-----");
						Jacobi.Met();
					break;
				case 6: System.out.println("------M�todo Gauss-Seidel------");
						GaussSeidel.Met();
					break;
				case 7: 
					return;
				default: System.out.println("**********Opci�n No Aceptada**********");
					break;
				}	
			}
		}
	}

