
package juego;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import Componentes.*;
/**
 *
 * @author Guillermo Marquez Ru
 */
public class Juego {

    public static void main(String[] args) {
        int ganador = 0, ventajaG = 0;
        
       capturaDatos cd = new capturaDatos();
       ListaDBL<Ronda> juego = new ListaDBL<Ronda>();
       ListaDBL<Resultado> diferencia = new ListaDBL<Resultado>();
       juego = cd.recuperaLista();
       NodoDBL<Ronda> aux = juego.DameFin();
       NodoDBL<Resultado> Aux = diferencia.DameFrente();
       System.out.println(cd.recuperaTurnos());
       while(aux != null){
           System.out.println(aux.Info.numR + " " + aux.Info.punJ1 + " " + aux.Info.punJ2);
           aux = aux.DameAnt();
       }
       aux = juego.DameFin();
       int resta;
       while(aux != null){
           if(aux.Info.punJ1 > aux.Info.punJ2){
                resta = aux.Info.punJ1 - aux.Info.punJ2;
                diferencia.InsertaFre(new  Resultado(resta, 1));
                aux = aux.DameAnt();
                continue;
           }
           resta = aux.Info.punJ2 - aux.Info.punJ1;
           diferencia.InsertaFre(new Resultado(resta, 2));
           aux = aux.DameAnt();
       }
       Aux = diferencia.DameFrente();
      
        ganador = Aux.Info.lider;
        ventajaG = Aux.Info.ventaja;
       
        Aux = Aux.DameSig();
       while(Aux != null){
           if(Aux.Info.ventaja > ventajaG){
               ganador = Aux.Info.lider;
               ventajaG = Aux.Info.ventaja;
               Aux = Aux.DameSig();
               
               continue;
           }
           Aux = Aux.DameSig();
           
       }
       
       PrintWriter salida = null;
       try{
           salida = new PrintWriter("salida.txt");
           String res = ganador + " " + ventajaG;
           salida.println(res);
           salida.flush();
       }catch(FileNotFoundException e){
           System.out.println(e.getMessage());
       }finally{
           salida.close();
       }
       
       
       
       
    }
    
}

class Resultado{
    int ventaja;
    int lider;
    
    public Resultado(){
        this(0,0);
    }
    public Resultado(int v, int l){
        ventaja = v;
        lider = l;
    }
}
