
package juego;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import Componentes.*;
/**
 *
 * @author Guillermo Marquez Ru
 */
public class capturaDatos {
    private int turnos;
    private ListaDBL<Ronda> puntaje = new ListaDBL<Ronda>();
    private FileReader fr = null;
    BufferedReader entrada;
    
    public ListaDBL<Ronda> recuperaLista(){
        int ronda = 0, p1 = 0, p2 = 0;
        String cad, cadj1 = "", cadj2 = "";
        boolean band = true;
        try{
            fr = new FileReader("juego.txt");
            entrada = new BufferedReader(fr);
            int car = 0;
            while(car != -1){
                car = entrada.read();
                if((!(car == 13 || car == 10)) && (band)){
                    if(car == 32){
                        band = false;
                    }
                    if(car >= 48 && car <= 57){
                        if(ronda == 0){
                            cad = (char)car + "";
                            turnos = Integer.parseInt(cad);
                            continue;
                        }
                        cad = (char)car + "";
                        cadj1 = cadj1 + cad;
                        p1 = Integer.parseInt(cadj1);
                        continue;
                    }
                }
                
                if(!(car == 32) && !band){   
                    if(car >= 48 && car <= 57){
                        cad = (char)car + "";
                        cadj2 = cadj2 + cad;
                        p2 = Integer.parseInt(cadj2);
                    }
                } 
                if((car == 10) || car == -1){
                    if (ronda >= 1){
                        puntaje.InsertaFre(new Ronda(ronda, p1, p2));
                        ronda++;
                        cadj1 = cadj2 = "";
                        p1 = p2 = 0;
                        band = true;
                    }else{
                        ronda++;
                    }
                    
                }
                 
            }
            System.out.println();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
       
        return puntaje;
    }
    
    public int recuperaTurnos(){
        return turnos;
    }
}
