package Componentes;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;

public class ComboListener extends KeyAdapter implements ActionListener{
	JComboBox comboListener;
	Vector vector;
	
	public ComboListener(JComboBox cbListenerParam, Vector vectorParam){
		comboListener = cbListenerParam;
		vector = vectorParam;
		comboListener.addActionListener(this);
	}
	
	public void keyReleased(KeyEvent key){ 
		
		String text = ((JTextField)key.getSource()).getText();
		comboListener.setModel(new DefaultComboBoxModel(getFilteredList(text)));
		comboListener.setSelectedIndex(-1);
		((JTextField)comboListener.getEditor().getEditorComponent()).setText(text);
		comboListener.showPopup();
		
	}
	
	public Vector getFilteredList(String text){ //En este metodo se va filtrando lo que se escribe para que se muestre en el pop up
		Vector v = new Vector();
		for(int i = 0;i<vector.size();i++){
			if(vector.get(i).toString().startsWith(text)){
				v.add(vector.get(i).toString());
			}
		}
		return v;
	}

	public void actionPerformed(ActionEvent evt) {
		
	}
}