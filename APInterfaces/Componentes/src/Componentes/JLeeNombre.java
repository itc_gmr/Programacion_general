package Componentes;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class JLeeNombre extends JTextField implements KeyListener{
	private int Tama;
	
	
	public JLeeNombre(){
		this(20);
	}
	public JLeeNombre(int Tam){
		super(Tam);
		Tama=Tam;
		addKeyListener(this);
	}
	
	public void keyPressed(KeyEvent e) {
		
	}
	public void keyReleased(KeyEvent e) {
		
	}
	public void keyTyped(KeyEvent e) {
		if(getText().length()==Tama){
			e.consume();
			return;
		}
		char T=e.getKeyChar();
			
			if (  ! ( T>='A' && T<='Z' || T>='a' && T<='z' || T == ' ' || T>='0' && T<='9'|| T=='-')    ){
				e.consume();
				return;
			}
	}
}
