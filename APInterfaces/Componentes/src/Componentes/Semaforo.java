package Componentes;
public class Semaforo{
private int Recursos;

public Semaforo(int r){
	Recursos = r;
}

public synchronized void Espera(){
	while(Recursos <= 0){
		try{
			wait();
		}catch(InterruptedException e){}
	}
	Recursos--;
}

public synchronized void libera(){
	Recursos++;
	notify();
}
}