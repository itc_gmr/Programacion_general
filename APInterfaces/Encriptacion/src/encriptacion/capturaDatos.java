/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encriptacion;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import Componentes.*;
/**
 *
 * @author Guillermo Marquez Ru
 */
public class capturaDatos {
    private FileReader fr = null;
    private BufferedReader entrada;
    private Resultado res;
    
    public capturaDatos(){
        try{
            fr = new FileReader("instruccion.txt");
            entrada = new BufferedReader(fr);
            int car = 0;
            String numero = "";
            String mensaje = "";
            ListaDBL<String> m = new ListaDBL<String>();
            ListaDBL<Integer> n = new ListaDBL<Integer>();
            
            boolean band = true;
            while(car != -1){
                car = entrada.read();
                System.out.print((char)car);
                if(car == 13){
                    continue;
                }
                if(band){
                    if(car == 10){
                       band = false;
                        n.InsertaFin(Integer.parseInt(numero));
                        numero = "";
                        continue;
                    }
                    if(car == 32){
                        n.InsertaFin(Integer.parseInt(numero));
                        numero = "";
                        continue;
                    }
                    numero = numero + (char)car;
                    continue;
                }
                
                if((car != 13)){
                    if((car == 10) || car == -1){
                        m.InsertaFin(mensaje);
                        mensaje = "";
                        continue;
                    }
                    mensaje = mensaje + (char)car;
                    continue;
                }
            }
            res = new Resultado(n, m);
            System.out.println();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    public Resultado recuperaResultado(){
        return res;
    }
}
