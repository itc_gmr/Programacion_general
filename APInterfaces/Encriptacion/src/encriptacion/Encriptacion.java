
package encriptacion;
import Componentes.*;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
/**
 *
 * @author Guillermo Marquez Ru
 */
public class Encriptacion {

  
    public static void main(String[] args) {
        String mnjs[] = new String[3];
        int nmrs[] = new int[3];
        int cont = 0, cont2 = 0;
        capturaDatos cd = new capturaDatos();
        Resultado r = cd.recuperaResultado();
        NodoDBL<Integer> auxn = r.numeros.DameFrente();
        NodoDBL<String> auxm = r.mensajes.DameFrente();
        while(auxn != null){
            System.out.print(auxn.Info + " ");
            nmrs[cont2] = auxn.Info;
            cont2++;
            auxn = auxn.DameSig();
        }
        System.out.println();
        while(auxm != null){
            System.out.println(auxm.Info);
            mnjs[cont] = auxm.Info;
            cont++;
            auxm = auxm.DameSig();
        }
        
        String frase = mensajeIgual(mnjs, 0), frase2 = mensajeIgual(mnjs, 1);
        frase = purifica(frase);
        frase2 = purifica(frase2);
        
        frase = purifica(frase, mnjs, 0);
        frase2 = purifica(frase2, mnjs, 1);
        
         PrintWriter salida = null;
        try{
           salida = new PrintWriter("salida.txt");
           String res = "";
           if(frase.equals(mnjs[0])){
               res =  "Si";
           }else{
                res = "No";
           }
           salida.println(res);
           if(frase2.equals(mnjs[1])){
               res = "Si";
           }else{
               res = "No";
           }
           salida.println(res);
           salida.flush();
       }catch(FileNotFoundException e){
           System.out.println(e.getMessage());
       }finally{
           salida.close();
       }
        
        
    }
    
    public static String mensajeIgual(String m[], int n){
        char ch, ch2, aca;
        String frase = "";
        boolean band[] = new boolean[m[n].length()];
        for(int i = 0; i < band.length; i++){
            band[i] = false;
        }
        for(int i = 0; i < m[n].length(); i++){
            ch = m[n].charAt(i);
            for(int j = 0; j < m[2].length(); j++){
                ch2 = m[2].charAt(j);
                if(ch == ch2){
                   if(j+1 < m[2].length() && i+1 < m[n].length()){
                        if(ch == m[2].charAt(j+1) ){ 
                            continue;
                        }
                        if(m[n].charAt(i+1) == m[2].charAt(j+1)){
                            frase = frase + ch;
                            continue;
                        }    
                    }
                    aca = m[n].charAt(m[n].length()-1);
                    if(aca == ch){
                        frase = frase + ch;
                    }
                }
            }
        }
       
        return frase;
    }
    
    public static String purifica(String mensaje){
        System.out.println(mensaje);
        String nvo = mensaje.charAt(0) + "";
        char car;
        for(int i = 1; i < mensaje.length(); i++){
            nvo = nvo + mensaje.charAt(i);
            car = mensaje.charAt(i);
            if(nvo.charAt(nvo.length()-2) == car){
                nvo = nvo.substring(0, nvo.length()-1);
            }
        }
        
       
        return nvo;
    }
    
    public static String purifica(String mensaje, String m[], int n){
        String nvo = mensaje.charAt(0) + "";
        char car;
        for(int i = 1; i < mensaje.length(); i++){
            if(mensaje.charAt(i) == m[n].charAt(i)){
                nvo = nvo + mensaje.charAt(i);
                continue;
            }
            
            nvo = nvo + m[n].charAt(i);
            mensaje = mensaje.substring(0, i) + m[n].charAt(i) + mensaje.substring(i, mensaje.length());
        }
        System.out.println(nvo);
        return nvo;
    }
    
}
