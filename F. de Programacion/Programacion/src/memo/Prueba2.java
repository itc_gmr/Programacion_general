package memo;
import java.util.*;
public class Prueba2 {

	public static void main(String[] args) 
	{
		double n;
		Scanner leer = new Scanner(System.in);
		do
		{
			System.out.print("Teclea un numero entero >=0: ");
			n = leer.nextInt();
			System.out.println(n + "! = " + fact(n));
			
		}while(n<0);

	}
	public static double fact(double n) 
	{
		double fact = 1;
		if (n == 0 || n == 1)
		{
			return fact;
		}
		else
		{
			for (double i = n; i > 0; i--)
			{
				fact = fact * i;
			}
			return fact;
		}
	}
	
}
