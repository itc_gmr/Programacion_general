/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;

import Componentes.*;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
/**
 *
 * @author guill
 */
public class Examen2 {

    final static int giros = 200;
    public static void main(String[] args) {
        int ruleta[] = new int[8];
        int j = 0;
        double nA;
        System.out.println("Simulación de giro de una ruleta");
        
        for(int i = 0;i < giros; i++){
            j = 0;
            nA = numeroAleatorio();
            if(nA >= 0 && nA <=0.125){
                ruleta[j]++;
            }
            j++;
            if(nA > 0.125 && nA <=0.25){
                ruleta[j]++;
            }
            j++;
            if(nA > 0.25 && nA <=0.375){
                ruleta[j]++;
            }
            j++;
            if(nA > 0.375 && nA <=0.5){
                ruleta[j]++;
            }
            j++;
            if(nA > 0.5 && nA <=0.625){
                ruleta[j]++;
            }
            j++;
            if(nA > 0.625 && nA <=0.75){
                ruleta[j]++;
            }
            j++;
            if(nA > 0.75 && nA <=0.875){
                ruleta[j]++;
            }
            j++;
            if(nA > 0.875 && nA <= 1.0){
                ruleta[j]++;
            }
            
        }
        
        DefaultPieDataset datosPastel = new DefaultPieDataset();
        for(int i = 0; i < ruleta.length; i++){
            System.out.println(ruleta[i] + " " + (double)((ruleta[i]*200)/(100)));
            datosPastel.setValue(" Giro "+ ruleta[i] + " \n" +(double)((ruleta[i]*giros)/(100)) + "%", ruleta[i]);
        }
        JFreeChart fc = ChartFactory.createPieChart("Dardos", datosPastel);
        ChartPanel cp = new ChartPanel(fc);
        JFrame fr = new JFrame("Grafico");
        fr.setSize(500, 300);
        fr.add(cp);
        fr.setVisible(true);
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado=(resultado-parteEntera)*Math.pow(10, numeroDecimales);
        resultado=Math.round(resultado);
        resultado=(resultado/Math.pow(10, numeroDecimales))+parteEntera;
        return resultado;
    }
    
    public static double numeroAleatorio(){
        return redondearDecimales((MyRandom.nextInt(0, 1000)*.001),5);
    }
    
}
