/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package liscencias;

import Componentes.MyRandom;
import java.util.Arrays;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author guill
 */
public class Liscencias {
    
   private static int N;
   private static int lC;
   private static int moda[];
   public static String nameModa;
   //private static Moda m[];
   
    public static DefaultTableModel simular(int n, int l){
        N = n;
        lC = l;
        double nA = 0.0;
        int lV = 0, lD = 0;// Liscencia vendida
        
        Object col[] = {"Número aleatorio","Liscencias vendidas","Liscencias devueltas","Costo","Ingreso de venta","Ingreso por devolución","Utilidad"};
        Object datos[][] = new Object[n][col.length];
        moda = new int [5];
        
        for(int i = 0; i< datos.length;i++){
            nA = numeroAleatorio();
            
            if(nA >= 0 && nA <= 0.3){
                lV = 100;
                if(lV > lC){
                    lV = lC;
                }
                lD = lC - lV;
                moda[0]++;
            }
            if(nA > 0.3 && nA <= 0.5){
                lV = 150;
                if(lV > lC){
                    lV = lC;
                }
                lD = lC - lV;
                moda[1]++;
            }
            if(nA > 0.5 && nA <= 0.8){
                lV = 200;
                if(lV > lC){
                    lV = lC;
                }
                lD = lC - lV;
                moda[2]++;
            }
            if(nA > 0.8 && nA <= 0.95){
                lV = 250;
                if(lV > lC){
                    lV = lC;
                }
                lD = lC - lV;
                moda[3]++;
            }
            if(nA > 0.95 && nA <= 1.00){
                lV = 300;
               if(lV > lC){
                    lV = lC;
                }
                lD = lC - lV;
                moda[4]++;
            }
            for(int j = 0; j < datos[i].length;j++){
                datos[i][j++] = nA;
                datos[i][j++] = lV;
                datos[i][j++] = lD;
                double costo = (75*lC);
                datos[i][j++] = costo;
                double ingVenta = (100*lV);
                datos[i][j++] = ingVenta;
                double ingDev = (25*lD);
                datos[i][j++] =  ingDev;
                double utilidad = (ingVenta + ingDev) - costo;
                datos[i][j] = utilidad;
            }
        }
        return new DefaultTableModel(datos,col);
    }
    
    public static int getModa(){
        int aca = 0;
        
        for(int i = 0;i < moda.length; i++){
            if(moda[i] > aca){
                aca = moda[i];
                switch(i){
                    case 0:
                        nameModa = "Número 100";
                        break;
                    case 1:
                        nameModa = "Número 150";
                        break;
                    case 2:
                        nameModa = "Número 200";
                        break;
                    case 3:
                        nameModa = "Número 250";
                        break;
                    case 4:
                        nameModa = "Número 300";
                        break;
                }
            }
            
        }
        return aca;
    }
    
    public static String getNameModa(){
        return nameModa;
    }
    
   public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado=(resultado-parteEntera)*Math.pow(10, numeroDecimales);
        resultado=Math.round(resultado);
        resultado=(resultado/Math.pow(10, numeroDecimales))+parteEntera;
        return resultado;
    }
    
    public static double numeroAleatorio(){
        return redondearDecimales((MyRandom.nextInt(0, 1000)*.001),5);
    } 
}

