package informacion;
import java.awt.Image;
import Componentes.MyRandom;
import javax.swing.JButton;
/**
 *
 * @author guill
 */
public class carta {
    private String id;
    private tipoCarta tipo;
    private int numero;
    public boolean disponible;
    private JButton imagen;
    
    public carta(tipoCarta tipo,int numero){
        this.tipo = tipo;
        this.numero = numero;
        disponible = true;
        id = MyRandom.PonCeros(numero, 3) + MyRandom.PonCeros(tipo.getValor(), 2);
    }
    
    public carta(String tipo,int numero,JButton i){
        this.tipo.nombre = tipo;
        this.numero = numero;
        disponible = true;
        id = tipo+numero;
        imagen = i;
    }
    public String getID(){
        return id;
    }
    public tipoCarta getTipo(){
        return tipo;
    }
    public int getNumero(){
        return numero;
    }
    
    public JButton getB(){
        return imagen;
    }
    
    
    public String toString(){
        return MyRandom.PonBlancos(id, 5);
    }
    
}
