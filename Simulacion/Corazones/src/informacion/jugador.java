/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package informacion;
import Componentes.*;
/**
 *
 * @author guill
 */
public class jugador {
    public ListaDBL<carta> mano;
    public carta seleccion;
    public int victorias;
    private String nombre;
    
    
    public jugador(String nombre){
        this.nombre = nombre;
        mano = new ListaDBL<>();
        victorias = 0;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public String toString(){
        return MyRandom.PonBlancos(nombre, 20);
    }
}
