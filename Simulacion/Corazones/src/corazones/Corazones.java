/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corazones;
import informacion.*;
import Componentes.*;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;

/**
 *
 * @author guill
 */
public class Corazones {
   
    public ListaDBL<carta> baraja;
    jugador[] jugadores;
    Partida p;
    int iteraciones;
    ListaDBL<jugador> ganador;
    Grafica grafica;
    
//    public Corazones(){
//        inicializaBaraja();
//        repartirBaraja();
//    }
    
    public Corazones(int it){
        iteraciones = it;
        grafica = new Grafica();
        
        empezarPartida();
    }

    private void empezarPartida(){
       ganador = new ListaDBL<>();
        for(int i = 0; i < iteraciones;i++){
            inicializaBaraja();
            repartirBaraja();
            p = new Partida(jugadores,i);
            ganador = p.starter();
            NodoDBL<jugador> aux = ganador.DameFrente();
            while(aux != null){
                grafica.ganadores.InsertaFin(aux.Info);
                System.out.println("Ganador " + aux.Info.getNombre());
                aux = aux.DameSig();
            }
        }
        NodoDBL<jugador> aux = grafica.ganadores.DameFrente();
        while(aux != null){
            System.out.println("Ganador " + aux.Info.getNombre());
            aux = aux.DameSig();
        }
        grafica.setJugadores();
    }
    
    private void inicializaBaraja() { //Inicializa la baraja
        System.out.println("-----Barajeando-----");
        baraja = new ListaDBL<>();
        carta dato;
        double nT,nA;
        for(int i = 0; i < 40; i++){
            nT = Soporte.numeroAleatorio();
            nA = Soporte.numeroAleatorio();
            dato = Soporte.seleccionaCarta(nT,nA);
            if(baraja.Busca(dato)){
                i--;
                continue;
            }
            baraja.InsertaFin(dato);
        }
    }
    
    private void repartirBaraja() {//Repartir baraja a jugadores
        jugadores = new jugador[4];
        for(int i = 0; i < jugadores.length; i++){
            jugadores[i] = new jugador("jugador" + (i+1));
            repartirMano(jugadores[i]);
        }
    }
    
    private void repartirMano(jugador j){//
        double nT,nA;
        carta dato;
        System.out.println("Reaprtir mano de: " + j.getNombre());
        for(int i = 0; i < 10; i++){
            nT = Soporte.numeroAleatorio();
            nA = Soporte.numeroAleatorio();
            dato = Soporte.seleccionaCarta(nT,nA);
            if(!baraja.Retira(dato)){
                i--;
                continue;
            }
            j.mano.InsertaFin(dato);
        }
    }
    
    public void graficarPas(JPanel panCentro){
        grafica.crearGraficaPastel();
        panCentro.removeAll();
        ChartPanel cp = null;
        cp = new ChartPanel(grafica.crearGraficaPastel());
        cp.setSize(panCentro.getWidth(), panCentro.getHeight());
        panCentro.add(cp);
        panCentro.updateUI();
    }
    
    public void graficaBar(JPanel panCentro){
        grafica.crearGraficoBarras();
        panCentro.removeAll();
        ChartPanel cp = null;
        cp = new ChartPanel(grafica.crearGraficoBarras());
        cp.setSize(panCentro.getWidth(), panCentro.getHeight());
        panCentro.add(cp);
        panCentro.updateUI();
    }
        
}
