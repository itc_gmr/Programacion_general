/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corazones;

import Componentes.*;
import informacion.jugador;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author guill
 */
public class Grafica {
    ListaDBL<jugador> ganadores = new ListaDBL<>();
    int[] jugadores;
    DefaultPieDataset datosPastel = new DefaultPieDataset();
    int it;
    public Grafica(){
        datosPastel = new  DefaultPieDataset();
        jugadores = new int[4];
        it = 0;
    }
    
    public void setJugadores(){
        NodoDBL<jugador> aux = ganadores.DameFrente();
        while(aux != null){
            System.out.println(aux.Info.getNombre());
            if(aux.Info.toString().equals(MyRandom.PonBlancos("jugador1", 20))){
                jugadores[0]++;
            }
            if(aux.Info.toString().equals(MyRandom.PonBlancos("jugador2", 20))){
                jugadores[1]++;
            }
            if(aux.Info.toString().equals(MyRandom.PonBlancos("jugador3", 20))){
                jugadores[2]++;
            }
            if(aux.Info.toString().equals(MyRandom.PonBlancos("jugador4", 20))){
                jugadores[3]++;
            }
            it++;
            aux = aux.DameSig();
        }
    }
    public JFreeChart crearGraficaPastel(){
        for(int i = 0; i < jugadores.length; i++){
            datosPastel.setValue("Jugador "+(i+1) + " " + (double)((jugadores[i]*100)/(it)) + "%", jugadores[i]);
        }
        return ChartFactory.createPieChart("Gráfico de jugadores", datosPastel, datosPastel, 0, true, true, false, false, false, false);
    }
    
    public JFreeChart crearGraficoBarras(){
        DefaultCategoryDataset datos = new DefaultCategoryDataset();
         for(int i = 0; i < jugadores.length; i++){
             datos.setValue(jugadores[i]," Jugador "+(i+1)," Jugador "+(i+1));
         }
         return ChartFactory.createBarChart("Gráfica de jugadores", "Jugadores", "Frecuencia", datos, PlotOrientation.VERTICAL, true, true, false);
    }
    
}
