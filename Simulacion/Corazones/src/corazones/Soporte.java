/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corazones;

import Componentes.MyRandom;
import informacion.*;

/**
 *
 * @author guill
 */
public class Soporte {
    
    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado=(resultado-parteEntera)*Math.pow(10, numeroDecimales);
        resultado=Math.round(resultado);
        resultado=(resultado/Math.pow(10, numeroDecimales))+parteEntera;
        return resultado;
    }
    
    public static double numeroAleatorio(){
        return redondearDecimales((MyRandom.nextInt(0, 1000)*.001),5);
    }

    public static carta seleccionaCarta(double nT,double nA){
        carta dato;
        tipoCarta nombre = new tipoCarta();
        int numero = 0;
        if(nT >= 0 && nT <= 0.25){
            nombre = new tipoCarta("oro",4);
        }
        if(nT > 0.25 && nT <= 0.5){
            nombre = new tipoCarta("copa",3);
        }
        if(nT > 0.5 && nT <= 0.75){
            nombre = new tipoCarta("espada",2);
        }
        if(nT > 0.75 && nT <= 1.0){
            nombre = new tipoCarta("basto",1);
        }
        if(nA >= 0 && nA <= 0.1){
            numero = 1;
        }
        if(nA > 0.1 && nA <= 0.2){
            numero = 2;
        }
        if(nA > 0.2 && nA <= 0.3){
            numero = 3;
        }
        if(nA > 0.3 && nA <= 0.4){
            numero = 4;
        }
        if(nA > 0.4 && nA <= 0.5){
            numero = 5;
        }
        if(nA > 0.5 && nA <= 0.6){
            numero = 6;
        }
        if(nA > 0.6 && nA <= 0.7){
            numero = 7;
        }
        if(nA > 0.7 && nA <= 0.8){
            numero = 10;
        }
        if(nA > 0.8 && nA <= 0.9){
            numero = 11;
        }
        if(nA > 0.9 && nA <= 1.0){
            numero = 12;
        } 
        dato = new carta(nombre,numero);
        //System.out.println("Se seleccionó la carta: " + dato.getID());
        return dato;
    }
    
}
