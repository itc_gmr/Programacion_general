/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package montecarlo;
import Componentes.MyRandom;
import org.jfree.data.category.DefaultCategoryDataset;
/**
 *
 * @author guill
 */
public class MonteCarlo {
    static private int contS,contA;
 
    public static Object[][] calculaMonteCarlo(int N){
        Object table[][] = new Object[N][3];//Tabla de valores
        Double nA;//Número aleatorio
        String nombre;//Nombre de moneda
        int j = 0;//Numero de iteraciones por columna
        contS = 0;//Contador sello
        contA = 0;//Contador aguila
        for (int i = 0; i < table.length;i++){
            j = 0;
            nA = redondearDecimales((MyRandom.nextInt(0, 1000)*.001),5);
           
            if(nA > 0.5){
                nombre = "Sello";
                contS++;
                table[i][j++] = i+1;
                table[i][j++] = nA;
                table[i][j++] = nombre;
                continue;
            }
            nombre = "Aguila";
            contA++;
            table[i][j++] = i+1;
            table[i][j++] = nA;
            table[i][j++] = nombre;
        }
         System.out.println("Done");
        return table;
    }
    
    public static void graficarMonteCarloBarras(DefaultCategoryDataset datos){
        datos.addValue(contS, "Sello", "Sello");
        datos.addValue(contA, "Águila", "Águila");
    }
    
    public static double getPorcentajeSello(){
        return (double)((contS*100)/(contA+contS));
    }
    
    public static double getPorcentajeAguila(){
        return (double)((contA*100)/(contA+contS));
    }
    
    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado=(resultado-parteEntera)*Math.pow(10, numeroDecimales);
        resultado=Math.round(resultado);
        resultado=(resultado/Math.pow(10, numeroDecimales))+parteEntera;
        return resultado;
    }
   
}
