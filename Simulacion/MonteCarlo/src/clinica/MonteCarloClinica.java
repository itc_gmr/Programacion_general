/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clinica;

import Componentes.MyRandom;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author guill
 */
public class MonteCarloClinica {
    private static int[] consultas;
    private static int N = 0;
    
    public static DefaultTableModel crearTabla(int n){
        N = n;
        consultas = new int[6];
        Object [] columnas = {"Iteración","Número generado","Días consultas"};
        Object[][] datos = new Object[n][columnas.length];
        int j = 0, contCon = 0;
        Double nA;//Número aleatorio
        for(int i = 0; i < datos.length;i++){
            j = 0;
            contCon = 0;
            nA = redondearDecimales((MyRandom.nextInt(0, 1000)*.001),5);
            datos[i][j++] = i+1;
            datos[i][j++] = nA;
            if(nA >= 0 && nA <=0.05){
                datos[i][j] = "0 Consultas";
                consultas[contCon]++;
                continue;
            }
            contCon++;
            if(nA > 0.05 && nA <= 0.15){
                datos[i][j] = "1 Consultas";
                consultas[contCon]++;
                continue;
            }
            contCon++;
            if(nA > 0.15 && nA <= 0.35){
                datos[i][j] = "2 Consultas";
                consultas[contCon]++;
                continue;
            }
            contCon++;
            if(nA > 0.35 && nA <= 0.65){
                datos[i][j] = "3 Consultas";
                consultas[contCon]++;
                continue;
            }
            contCon++;
            if(nA > 0.65 && nA <= 0.85){
                datos[i][j] = "4 Consultas";
                consultas[contCon]++;
                continue;
            }
            contCon++;
            datos[i][j] = "5 Consultas";
            consultas[contCon]++;
        }
        DefaultTableModel dtm = new DefaultTableModel(datos,columnas);
        return dtm;
    }
    
    public static JFreeChart crearGraficaPastel(){
        DefaultPieDataset datos = new DefaultPieDataset();
        for(int i = 0; i < consultas.length; i++){
            datos.setValue(i+" Consultas "+(double)((consultas[i]*100)/(N)) + "%", consultas[i]);
        }
        return ChartFactory.createPieChart("Gráfico de días", datos, datos, 0, true, true, false, false, false, false);
    }
    
    public static JFreeChart crearGraficoBarras(){
        DefaultCategoryDataset datos = new DefaultCategoryDataset();
         for(int i = 0; i < consultas.length; i++){
             datos.setValue(consultas[i],i+" Consultas",i+" Consultas");
         }
         return ChartFactory.createBarChart("Gráfica de días", "Posibilidades", "Frecuencia", datos, PlotOrientation.VERTICAL, true, true, false);
    }
    
    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado=(resultado-parteEntera)*Math.pow(10, numeroDecimales);
        resultado=Math.round(resultado);
        resultado=(resultado/Math.pow(10, numeroDecimales))+parteEntera;
        return resultado;
    }
}
