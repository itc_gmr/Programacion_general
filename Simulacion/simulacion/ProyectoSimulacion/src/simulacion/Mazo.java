package simulacion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

public class Mazo extends ArrayList<Cartas>
{
    public void mix()
    {
        long seed = System.nanoTime();
        Collections.shuffle(this, new Random(seed)); 
    }
    
    public void imprime()
    {
        Iterator en = this.iterator();
        while (en.hasNext()) 
        {
            Cartas nextElement = (Cartas)en.next();
            System.out.println("Carta "+nextElement.nombre+nextElement.valor);            
        }
        System.err.println("\n\n\n\n");
    }
 }



