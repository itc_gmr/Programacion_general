package simulacion;
import javax.swing.*;
import java.util.*;

public class Juego extends JFrame 
{   
    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);    
        new Juego();
    }    
    
    public Juego()
    {
        hazInterfaz();
    }

    private void hazInterfaz() 
    {
        setSize(1160, 760);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo( null );
        getContentPane().add(new VentanaGrafica());
        setVisible(true);       
    }   	
}

