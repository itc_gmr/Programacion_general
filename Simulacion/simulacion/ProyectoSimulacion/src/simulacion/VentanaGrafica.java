package simulacion;
import java.awt.*;
import java.awt.event.*;
import java.util.Iterator;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.Timer;

class VentanaGrafica extends Canvas implements ActionListener
{ 
	  int turno=10; 
	  int juegos =0;
	  int Cont=100;
	  Timer timer;
	  Graphics2D g2d;
	  
	  public static Mazo mazo,jugadorUno,jugadorDos,jugadorTres,jugadorCuatro;
	  public int conParJu=0,conParJd=0,conParJt=0,conParJc=0;
	  
	  
	  public VentanaGrafica()
	  {
		  mazo = new Mazo();
		  jugadorUno = new Mazo();
		  jugadorDos = new Mazo();
		  jugadorTres = new Mazo();
		  jugadorCuatro = new Mazo();
	        
		  partida();
		  
		  System.err.println("Inicia partida");
		
		  timer = new Timer (10, this) ;
		  timer.setRepeats(true);
		  timer.start();
	
		  addMouseListener(new MouseListener() 
		  {
			  	@Override
				public void mouseClicked(MouseEvent e) 
			  	{
			
			  		System.err.println();
			  	}

			  	@Override
			  	public void mousePressed(MouseEvent e) 
			  	{
			  		if(e.getX()>430 && e.getX()<730 && e.getY()>340 && e.getY()<420 )
			  		{
			  			partida();
			  			turno=10;
			  			timer.start();
			  		}
			  	}

			  	@Override
			  	public void mouseReleased(MouseEvent e) 
			  	{
			  		System.err.println();
			  	}

			  	@Override
			  	public void mouseEntered(MouseEvent e) 
			  	{
			  		System.err.println("");
			  	}

			  	@Override
			  	public void mouseExited(MouseEvent e) 
			  	{
			  		System.err.println("");
			  	}
		  	});
	  	};
	  
	  
	  	@Override
	  	public void actionPerformed(ActionEvent e) 
	  	{      
	  		Cont--;
	  		if(Cont<0)
	  		{
	  			timer.stop();
	  			repaint();
	  			Cont = 100;
	  			timer.start();
	  			turno--;
	  			System.err.println("Sugue el jugador numero :"+turno);
	  		}
	  		
	  		if(turno<0)
	  		{
	  			timer.stop();
	  		}
		
	  		if(turno == 0 && juegos !=0)
	  		{
	  			juegos--;
	  			partida();
	  			turno=60;
	  			timer.start();
	  		}
		
	  		if(turno==0 && juegos ==0)
	  		{
	  			System.out.println("El juego a acabado");
	            System.err.println("\t\t\tRESULTADOS");
	            System.out.println("\n\n\t Jugador Uno "+conParJu
	                                 +"\n\t Jugador Dos "+conParJd
	                                 +"\n\t Jugador Tres "+conParJt
	                                 +"\n\t Jugador Cuatro "+conParJc);
	            timer.stop();
	            JOptionPane.showMessageDialog(null,"Para Iniciar una Nueva Partida haz Click en Repartir de Nuevo", "El Juego a Acabado", 1);
	        }		
	  	}
	 
	  	public void paint(Graphics g)
	  	{  
	  		Color colorVerde = new Color (0,180,0);
	  		
	  		g2d = (Graphics2D) g;
	  		g2d.setColor(colorVerde);
	  		g2d.fillRect(0,0,getWidth(),getHeight());
	  		g2d.setColor(Color.GRAY);
	  		g2d.fillRect(430,320,300,80);
	  		g2d.setColor(Color.BLUE);
	  		g2d.fillRect(480,10,200,60);
	        g2d.setColor(Color.RED);
	        g2d.fillRect(830,195,200,60);
	        g2d.setColor(Color.ORANGE);
	        g2d.fillRect(480,650,200,60);
	        g2d.setColor(Color.MAGENTA);
	        g2d.fillRect(100,195,200,60);
		
	        if(turno>0)dibuja(g2d);
	        	monedas(g2d,conParJu,1);
	        	monedas(g2d,conParJd,2);
	        	monedas(g2d,conParJt,3);
	        	monedas(g2d,conParJc,4);
	  	}

	  	public Color colorRandom()
	  	{
	  		Random r = new Random();
	  		return new Color(r.nextFloat(), r.nextFloat(), r.nextFloat());
	  	}
	 
	  	public synchronized void reparte() 
	  	{
	  		Iterator en = mazo.iterator();
	  		Cartas aux;
	  		while (en.hasNext()) 
	  		{
	  			aux = (Cartas)en.next();
	  			aux.jugador=1;
	  			jugadorUno.add(aux);
	  			aux = (Cartas)en.next();
	  			aux.jugador=2;
	  			jugadorDos.add(aux);
	  			aux = (Cartas)en.next();
	  			aux.jugador=3;
	  			jugadorTres.add(aux);
	  			aux = (Cartas)en.next();
	  			aux.jugador=4;
	  			jugadorCuatro.add(aux);
	  		}
	  		mazo.removeAll(mazo);
	  	}
	 
	  	private void partida() 
	  	{    
	  		lee("bastos_",7);
	  		lee("copas_",6);
	  		lee("oros_",5);
	  		lee("espadas_",8);
	  		System.out.print(mazo.size());
	  		mazo.mix();
	  		reparte();
	  	}

	  	private void lee(String aux, int l) 
	  	{
	  		for (int i = 1; i < 11; i++) 
	  		{
	  			aux = aux+i+"s";
	  			aux=aux.substring(0, l);
	  			Cartas cartaAux =new Cartas(aux, i,0);
	  			mazo.add(cartaAux);
	  		}     
	  		System.out.println("Se estan cargando los graficos");
	  	}

	  	private void dibuja(Graphics2D g2d) 
	  	{
	  		System.out.println("Numero de cartas en mazo "+mazo.size());
	  		Cartas imprime;
	  		Cartas tablero[] = new Cartas[4];
		
	  		imprime = jugadorUno.remove(0);
	  		ImageIcon ImagenFondo=new ImageIcon( imprime.nombre+imprime.valor+"s.jpg" );
	  		g2d.drawImage(ImagenFondo.getImage(),490,90,180,210,null);
	        g2d.fillOval(Cont, Cont, Cont, juegos);
	        tablero[0]=imprime;
		
	        imprime = jugadorDos.remove(0);
	        ImagenFondo=new ImageIcon(imprime.nombre+imprime.valor+"s.jpg" );
	        g2d.drawImage(ImagenFondo.getImage(),840,275,180,210,null);
	        tablero[1]=imprime;
		
	        imprime = jugadorTres.remove(0);
	        ImagenFondo=new ImageIcon(imprime.nombre+imprime.valor+"s.jpg" );
	        g2d.drawImage(ImagenFondo.getImage(),490,420,180,210,null);
	        tablero[2]=imprime;
		
	        imprime = jugadorCuatro.remove(0);
	        ImagenFondo=new ImageIcon(imprime.nombre+imprime.valor+"s.jpg" );
	        g2d.drawImage(ImagenFondo.getImage(),110,275,180,210,null);
	        tablero[3]=imprime;

	        compara(tablero);		
	  	}

	    private void compara(Cartas[] tablero) 
	    {     
	    	for (Cartas tablero1 : tablero) 
	    	{
	    		System.out.println(tablero1.nombre+tablero1.valor);
		  
	    	}
	    	System.out.println("____________________________________________");
	    	
	    	boolean swapped = true;
	    	int j = 0;
	    	Cartas tmp;
	 
	    	while (swapped) 
	    	{
	    		swapped = false;
	    		j++;
	    		for (int i = 0; i < tablero.length - j; i++) 
	    		{                                       
	    			if (tablero[i].valor > tablero[i + 1].valor) 
	    			{  
	    				if (tablero[i].tipo > tablero[i + 1].tipo && tablero[i].valor < tablero[i + 1].valor) 
	    				{ 
	    					continue;
	    				}
	    				tmp = tablero[i];
	    				tablero[i] = tablero[i + 1];
	    				tablero[i + 1] = tmp;
	    				swapped = true; 
	    			}
	    		}                
	    	}
	 
	    	for (Cartas tablero1 : tablero) 
	    	{
	    		System.out.println(tablero1.nombre+tablero1.valor); 
	    	}
	        
	        marcador(tablero[3].jugador);	    
	    }

	    private void marcador(int jugador) 
	    {        
	    	System.out.println("Gano el jugador "+jugador);
	        switch (jugador)
	        {
	            case 1:
	                conParJu++;
	                ;break;
	                
	            case 2:
	                conParJd++;
	                ;break;
	            case 3:
	                conParJt++;
	                ;break;
	            case 4:
	                conParJc++;
	                ;break;                 
	        }

	    }

	    private void monedas(Graphics2D g2d, int conParJ, int i) 
	    {    
	        Font font = new Font("Monaco", Font.BOLD, 28);
	        g2d.setFont(font);

	        g2d.setColor(Color.BLACK);
	        g2d.drawString("Repartir de Nuevo", 460  , 370);
	        
	        g2d.setColor(Color.BLACK);
	    
	        g2d.drawString("Jugador 1 = "+conParJu, 490  , 45);
	        g2d.drawString("Jugador 2 = "+conParJd, 840  , 230);
	        g2d.drawString("Jugador 3 = "+conParJt, 490  , 685);
	        g2d.drawString("Jugador 4 = "+conParJc, 110  , 230);
	        
	        if(turno==0)
	        {
	        	ImageIcon ImagenFondo=new ImageIcon("cero.jpg" );
	        	g2d.drawImage(ImagenFondo.getImage(),490,90,180,210,null); 
	        	g2d.drawImage(ImagenFondo.getImage(),840,275,180,210,null);  
	        	g2d.drawImage(ImagenFondo.getImage(),490,420,180,210,null);  
	        	g2d.drawImage(ImagenFondo.getImage(),110,275,180,210,null); 
	        }
	    }
	}


