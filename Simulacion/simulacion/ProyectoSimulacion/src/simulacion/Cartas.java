package simulacion;

public class Cartas
{
    public String nombre;
    public int valor;
    public int tipo;
    public int jugador;
    
    public Cartas (String n,int val,int j)
    {
        nombre = n;
        valor= val;
        jugador =j;
        if(nombre.compareTo("oros_")==0)tipo=4;
        if(nombre.compareTo("copas_")==0)tipo=3;
        if(nombre.compareTo("espadas_")==0)tipo=2;
        if(nombre.compareTo("bastos_")==0)tipo=1;
    }
    
}