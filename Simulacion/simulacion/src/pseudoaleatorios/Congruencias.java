/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pseudoaleatorios;

import java.util.*;
import Componentes.MyRandom;
public class Congruencias {
    
    public static void main(String args[]){
        int a, c, m = MyRandom.nextInt(0,1000), X0 = MyRandom.nextInt(0, 1000),cont = 0, X1;
        Scanner leer = new Scanner(System.in);
        HashSet<Integer> set = new HashSet<>();
        System.out.println("Teclee el multiplicador");
        a = leer.nextInt();
        if((a%2 == 0)||(a%3 == 0) || (a%5) == 0){//2K + 1
            return;
        }
        System.out.println("Teclee el incremento");
        c = leer.nextInt();
        if((c%2 == 0)){
            return;
        }
        System.out.println("-----Metodo de Congruencias-----");
        X1 = generador(X0,a,c,m);
        System.out.println(++cont + "\t" + X0 + "\t" + ((a*X0)+c) + "\t" +((a*X0)+c)+"/"+m + "\t" + X1);
        while (set.add(X1)){
            System.out.print(++cont + "\t" + X1 + "\t" + ((a*X1)+c) + "\t" +((a*X1)+c)+"/"+m + "\t" );
            X1 = generador(X1,a,c,m);
            System.out.print(X1);
            System.out.println();
        }
    }
    
    public static int generador(int X0, int a, int c, int m){
        int X1;
        X1 = ((a*X0)+c)%m;
        return X1;
    }
    
}
