
package pseudoaleatorios;
import java.util.*;
import java.util.HashSet;
import Componentes.*;
/**
 *
 * @author Guillermo Marquez Ru
 */
public class pseudo {
     static Random R=new Random();
    public static void main(String a[]){
        int n = 0, se = 0;
        Scanner leer = new Scanner(System.in);
        long sem = 0;
        while(true){
            System.out.println(".:Selecciona el método a utilizar:.");
            System.out.println("1.-Generar número con MM2 manual");
            System.out.println("2.-Generar número con MM2 aleatorio");
            System.out.println("3.-Generar número con MN manual");
            System.out.println("4.-Generar número con MN aleatorio");
            System.out.println("0.-Salir");
            try{
                n = leer.nextInt();
            }catch(Exception e){System.out.println("Error con la lectura de datos");break;}
            switch(n){
                case 1:
                    while(se < 1000 || se > 9999){
                        System.out.println("Teclee un número de 4 dígitos:");
                        se = leer.nextInt();
                    }
                    MM2(se);
                    break;
                case 2:
                    se = MyRandom.nextInt(1000, 9999);
                    MM2(se);
                    break;
                case 3:
                    System.out.println("Teclee un número de 10 dígitos:");
                    sem = leer.nextLong();
                    MN(sem);
                    break;
                case 4:
                    String concatena = "";
                    for(int i = 0; i < 10; i++){
                        concatena = concatena + MyRandom.nextInt(0, 9);
                    }
                    sem = Long.parseLong(concatena);
                    MN(sem);
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Elige número valido");
                    break;
            }
        }
    }
    
    public static void MM2(int se){
        int cont = 0;
        HashSet<Integer> numero = new HashSet<>();
        int na = generaMM2(se);
        if(na == 0 || na == se)
            return;
        cont++;
        System.out.println("Semilla: " + se + "\n"+cont+".-Número generado: " + na);
        while(numero.add(na)){
            na = generaMM2(na);
            cont++;
            System.out.println(cont+".-Número generado: " + na);
            if(na == 0 || na == se)
                break;
        }
    }
    public static int generaMM2(int se){
        int na = (int)Math.pow(se, 2);
        String num = MyRandom.PonCeros(na, 8);
        num = num.substring(2, 6);
        na = Integer.parseInt(num);
        return na;
    }
    
    public static void MN(long se){
        int cont = 0;
        HashSet<Long> numero = new HashSet<>();
        long na = generaMN(se);
        if(na == 0 || na == se)
            return;
        cont++;
        System.out.println("Semilla: " + se + "\n"+cont+".-Número generado:" + na);
        while(numero.add(na)){
            na = generaMN(na);
            cont++;
            System.out.println(cont+".-Número generado:" + na);
            if(na == 0 || na == se)
                break;
        }
    }
    
    public static long generaMN(long se){
        long na = (long)Math.pow(se, 2);
        String num = Long.toString(na);
        if((num.length()%2) == 0){
            num = PonCeros(Long.parseLong(num),(num.length()+1));
        }
        int n = (num.length()/2)-2;
        num = num.substring(n, (n+5));
        na = Long.parseLong(num);
        return na;
    }
    
    public static String PonCeros(long Valor,int Tam){
        String Cad=Valor+"";
        while(Cad.length()<Tam)
            Cad="0"+Cad;
            return Cad;
    }
    
 
    
   
}
