import javax.swing.*;
import java.awt.*;
class FrameZonas extends JFrame {
   public FrameZonas() {
      super("**ZONAS**");
      setSize( 350, 150 );
      setLocation(500,300);
      JLabel Etiqueta1 = new JLabel("HOLA MUNDO NORTE",SwingConstants.CENTER);
      add(Etiqueta1,BorderLayout.NORTH);
      JLabel Etiqueta2 = new JLabel("*Hola MUNDO EAST*",SwingConstants.CENTER);
      add(Etiqueta2,BorderLayout.EAST);
      add(new JLabel("*Hola MUNDO WEST*",SwingConstants.CENTER),BorderLayout.WEST);
      add(new JLabel("*Hola MUNDO SOUTH*",SwingConstants.CENTER),BorderLayout.SOUTH);
      add(new JLabel("Hola",SwingConstants.CENTER));
      setVisible(true);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   }
   public static void main(String [] a) {
      new FrameZonas();

    }
}