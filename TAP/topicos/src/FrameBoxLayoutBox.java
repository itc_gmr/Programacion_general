import javax.swing.*;import java.awt.*;import java.awt.event.*;
class FrameBoxLayoutBox extends JFrame implements ActionListener{
	JButton Cancelar,Grabar;
	Box H,V;
	public FrameBoxLayoutBox() {
		super("**BOXlayoutbox**"); 
		HazInterfaz();
		HazEscucha();
	}
	public void HazInterfaz(){
		 setSize( 800, 500 );  
		 setLocation(100,300);
		 setLocationRelativeTo(null);
		 H=Box.createHorizontalBox();
         V=Box.createVerticalBox();
        JLabel Etiqueta1 = new JLabel("No.Control",SwingConstants.RIGHT);
        H.add(Etiqueta1);
        H.add(Box.createHorizontalStrut(10));
        JTextField NoControl=new JTextField(10);
        H.add(NoControl); JLabel Etiqueta2 = new JLabel("Nombre",SwingConstants.RIGHT);
        H.add(Box.createHorizontalStrut(10));
        H.add(Etiqueta2);         JTextField Nombre=new JTextField(10);
        H.add(Box.createHorizontalStrut(10));
        H.add(Nombre);
        Grabar=new JButton("Grabar");
        V.add(Box.createVerticalStrut(50)); 
        V.add(Grabar);
        V.add(Box.createVerticalStrut(10)); 
        Cancelar=new JButton("Cancelar"); V.add(Cancelar);
        
     for(int i=0;i<25;i++){
        V.add(Box.createVerticalStrut(10)); 
        V.add(new JButton("Limpiar"));
     }
     V.add(Box.createVerticalStrut(10));
     add(H,BorderLayout.NORTH); 
     add(V,BorderLayout.EAST);
		setVisible(true);	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		InputMap map = new InputMap();
		 
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), "pressed");
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), "released");
		 
		Cancelar.setInputMap(0, map);
		Grabar.setInputMap(0, map);
		
	}
	public void HazEscucha(){
		Cancelar.addActionListener(this);
		Grabar.addActionListener(this);
	}
	public void actionPerformed(ActionEvent E){
		if(E.getSource() == Cancelar){
			V.remove(Cancelar);
			H.add(Cancelar,3);
			// setSize( 801, 501 );
			// setVisible(true);
			/*repaint();
			H.update(H.getGraphics());
			V.update(V.getGraphics());
			*/
		//	H.paintAll(H.getGraphics());
		//	V.paintAll(V.getGraphics());
			paintAll(getGraphics());
			System.out.println("cancelar");
		}
		if(E.getSource()==Grabar){
			System.out.println("bot�n grabar con enter");
		}
	}
	public static void main(String [] a) {
         new FrameBoxLayoutBox();
         
	}
}