import java.awt.Color;
import java.util.*;
public class MyRandom {
	static Random R=new Random();
	public static int nextInt(int Num){
		return R.nextInt(Num);
	}
	public static int nextInt(int LimInf,int LimSup){
		return R.nextInt(  LimSup-LimInf+1)+LimInf;
	}
	
	
	static String [] Nom={"Alicia","Sofia","Juan","Arturo","Eliseo","Federico","Natalia"};
	static String [] Ap={"Lopez","Perez","Garcia","Hernandez","Rodriguez","Pe�a","Nieto"};
	public static String nextName(){
		return Nom[nextInt(Nom.length)]+" "+Ap[ nextInt(Ap.length)]+" "+Ap[nextInt(Ap.length)];   
		
	}
	public static Color nextColor(){
		Color c[] = {Color.BLACK, Color.BLUE, Color.CYAN, Color.GREEN, Color.ORANGE, Color.magenta, Color.RED, Color.GRAY, Color.PINK, Color.ORANGE, new Color(35, 100, 255), new Color(30, 200, 125)};
		return c[MyRandom.nextInt(c.length)];
	}
}
