package CombosDependientes;
import java.io.*;	
import java.util.*;
public class ArchivosAlumnos {
	static final int longitud=65,logindex=8;
	public static void main(String [] a ) throws IOException{ 
		File F=new File("Alumnos.dat");
		RandomAccessFile Arch_Alumnos= new RandomAccessFile( F,"rw" );
		File F1=new File("Index_Alumnos.dat");
		RandomAccessFile Arch_index= new RandomAccessFile( F1,"rw" );
		//captura(Arch_Alumnos,Arch_index);
		muestraalumnos(Arch_Alumnos);
		muestraindex(Arch_index);
		//System.out.println(Buscar(Arch_Alumnos,Arch_index));
		Arch_index.close();
		Arch_Alumnos.close();
	}
	public static String Buscar(RandomAccessFile Arch_Alumnos,RandomAccessFile Arch_Index) throws IOException{
		Arch_Alumnos.seek(0);
		Scanner leer=new Scanner (System.in);
		int buscar,Registro;
		int edad;
		String nombre,clave;
		char sexo;
		System.out.println("Ingrese el Numero de Cuenta a Buscar");
		buscar=leer.nextInt();
		Registro=BusquedaBinaria(buscar,Arch_Index);
		if (Registro==-1){
			return "El numero de cuenta no existe";
		}
		Arch_Alumnos.seek(Registro*longitud);
		nombre=Arch_Alumnos.readUTF();
		clave=Arch_Alumnos.readUTF();
		sexo=Arch_Alumnos.readChar();
		edad=Arch_Alumnos.readInt();
		
		return nombre+" "+clave+" "+sexo+" "+edad;
	}
	public static String Ajusta(String dato,int Ancho){
	   if(dato.length()>Ancho){
		   dato=dato.substring(0,Ancho);
		   return dato;
	   }
	   while (dato.length()<Ancho)
		   dato=dato+" ";
	       return dato;
	     }
	public static void captura(RandomAccessFile Arch_Alumnos,RandomAccessFile Arch_Index) throws IOException{
		Scanner leer=new Scanner (System.in);
	    Arch_Index.seek(Arch_Index.length());
		Arch_Alumnos.seek(Arch_Alumnos.length());
        int tam=(int)(Arch_Alumnos.length())/longitud;
		int Ncta=0;
        int NumReg=tam+1;
		String Nombre=null;
        String clave=null;
        char sexo=0;
        int edad=0;
        while(true){
        	System.out.print("Teclee el Numero de cuenta: ");
        	try{
        		Ncta=leer.nextInt();
        		if (Ncta<0){
        			System.out.println("El numero de cuenta debe de ser mayor a 0");
        			//captura(Arch_Alumnos,Arch_Index);
        			continue;
        		}
        	}
        	catch(Exception e){
        		System.out.println("El Numero de cuenta solo debe contener solo datos de tipo numerico");
        		//captura(Arch_Alumnos,Arch_Index);
        		continue;
        	}
        	if(Ncta==0){
        		break;
        	}
        	if (BusquedaBinaria(Ncta,Arch_Index)==-1){

        		System.out.println("Ingrese el Nombre completo del Alumno");
        		Nombre=new Scanner (System.in).next();
                Nombre=Ajusta(Nombre,50);
                System.out.println("Ingrese la clave de la carrera");
                clave=new Scanner (System.in).next();
                clave=Ajusta(clave,5);
                System.out.println("Ingrese el sexo del alumno M para masculino y F para femenino");
                sexo=(char)System.in.read();
                System.out.println("Ingrese la edad");
                try{
                	edad=leer.nextInt();
                }
                catch(Exception e){
               	 	System.out.println("la edad debe de ser un numero mayor a 0");
               	 	edad=leer.nextInt();
                }
                Arch_Index.writeInt(Ncta);
                Arch_Index.writeInt(NumReg);
                ordena(Arch_Index);
                Arch_Alumnos.writeUTF(Nombre);
                Arch_Alumnos.writeUTF(clave);
                Arch_Alumnos.writeChar(sexo);
                Arch_Alumnos.writeInt(edad);
                NumReg++;
    
        	}
        	else{ System.out.println("El numero de cuenta ya existe");
			continue;}
        	    		
	}
        
        System.out.println("Los Datos han sido registrados");
	}
	public static void ordena(RandomAccessFile Arch_Index) throws IOException{
		int cuenta_i,cuenta_j;
		int registro_i,registro_j;
		int TotalReg=(int)Arch_Index.length()/logindex;
		for(int i=0;i<TotalReg-1;i++){
			for(int j=i+1;j<TotalReg;j++){			
				Arch_Index.seek(i*logindex);
	        	cuenta_i=Arch_Index.readInt();
	        	registro_i=Arch_Index.readInt();						
				Arch_Index.seek(j*logindex);
	        	cuenta_j=Arch_Index.readInt();
	        	registro_j=Arch_Index.readInt();
	        	if(cuenta_i>cuenta_j){
	        		Arch_Index.seek(i*logindex);
	        		Arch_Index.writeInt(cuenta_j);
	        		Arch_Index.writeInt(registro_j);
	        		Arch_Index.seek(j*logindex);
	        		Arch_Index.writeInt(cuenta_i);
	        		Arch_Index.writeInt(registro_i);	
	        	}
			}
		}
	}
	public static int BusquedaBinaria(int Cuenta,RandomAccessFile Arch_Index) throws IOException {
		Arch_Index.seek(0);
		int Arriba=(int)Arch_Index.length()/logindex;
		int Abajo=1;
		int Centro;
		int cuentaleida;
		while (Abajo <=Arriba){
			Centro=(Arriba+Abajo)/2;
			Arch_Index.seek((Centro-1) *logindex );
			cuentaleida=Arch_Index.readInt();
			if (cuentaleida==Cuenta)
				return Centro;
			if(Cuenta>cuentaleida){
			Abajo=Centro+1;
			continue;
  			}
  			Arriba=Centro-1;
		}
		return -1;
	}
	public static void muestraindex(RandomAccessFile Arch_Index) throws IOException{
		int cuenta=0;
		int registro=0;
		int cont=0;
        while(true){
        	try {
        		cuenta=Arch_Index.readInt();
        		registro=Arch_Index.readInt();
        		cont ++;
        		System.out.println(cuenta+" "+registro);
        	}
        	catch (EOFException E){
        		System.out.println("Total Reg="+cont);
        	break;
        	} 
        }
	}
    public static void muestraalumnos(RandomAccessFile Arch_Alumnos) throws IOException{
   		int edad;
   		String nombre,clave;
   		char sexo;
   		int cont=0;
   		while(true){
   			try {
   				nombre=Arch_Alumnos.readUTF();
            	clave=Arch_Alumnos.readUTF();
           		sexo=Arch_Alumnos.readChar();
           		edad=Arch_Alumnos.readInt();
           		cont ++;
           		System.out.println(nombre+" "+clave+" "+sexo+" "+edad);
           	}
   			catch (EOFException E){
   				System.out.println("Total Reg="+cont);
           	break;
           	} 	
   		}
   	}
}