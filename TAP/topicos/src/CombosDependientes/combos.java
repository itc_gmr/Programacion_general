package CombosDependientes;
import java.awt.*;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.*;

import Componentes.MyRandom;
class Estados {
	int Codigo;
	String Estado;
	public Estados(int c, String e){
		Codigo = c;
		Estado = e;
	}
}
class Ciudades{
	int Codigo;
	String Ciudad;
	int codEstado;
	public Ciudades(int c, String e, int ce){
		Codigo = c;
		Ciudad = e;
		codEstado = ce;
	}
}
class Colonias{
	int Codigo;
	String Colonia;
	int codEstado;
	int codCiudad;
	public Colonias(int cod, String col, int ce, int cc){
		Codigo = cod;
		Colonia = col;
		codEstado = ce;
		codCiudad = cc;
	}
}
public class combos extends JPanel implements ActionListener{
	JComboBox estado, ciudad, colonia;
	String VE[], VC[], VCol[];
	crearArchivos e;
	boolean banCid = false, banCol = false;
	
	public combos() throws IOException{
		e = new crearArchivos();
		creaComboE(0);
		//e.creaArchivos();
	}
	
	public combos(int index)throws IOException{
		e = new crearArchivos();
		creaComboE(index);
		//e.creaArchivos();
	}
	
	public combos(int index, int index2)throws IOException{
		e = new crearArchivos();
		creaComboE(index);
		creaComboC(index,index2);
		creaComboCol(index2-1);
	}
	
	public void creaComboE(int index) throws IOException{
		Estados esta[] = e.creaEstados();
		if(index != 0){
			VE = new String[index];
		}else{
			VE = new String[esta.length];
		}
		if(index != 0){
			for(int i = 0; i < esta.length; i++){
				if(index != 0){
					VE[i] = esta[index-1].Estado;
					break;
				}
				VE[i] = esta[i].Estado;
			}
		}else{
			for(int i = 0; i < esta.length; i++){
				if(index != 0){
					VE[i] = esta[index].Estado;
					break;
				}
				VE[i] = esta[i].Estado;
			}
		}
		estado = new JComboBox(VE);
		estado.setSelectedIndex(0);
		System.out.println(VE.length);
		add(estado);
		estado.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() == estado){
			for(int i = 0; i < VE.length; i++){
				if(estado.getSelectedItem().equals(VE[i])){
					try {creaComboC(estado.getSelectedIndex()+1,0);} catch (IOException e1) {e1.printStackTrace(); return;}
				}
			}
		}
		if(evt.getSource() == ciudad){
			String[] cid = e.cid;
			int cont = 0;
			String aux;
			for(int i = 0; i < cid.length; i++){
				System.out.println(cid[i] + "   " + ciudad.getSelectedItem());
				aux = MyRandom.PonBlancos(cid[i], 20);
				if(ciudad.getSelectedItem().equals(aux)){
					try {creaComboCol(i+1);} catch (IOException e1) {e1.printStackTrace(); return;}
				}
				cont++;
			}
		}
	}
	public void creaComboCol(int index) throws IOException{
		Colonias col[] = e.creaColonias();
		System.out.println(index);
		int cont = 0;
		for(int i = 0; i < col.length; i++){
			if(col[i].codCiudad == index){
				cont++;
			}
		}
		VCol = new String[cont];
		cont = 0;
		for(int i = 0; i < col.length; i++){
			//VCol[i] = col[i].Colonia;
			if(col[i].codCiudad == index){
				VCol[cont] = col[i].Colonia;
				cont++;
			}
		}
		colonia = new JComboBox(VCol);
		if(!banCol){
			add(colonia);
			banCol = true;
			updateUI();
			return;
		}
		remove(2);
		add(colonia);
		updateUI(); 
	}
	public void creaComboC(int index, int index2) throws IOException{
		Ciudades cid[] = e.creaCiudades();
		int cont = 0;
		String c = null;
		for(int i = 0; i < cid.length; i++){
			if(cid[i].codEstado == index){
				cont++;
			}
		}
		VC = new String[cont];
		cont = 0;
		for(int i = 0; i < cid.length; i++){
			if(cid[i].codEstado == index){
				VC[cont] = cid[i].Ciudad;
				if(cont == index2){
					c = VC[cont];
				}
				cont++;
			}
		}
		if(index2 != 0){
			cont = 0;
			for(int i = 0; i < cid.length; i++){
				if(cid[i].codEstado == index){
					VC[cont] = cid[i].Ciudad;
					if(cont == index2){
						c = VC[cont-1];
					}
					cont++;
				}
			}
			cont = 0;
			for(int i = 0; i < VC.length; i++){
				if(i == index2){
					cont++;
				}
			}
			VC = new String[cont];
			VC[cont-1] = c;
		}
		System.out.println(cont);
		
		/*
		 *if(index2 != 0){
					VC[cont] = cid[index2].Ciudad;
					break;
				}
		 */
		ciudad = new JComboBox(VC);
		ciudad.addActionListener(this);
		if(!banCid){
			add(ciudad);
			banCid = true;
			updateUI();
			return;
		}
		if(banCol){
			remove(2);
			banCol = false;
		}
		remove(1);
		
		add(ciudad);
		updateUI();
	}
		
}
