package CombosDependientes; 
import java.io.*;
import java.util.*;
import Componentes.*;
class AplCreaClientes {
   public static String PonBlancos(String Nom,int Ancho){
       for(int i=0;Nom.length()<Ancho;i++)
          Nom=Nom+" ";
          return Nom;
     }
     public static void main( String args[] ) throws IOException
     {
         File F=new File("Clientes.dat");
         RandomAccessFile Arch= new RandomAccessFile( F,"rw" );
         Arch.seek(Arch.length());

         String Nombre;
         int Codigo;
         long Edad;
         double Estatura;
         Codigo=(int)Arch.length()/52;
         for(int i=0 ;i<10;i++){
            Codigo++;
            System.out.println(Codigo);
             Nombre=MyRandom.nextName();
             Nombre=PonBlancos(Nombre,30);
             Edad=MyRandom.nextInt(18,99);
             Estatura=1+MyRandom.nextInt(1,99)/100.0f;
// Long. Reg=52
             Arch.writeInt(Codigo);
             Arch.writeUTF(Nombre);
             Arch.writeLong(Edad);
             Arch.writeDouble(Estatura);

         }
         Muestra(Arch);
         Arch.close();

     }
     public static void Muestra(RandomAccessFile Arch) throws IOException{
    	 Arch.seek(0);
    	 int Codigo;
    	 String Nombre;
    	 long Edad;
    	 double Esta;
    	 for(int i=0; i<Arch.length()/52;i++){
    		 Codigo=Arch.readInt();
    		 Nombre=Arch.readUTF();
    		 Edad=Arch.readLong();
    		 Esta=Arch.readDouble();
    		 System.out.println(Codigo+" "+Nombre+" "+Edad+" "+Esta);
    	 }
     }
}
