import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
class dato{
	Icon icono;
	int par;
	
	public dato(Icon ic, int pare){
		icono = ic;
		par = pare;
		
		
	}
	
}
public class Pares extends JFrame implements ActionListener{
	int n = MyRandom.nextInt(4, 4);
	JButton pares[][];
	JButton Aux[][];
	Boolean mostrado[][];
	JPanel nort;
	dato ic[];
	int pulsa = 0;
	int cuenta = 0, aux, m = 0;
	boolean ban = true;
	
	public Pares(){
		super("Juego de Pares");
		interfaz();
		escuchadores();
	}
	
	public void interfaz(){
		setSize(1280,720);
		setLocationRelativeTo(null);
		nort = new JPanel();
		nort.setLayout(new GridLayout(n,n,10,10));
		pares = new JButton[n][n];
		Aux = new JButton[n][n];
		mostrado = new Boolean[n][n];
		ic = new dato [8];
		agregaImagenes(ic);
		for(int i = 0; i < pares.length; i++){
			for(int j = 0; j < pares[i].length; j++){
				pares[i][j] = new JButton();
				Aux[i][j] = new JButton();
				mostrado[i][j] = false;
				int ene = MyRandom.nextInt(ic.length);
				if(ic[ene].par == 2){
					j--;
					continue;
				}
				Aux[i][j].setIcon(new ImageIcon("Pregunta.JPG"));
				pares[i][j].setIcon(ic[ene].icono);
				ic[ene].par++;
				nort.add(Aux[i][j]);
			}
		}
		add(nort);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void agregaImagenes(dato c[]){
		int cont = 0;
		c[cont] = new dato(new ImageIcon("i1.JPG") , 0);
		cont++;
		c[cont] = new dato(new ImageIcon("i2.JPG") , 0);
		cont++;
		c[cont] = new dato(new ImageIcon("i3.JPG") , 0);
		cont++;
		c[cont] = new dato(new ImageIcon("i4.JPG") , 0);
		cont++;
		c[cont] = new dato(new ImageIcon("i5.JPG") , 0);
		cont++;
		c[cont] = new dato(new ImageIcon("i6.JPG") , 0);
		cont++;
		c[cont] = new dato(new ImageIcon("i7.PNG") , 0);
		cont++;
		c[cont] = new dato(new ImageIcon("i8.PNG") , 0);
	}
	
	public void escuchadores(){
		for(int i = 0; i < Aux.length; i++){
			for(int j = 0; j < Aux[i].length; j++){
				Aux[i][j].addActionListener(this);
				pares[i][j].addActionListener(this);
			}
		}
	}
	
	public void actionPerformed(ActionEvent evt) {
	
		for(int i = 0; i < Aux.length; i++){
			for(int j = 0; j < Aux[i].length; j++){
				if(evt.getSource() == Aux[i][j]){
					System.out.println("Llego");
					nort.remove(cuenta);
					nort.add(pares[i][j], cuenta);
					try{
						Thread.sleep(1);
					}catch(Exception e){}
					return;
				}
				repaint();
				cuenta++;
			}
			
		}
		cuenta=-1;

	}

	public static void main(String[] args) {
		new Pares();

	}

}
