import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
public class GraficoTemporizdorScreenOff extends JFrame implements ActionListener{
	Timer timer;
	Graphics g;
	Image backbuffer = null;
	int Valor=10,X=10;
	boolean Band=true;
	public GraficoTemporizdorScreenOff(){
		super("Temporizado mueve avion");
		HazInterfaz();
	}
	public void HazInterfaz(){
		setSize(400,500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		// SCREEN OFF
		backbuffer = createImage(getWidth(), getHeight());
		g =backbuffer.getGraphics();
		
		timer=new Timer(500,this);
		timer.setRepeats(true);
		timer.start();
		
		Animacion();
	}
	
	public void Animacion(){
		super.paint(g);
		g.setColor(Color.RED);
		g.drawLine(0,300,300,300);
		g.drawImage(ajustarImagen1("AVION.JPG",50,30).getImage(),X,310,50,30,this);
		repaint();
	}
	public void paint( Graphics g) 	{

		
		g.drawImage(backbuffer, 0, 0, getWidth(), getHeight(), this);
	}
	
	public void actionPerformed(ActionEvent arg0) {

		X+=Valor;
		Animacion();
		if(Band && X>300){
			Band=false;
			Valor=-10;
		}else {
			if(!Band && X<=10){
				Band=true;
				Valor=10;
			}
		}



		
	}
	   private ImageIcon ajustarImagen1(String ico,int Ancho,int Alto)
	    {
	        ImageIcon tmpIconAux = new ImageIcon(ico);
	        ImageIcon tmpIcon = new ImageIcon(tmpIconAux.getImage().getScaledInstance(Ancho,Alto, Image.SCALE_SMOOTH));//SCALE_DEFAULT
	        return tmpIcon;
	    }	
	public static void main(String [] a){
		new GraficoTemporizdorScreenOff();
	}

}
