import java.awt.*;
import java.awt.Dialog.*;
import java.awt.event.*;
import javax.swing.*;

public class ClaseModal extends JFrame implements ActionListener {

   JButton BtnModal,BtnNoModal;
   JButton BtnSalvaComo;
	public ClaseModal(){
		super("Marco principal");
		HazInterfaz();
		HazEscucha();
	}
	public void HazInterfaz(){
      setLayout(new BorderLayout());
      BtnNoModal = new JButton("Nva. pantalla NO MODAL");
      BtnModal = new JButton("Nva. Pantalla MODAL");
      add(BtnNoModal, BorderLayout.NORTH);
      add(BtnModal, BorderLayout.SOUTH);
      setSize(300,150);
      setLocationRelativeTo(null);
      setVisible(true);

	}
	public void HazEscucha(){
      BtnModal.addActionListener(this);
      BtnNoModal.addActionListener(this);

	}
   public static void main(String[] args) {
   	  new ClaseModal();
   }

   public void actionPerformed(ActionEvent E){
		if(E.getSource()==BtnNoModal){
	       	JFrame VentanaNoModal = new JFrame("Ventana Independiente");
      	//	VentanaNoModal.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
      		VentanaNoModal.setSize(500,500);
      		VentanaNoModal.setLocation(75, 75);
      		VentanaNoModal.setVisible(true);
			return;
		}
		if(E.getSource()==BtnModal){
     //       JDialog VentanaModal = new JDialog(this, "hija",Dialog.ModalityType.APPLICATION_MODAL);
			JDialog VentanaModal = new JDialog(); 
            VentanaModal.setModal(true);
   
            BtnSalvaComo = new JButton("Salvar como...");
            BtnSalvaComo.addActionListener(this);
            VentanaModal.setLayout(new FlowLayout());
			VentanaModal.add(BtnSalvaComo);
            VentanaModal.setSize(300,300);
            VentanaModal.setLocation(150,150);
            VentanaModal.setVisible(true);
            VentanaModal.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            return;
		}
		if(E.getSource()==BtnSalvaComo){
			System.out.println("*****");
			FileDialog fileDialog = new FileDialog(this,"Guardar", FileDialog.SAVE);
            fileDialog.setSize(400,400);
            fileDialog.setVisible(true);
			return;
		}


   }


}