package MinaPlataGrafico;
import java.awt.*;
import javax.swing.*;
import Componentes.Lista;
import Componentes.MyRandom;
import Componentes.Nodo;
import Componentes.Semaforo;

public class Vista extends JFrame{
	JButton inicio, reporte;
	JLabel cap, pEu, pAs;
	JPanel norte, centro, centroIzq, centroDer, surCentroIzq, surCentroDer, centroCentroIzq, centroCentroDer;
	
	JLabel capacidadTotal, tipoPlata[], paisesE[], paisesA[];
	
	Lista<paisReporte> prE = new Lista<paisReporte>();
	Lista<paisReporte> prA = new Lista<paisReporte>();
	public Vista(){
		super("Mina de plata");
		setInterfaz();
	}

	public void setInterfaz(){
		surCentroIzq = new JPanel();
		surCentroDer = new JPanel();
		centroCentroIzq = new JPanel();
		centroCentroDer = new JPanel();
		
		inicio = new JButton("Iniciar");
		reporte = new JButton("Reporte");
		reporte.setEnabled(false);
		cap = new JLabel("Capacidad...");
		norte = new JPanel();
		centro = new JPanel();
		centro.setLayout(new GridLayout(0,2));
		centroIzq = new JPanel();
		centroDer = new JPanel();
		pEu = new JLabel("Pa�ses Europeos");
		pAs = new JLabel("Pa�ses Asiaticos");
		pEu.setHorizontalAlignment(SwingConstants.CENTER);
		pAs.setHorizontalAlignment(SwingConstants.CENTER);
		pEu.setFont(new Font("Calibri", Font.BOLD, 30));
		pAs.setFont(new Font("Calibri", Font.BOLD, 30));
		
		inicio.setFont(new Font("Calibri", Font.PLAIN, 25));
		reporte.setFont(new Font("Calibri", Font.PLAIN, 25));
		cap.setFont(new Font("Calibri", Font.PLAIN, 25));
		
		centroIzq.setLayout(new BorderLayout());
		centroDer.setLayout(new BorderLayout());
		centroIzq.add(pEu, BorderLayout.NORTH);
		centroDer.add(pAs, BorderLayout.NORTH);
		centroIzq.add(centroCentroIzq, BorderLayout.CENTER);
		centroDer.add(centroCentroDer, BorderLayout.CENTER);
		centroIzq.add(surCentroIzq, BorderLayout.SOUTH);
		centroDer.add(surCentroDer, BorderLayout.SOUTH);
		
		norte.add(inicio);
		norte.add(reporte);
		norte.add(cap);
		centro.add(centroIzq);
		centro.add(centroDer);
		
		add(norte, BorderLayout.NORTH);
		add(centro, BorderLayout.CENTER);
		setSize(1200, 800);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void inicia(){
		
		
		int cont = 0;
		int capacidad;
		while(true){
			capacidad = MyRandom.nextInt(10000, 20000);//MyRandom.nextInt(10000, 20000);
			if (capacidad % 2 == 0){
				break;
			}
		}
		
		paisesE = new JLabel[MyRandom.nextInt(10, 30)];
		paisesA = new JLabel[MyRandom.nextInt(5, 7)];
		
		pais[]pE = new pais[paisesE.length];
		pais[]pA = new pais[paisesA.length];
		
		centroCentroIzq.setLayout(new GridLayout(0, 1));
		centroCentroDer.setLayout(new GridLayout(0, 1));
	
		
		cap.setText("Capacidad: " + capacidad);
		int tipo[] = new int [3]; //cantidad de plata
		tipoPlata = new JLabel[3];//
		contPlata cE = new contPlata((capacidad/2), 0);
		contPlata cA = new contPlata((capacidad/2), 0);
		System.out.println(cE.contP + "   " + cA.contP);
		
		Semaforo SC[] = new Semaforo[3];
		Semaforo SEu = new Semaforo(1);
		Semaforo SAs = new Semaforo(1);
		Semaforo SIDE = new Semaforo(1);
		Semaforo SIDA = new Semaforo(1);
		
		cont = 0;
		tipo[cont++] = (int) (Math.round((capacidad) * 0.3));
		tipo[cont++] = (int) (Math.round((capacidad) * 0.6));
		tipo[cont++] = (int) (Math.round((capacidad) * 0.1));
		
		
		
		capacidadTotal = new JLabel();
		for(int i = 0; i < tipoPlata.length; i++){
			SC[i] = new Semaforo(1);
			tipoPlata[i] = new JLabel("   " + tipo[i]+"   ");
			tipoPlata[i].setFont(new Font("Calibri", Font.BOLD, 25));
			norte.add(tipoPlata[i]);
		}
		
		for(int i = 0; i < paisesE.length; i++){
			paisesE[i] = new JLabel("Pa�s: " + (i+1));
			centroCentroIzq.add(paisesE[i]);
		}
		
		for(int i = 0; i < paisesA.length; i++){
			paisesA[i] = new JLabel("Pa�s: " + (i+1));
			centroCentroDer.add(paisesA[i]);
		}
		
		for(int i = 0; i < pE.length; i++){
			pE[i] = new pais((i), tipo, SC, SEu, tipoPlata,paisesE, cE, prE, SIDE);
			pE[i].start();
		}
		
		for(int i = 0; i < pA.length; i++){
			pA[i] = new pais((i), tipo, SC, SAs, tipoPlata ,paisesA, cA, prA, SIDA);
			pA[i].start();
		}
	}
	
	public void reporte(){
		JDialog dialogE, dialogA;
		JTable tablaE, tablaA;
		JScrollPane scrollE, scrollA;
		
		int contE = 0, contA = 0;
		Nodo<paisReporte> Aux = prE.DameFrente();
		while( Aux != null){
			contE++;
			//System.out.println("Pa�s: " + Aux.Info.noPais + " pidi� " + Aux.Info.cantidad + " de plata tipo " + Aux.Info.tipo);
			Aux = Aux.Sig;
		}
		Aux = prA.DameFrente();
		while( Aux != null){
			contA++;
			//System.out.println("Pa�s: " + Aux.Info.noPais + " pidi� " + Aux.Info.cantidad + " de plata tipo " + Aux.Info.tipo);
			Aux = Aux.Sig;
		}
		System.out.println((contE+contA));
		
		Object columnasE[] = {"ID Pedido", "Pa�s", "Cantidad", "Tipo"};
		Object dataE[][] = new Object [contE][columnasE.length];
		Aux = prE.DameFrente();
		contE = 0;
		int c = 0;
		while( Aux != null){
			dataE[contE][c] = Aux.Info.ID;
			c++;
			dataE[contE][c] = Aux.Info.noPais;
			c++;
			dataE[contE][c] = Aux.Info.cantidad;
			c++;
			dataE[contE][c] = Aux.Info.tipo;
			c = 0;
			contE++;
			Aux = Aux.Sig;
		}
		
		Object columnasA[] = {"ID Pedido", "Pa�s", "Cantidad", "Tipo"};
		Object dataA[][] = new Object [contA][columnasA.length];
		Aux = prA.DameFrente();
		contA = 0;
		c = 0;
		while( Aux != null){
			dataA[contA][c] = Aux.Info.ID;
			c++;
			dataA[contA][c] = Aux.Info.noPais;
			c++;
			dataA[contA][c] = Aux.Info.cantidad;
			c++;
			dataA[contA][c] = Aux.Info.tipo;
			c = 0;
			contA++;
			Aux = Aux.Sig;
		}
		
		tablaE = new JTable(dataE, columnasE);
		tablaE.setEnabled(false);
		scrollE = new JScrollPane(tablaE);
		tablaA = new JTable(dataA, columnasA);
		tablaA.setEnabled(false);
		scrollA = new JScrollPane(tablaA);
		
		dialogE = new JDialog();
		dialogE.setSize(400, 400);
		dialogE.add(scrollE);
		dialogE.setTitle("Movimientos Pa�ses Europeos");
		dialogE.setVisible(true);
		dialogA = new JDialog();
		dialogA.setSize(400, 400);
		dialogA.add(scrollA);
		dialogA.setTitle("Movimientos Pa�ses Asiaticos");
		dialogA.setVisible(true);
		
	}
	
	public void setControlador(Controlador c) {
		inicio.addActionListener(c);
		reporte.addActionListener(c);
	}

}

class pais extends Thread{
	int noPais;
	//int repeticiones;
	int cantidad;
	int seleccionTipo;
	contPlata contadorPlata;
	
	int VT[];//RC okok
	Semaforo SC[];
	Semaforo S;
	Semaforo ID;
	
	JLabel tipoPlata [];
	JLabel listaPais[];
	
	Lista<paisReporte> r;
	
	boolean band = false;
	
	public pais(int noPais, int VT[], Semaforo SC[], Semaforo S, JLabel tipoPlata[], JLabel listaPais[], contPlata contadorPlata, Lista<paisReporte> r, Semaforo ID){
		this.noPais = noPais;//Depende del Continente
		
		cantidad = 0;
		seleccionTipo = 0;
		this.contadorPlata = contadorPlata;
		
		this.VT = VT;//RC
		this.SC = SC;
		this.S = S;
		this.ID = ID;
		
		this.tipoPlata = tipoPlata;
		this.listaPais = listaPais;//Depende del Continente, AGREGAR POR INDEX EN EL HILO!!!!!
		this.r = r;
	}
	
	public void run(){
		while(true){	
			cantidad = MyRandom.nextInt(1, 3);
			seleccionTipo = MyRandom.nextInt(3);	
			SC[seleccionTipo].Espera();
			if((VT[seleccionTipo] - cantidad) < 0){
				if(!band){
					tipoPlata[seleccionTipo].setText(VT[seleccionTipo]+"");
					band = true;
				}
				SC[seleccionTipo].libera();
				continue;
			}
			if((contadorPlata.contP) < 0){
				SC[seleccionTipo].libera();
				break;
			}
			
			S.Espera();
			contadorPlata.contP = contadorPlata.contP - cantidad;
			S.libera();
			VT[seleccionTipo] = VT[seleccionTipo] - cantidad;
			tipoPlata[seleccionTipo].setText(VT[seleccionTipo]+"");
			listaPais[noPais].setText("Pa�s: " + (noPais+1) + " pidi� " + cantidad + " de plata tipo " + (seleccionTipo+1));
			try{sleep(10);}catch(Exception e){}
			listaPais[noPais].setText("Pa�s: " + (noPais+1));
			ID.Espera();
			contadorPlata.ID++;
			r.InsertaOrd(new paisReporte(contadorPlata.ID, (noPais+1), cantidad, (seleccionTipo+1)));
			ID.libera();
			SC[seleccionTipo].libera();
			
		}
	}
}

class contPlata{
	int contP;
	int ID;
	public contPlata(int cont, int ID){
		this.ID = ID;
		contP = cont;
	}
}

class paisReporte{
	int ID;
	int noPais;
	int cantidad;
	int tipo;
	
	public paisReporte(int ID,int noPais, int cantidad, int tipo){
		this.ID = ID;
		this.noPais = noPais;
		this.cantidad = cantidad;
		this.tipo = tipo;
	}
	
	public String toString(){
		return MyRandom.PonCeros(ID, 5);
	}
}
