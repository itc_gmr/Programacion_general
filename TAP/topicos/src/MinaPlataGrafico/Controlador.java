package MinaPlataGrafico;
import java.awt.event.*;
public class Controlador implements ActionListener{
	Vista vista;

	public Controlador(Vista vista) {
		this.vista = vista;
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() == vista.inicio){
			vista.inicio.setEnabled(false);
			vista.reporte.setEnabled(true);
			vista.inicia();
			return;
		}
		if(evt.getSource() == vista.reporte){
			vista.reporte();
			return;
		}
	}

}
