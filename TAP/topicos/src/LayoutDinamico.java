import javax.swing.*;
import java.awt.*; 
import java.awt.event.*;
public class LayoutDinamico extends JFrame{
	public LayoutDinamico(){
		HazInterfaz();
		HazEscuchas();
		
		
	}
	public void HazInterfaz(){
		setBounds(100,100,400,400);
		setLayout(new GridLayout(0,3,10,15));
		JLabel Et1=new JLabel("Peligro",SwingConstants.CENTER);
		Et1.setFont(new Font("Arial",Font.BOLD ,22));
		add(Et1);
		add(new JLabel("Peligro medio"));
		add(new JLabel("Peligro bajo"));
		for(int i=0;i<20;i++){
			add(new JButton("Btn #"+(i+1)));
		}
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void HazEscuchas(){
		
	}
	public static void main(String [] a){
		new LayoutDinamico();
	}

}
