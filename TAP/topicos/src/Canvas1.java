import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
public class Canvas1 extends JFrame implements ActionListener{
	CCanvas Screen;
	JButton Btn;
	public Canvas1(){
		super("Graficos canvas");
		setSize(500,400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Screen=new CCanvas();
		Btn=new JButton("Movimiento");
		Btn.addActionListener(this);
			
	
		add(Screen);
		add(Btn,BorderLayout.NORTH);
		
		setVisible(true);
	}
	
	public static void main(String [] a){
		new Canvas1();
	}

	
	public void actionPerformed(ActionEvent arg0) {
		Screen.repaint();
		
	}
}
class CCanvas extends Canvas {
	// se puede a�adir a un frame para visualizarlo
	String cad = "GRUPO DE SISTEMASSS";
	// este metodo se ejecuta automaticamente cuando Java necesita mostrar la ventana
	public void paint(Graphics g) {
		super.paint(g);
		System.out.println("esta llegando 00000");
	// obtener el color original
	Color colorOriginal = g.getColor();
	// escribir texto grafico en la ventana y recuadrarlo
	g.drawString(cad, 40, 20);
	g.drawRect(35, 8, (cad.length()*7), 14);
	// dibujo de algunas lineas
	for (int i=20; i< 50; i= i+3) {
	if ((i % 2) == 0) g.setColor(Color.blue);
	else g.setColor(Color.red);
	g.drawLine(40, (90-i), 120, 25+i);
	}
	// dibujo y relleno de un �valo
	g.drawOval(40, 95, 120, 20);
	g.fillOval(40, 95, 120, 20);
	g.setColor(colorOriginal);

	
	}
}
