//Guillermo M�rquez Ruiz T�picos Avanzados de Programaci�n
public class MatrizHilosV3 {
	static class calculoMatriz extends Thread{
		int a[][];
		int b[][];
		int r[][];
		Semaforo vs[][];
		boolean vb[][];
		Semaforo S;
		public calculoMatriz(int aa[][], int bb[][], int rr[][],Semaforo s[][], boolean boo[][], Semaforo ss){
			a = aa;
			b = bb;
			r = rr;
			vs = s;
			vb = boo;
			S = ss;
		}
		
		public boolean checa(boolean V[][]){
			for(int i = 0; i < V.length; i++){
				for(int j = 0; j < V[i].length; j++){
					if(!V[i][j])
						return false;
				}
			}
			return true;
		}
		
		public void imprimeBoolean(boolean V[][]){
			for(int i = 0; i < V.length; i++){
				for(int j = 0; j < V[i].length; j++){
					System.out.print(V[i][j] + "\t");
				}
				System.out.println();
			}
			System.out.println();
		}
		
		public int calculoCelda(int a[][], int b[][], int fila, int columna){
			int suma = 0;
			int filaMA[] = fila(a, fila);
			int columnaMB[] = columna(b, columna);
			for(int i = 0; i < filaMA.length; i++){
				suma += filaMA[i] * columnaMB[i];
			}
			return suma;
		}
		
		public int [] fila(int m[][], int fila){
			int arreglo[] = new int [m[fila].length];
			for(int i = 0; i < arreglo.length; i++){
				arreglo[i] = m[fila][i];
			}
			return arreglo;
		}
		
		public int [] columna(int m[][], int columna){
			int arreglo[] = new int [m.length];
			for(int i = 0; i < arreglo.length; i++){
				arreglo[i] = m[i][columna];
			}
			return arreglo;
		}

		public void run() {//RUN
			int ren = 0;
			int col = 0;
			while(true){
				S.Espera();
				System.out.println("termino? " + checa(vb));
				if(checa(vb)){
					S.libera();
					return;
				}
				S.libera();
				while(true){
					ren = MyRandom.nextInt(r.length);
					col = MyRandom.nextInt(r[0].length);
					vs[ren][col].Espera();
					System.out.println(getName() + "  " +vb[ren][col] + "  " + ren + " " + col);
					try {sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
					imprimeBoolean(vb);
					try {sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
					if(vb[ren][col]){
						vs[ren][col].libera();
						continue;
					}
					vb[ren][col] = true;
					vs[ren][col].libera();
					break;
				}
				r[ren][col] = calculoCelda(a, b, ren, col);
			}
		}
	}
	public static void main(String[] args) {  
		int a[][], b[][], r[][];
		calculoMatriz calculoM[] = new calculoMatriz[MyRandom.nextInt(5, 15)];
		boolean VB[][];
		Semaforo VS[][];
		Semaforo S = new Semaforo(1);
		while(true){
			int m1 = MyRandom.nextInt(3, 25);
			int n1 = MyRandom.nextInt(3, 25);
			int m2 = MyRandom.nextInt(3, 25);
			int n2 = MyRandom.nextInt(3, 25);
			if(!(n1 == m2)){
				continue;
			}
			System.out.println("Son compatibles");
			System.out.println(m1 + "   "  + n1);
			System.out.println(m2 + "   " + n2);
			System.out.println("Cantidad de Hilos: " + (calculoM.length));
			System.out.println("---------------");

			a = new int [m1][n1];
			b = new int [m2][n2];
			r = new int [m1][n2];
			generaMatriz(a);
			System.out.println("---------------");
			generaMatriz(b);
			System.out.println("---------------");
			break;
		}
	
		VB = new boolean[r.length][r[0].length];
		VS = new Semaforo[r.length][r[0].length];
		
		for(int i = 0; i < VB.length; i++){
			for(int j = 0; j < VB[i].length; j++){
				VB[i][j] = false;
				VS[i][j] = new Semaforo(1);
			}
			
		}
		
		for(int i = 0; i < calculoM.length; i++){
			calculoM[i] = new calculoMatriz(a, b, r, VS, VB, S);
		}
		
		for(int i = 0; i < calculoM.length; i++){
			calculoM[i].start();
		}
		
		
		while(vivos(calculoM));
		for(int i = 0; i < r.length; i++){
			for(int j = 0; j < r[i].length; j++){
				System.out.print(r[i][j] + "  ");
			}
			System.out.println();
		}

	}

	public static boolean vivos(calculoMatriz cm[]){
		for(int i =0; i < cm.length;i++){
			if(cm[i].isAlive())
				return true;
		}
		return false;
	}
	
	public static void generaMatriz(int ma[][]){
		for(int i = 0; i < ma.length; i++){
			for(int j = 0; j < ma[i].length; j++){
				ma[i][j] = MyRandom.nextInt(1, 9);
				System.out.print(ma[i][j] + "\t");
			}
			System.out.println();
		}
	}
}
/*class Semaforo{
	private int Recursos;
	
	public Semaforo(int r){
		Recursos = r;
	}
	
	public synchronized void Espera(){
		while(Recursos <= 0){
			try{
				wait();
			}catch(InterruptedException e){}
		}
		Recursos--;
	}
	
	public synchronized void libera(){
		Recursos++;
		notify();
	}
}*/
