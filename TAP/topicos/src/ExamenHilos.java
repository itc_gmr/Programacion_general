import Componentes.*;
public class ExamenHilos {

	public static void main(String[] args) {
		Semaforo S []= new Semaforo[50];
		for(int i = 0; i < S.length; i++){
			S[i] = new Semaforo(0);
		}
		Semaforo SP[][] = new Semaforo[50][10];
		Producto CP[][] = new Producto[50][10];
		for(int i = 0; i < CP.length; i++){
			for(int j = 0; j < CP[i].length; j++){
				CP[i][j] = new Producto(i+1000);
				SP[i][j] = new Semaforo(1);
			}
		}
		
		Caja caja[] = new Caja[5];
		for(int i = 0; i < caja.length; i++){
			caja[i] = new Caja(SP, CP, S);
			caja[i].start();
		}
		Proveedor proveedor = new Proveedor(SP, CP, S);
		proveedor.start();
		
		
		
	}

}

class Caja extends Thread{
	Semaforo SP[][];
	Semaforo S[];
	Producto CP[][];
	
	int clientes;
	int producto;
	int cantidad;
	boolean band =true;
	public Caja(Semaforo SP[][], Producto CP[][], Semaforo S[]){
		this.CP = CP;
		this.SP = SP;
		this.S = S;
	}
	
	public void run(){
		while(true){
			if(clientes == 50) //Condicion de salida, no es recurso compartido
				return;
			//for(int i = 0; i < 5; i++){
			System.out.println("Entra");
			int i = 0;
			int j = 1;
			int cajon = 0;
			while(true){
				i++;
				if(band){
					producto = MyRandom.nextInt(CP.length);
					cantidad = MyRandom.nextInt(2, 10);
				}
				SP[producto][cajon].Espera();
				System.out.println(CP[producto][9].existencia);
				if(CP[producto][9].existencia == 0){
					S[producto].Espera();
					System.out.println("Liberado " + producto);
				}
				if(CP[producto][cajon].existencia == 0 || CP[producto][cajon].existencia < cantidad){
					SP[producto][j].Espera();
					CP[producto][cajon].existencia += CP[producto][j].existencia;
					CP[producto][j].existencia = 0;
					SP[producto][j].libera();
					SP[producto][cajon].libera();
					j++;
					cajon++;
					i--;
					band = false;
					continue;
				}
				CP[producto][cajon].existencia -= cantidad;
				System.out.println("Cliente No " + (clientes+1) + " se llevo " + cantidad + " del producto No " + (producto+1));
				SP[producto][cajon].libera();
				if(i == 5){
					break;
				}
				j = 0;
				cajon = 0;
				band = true;
				continue;	
			}
			clientes++;
		}
	}
}

class Proveedor extends Thread{
	Semaforo SP[][];
	Producto CP[][];
	Semaforo S[];
	
	int cantidad;
	public Proveedor(Semaforo SP[][], Producto CP[][], Semaforo S[]){
		this.SP = SP;
		this.CP = CP;
		this.S = S;
	}
	
	public void run(){
		while(true){
			
			for(int i = 0; i < CP.length; i++){
				SP[i][9].Espera();
				//S[i].Espera();
				if(CP[i][9].existencia == 0){
					cantidad = MyRandom.nextInt(20, 50);
					CP[i][9].existencia += cantidad;
					System.out.println("Proveedor surte "+ cantidad + " productos de tipo " + (i+1));
					S[i].libera();
					SP[i][9].libera();
					continue;
				}
				SP[9][i].libera();
			}
			
			
		}	
	}
}

class Producto{
	int codigoProducto;
	int existencia;
	
	public Producto(int cod){
		codigoProducto = cod;
		existencia = 0;
	}
}

