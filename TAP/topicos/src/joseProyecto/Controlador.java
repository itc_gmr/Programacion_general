package joseProyecto;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.JOptionPane;

import com.itextpdf.text.DocumentException;
public class Controlador implements ActionListener, KeyListener{
	private static Modelo modelo;
	private static Vista vista;
	public Controlador(){
		this(modelo, vista);
	}
	public Controlador(Modelo m, Vista v) {
		modelo = m;
		vista = v;
	}
	
	public void actionPerformed(ActionEvent evt) {
		((JComponent) evt.getSource()).transferFocus();
		if(evt.getSource() == vista.guardar){
			if(vista.lug.getText().length() == 0 || vista.mon.getText().length() == 0 || vista.con.getText().length() == 0 || vista.tar.getText().length() == 0){
				System.out.println("Campo vacio, reingrese valores");
				JOptionPane.showMessageDialog(null, "Alguno de los campos esta vac�o, reingrese valores");
				return;
			}
			try {
				modelo.grabar(vista.f.getText(), vista.con.getText(), Double.parseDouble(vista.mon.getText()), vista.tar.getText(), vista.lug.getText());
				vista.con.setText("");
				vista.mon.setText("");
				vista.tar.setText("");
				vista.lug.setText("");
			} catch (IOException e) {e.printStackTrace();}
		}
		if(evt.getSource() == vista.consulta){
			try {
				modelo.reporte(vista.reporte, vista.repCodigo, vista.repFecha, vista.repConcepto, vista.repMonto, vista.repTarjeta, vista.repLugar);
			} catch (IOException | DocumentException e) {e.printStackTrace();}
		}
	}
	
	public void keyPressed(KeyEvent evt) {
	
	}
	
	public void keyReleased(KeyEvent evt) {
	
	}
	
	public void keyTyped(KeyEvent evt) {
		char cTeclaPresionada = evt.getKeyChar();
		if(evt.getSource() == vista.guardar){
			if(cTeclaPresionada == KeyEvent.VK_ENTER){
				vista.guardar.doClick();
				System.out.println("Click en guardar");
				vista.f.grabFocus();
			}
		}
		if(evt.getSource() == vista.consulta){
			if(cTeclaPresionada == KeyEvent.VK_ENTER){
				vista.consulta.doClick();
				System.out.println("Click en consulta");
			}
		}
	}

}
