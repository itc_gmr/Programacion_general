package joseProyecto;
import java.awt.*;
import javax.swing.*;
import Componentes.MyRandom;
import Componentes.JLeeReal;
import Componentes.JLeeNombre;
import Componentes.JLeeReal;
import Componentes.fecha;

public class Vista extends JFrame{
	fecha f = new fecha();
	JLeeNombre con, tar, lug;
	JLeeReal mon;
	JButton guardar = new JButton("Guardar");
	JButton consulta = new JButton("Consulta");
	JLabel fecha, concepto, monto, tarjeta, lugar;
	
	JDialog reporte;
	Box repCodigo;
	Box repFecha;
	Box repConcepto;
	Box repMonto;
	Box repTarjeta;
	Box repLugar;
	
	public Vista(){
		guardar.setFont(new Font("Calibri",Font.BOLD,25));
		consulta.setFont(new Font("Calibri",Font.BOLD,25));
		fecha = new JLabel("Fecha: ");
		fecha.setFont(new Font("Calibri",Font.BOLD,25));
		concepto = new JLabel("Concepto: ");
		concepto.setFont(new Font("Calibri",Font.BOLD,25));
		monto = new JLabel("Monto: ");
		monto.setFont(new Font("Calibri",Font.BOLD,25));
		tarjeta = new JLabel("Tarjeta: ");
		tarjeta.setFont(new Font("Calibri",Font.BOLD,25));
		lugar = new JLabel("Lugar: ");
		lugar.setFont(new Font("Calibri",Font.BOLD,25));
		
		con = new JLeeNombre(50);
		con.setFont(new Font("Calibri",Font.BOLD,25));
		tar = new JLeeNombre(50);
		tar.setFont(new Font("Calibri",Font.BOLD,25));
		lug = new JLeeNombre(50);
		lug.setFont(new Font("Calibri",Font.BOLD,25));
		mon = new JLeeReal(15);
		mon.setFont(new Font("Calibri",Font.BOLD,25));
		
		setSize(600, 350);
		setLayout(new GridLayout(0, 2));
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		add(fecha);
		add(f);
		add(concepto);
		add(con);
		add(monto);
		add(mon);
		add(tarjeta);
		add(tar);
		add(lugar);
		add(lug);
		add(guardar);
		add(consulta);
		
		setVisible(true);
	}
	
	public void setControlador(Controlador c) {
		guardar.addActionListener(c);
		consulta.addActionListener(c);
		guardar.addKeyListener(c);
		consulta.addKeyListener(c);
		f.addActionListener(c);
		con.addActionListener(c);
		mon.addActionListener(c);
		tar.addActionListener(c);
		lug.addActionListener(c);
	}

}
