package joseProyecto;
public class Menu {
	public Menu(){
		Vista vista = new Vista();
		Modelo modelo = new Modelo();
		Controlador controlador = new Controlador(modelo, vista);
		vista.setControlador(controlador);
	}
	public static void main(String[] args) {
		new Menu();
	}

}
