import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
public class Graphics1 extends JFrame implements ActionListener, MouseListener{
	JButton Btn;
	int X=10;
	boolean Band=false;
	public Graphics1(){
		super("Interfaz Gr�fica");
		HazInterfaz();
		HazEscucha();
	}
	public void HazInterfaz(){
		Btn=new JButton("Avanza");
		Btn.setBounds(10,280,100,50);
		setLayout(null);
		setSize(400,500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		add(Btn);
		setVisible(true);		
	}
	public void HazEscucha(){
		Btn.addActionListener(this);
		Btn.addMouseListener(this);
	}
	public void paint( Graphics g) 	{
		super.paint(g);
	       g.drawString("INSTITUTO TECNOL�GICO DE CULIAC�N",50,40);
	        g.drawLine(20,45,300,45);
	        g.drawString("INGENIER�A EN SISTEMAS COMPUTACIONALES",50,60);
	        g.setColor(Color.BLUE);
	        g.fillRect(50,70,100,50);
	        g.fillRoundRect(250, 70,100, 50, 20, 20);
	        Color CAFE=new Color(192,128,64);
	        g.setColor(CAFE);
	        g.fillRect(50,150,200,100);
	        g.setColor(Color.RED);
	        g.fillOval(60,220,60,60);
	        g.fillOval(190,220,50,60);
	        g.setColor(Color.WHITE);
	        g.fillRect(200,160,50,30);
	        g.setColor(CAFE);
	        g.fillOval(X,350,60,60);
	        if(Band){
	        	g.setColor(Color.RED);
		        g.fillOval(300,350,60,60);
	
	        	
	        	
	        	
	        }
	}
	public static void main(String [] a){
		new Graphics1();
	}
	
	public void actionPerformed(ActionEvent arg0) {
		X+=10;
		repaint();
		
	}
	
	public void mouseClicked(MouseEvent arg0) {
		
		
	}
	
	public void mouseEntered(MouseEvent E) {
		Band=true;
    	if(X>200)
    		X-=10;
    	else
		   X+=10;

		repaint();
 	}
	
	public void mouseExited(MouseEvent arg0) {
		Band=false;
		repaint();
		
		
	}
	
	public void mousePressed(MouseEvent arg0) {
		
		
	}
	
	public void mouseReleased(MouseEvent arg0) {
		
	}

}
