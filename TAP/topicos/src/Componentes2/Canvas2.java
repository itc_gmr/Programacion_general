package Componentes2;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Line2D;
public class Canvas2 extends JFrame implements ActionListener{
	XCanvas Screen;
	JButton Btn;
	Image backbuffer = null;
	Graphics a;
	Graphics2D g;
	CanvasScreenOf ScreenOff;
	public Canvas2(){
		super("Graficos canvas");
		setLayout(null);
		
		setSize(600,500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Screen=new XCanvas();
		ScreenOff=new CanvasScreenOf();
		Screen.setBounds(10, 200,200, 200);
		ScreenOff.setBounds(200, 10,200, 200);
		Btn=new JButton("Movimiento");
		Btn.setBounds(0, 0, 120, 30);
		Btn.addActionListener(this);
			
	
		add(Screen);
		add(Btn);
		add(ScreenOff);
		
		setVisible(true);
		backbuffer = createImage(ScreenOff.getWidth(), ScreenOff.getHeight());
		 a =backbuffer.getGraphics();
		 g= (Graphics2D) a;
		
		Dibuja();
	}
	
	public void Dibuja(){
	
		g.drawLine(10, 10, 100, 10);
		g.drawString("tetetetetet", 10, 20);

	
		ScreenOff.repaint();
	}
	
	public static void main(String [] a){
		new Canvas2();
	}

	
	public void actionPerformed(ActionEvent arg0) {
		
		Dibuja();
		g.setPaint(Color.MAGENTA);
		g.setStroke(new BasicStroke(1));
		g.drawLine(10,95,300,95);
		g.drawImage(ajustarImagen1("AVION.JPG",90,50).getImage(), 10,100,90,50,this);

		ScreenOff.repaint();
		for(int X=20;X<130;X+=10){
			g.drawImage(ajustarImagen1("AVION.JPG",90,50).getImage(), X,100,90,50,this);
			ScreenOff.paintAll(ScreenOff.getGraphics());
	//		OScreen.repaint();
			try{
				Thread.sleep(100);
			} catch (Exception E){}
		}		
		
		
		Screen.repaint();
		
	}
	   private ImageIcon ajustarImagen1(String ico,int Ancho,int Alto)
	    {
	        ImageIcon tmpIconAux = new ImageIcon(ico);
	        //Escalar Imagen
	        ImageIcon tmpIcon = new ImageIcon(tmpIconAux.getImage().getScaledInstance(Ancho, 
	        		Alto, Image.SCALE_SMOOTH));//SCALE_DEFAULT
	        return tmpIcon;
	    }
	class CanvasScreenOf extends Canvas{
		public void paint( Graphics G)
		{	
			G.drawImage(backbuffer, 0, 0,getWidth(), getHeight(), this);
		//	G.dispose();
		}	
	}
}
class XCanvas extends Canvas {
	// se puede a�adir a un frame para visualizarlo
	String cad = "GRUPO DE SISTEMASSS";
	// este metodo se ejecuta automaticamente cuando Java necesita mostrar la ventana
	public void paint(Graphics g) {
	//	super.paint(g);
		
		// obtener el color original
		Color colorOriginal = g.getColor();
		// escribir texto grafico en la ventana y recuadrarlo
		g.drawString(cad, 40, 20);
		g.drawRect(35, 8, (cad.length()*7), 14);
		// dibujo de algunas lineas
		for (int i=20; i< 50; i= i+3) {
		if ((i % 2) == 0) g.setColor(Color.blue);
		else g.setColor(Color.red);
		g.drawLine(40, (90-i), 120, 25+i);
		}
		// dibujo y relleno de un �valo
		g.drawOval(40, 95, 120, 20);
		g.fillOval(40, 95, 120, 20);
		g.setColor(colorOriginal);
	}
}
