package Hanoi2;

public class Modelo {
	Controlador controlador = new Controlador();
	static int Contador=0;
	public void Hanoi(char Inicial,char Central,char Final,int N) {
	   	  if(N==1) {
	         Contador++;
	   	  	System.out.println(Contador + " Move disco "+N+" de la torre "+Inicial+" a la torre "+Final);
	   	  	System.out.println(Inicial - Final);
	   	  	controlador.mueve(Inicial - Final, Inicial, Final);
	   	  }
	   	  else {
	   	  	Hanoi(Inicial,Final,Central,N-1);
	        Contador++;
	   	  	System.out.println(Contador   +    " Move disco "+N+" de la torre "+Inicial+" a la torre "+Final);
	   	  	System.out.println(Inicial - Final);
	   	  	controlador.mueve(Inicial - Final, Inicial, Final);
	   	  	Hanoi(Central,Inicial,Final,N-1);
	   	  }
	 }
}
