package Hanoi2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class AplHanoi2 {
	public AplHanoi2(){
		Vista vista = new Vista();
		Modelo modelo = new Modelo();
		Controlador controlador = new Controlador(modelo, vista);
		vista.setControlador(controlador);
	}
	public static void main(String[] args) {
		new AplHanoi2();

	}

}
