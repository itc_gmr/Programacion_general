package Hanoi2;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Controlador implements ActionListener{
	private static Modelo modelo;
	private static Vista vista;
	public Controlador(){
		this(modelo, vista);
	}
	public Controlador(Modelo m,Vista v){
		modelo = m;
		vista = v;
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() == vista.nTorres){
			if(vista.nTorres.getSelectedIndex() == 0){
				System.out.println("Seleccione un n�mero v�lido");
				return;
			}
			vista.nDiscos = Integer.parseInt(vista.nTorres.getSelectedItem().toString());
			vista.Discos();
			return;
		}
		if(evt.getSource() == vista.inicia){
			modelo.Hanoi(vista.inicial, vista.central, vista.ultimo, vista.nDiscos);
			vista.update(vista.getGraphics());
		}
	}
	public void mueve(int lado, char ini, char fin){
		if(lado < 0){
			vista.MovimientoDer(ini, fin);
		}else{
			vista.MovimientoIzq(ini, fin);
		}
	}
}
