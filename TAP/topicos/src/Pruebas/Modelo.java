package Pruebas;
import java.io.*;
import java.awt.*;
import javax.swing.*;
import Componentes.MyRandom;
public class Modelo {
	private int codigo;
	public void grabar(String nombre)throws IOException{
		nombre = MyRandom.PonBlancos(nombre, 50);
		File F=new File("Nombres.dat");
		RandomAccessFile ArNom= new RandomAccessFile( F,"rw" );
		ArNom.seek(ArNom.length());
		codigo = (int) (ArNom.length()/56);
		codigo++;
		ArNom.writeInt(codigo);
		ArNom.writeUTF(nombre);
		System.out.println(codigo + "   " + nombre);
		ArNom.close();
	}
	public void reporte(JDialog j, Box c, Box n)throws IOException{
		File F=new File("Nombres.dat");
		RandomAccessFile nom= new RandomAccessFile( F,"rw" );
		j = new JDialog();
		c = Box.createVerticalBox();
		n = Box.createVerticalBox();
		j.setModal(true);
		j.setSize(500, 800);
		j.setLayout(new FlowLayout());
		nom.seek(0);
		int cod;
		String nomb;
		for(int i = 0; i<nom.length()/56; i++){
			cod = nom.readInt();
			nomb = nom.readUTF();
			System.out.println(cod + "   " + nomb);
			c.add(new JLabel(cod + ""));
			n.add(new JLabel(nomb));
		}
		nom.close();
		j.add(c);
		j.add(n);
		j.setVisible(true);
	}
}
