package Pruebas;
import java.awt.event.*;
import java.io.IOException;

public class Controlador implements ActionListener{
	private static Modelo modelo;
	private static Vista vista;
	public Controlador(){
		this(modelo, vista);
	}
	public Controlador(Modelo m, Vista v){
		modelo = m;
		vista = v;
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == vista.grabar){
			try {
				System.out.println(vista.leeNombre.getText());
				if(vista.leeNombre.getText().length() == 0){
					System.out.println("No hay texto");
					return;
				}
				modelo.grabar(vista.leeNombre.getText());
				vista.leeNombre.setText("");
			} catch (IOException e1) {e1.printStackTrace();}
		}
		if(e.getSource() == vista.muestraReporte){
			try {
				modelo.reporte(vista.reporte, vista.repCodigo, vista.repNombre);
			} catch (IOException e1) {e1.printStackTrace();}
		}
	}
}
