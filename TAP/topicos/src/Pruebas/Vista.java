package Pruebas;
import java.awt.*;
import javax.swing.*;
import Componentes.MyRandom;
import Componentes.JLeeReal;
import Componentes.JLeeNombre;

public class Vista extends JFrame{
	JLeeReal leeReal;
	
	JLeeNombre leeNombre;
	JButton grabar;
	JButton muestraReporte;
	JLabel nombre;
	
	JDialog reporte;
	Box repCodigo;
	Box repNombre;
	
	public Vista(){
		grabar = new JButton("Grabar");
		leeNombre = new JLeeNombre(50);
		muestraReporte = new JButton("Lista de Nombres");
		nombre = new JLabel("Nombre: ");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(650, 150);
		setLocationRelativeTo(null);
		setLayout(new FlowLayout());
		setResizable(false);
		
		add(nombre);
		add(leeNombre);
		add(grabar);
		add(muestraReporte);
		
		setVisible(true);
	}
	
	private void setRezisible() {
		// TODO Auto-generated method stub
		
	}

	public void setControlador(Controlador c) {
		grabar.addActionListener(c);
		muestraReporte.addActionListener(c);
	}

}
