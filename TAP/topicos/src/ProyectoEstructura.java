import javax.swing.*;

import Componentes.ListaDBL;
import Componentes.NodoDBL;

import java.awt.*;
import java.util.*;
public class ProyectoEstructura {
public static class frecuencia{
	int edad;
	int frec;
	public String toString(){
		return PonCeros(edad, 2);
	}
	public String PonCeros(int Valor,int Tam){
		String Cad=Valor+"";
		while(Cad.length()<Tam)
			Cad="0"+Cad;
		return Cad;
	}
}
public static class persona{
	int edad;
	String nombre;
	
	public String toString(){
		return PonBlancos(nombre, 20);
	}
	public String PonBlancos(String Cad,int Tam){
		while(Cad.length()<Tam)
			Cad=Cad+" ";
		return Cad;
	}	
}
	public static void main(String[] args) {
		JFrame j = new JFrame();
		j.setLocationRelativeTo(null);
		ListaDBL<persona> persona = new ListaDBL<persona>();
		ArbolBB<frecuencia> frecu = new ArbolBB<frecuencia>();
		menu(persona, frecu);
	}
	
	public static void menu(ListaDBL<persona> p, ArbolBB<frecuencia> f){
		while(true){
			int n = 0;
			Scanner leer = new Scanner(System.in);
			System.out.println("......:Men� de Opciones:......");
			System.out.println("1.- Ingresar Datos");
			System.out.println("2.- Reporte de Datos en orden Alfab�tico");
			System.out.println("3.- Reporte de Frecuencia de edades");
			System.out.println("0.- Salir");
			try{
				n = leer.nextInt();
			}catch(Exception e){
				System.out.println("Opci�n inv�lida");
				n = -1;
				continue;
			}
			
			switch(n){
			case 0:
				return;
			case 1:
				persona per = new persona();
				frecuencia fre = new frecuencia();
				System.out.println("Ingrese nombre");
				per.nombre = new Scanner(System.in).nextLine();
				try{
					System.out.println("Ingrese edad");
					per.edad = new Scanner(System.in).nextInt();
					fre.edad = per.edad;
					fre.frec++;
				}catch(Exception e){
					System.out.println("Escribi� algun caracter inv�lido, intentelo de nuevo");
					continue;
				}
				p.InsertaOrd(per);
				if(!f.Inserta(fre)){
					f.Busca(f.DameRaiz(), fre);
					f.Dr.frec++;
				}
				break;
			case 2:
				NodoDBL<persona> aux = p.DameFrente();
				while(aux != null){
					System.out.println("Nombre: " + aux.Info.nombre);
					System.out.println("Edad: " + aux.Info.edad);
					aux = aux.DameSig();
				}
				break;
			case 3:
				frecuEdad(f.DameRaiz());
			}
			
			if(n > 3 || n < 0){
				System.out.println("Opci�n inv�lida");
			}
		}
	}
	public static void frecuEdad(NodoABB<frecuencia> Aux){
		if(Aux==null)
			return;
		frecuEdad(Aux.DameSubDer());
		System.out.println("---------------------------------------");
		System.out.println("Edad: " + Aux.Info.edad);
		System.out.println("Frecuencia: " + Aux.Info.frec);
		frecuEdad(Aux.DameSubIzq());
	}

}
