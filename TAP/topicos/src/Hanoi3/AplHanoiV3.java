package Hanoi3;


public class AplHanoiV3 {
	public AplHanoiV3(){
		Vista vista = new Vista();
		Modelo modelo = new Modelo();
		Controlador controlador = new Controlador(vista, modelo);
		vista.setControlador(controlador);
	}
	public static void main(String[] args) {
		new AplHanoiV3();

	}

}
