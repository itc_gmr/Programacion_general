package Hanoi3;
import java.awt.event.*;

import javax.swing.JOptionPane;
public class Controlador implements ActionListener{
	private Vista vista;
	private Modelo modelo;
	public Controlador(Vista v, Modelo m) {
		vista = v;
		modelo = m;
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() == vista.discos){
			if(vista.discos.getSelectedIndex() == 0){
				return;
			}
			vista.nDiscos = Integer.parseInt(vista.discos.getSelectedItem().toString());
			vista.dibujaDiscos();
			return;
		}
		if(evt.getSource() == vista.inicia){
			if(vista.discos.getSelectedIndex() == 0){
				return;
			}
			return;
		}
	}

}
