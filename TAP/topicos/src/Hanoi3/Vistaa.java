package Hanoi3;

import javax.swing.*;
import java.awt.*;
import Componentes.MyRandom;
import Componentes.Pila;
public class Vistaa extends JFrame {
	Image backbuffer = null;
	Graphics a;
	int nDiscos;
	JButton inicia;
	JComboBox nTorres;
	int alto = getHeight();
	int ancho = getWidth();
	int tX = 200, tY = 50, pX = 50, pY = 500;
	int posXa = 150, posXb = 550, posXc = 950;
	Pila<Dimensiones> tA;
	Pila<Dimensiones> tB;
	Pila<Dimensiones> tC;
	char inicial = 'A', central = 'B', ultimo = 'C';
	public Vistaa(){
		super("Torres de Hanoi V3.0");
		interfaz();
	}
	
	public void paint(Graphics g){
		g.drawImage(backbuffer, 0, 0,ancho, alto, this);
		g.dispose();
	}
	
	public void setControlador(Controladorr C){
		
	}
	
	public void dibuja(){
		super.paint(a);
		a.setColor(Color.darkGray);
		a.fillRect(50, 500, 200, 50);
		a.fillRect(450, 500, 200, 50);
		a.fillRect(850, 500, 200, 50);
		a.drawLine(posXa, 500, 150, 300);
		a.drawLine(posXb, 500, 550, 300);
		a.drawLine(posXc, 500, 950, 300);
		Discos(a);
		repaint();
	}
	public void interfaz(){
		setSize(1200, 750);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
		alto = getHeight();
		ancho = getWidth();
		System.out.println(ancho + "   " + alto);
		backbuffer = createImage(ancho, alto);
		a =backbuffer.getGraphics();
		dibuja();
		
	}
	
	public void Discos(Graphics a){
		//super.paint(a);
		pY -= 50;
		a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(tX),tY).getImage(), (pX), (pY),(tX),tY,this);
		repaint();
		for(int i = pY; i >= 100; i -= 10){
			a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(tX),tY).getImage(), (pX), (i),(tX),tY,this);
			//paintAll(getGraphics());
			repaint();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
			System.out.println(i + "   "+ pY);
			
		}
		a.dispose();
	}
	
	class CanvasScreenOf extends Canvas{
		public void paint( Graphics G){	
			super.paint(G);
			G.drawImage(backbuffer, 0, 0,getWidth(), getHeight(), this);
			G.dispose();
		}	
	}
	
	class Dimensiones{
		int tX;
		int tY;
		int pX;
		int pY;
		public Dimensiones(int x, int y, int xx, int yy){
			tX = x;
			tY = y;
			pX = xx;
			pY = yy;
		}
	}
}
