package Hanoi3;
import java.awt.*;
import javax.swing.*;
import Componentes.*;

public class Vista extends JFrame{
	JButton inicia;
	JButton detener;
	
	JComboBox discos;
	JPanel torres;
	Image backbuffer = null;
	Graphics a;
	
	int nDiscos = 0;
	int posXa = 150, posXb = 550, posXc = 950;
	public Vista(){
		String nT[] = {"Seleccione", "3", "4", "5"};
		inicia = new JButton("Iniciar Torre");
		detener = new JButton("Detener");
		discos = new JComboBox(nT);
		torres = new JPanel();
		
		torres.add(inicia);
		torres.add(detener);
		torres.add(discos);
		
		add(torres, BorderLayout.NORTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(1150,600);
		setVisible(true);	
		//a = torres.getGraphics();
		backbuffer = createImage(torres.getWidth(), torres.getHeight());
		a =backbuffer.getGraphics();
		//torres.setDoubleBuffered(true);
		dibuja(a);
	}
	
	 public void update (Graphics g) {

         a.setColor(Color.RED);
         a.fillRect(0, 0, this.getSize().width, this.getSize().height);
         // Actualiza el Foreground
         //a.setColor(getForeground());
         paint(a);
         // Dibuja la imagen actualizada y con esto ya no se ve parpadeo
         g.drawImage(backbuffer, 0, 0,torres.getWidth(), torres.getHeight(), this);
}
	public void dibuja(Graphics g){
		a.setColor(Color.darkGray);
		a.fillRect(50, 500, 200, 50);
		a.fillRect(450, 500, 200, 50);
		a.fillRect(850, 500, 200, 50);
		a.drawLine(posXa, 500, 150, 300);
		a.drawLine(posXb, 500, 550, 300);
		a.drawLine(posXc, 500, 950, 300);
		paint(a);
		g.drawImage(backbuffer, 0, 0, torres.getWidth(), torres.getHeight(), this);
	}
	
	public void dibujaDiscos(){
		int tX = 200, tY = 50, pX = 50, pY = 500;
		for(int i = 0; i < nDiscos; i++){
			tX = tX - 20;
			pX = pX + 10;
			pY = pY - 50;
			a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(tX),tY).getImage(), (pX), (pY),(tX),tY,this);
			repaint();
		}
	}
	
	public void setControlador(Controlador c) {
		inicia.addActionListener(c);
		detener.addActionListener(c);
		discos.addActionListener(c);
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	/*class CanvasScreenOf extends Canvas{
		Vista vista;
		public void paint( Graphics G){	
			super.paint(G);
			G.drawImage(backbuffer, 0, 0,getWidth(), getHeight(), this);
			//G.drawImage(img, x, y, width, height, observer)
			//G.dispose();
		}	
	}*/
	
	
	class Dimensiones{
		int tX;
		int tY;
		int pX;
		int pY;
		public Dimensiones(int x, int y, int xx, int yy){
			tX = x;
			tY = y;
			pX = xx;
			pY = yy;
		}
	}

}
