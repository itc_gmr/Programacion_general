import Componentes.*;
public class MatrizHilos {

	public static void main(String[] args) {
		int a[][], b[][], r[][];
		while(true){
			int m1 = MyRandom.nextInt(2, 5);
			int n1 = MyRandom.nextInt(2, 5);
			int m2 = MyRandom.nextInt(2, 5);
			int n2 = MyRandom.nextInt(2, 5);
			if(!(n1 == m2)){
				continue;
			}
			System.out.println("Son compatibles");
			System.out.println(m1 + "   "  + n1);
			System.out.println(m2 + "   " + n2);
			System.out.println("---------------");

			a = new int [m1][n1];
			b = new int [m2][n2];
			r = new int [m1][n2];
			generaMatriz(a);
			System.out.println("---------------");
			generaMatriz(b);
			System.out.println("---------------");
			calculoMatriz p = new calculoMatriz(a, b, r, 'p');
			calculoMatriz im = new calculoMatriz(a, b, r, 'i');
			//impares im = new impares(a, b, r);
			p.start();
			im.start();
			while(p.isAlive() || im.isAlive());
			for(int i = 0; i < r.length; i++){
				for(int j = 0; j < r[i].length; j++){
					System.out.print(r[i][j] + "  ");
				}
				System.out.println();
			}
			return;
		}
		
		
	}

	public static void generaMatriz(int ma[][]){
		for(int i = 0; i < ma.length; i++){
			for(int j = 0; j < ma[i].length; j++){
				ma[i][j] = MyRandom.nextInt(1, 2);
				System.out.print(ma[i][j] + "   ");
			}
			System.out.println();
		}
	}
}

class calculoMatriz extends Thread{
	int a[][];
	int b[][];
	int r[][];
	char destino;
	public calculoMatriz(int aa[][], int bb[][], int rr[][], char d){
		a = aa;
		b = bb;
		r = rr;
		destino = d;
	}
	public static int calculoCelda(int a[][], int b[][], int fila, int columna){
		int suma = 0;
		int filaMA[] = fila(a, fila);
		int columnaMB[] = columna(b, columna);
		for(int i = 0; i < filaMA.length; i++){
			suma += filaMA[i] * columnaMB[i];
		}
		return suma;
	}
	
	public static int [] fila(int m[][], int fila){
		int arreglo[] = new int [m[fila].length];
		for(int i = 0; i < arreglo.length; i++){
			arreglo[i] = m[fila][i];
		}
		return arreglo;
	}
	
	public static int [] columna(int m[][], int columna){
		int arreglo[] = new int [m.length];
		for(int i = 0; i < arreglo.length; i++){
			arreglo[i] = m[i][columna];
		}
		return arreglo;
	}

	public void run() {
		for(int i = 0; i < r.length; i++){
			if(destino == 'i'){//impar
				for(int j = 0; j < r[i].length; j++){
					r[i][j] = calculoCelda(a, b, i, j);
				}
				continue;
			}//Par
			for(int j = 0; j < r[i].length; j++){
				r[i][j] = calculoCelda(a, b, i, j);
			}
		}
	}
}



