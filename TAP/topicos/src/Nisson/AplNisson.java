package Nisson;
import Componentes.MyRandom;
import Componentes.Semaforo;

public class AplNisson {

	public static void main(String[] args) {
		lineaDeTrabajo linea [] = new lineaDeTrabajo[MyRandom.nextInt(5, 10)];
		Semaforo CH = new Semaforo(5);
		Semaforo M = new Semaforo(4);
		Semaforo T = new Semaforo(2);
		Semaforo C = new Semaforo(3);
		Semaforo I = new Semaforo(linea.length);
		Semaforo LL = new Semaforo(linea.length);
		Semaforo P = new Semaforo(linea.length);
		
		Semaforo carro = new Semaforo(1);
		carro c = new carro();
		
		for (int i = 0; i < linea.length; i++){
			linea[i] = new lineaDeTrabajo(CH, M, T, C ,I, LL, P, (i+1), c, carro);
			linea[i].start();
		}
	}

}

class lineaDeTrabajo extends Thread{
	Semaforo CH, M, T, C,I, LL, P, carro;
	int lineaT;
	carro c;
	
	public lineaDeTrabajo(Semaforo CH, Semaforo M, Semaforo T, Semaforo C,Semaforo I, Semaforo LL, Semaforo P, int linea, carro c, Semaforo carro){
		this.CH = CH;
		this.M = M;
		this.T = T;
		this.C = C;
		this.I = I;
		this.LL = LL;
		this.P = P;
		lineaT = linea;
		this.c = c;
		this.carro = carro;
	}
	
	public void run(){
		while(true){
			System.out.println("Entrando linea " + lineaT);
			CH.Espera();
			System.out.println("Linea " + lineaT + " instalando chasis.");
			try {sleep(200);} catch (InterruptedException e) {e.printStackTrace();}
			CH.libera();
			M.Espera();
			System.out.println("Linea " + lineaT + " esperando transmisión.");
			try {sleep(60);} catch (InterruptedException e) {e.printStackTrace();}
			T.Espera();
			System.out.println("Linea " + lineaT + " colocando de transmisión.");
			try {sleep(40);} catch (InterruptedException e) {e.printStackTrace();}
			T.libera();
			System.out.println("Linea " + lineaT + " instalación de motor exitosa.");
			try {sleep(60);} catch (InterruptedException e) {e.printStackTrace();}
			M.libera();
			C.Espera();
			System.out.println("Linea " + lineaT + " instalando carrocería.");
			try {sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
			C.libera();
			I.Espera();
			System.out.println("Linea " + lineaT + " instalando interiores.");
			try {sleep(50);} catch (InterruptedException e) {e.printStackTrace();}
			I.libera();
			LL.Espera();
			System.out.println("Linea " + lineaT + " instalando llantas.");
			try {sleep(50);} catch (InterruptedException e) {e.printStackTrace();}
			LL.libera();
			P.Espera();
			System.out.println("Linea " + lineaT + " realizando pruebas.");
			try {sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
			P.libera();
			carro.Espera();
			if(c.noCarros == 100){
				carro.libera();
				return;
			}
			c.noCarros++;
			System.out.println("Carro terminado terminado para linea " + lineaT + " " + c.noCarros);
			System.out.println(c.noCarros == 100);
			carro.libera();
			
		}
	}
}

class carro{
	int noCarros;
}
