
public class MatrizHilos1 {
	static class calculoMatriz extends Thread{
		int a[][];
		int b[][];
		int r[][];
		boolean vb[];
		Semaforo S;
		Semaforo SB;
		public calculoMatriz(int aa[][], int bb[][], int rr[][], boolean boo[], Semaforo ss, Semaforo sb){
			a = aa;
			b = bb;
			r = rr;
			vb = boo;
			S = ss;
			SB = sb;
		}
		
		public boolean checa(boolean V[]){
			for(int i = 0; i < V.length; i++){
				if(!V[i])
					return false;
			}
			return true;
		}
		
		public int calculoCelda(int a[][], int b[][], int fila, int columna){
			int suma = 0;
			int filaMA[] = fila(a, fila);
			int columnaMB[] = columna(b, columna);
			for(int i = 0; i < filaMA.length; i++){
				suma += filaMA[i] * columnaMB[i];
			}
			return suma;
		}
		
		public int [] fila(int m[][], int fila){
			int arreglo[] = new int [m[fila].length];
			for(int i = 0; i < arreglo.length; i++){
				arreglo[i] = m[fila][i];
			}
			return arreglo;
		}
		
		public int [] columna(int m[][], int columna){
			int arreglo[] = new int [m.length];
			for(int i = 0; i < arreglo.length; i++){
				arreglo[i] = m[i][columna];
			}
			return arreglo;
		}

		public void run() {// RU� 
			int ren = 0;
			while(true){
				SB.Espera();
				System.out.println("termino? " + checa(vb));
				if(checa(vb)){
					SB.libera(); 
					return;
				} 
				SB.libera();
				while(true){
					ren = MyRandom.nextInt(a.length);
					S.Espera();
					System.out.println(getName() + "  " +vb[ren] + "  " + ren);
					if(vb[ren]){
						S.libera();
						continue;
					}
					vb[ren] = true;
					S.libera();
					break;
				}
				for(int j = 0; j < r[ren].length; j++){
					r[ren][j] = calculoCelda(a, b, ren, j);
				}
			}
		}
	}

	public static void main(String[] args) {
		int a[][], b[][], r[][];
		calculoMatriz calculoM[] = new calculoMatriz[MyRandom.nextInt(5, 15)];
		boolean VB[];
		//Semaforo VS[];
		Semaforo S = new Semaforo(1);
		Semaforo SB = new Semaforo(1);
		while(true){
			int m1 = MyRandom.nextInt(3, 50);
			int n1 = MyRandom.nextInt(3, 50);
			int m2 = MyRandom.nextInt(3, 50);
			int n2 = MyRandom.nextInt(3, 50);
			if(!(n1 == m2)){
				continue;
			}
			System.out.println("Son compatibles");
			System.out.println(m1 + "   "  + n1);
			System.out.println(m2 + "   " + n2);
			System.out.println("Cantidad de Hilos: " + (calculoM.length));
			System.out.println("---------------");

			a = new int [m1][n1];
			b = new int [m2][n2];
			r = new int [m1][n2];
			generaMatriz(a);
			System.out.println("---------------");
			generaMatriz(b);
			System.out.println("---------------");
			break;
		}
	
		VB = new boolean[a.length];
		//VS = new Semaforo[a.length];
		
		for(int i = 0; i < VB.length; i++){
			VB[i] = false;
			//VS[i] = new Semaforo(1);
		}
		
		for(int i = 0; i < calculoM.length; i++){
			calculoM[i] = new calculoMatriz(a, b, r, VB, S, SB);
		}
		System.out.println(calculoM.length);
		for(int i = 0; i < calculoM.length; i++){
			calculoM[i].start();
		}
		
		
		while(vivos(calculoM));
		for(int i = 0; i < r.length; i++){
			for(int j = 0; j < r[i].length; j++){
				System.out.print(r[i][j] + "\t");
			}
			System.out.println();
		}
	}
	
	public static boolean vivos(calculoMatriz cm[]){
		for(int i =0; i < cm.length;i++){
			if(cm[i].isAlive())
				return true;
		}
		return false;
	}
	
	public static void generaMatriz(int ma[][]){
		for(int i = 0; i < ma.length; i++){
			for(int j = 0; j < ma[i].length; j++){
				ma[i][j] = MyRandom.nextInt(1, 9);
				System.out.print(ma[i][j] + "   ");
			}
			System.out.println();
		}
	}

}
