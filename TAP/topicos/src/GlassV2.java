import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
class GlassV2 extends JFrame implements ActionListener{

	JButton BotonP,BotonQ,glassBoton;
	JButton [] VB;
	JPanel glass,PBtns;
	JLabel Eti;
	boolean Band=true;
	public GlassV2() {
		
		HazInterfaz();
		HazEscuchas();
	}	
	public void HazInterfaz(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		BotonP=new JButton("Poner cristal");
		BotonQ=new JButton("Quitar cristal");
		add(BotonP,BorderLayout.EAST);
		add(BotonQ, BorderLayout.WEST);

		setLocation(200,200);
		setSize(500,400);
		setVisible(true);
		VB=new JButton[5];
		PBtns=new JPanel();
		PBtns.setLayout(new FlowLayout(FlowLayout.RIGHT,30,15));
		for(int i=0;i<VB.length;i++){
			VB[i]=new JButton("Bot�n "+i,new ImageIcon( "Osito1.GIF" ));
			PBtns.add(VB[i]);
		}
			
		add(PBtns);
		
		//pack();

	glass = (JPanel) getGlassPane();
    glass.add(new JLabel("SE PRESENTO UN ERROR EN EL PROCESO DEBES VALIDAR LA CAPTURA"));
	glassBoton = new JButton("pulse click para  quitar el mensaje");
    glass.add(glassBoton);

		
	}
	public void HazEscuchas(){
		   glassBoton.addActionListener(this);
		   BotonP.addActionListener(this);
		   BotonQ.addActionListener(this);
		   
		   for(int i=0;i<VB.length;i++){
			   VB[i].addActionListener(this);
		   }
	}

	public static void main(String [] V){
		new GlassV2();
	}
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource()==BotonQ){
//			VERSI�N 3
			VB[2].setEnabled(  ! VB[2].isEnabled()      );
/*
			VERSION 2
			if (VB[2].isEnabled()){
				VB[2].setEnabled(false);
			} else VB[2].setEnabled(true);
			
 * 	VERSION 1
			if(Band){
				VB[2].setEnabled(false);
				Band=false;
			} else {
				VB[2].setEnabled(true);
				Band=true;
			}
*/			
			return;
		}
		if(evt.getSource()==glassBoton){
			glass.setVisible(false);
			return;
		}
		if(evt.getSource()==BotonP){
			glass.setOpaque(true);
			glass.setVisible(true);
			return;
		}
/*		
			JButton BtnLocal=(JButton) evt.getSource();
			BtnLocal.setBackground(Color.YELLOW);
*/
		int Sub=-1;
		for(int i=0;i<VB.length;i++){
			if(evt.getSource()==VB[i]){
				Sub=i;
				break;
			}
		}
		JButton BtnLocal=(JButton) evt.getSource();
		if(Sub %2==0){
			
			BtnLocal.setBackground(Color.YELLOW);
		} else {
			BtnLocal.setBackground(Color.RED);
		}
		
	}
}