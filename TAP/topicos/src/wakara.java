import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.FlowLayout;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
public class wakara extends JFrame {

public static void main(String... algo) {
SwingUtilities.invokeLater(new Runnable() {
@Override
public void run() {
new wakara();
}
});
}
private PanelGrafico panel;
private int altura;
private int lados;

public wakara() {
try {
UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
} catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
JOptionPane.showMessageDialog(null, e.toString());
}
this.lados = 230;
this.altura = 160;
this.panel = new PanelGrafico();
JFrame app = new JFrame();
app.setLayout(new BorderLayout());
panel.setBackground(Color.ORANGE);
app.add(panel, BorderLayout.CENTER);
app.add(new PanelBotones().getBotones(), BorderLayout.SOUTH);
app.setTitle("Bola Movible");
app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
app.setSize(550, 550);
app.setResizable(false);
app.setVisible(true);
}

public class PanelGrafico extends JPanel {

@Override
public void paintComponent(Graphics g) {
super.paintComponent(g);
Graphics2D graphics = (Graphics2D) (g);
// establece antialiasing (bordes finos)
graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
RenderingHints.VALUE_ANTIALIAS_ON);
graphics.setColor(Color.black);
graphics.fillOval(lados, altura, 100, 100);
}

public void dibujar() {
repaint();
System.out.println("Valor de altura: \t" + altura + "\nValor de lados: \t" + lados);
}
}

public class PanelBotones extends JPanel {

public JPanel getBotones() {

JPanel botones = new JPanel(new FlowLayout());
JPanel botonesContenedor = new JPanel(new BorderLayout());
botones.setBackground(Color.white);
botones.setBorder(new EmptyBorder(30, 60, 0, 60));
botonesContenedor.setBackground(Color.white);
botonesContenedor.setPreferredSize(new Dimension(550, 100));
botonesContenedor.add(botones, BorderLayout.CENTER);

JButton bi = new JButton("Izquierda");
bi.setPreferredSize(new Dimension(90, 35));
bi.addActionListener(new ActionListener() {
@Override
public void actionPerformed(ActionEvent e) {
lados -= 10;
panel.repaint();
}
});
botones.add(bi);

JButton bd = new JButton("Derecha");
bd.setPreferredSize(new Dimension(90, 35));
bd.addActionListener(new ActionListener() {
@Override
public void actionPerformed(ActionEvent e) {
lados += 10;
panel.repaint();
}
});
botones.add(bd);


JButton ba = new JButton("Debajo");
ba.setPreferredSize(new Dimension(90, 35));
ba.addActionListener(new ActionListener() {
@Override
public void actionPerformed(ActionEvent e) {
altura += 10;
panel.repaint();
}
});
botones.add(ba);


JButton bs = new JButton("Arriba");
bs.setPreferredSize(new Dimension(90, 35));
bs.addActionListener(new ActionListener() {
@Override
public void actionPerformed(ActionEvent e) {
altura -= 10;
panel.repaint();
}
});
botones.add(bs);

return botonesContenedor;
}
}
} // end class.
