package mvc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class ConvertidorVista extends JFrame{
	JButton BtnDolar,BtnPeso;
	JTextField Cantidad;
	JLabel Resultado;
	
	public ConvertidorVista(){
		super("Convertidor MVC");
		HazInterfaz();
	}
	
	private void HazInterfaz(){
		setSize(250,150);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(0,2));
		add(new JLabel("Cantidad:",SwingConstants.RIGHT));
		Cantidad=new JTextField(5);
		add(Cantidad);
		BtnDolar=new JButton("D�lar");
		BtnPeso=new JButton("Peso");
		add(BtnDolar);
		add(BtnPeso);
		add(new JLabel("Resultado",SwingConstants.LEFT) );
		Resultado=new JLabel("");
		add(Resultado);
	}
	public void setControlador(ConvertidorControlador C){
		BtnPeso.addActionListener(C);
		BtnDolar.addActionListener(C);
		
	}
	public double getCantidad(){
		double Res=0.0;
		try {
			Res=Double.parseDouble(Cantidad.getText());
		} catch (Exception E){Res=0.0;}
		return Res;
	}
	public void setResultado(double Res){
		Resultado.setText(Res+"");
	}
	public void Muestra(){
		setVisible(true);
	}
}
