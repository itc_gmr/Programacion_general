package mvc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class ConvertidorAplicacion {
	public ConvertidorAplicacion(){
		ConvertidorVista Vista=new ConvertidorVista();
		ConvertidorModelo Modelo=new ConvertidorModelo(19.50);
		ConvertidorControlador Controlador=new ConvertidorControlador(Vista,Modelo);
		Vista.setControlador(Controlador);
		Vista.Muestra();
	}

	public static void main(String[] args) {
		new ConvertidorAplicacion();

	}

}
