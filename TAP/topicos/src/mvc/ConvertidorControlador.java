package mvc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class ConvertidorControlador implements ActionListener{
	ConvertidorModelo Modelo;
	ConvertidorVista  Vista;
	public ConvertidorControlador(ConvertidorVista Vista,ConvertidorModelo Modelo){
		this.Modelo=Modelo;
		this.Vista=Vista;
	}
	
	public void actionPerformed(ActionEvent E){
		if(E.getSource()==Vista.BtnDolar){
			double Cant=Vista.getCantidad();
			double Dolares=Modelo.Dolar(Cant);
			Vista.setResultado(Dolares);
			return;
		}
		if(E.getSource()==Vista.BtnPeso){
			Vista.setResultado(Modelo.Peso(Vista.getCantidad()));
			return;		
		}
	}


	

}
