package mvc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class ConvertidorModelo {
	double Cotizacion;
	public ConvertidorModelo(){
		Cotizacion=16.50;
	}
	public ConvertidorModelo(double Cotizacion){
		this.Cotizacion=Cotizacion;
	}
	public double Dolar(double Cantidad){
		return Cantidad/Cotizacion;
	}
	public double Peso(double Cantidad){
		return Cantidad*Cotizacion;
	}

}
