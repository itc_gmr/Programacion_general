import java.awt.*;
import javax.swing.*;
import java.util.Vector;
public class Tabla extends JFrame {
	JButton Btn;
    public Tabla()  {
        super("Mi tabla");
        HazInterfaz();
        HazEscucha();
    }
    public void HazInterfaz(){
        this.setSize(300,200);
        Vector columnas = new Vector();
        /* A ese vector le agrego datos, estos datos 
           vendr�n a ser las columnas de la tabla.   */
        columnas.addElement("Columna A");
        columnas.addElement("Columna B");
        columnas.addElement("Columna C");
        

        /*
            Creo una instancia de la clase Vector llamada 'filas�,
            este vector tendr� todas las filas de la tabla.
        */
        Vector filas = new Vector();
        /*  objeto llamado 'fila', esto representar� a           una fila en particular y cada elemento que agregue a este vector ser� una celda. */
              Vector fila = new Vector();
              fila.addElement("X");
              fila.addElement("Y");
              fila.addElement("Z");
                 filas.add(fila);
              fila=new Vector();
              fila.addElement("A");
              fila.addElement("B");
              fila.addElement(5);
              filas.addElement(fila);
              
              for(int i=0;i<10;i++){
            	  fila=new Vector();
                  fila.addElement(i+1);
                  fila.addElement(i+2);
                  fila.addElement(i+3);
                  filas.addElement(fila);        	  
              }
              
              
              JTable tbl = new JTable(filas,columnas);
              /* Creo una instancia de JScrollPane y le paso como                  parametro la tabla */
              JScrollPane panel =new JScrollPane(tbl);
              /* Por ultimo agrego ese objeto de JScrollPane al contenedor de la ventana */
              add(panel);
              this.setVisible(true);
              setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
              setLocationRelativeTo(null);

    }
          public void HazEscucha(){
        	  
          }
          public static void main(String[] args)  {
              new Tabla();
          }
      }
