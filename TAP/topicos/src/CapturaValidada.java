import java.awt.*;

import javax.swing.*;

import java.awt.event.*;

public class CapturaValidada extends JFrame implements KeyListener,ActionListener{
	JTextField NoControl,Edad,Estatura;
	JButton BtnGrabar;
	ImageIcon Imagen,Imagen2,Imagen3;
	public CapturaValidada(){
		super("Captura");
		HazInterfaz();
		HazEscucha();
	}
	public void HazInterfaz(){
		NoControl=new JTextField();
		Edad=new JTextField();
		Estatura=new JTextField();
		setSize(400,300);
		setLocationRelativeTo(null);
		
		setLayout(new GridLayout(0,2,10,10));
		add(new JLabel("NoCotrol",SwingConstants.RIGHT));
		add(NoControl);
		add(new JLabel("Edad",SwingConstants.RIGHT));
		add(Edad);
		add(new JLabel("Estatura",SwingConstants.RIGHT));
		add(Estatura);
		Imagen=new ImageIcon( "Osito1.gif" );
		Imagen2=new ImageIcon( "Maquina.JPG" );
		Imagen3=new ImageIcon( "Persona1.JPG" );	
		
	//	new JButton("Bot�n "+i,new ImageIcon( "Osito1.GIF" ));
		BtnGrabar=new JButton("Grabar",Imagen);
		BtnGrabar.setRolloverIcon(Imagen2);
		BtnGrabar.setPressedIcon(Imagen3);
		add(BtnGrabar);
		setVisible(true);
	}
	public void HazEscucha(){
		NoControl.addActionListener(this);
		NoControl.addKeyListener(this);
		
		Edad.addActionListener(this);
		Edad.addKeyListener(this);

		Estatura.addActionListener(this);
		Estatura.addKeyListener(this);

		
	}
	public void actionPerformed(ActionEvent E){
		if(E.getSource() instanceof JTextField){
			//TRANSFERIR EL FOCO AL SIG.COMPONENTE
	        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
	        manager.focusNextComponent();
	        
			if(E.getSource()==NoControl){
	
				return;
			}
			
			//nextFocus();
			
		}
	}
	
	
	
	public void keyPressed(KeyEvent E){
		if(E.isControlDown()){
	    	 if(E.getKeyCode() == KeyEvent.VK_X  ||
	    	 	E.getKeyCode() == KeyEvent.VK_C ||
	    	 	E.getKeyCode() == KeyEvent.VK_V){
	    		 E.consume();
	    		 return;
            }
      }		

	}
	public void keyReleased(KeyEvent E){
		System.out.println("LIBERADO");
	}
	public void keyTyped(KeyEvent E){
	
		
		if(E.getSource()==NoControl){
			Metodo(E,8);

		}
		if(E.getSource()==Edad){
			Metodo(E,3);

		}
		if(E.getSource()==Estatura){
			Metodo(E,4);

		}		
	}
	public void Metodo(KeyEvent E,int Tamano){
		char T=E.getKeyChar();
		JTextField Txt=(JTextField) E.getSource();
		if(Txt.getText().length()==Tamano ){
			E.consume();
			Toolkit.getDefaultToolkit().beep();
			return;
		}
		if( T<'0' || T>'9'){
			E.consume();
			Toolkit.getDefaultToolkit().beep();
			return;
		}		
		return;		
	}
	public static void main(String[] args) {
		new CapturaValidada();


	}

}