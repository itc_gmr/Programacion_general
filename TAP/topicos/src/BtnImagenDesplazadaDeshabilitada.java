import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class BtnImagenDesplazadaDeshabilitada extends JFrame implements ActionListener {

	JButton [] VB;
	Icon Imagen2,Imagen3;
	JButton BtnGrabar,BtnLimpiar;
	ImageIcon Imagen;
	JPanel Panel;

	public BtnImagenDesplazadaDeshabilitada(){
		super("Botones con imagen");
		HazInterfaz();
		HazEscuchas();
	}
	public void HazInterfaz(){
		Imagen=new ImageIcon( "Osito1.gif" );
		Imagen2=new ImageIcon( "Maquina.JPG" );
		Imagen3=new ImageIcon( "Persona1.JPG" );
		Panel=new JPanel(new GridLayout(0,2,5,5));
		Panel.setBounds(10,10,400,500);
		setLayout(null);
		VB=new JButton[10];

		for(int i=0;i<VB.length;i++){

			VB[i]=new JButton("Btn # "+(i+1),Imagen);
			VB[i].setVerticalTextPosition(JLabel.TOP);
			VB[i].setMnemonic(i+49);
			Panel.add(VB[i]);
		}

		BtnGrabar=new JButton("Grabar",Imagen);
		BtnGrabar.setRolloverIcon(Imagen2);
        		BtnGrabar.setPressedIcon(Imagen3);
   //     BtnGrabar.doClick(10500);







		BtnLimpiar=new JButton("Limpiar");
		Panel.add(BtnGrabar);
		Panel.add(BtnLimpiar);
		add(Panel);
		setSize(400,500);
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void HazEscuchas(){
		for(int i=0;i<VB.length;i++)
			VB[i].addActionListener(this);
		BtnLimpiar.addActionListener(this);
		BtnGrabar.addActionListener(this);
	}

	public static void main(String [] a){
		new BtnImagenDesplazadaDeshabilitada();
	}
	public void actionPerformed(ActionEvent e){

/*
		//version 1
		if( e.getSource() == VB[0]){
			VB[0].setIcon(Imagen2);
		}

		version 2
		int Sub=-1;
		for(int i=0;i<VB.length;i++){
			if(e.getSource()==VB[i]){
				Sub=i;
				break;
			}
		}
		VB[Sub].setIcon(Imagen2);
*/
		if (e.getSource() instanceof JButton ){

			if(e.getSource()==BtnLimpiar){

				Panel.remove(VB[0]);
				Panel.update(Panel.getGraphics());
			//	for(int i=0;i<VB.length;i++)
			//		VB[i].setIcon(null);

				return;
			}
			if(e.getSource()==BtnGrabar){

				for(int i=0;i<VB.length;i++)
					VB[i].setIcon(Imagen);


				return;
			}

			int Sub=-1;
			for(int i=0;i<VB.length;i++){
				if(e.getSource()==VB[i]){
					Sub=i;
					break;
				}
			}
			Icon Tra=Imagen2;
			if((Sub+1)%2==0){
				Tra=Imagen3;
			}
			JButton Btn=(JButton) e.getSource();
		//	Btn.setContentAreaFilled(false);
		 	Btn.setDisabledIcon(Tra);
			Btn.setEnabled(false);
		//	Btn.setIcon(Tra);

			return;
		}


	}

}
