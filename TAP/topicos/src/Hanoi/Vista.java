package Hanoi;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import Componentes.MyRandom;
public class Vista extends JFrame{//Metodo para crear discos dependiendo del numero en el combo
	JButton inicia;
	JComboBox nTorres;
	//int alto, ancho;
	int yGenerico = 500;
	//Image backbuffer = null;
	JPanel torres, norte;
	Graphics a;
	public Vista(){
		super("Torres de Hanoi");
		interfaz();
	}
	public void setControlador(Controlador C){
		
	}
	public void interfaz(){
		String nT[] = {"#Discos", "3", "4", "5", "6"};
		torres = new JPanel();
		norte = new JPanel();
		inicia = new JButton("Iniciar...");
		nTorres = new JComboBox(nT);
		//a = backbuffer.getGraphics();
		a = torres.getGraphics();
		inicia.setFont(new Font("Calibri",Font.ITALIC,30));
		nTorres.setFont(new Font("Calibri",Font.ITALIC,30));
		norte.add(inicia);
		norte.add(nTorres);
		add(norte, BorderLayout.NORTH);
		add(torres);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(1200, 700);
		setVisible(true);
		
		
		
	}
	
	public void paint( Graphics a)
	{	
		super.paint(a);
		a.setColor(Color.darkGray);
		a.fillRect(100, yGenerico, 100, 50);
		a.fillRect(500, yGenerico, 100, 50);
		a.fillRect(900, yGenerico, 100, 50);
		a.drawLine(150, yGenerico, 150, 300);
		a.drawLine(550, yGenerico, 550, 300);
		a.drawLine(950, yGenerico, 950, 300);
		a.setColor(Color.blue);;
		a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",70,50).getImage(), 115,450,70,50,this);
		inicia.grabFocus();
		nTorres.grabFocus();
	}	
}
