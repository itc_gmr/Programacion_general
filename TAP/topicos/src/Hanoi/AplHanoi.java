package Hanoi;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class AplHanoi {
	public AplHanoi(){
		Vista vista = new Vista();
		Modelo modelo = new Modelo();
		Controlador controlador = new Controlador(modelo, vista);
		vista.setControlador(controlador);
	}
	public static void main(String[] args) {
		new AplHanoi();

	}
	
}
