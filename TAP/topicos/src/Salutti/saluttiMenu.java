package Salutti;

public class saluttiMenu {
	public saluttiMenu(){
		Vista vista = new Vista();
		Modelo modelo = new Modelo();
		Controlador controlador = new Controlador(vista, modelo);
		vista.setControlador(controlador);
	}
	
	public static void main(String[] args) {
		new saluttiMenu();
	}

}
