package Salutti;
import java.awt.*;
import javax.swing.*;
import Componentes.*;
public class Vista extends JFrame{
	JPanel abajo, arriba;
	JTable tabla;
	JScrollPane scrollTabla;
	
	JComboBox articulos;
	JButton guardar;
	
	JLabel imagen;
	
	public Vista(){
		super("Salutti");
		interfaz();
	}
	
	private void interfaz(){
		JPanel abajo = new JPanel();
		JPanel arriba = new JPanel();
		
		imagen = new JLabel();
		ImageIcon s = new ImageIcon(MyRandom.ajustarImagen1("salutti.JPG", 150, 150).getImage());
		imagen.setIcon(s);
	
		articulos = new JComboBox();
		guardar = new JButton("Guardar");
		guardar.setFont(new Font("Calibri",Font.BOLD,25));
		
		abajo.add(guardar);
		arriba.add(imagen);
		add(abajo, BorderLayout.SOUTH);
		add(arriba, BorderLayout.NORTH);
		
		setSize(800,600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public void setControlador(Controlador c) {
		
	}

}
