import javax.swing.*; import java.awt.*; import java.awt.event.*;
class FrameBoxLayout extends JFrame {
	ButtonGroup Grupo=new ButtonGroup();
	public FrameBoxLayout() {
	    super("**Box**");
	     // el marco por default tiene BorderLayout
         setSize( 500, 150 );
        setLocation(100,300);
	    JPanel Panel1=new JPanel();  Panel1.setLayout(new BoxLayout(Panel1,BoxLayout.Y_AXIS));
	    JPanel Panel2=new JPanel();   Panel2.setLayout(new BoxLayout(Panel2,BoxLayout.X_AXIS));
	    Panel1.add(new JButton("Aceptar")); Panel1.add(new JButton("Cancelar")); 
	    Panel1.add(new JButton("    Salir     ")); 
        add(Panel1,BorderLayout.EAST);
        Panel2.add(new JLabel("Nombre")); Panel2.add(new JTextField(10));
        Panel2.add(new JLabel("Sexo")); Panel2.add(new JTextField(2));
        for(int i=0;i<15;i++)
        	Panel2.add(new JButton());
        add(Panel2,BorderLayout.NORTH);
     	    JPanel Panel3=new JPanel(); 
     	    Panel3.setLayout(new BoxLayout(Panel3,BoxLayout.Y_AXIS));
         JRadioButton Radio1=new JRadioButton("Mascúlino");
         JRadioButton Radio2=new JRadioButton("Femenino");
         Grupo.add(Radio1);
         Grupo.add(Radio2);
         Panel3.add(new JLabel("Seleccione sexo")); 
         Panel3.add(Radio1);         Panel3.add(Radio2);
        add(Panel3,BorderLayout.WEST);         setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
	}
	public static void main(String [] a) {
         new FrameBoxLayout();

	}
}
