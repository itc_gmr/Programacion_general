package Aviones;
import java.awt.*;
import javax.swing.*;

import Componentes.MyRandom;
import Componentes.Semaforo;
public class Vista extends JFrame{
	ImageIcon p;
	JLabel imagen;
	public Vista(){
		super("Vuelos");
		interfaz();
	}
	
	private void interfaz(){
		p = new ImageIcon();
		p = MyRandom.ajustarImagen1("pista.JPG", 800, 100);
		imagen = new JLabel();
		imagen.setIcon(p);
		avionSiguiente aS = new avionSiguiente(1);
		Semaforo S = new Semaforo(1);
		
		int nAviones = MyRandom.nextInt(10, 20);
		avion a []= new avion[nAviones];
		Thread av []= new Thread[nAviones];
		for(int i = 0; i < av.length; i++){
			a[i] = new avion(MyRandom.nextColor(), (i+1), aS, S);
			av[i] = new Thread(a[i]);
			add(a[i]);
			av[i].start();
		}
		
		
		setLayout(null);
	//	add(imagen, 0, 500);
		setSize(800,600);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}

	public void setControlador(Controlador c) {
		
	}

}

class avion extends JButton implements Runnable{
	Color b;
	int cont = MyRandom.nextInt(500);
	int x = MyRandom.nextInt(0, 700), y = MyRandom.nextInt(1,150);
	int limIzq = 0, limDer = 700;
	boolean band = true;
	int turno, repeticiones = 0;
	avionSiguiente avionSig;
	Semaforo S;
	
	public avion(Color b, int turno, avionSiguiente av, Semaforo S){
		super("AVION");
		setSize(100, 50);
		
		this.b = b;
		this.turno = turno;
		this.avionSig = av;
		this.S = S;
		
		ImageIcon p = new ImageIcon();
		p = MyRandom.ajustarImagen1("avion.PNG", 100, 50);
		setIcon(p);
		setLocation(x,y);
	}
	
	public void run() {
		
		while(true){
			if(band){
				band = false;
				for(int i = x; i < limDer; i+=10){
					setLocation(i, y);
					try{Thread.sleep(30);}catch(Exception e){}
					if(MyRandom.nextInt(1000) == MyRandom.nextInt(1000)){
						if(movVertical(i)){
							setVisible(false);
							System.out.println("Repeticiones fallidas: " + repeticiones);
							return;
						}
							
					}
				}
			}else{
				for(int i = 0; i < limDer; i+=10){
					setLocation(i, y);
					try{Thread.sleep(30);}catch(Exception e){}
					if(MyRandom.nextInt(1000) == MyRandom.nextInt(1000)){
						if(movVertical(i)){
							setVisible(false);
							System.out.println("Repeticiones fallidas: " + repeticiones);
							return;
						}
							
					}
				}
			}
			
			for(int i = limDer; i > limIzq; i-=10){
				setLocation(i, y);
				try{Thread.sleep(30);}catch(Exception e){}
				if(MyRandom.nextInt(1000) == MyRandom.nextInt(1000)){
					if(movVertical(i)){
						setVisible(false);
						System.out.println("Repeticiones fallidas: " + repeticiones);
						return;
					}
						
				}
			}
		}
		
	}
	
	public boolean movVertical(int ex){
		System.out.println("Es tu turno????");
		System.out.println(avionSig.avionS + "   " + turno);
		S.Espera();
		if(avionSig.avionS == turno){
			//S.Espera();
			for(int i = y; i < 500; i += 10){
				setLocation(ex, i);
				try{Thread.sleep(30);}catch(Exception e){}
			}
			avionSig.avionS++;
			try {Thread.sleep(2000);} catch (InterruptedException e) {e.printStackTrace();}
			S.libera();
			return true;
		}
		//S.libera();
		for(int i = y; i < 280; i += 10){
			setLocation(ex, i);
			try{Thread.sleep(30);}catch(Exception e){}
		}
		for(int i = 280; i > y; i -= 10){
			setLocation(ex, i);
			try{Thread.sleep(30);}catch(Exception e){}
		}
		repeticiones++;
		S.libera();
		return false;
	}
}

class avionSiguiente{
	int avionS;
	public avionSiguiente(int av){
		avionS = av;
	}
}
