
import java.util.Random;
public class AplRecursiva {
	public AplRecursiva(){
		Recursividad Obj=new Recursividad();
		System.out.println("8!="+Obj.Fact(8));
	//	ImpFib(Obj);
		ImpEleva(Obj);
		int [] V={3,5,7,7,5,3,2,1,6,6,6,6,6};
		int [] VF=new int[10];
		System.out.println("Suma "+Obj.Suma(V,V.length));
		System.out.println("Menor "+Obj.Menor(V,V.length));
		Obj.Frecuencia(V,V.length,VF);
		for(int i=0;i<VF.length;i++)
			System.out.println((i+1)+"\t"+VF[i]);
		
		for(int i=5;i<200;i+=5){
			System.out.print(i+"\t"+Obj.Binario(i)+"\t"+Obj.Octal(i));
		     System.out.println("\t"+Obj.Convierte(i,2)+"\t"+Obj.Convierte(i,8)+"\t"+Obj.Convierte(i,16));
		}
		
		Random R=new Random();
		System.out.println("M�ximo com�n de dos n�meros");
		for(int i=0;i<10;i++){
			int N1=R.nextInt(56)+15;
			int N2=R.nextInt(56)+15;
			System.out.println("M�ximo com�n denominador del "+N1+"  "+N2+" ="+Obj.MCD(N1,N2));
		}
		System.out.println("****TORRE DE HANOI **********");
		Obj.Hanoi('A','B','C',3);
	}

	public void ImpEleva(Recursividad Obj){
		for(int i=1;i<60;i++){
			System.out.println("2 a la "+i+" ="+Obj.Eleva(2,i));
		}
		System.out.println("===2 a la 200 ="+Obj.Eleva(2,200));
	}	
	public void ImpFib(Recursividad Obj){
		for(int i=1;i<60;i++){
			System.out.println("No de la seria = "+i+" ="+Obj.Fib(i));
		}
		
	}
	public static void main(String[] args) {
		new AplRecursiva();
	}

}
