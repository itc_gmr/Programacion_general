package HanoiV4;
import java.awt.*;
import java.awt.image.ImageObserver;

import javax.swing.*;

import Componentes.*;
import HanoiV4.Dimension;
public class Vista extends JFrame{
	Image backbuffer = null;
	Graphics g;
	int posXa = 150, posXb = 550, posXc = 950;
	int discos = 0;
	//ListaDBL<Dimension> mov = new ListaDBL<Dimension>();
	Pila<Dimension> A;
	
	JDialog preferencias;
	JButton iniciar, reiniciar;
	JComboBox nDiscos;
	
	public Vista(){
		super("Torres de Hanoi V4.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setLocationRelativeTo(null);
		String vT[] = {"Seleccione", "3", "4", "5", "6", "7"};
		iniciar = new JButton("Iniciar");
		reiniciar = new JButton("Reiniciar");
		reiniciar.setEnabled(false);
		nDiscos = new JComboBox(vT);
		
		preferencias = new JDialog();
		preferencias.setLayout(new FlowLayout());
		preferencias.add(iniciar);
		preferencias.add(reiniciar);
		
		preferencias.add(nDiscos);
		
		setSize(1150, 800);
		preferencias.setSize(350,100);
		
		setVisible(true);
		//preferencias.setModal(true);
		preferencias.setVisible(true);
		preferencias.setDefaultCloseOperation(0);
		backbuffer = createImage(getWidth(), getHeight());
		g = backbuffer.getGraphics();
		Dibuja();
	}
	
	public void Dibuja(){
		super.paint(g);
		g.setColor(Color.darkGray);
		g.fillRect(100, 500, 200, 50);
		g.fillRect(500, 500, 200, 50);
		g.fillRect(900, 500, 200, 50);
		g.drawLine(200, 500, 200, 300);
		g.drawLine(600, 500, 600, 300);
		g.drawLine(1000, 500, 1000, 300);
		repaint();
	}
	
	public void setDiscos(Graphics a){
		A = new Pila<Dimension>(discos);
		int tX = 200, tY = 50, pX = 50, pY = 500;
		for(int i = 0; i < discos; i++){
			tX = tX - 20;
			pX = pX + 10;
			pY = pY - 50;
			A.Inserta(new Dimension(tX, tY, pX, pY, 0, 0, 0));
			a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(tX),tY).getImage(), (pX), (pY),(tX),tY,this);
			repaint();
		}
	}
	
	public void movimiento(Dimension []mov, Graphics a){
		//super.paint(a);
		for(int i = 0; i < mov.length;i++){
			System.out.println(i);
			if(mov[i].direccion < 0){
				System.out.println("Derecha");
				for(int j = mov[i].pYant; j >= 0; j -= 10){
					a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(mov[i].tX),mov[i].tY).getImage(), (mov[i].pXant), (j),mov[i].tX,mov[i].tY,this);
					a.finalize();
					paintAll(getGraphics());
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
				for(int j = mov[i].pXant; j <= mov[i].pX; j += 10){
					a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(mov[i].tX),mov[i].tY).getImage(), (j), (0),mov[i].tX,mov[i].tY,this);
					a.finalize();
					paintAll(getGraphics());
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
				for(int j = 0; j <= mov[i].pY; j += 10){
					a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(mov[i].tX),mov[i].tY).getImage(), (mov[i].pX), (j),mov[i].tX,mov[i].tY,this);
					a.finalize();
					paintAll(getGraphics());
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
			}else{
				System.out.println("Izquierda");
				for(int j = mov[i].pYant; j >= 0; j -= 10){
					a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(mov[i].tX),mov[i].tY).getImage(), (mov[i].pXant), (j),mov[i].tX,mov[i].tY,this);
					a.finalize();
					paintAll(getGraphics());
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
				for(int j = mov[i].pXant; j >= mov[i].pX; j -= 10){
					a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(mov[i].tX),mov[i].tY).getImage(), (j), (0),mov[i].tX,mov[i].tY,this);
					a.finalize();
					paintAll(getGraphics());
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
				for(int j = 0; j <= mov[i].pY; j += 10){
					a.drawImage(MyRandom.ajustarImagen1("AVION.JPG",(mov[i].tX),mov[i].tY).getImage(), (mov[i].pX), (j),mov[i].tX,mov[i].tY,this);
					a.finalize();
					paintAll(getGraphics());
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
			}
		}
	}
	
	public void setControlador(Controlador c) {
		nDiscos.addActionListener(c);
		iniciar.addActionListener(c);
		//reiniciar.addActionListener(c);
	}
	
	public void paint(Graphics a){
		a.drawImage(backbuffer, 0, 0, getWidth(), getHeight(), this);
		//a.dispose();
		
	}

}

