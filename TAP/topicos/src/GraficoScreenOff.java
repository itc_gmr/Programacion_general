import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
public class GraficoScreenOff extends JFrame implements ActionListener, MouseListener{
	JButton Btn=new JButton("Avanza");
	JButton BtnR=new JButton("Regresa");
	
	Graphics g;
	Image backbuffer = null;
	boolean BandTri=false;
	int X=30;
	int Y=300;
	public GraficoScreenOff(){
		super("Graficando");
		setLayout(null);
		Btn.setBounds(0,30,80,30);
		BtnR.setBounds(100,30,80,30);
		
		setSize(400,400);
		setLocationRelativeTo(null);
		add(Btn);
		add(BtnR);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Btn.addActionListener(this);
		

		// SCREEN OFF
		backbuffer = createImage(getWidth(), getHeight());
		g =backbuffer.getGraphics();
		
		Btn.addActionListener(this);
		BtnR.addActionListener(this);
		BtnR.addMouseListener(this);
		Dibuja();	
		
	}
	public void Dibuja(){
		super.paint(g);
		Color Color=new Color(192,128,64);
		g.setColor(Color);

		ImageIcon icon = new ImageIcon("Maquina.JPG");
		Image img = icon.getImage(); //convertimos icon en una imagen
		Image otraimg = img.getScaledInstance(getWidth(),getHeight(),java.awt.Image.SCALE_SMOOTH);
		ImageIcon ImagenFondo = new ImageIcon(otraimg);
		g.drawImage(ImagenFondo.getImage(),0,0,getWidth(),getHeight(),null);

	       g.drawString("INSTITUTO TECNOL�GICO DE CULIAC�N",50,40);
	        g.drawLine(20,45,300,45);
	        g.drawString("INGENIER�A EN SISTEMAS COMPUTACIONALES",50,60);
	        g.setColor(Color.BLUE);
	        g.fillRect(50,70,100,50);
	        g.fillRoundRect(250, 70,100, 50, 20, 20);
	        Color CAFE=new Color(192,128,64);
	        g.setColor(CAFE);
	        g.fillRect(50,150,200,100);
	        g.setColor(Color.RED);
	        g.fillOval(60,220,60,60);
	        g.fillOval(190,220,50,60);
	        g.setColor(Color.WHITE);
	        g.fillRect(200,160,50,30);
	        g.setColor(CAFE);
	        g.fillRect(X,Y,60,40);
	        
	        Metodo(g);
	        

	        repaint();
	        Btn.grabFocus();
	        BtnR.grabFocus();
	}	
    private ImageIcon ajustarImagen1(String ico,int Ancho,int Alto)
    {
        ImageIcon tmpIconAux = new ImageIcon(ico);
        //Escalar Imagen
        ImageIcon tmpIcon = new ImageIcon(tmpIconAux.getImage().getScaledInstance(Ancho, 
        		Alto, Image.SCALE_SMOOTH));//SCALE_DEFAULT
        return tmpIcon;
    }
	public void Metodo(Graphics g){
		g.setColor(Color.RED);
		g.drawLine(0,300,300,300);
		g.drawImage(ajustarImagen1("Osito1.GIF",50,30).getImage(),
				250,350,50,30,this);
		
		if(BandTri){
	//		g.drawLine(250,300,200,350);
		//	g.drawLine(250,300,300,350);
			//g.drawLine(200,350,300,350);
			g.setColor(Color.BLUE);
	//		int [] VX={200,250,300,290,170};
		//	int [] VY={300,250,300,180,280};
			
			int [] VX={100,150,200,250,280,270,255,255,150};
			int [] VY={300,280,250,200,280,350,350,180,280};
			
			g.fillPolygon(VX, VY, VX.length);
			
		}
		
		
		
	}
	public static void main(String [] a){
		new GraficoScreenOff ();
	}
	public void actionPerformed(ActionEvent E){

		if(E.getSource()==Btn && X<250)
		    X+=10;
		else{
			if(X<10)
				return;
			X-=10;
		}
		Dibuja();
		Btn.grabFocus();
		paintAll(getGraphics());
	}
	public void paint( Graphics g) 	{

		g.drawImage(backbuffer, 0, 0, getWidth(), getHeight(), this);
	//	g.dispose();
		
	       
	}

	public void mouseClicked(MouseEvent arg0) {
		
		
	}
	
	public void mouseEntered(MouseEvent E) {


			BandTri=true;
			Dibuja();
			

		
	}
	
	public void mouseExited(MouseEvent arg0) {
		
		BandTri=false;
		Dibuja();
	}
	
	public void mousePressed(MouseEvent arg0) {
		
		
	}
	
	public void mouseReleased(MouseEvent arg0) {
		
		
	}

}