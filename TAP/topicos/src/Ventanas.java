
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import Componentes.ListaDBL;

public class Ventanas extends JFrame implements ActionListener, FocusListener{
	public static class persona{
		int edad;
		String nombre;
		
		public String toString(){
			return PonBlancos(nombre, 20);
		}
		public String PonBlancos(String Cad,int Tam){
			while(Cad.length()<Tam)
				Cad=Cad+" ";
			return Cad;
		}	
	}
	
	JButton reporte = new JButton("Reporte");
	JButton capturaD = new JButton("Captura");
	
	JLabel nombre = new JLabel("Nombre", SwingConstants.RIGHT);
	JLabel edad = new JLabel("Edad", SwingConstants.RIGHT);
	
	JTextField fieldNom = new JTextField();
	JTextField fieldEd = new JTextField(new Integer(3));
	
	JPanel panelN = new JPanel();
	
	Font inicio;
	
	ListaDBL<persona> lista = new ListaDBL<persona>();
	public Ventanas(){
		interfaz();
		escuchadores();
	}
	
	public void interfaz(){
		panelN.setLayout(new GridLayout(0,2,20,20));
		panelN.add(nombre);
		panelN.add(fieldNom);
		panelN.add(edad);
		panelN.add(fieldEd);
		panelN.add(capturaD);
		panelN.add(reporte);
		inicio = fieldNom.getFont();
		setBounds(500,500,400,200);
		add(panelN);
		setVisible(true);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
	}
	
	public void escuchadores(){
		fieldNom.addFocusListener(this);
		fieldEd.addFocusListener(this);
		capturaD.addActionListener(this);
	}
	public void actionPerformed(ActionEvent AE){
		if(AE.getSource() == capturaD){
			String nom = fieldNom.getText();
			int eda;// = fieldEd.getText();
		}
	}
	public void focusGained(FocusEvent FE){
		if(FE.getSource() == fieldNom){
			Font fuente=new Font("Calibri", Font.BOLD, 22);
			fieldNom.setFont(fuente);
		}
		if(FE.getSource() == fieldEd){
			Font fuente=new Font("Calibri", Font.BOLD, 22);
			fieldEd.setFont(fuente);
		}
	}
	public void focusLost(FocusEvent FE){
		if(FE.getSource() == fieldNom){
			fieldNom.setFont(inicio);
		}
		if(FE.getSource() == fieldEd){
			fieldEd.setFont(inicio);
		}
	}

	public static void main(String[] args) {
		new Ventanas();

	}

}
