import javax.swing.*; 
import java.awt.*; import java.awt.event.*;
class FrameGridLayout extends JFrame implements FocusListener,ActionListener {
        JTextField Nombre=new JTextField(20);
        JTextField NoControl;
        Font FondTxt;
        JButton Grabar;
      public FrameGridLayout() {
          super("**Grid**");
    	  HazInterfaz();
    	  HazEscuchas();
   }
   public void HazInterfaz() {
       setLayout(new GridLayout(3,2,5,10));
       setSize( 350, 150 );
       setLocation(100,300);
       JLabel Etiqueta1 = new JLabel("No.Control",SwingConstants.RIGHT);
       add(Etiqueta1);
      NoControl=new JTextField(10);
 //     NoControl.setFont(new Font("Arial",Font.BOLD ,42));
      add(NoControl);
      JLabel Etiqueta2 = new JLabel("Nombre",SwingConstants.RIGHT);
      add(Etiqueta2);
      add(Nombre);
      Grabar=new JButton("Grabar");
      add(Grabar);
      JButton BtnSalir;
      add(BtnSalir=new JButton("Limpiar",new ImageIcon( "Osito1.GIF" )));
//      JButton Btn=new JButton(new ImageIcon( "Caballo.JPG" ));
      setVisible(true);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setLocationRelativeTo(null);
      setIconImage (new ImageIcon("Osito1.GIF").getImage());
      
      FondTxt=NoControl.getFont();
   //   BtnSalir.setEnabled(false);
   }
   public void HazEscuchas(){
	   NoControl.addFocusListener(this);
	   NoControl.addActionListener(this);
	   
	   Nombre.addFocusListener(this);
	   Nombre.addActionListener(this);
	   
	   Grabar.addActionListener(this);
   }
   public void focusGained(FocusEvent e){
	   JTextField Txt=(JTextField) e.getSource();
	   Txt.setFont(new Font("Arial",Font.ITALIC,22));
	   Txt.selectAll();
   }
   public void focusLost(FocusEvent e){
	   JTextField Txt=(JTextField) e.getSource();
	   Txt.setFont(FondTxt);	   
   }
   public void actionPerformed(ActionEvent E){
	   if(E.getSource()==NoControl){
		   Nombre.requestFocus();
		   return;
	   }
	   if(E.getSource()==Nombre){
		   Grabar.requestFocus();
		   return;
	   }
	   if(E.getSource()==Grabar){
		   if(NoControl.getText().length()==0){
			   JOptionPane.showMessageDialog (null," proporcione n�mero de control " );
			   NoControl.requestFocus();
			   return;
		   }
		   
		   NoControl.setText("");
		   Nombre.setText("");
		   NoControl.requestFocus();
		   
	   }
   }
    
   public static void main(String [] a) {
    new FrameGridLayout();

   }
}