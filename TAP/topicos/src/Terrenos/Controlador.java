package Terrenos;
import java.awt.event.*;
public class Controlador implements ActionListener{
	
	Vista vista;
	public Controlador(Vista vista) {
		this.vista = vista;
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() == vista.inicia){
			System.out.println("Iniciando hilos...");
			vista.inicia.setEnabled(false);
			vista.reporte.setEnabled(true);
			vista.iniciar();
			return;
		}
		if(evt.getSource() == vista.reporte){
			vista.reporte();
			return;
		}
	}

}
