package Terrenos;
import Componentes.*;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class Vista extends JFrame{
	JPanel norte, centro;
	JButton mB [][], inicia, reporte;
	
	Lista<reporteHnos> reportes = new Lista<reporteHnos>();
	
	public Vista(){
		super("Terrenos");
		setInterfaz();
	}
	
	public void setInterfaz(){
		norte = new JPanel();
		centro = new JPanel();
		inicia = new JButton("Iniciar");
		reporte = new JButton("Lanzar Reporte");
		reporte.setEnabled(false);
		
		mB = new JButton[10][10];
		centro.setLayout(new GridLayout(0, mB.length));
		int cont = 0;
		for(int i = 0; i < mB.length; i++){
			for(int j = 0; j < mB[i].length; j++){
				mB[i][j] = new JButton(""+ ++cont);
				mB[i][j].setBackground(Color.CYAN);
				centro.add(mB[i][j]);
			}
		}
		norte.add(inicia);
		norte.add(reporte);
		add(norte, BorderLayout.NORTH);
		add(centro);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1200, 800);
		setVisible(true);
		
	}
	
	public void setControlador(Controlador c) {
		inicia.addActionListener(c);
		reporte.addActionListener(c);
	}
	
	public void reporte(){
		JDialog repo = new JDialog();
		repo.setSize(400, 300);
		repo.setLayout(new GridLayout(0,2));
		Nodo<reporteHnos> Aux = reportes.DameFrente();
		while(Aux != null){
			JLabel l = new JLabel();
			JLabel m = new JLabel();
			
			l.setText((Aux.Info.el+1)+"");
			l.setFont(new Font("Calibri", Font.BOLD, 36));
			repo.add(l);
			m.setText(Aux.Info.NoTerrenos+"");
			m.setFont(new Font("Calibri", Font.BOLD, 36));
			repo.add(m);
			
			Aux = Aux.Sig;
		}
		repo.setVisible(true);
	}
	
	public void iniciar(){
		Semaforo rep = new Semaforo(1);
		
		
		Semaforo SB = new Semaforo(1);
		Semaforo MSB[][] = new Semaforo[10][10];
		int MBB[][] = new int[10][10];
		Color MC[][] = new Color[10][10];
		int VN[] = new int [3];
		for(int i = 0; i < MBB.length; i++){
			for(int j = 0; j < MBB[i].length; j++){
				MSB[i][j] = new Semaforo(1);
				MBB[i][j] = 0;
				MC[i][j] = Color.WHITE;
			}
		}
		Hermano h1 = new Hermano(mB, SB, MSB, MBB, MC, VN, 0, rep, reportes, Color.red);
		Hermano h2 = new Hermano(mB, SB, MSB, MBB, MC, VN, 1, rep, reportes, Color.BLUE);
		Hermano h3 = new Hermano(mB, SB, MSB, MBB, MC, VN, 2, rep, reportes,Color.green);
		h1.start();
		h2.start();
		h3.start();
	}
}

class Hermano extends Thread{
	JButton mB [][];
	Semaforo SB;
	Semaforo[][] MSB;
	int [][] MBB;
	Color [][] MC;
	Color u;
	
	int el;
	int VN[];
	
	Semaforo rep = new Semaforo(1);
	Lista<reporteHnos> reportes = new Lista<reporteHnos>();
	
	
	public Hermano(JButton [][] mB, Semaforo SB, Semaforo [][] MSB, int [][] MBB, Color[][] MC, int []VN, int el, Semaforo rep, Lista<reporteHnos> reportes, Color u){
		this.mB = mB;
		this.SB = SB;
		this.MSB = MSB;
		this.MBB = MBB;
		this.MC = MC;
		this.VN = VN;
		this.el = el;
		this.rep = rep;
		this.reportes = reportes;
		this.u = u;
	}
	
	public void run(){
		System.out.println(getName());
		int renglon = 0, columna = 0;
			while(true){
				SB.Espera();
				System.out.println("Entre...");
				if(checa(MBB)){
					SB.libera();
					break;
				}
				SB.libera();
				renglon = MyRandom.nextInt(10);
				columna = MyRandom.nextInt(10);
				MSB[renglon][columna].Espera();
				mB[renglon][columna].setBackground(u);
				try {sleep(100);} catch (InterruptedException e) {e.printStackTrace();}
				if(MBB[renglon][columna] == 1){
					mB[renglon][columna].setBackground(MC[renglon][columna]);
					MSB[renglon][columna].libera();
					continue;
				}
				MBB[renglon][columna] = 1;
				MC[renglon][columna] = u;
				mB[renglon][columna].setBackground(MC[renglon][columna]);
				VN[el]++;
				MSB[renglon][columna].libera();
			}
			rep.Espera();
			reportes.InsertaOrd(new reporteHnos(el, VN[el]));
			rep.libera();
	}
	
	public boolean checa(int [][] MBB){
		for(int i = 0; i < MBB.length; i++){
			for(int j = 0; j < MBB[i].length; j++){
				if(MBB[i][j] == 0)
					return false;
			}
		}
		System.out.println("TERMIN�!!");
		return true;
	}
}

class reporteHnos{//Ocupa Semaforo
	int el;
	int NoTerrenos;
	public reporteHnos(int el, int NoTerrenos){
		this.el = el;
		this.NoTerrenos = NoTerrenos;
	}
	public String toString(){
		return MyRandom.PonBlancos(el+"", 3);
	}
}
