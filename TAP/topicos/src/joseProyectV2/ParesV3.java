package joseProyectV2;
import javax.swing.*;

import Componentes.MyRandom;

import java.awt.*;
import java.awt.event.*;
public class ParesV3 extends JDialog implements ActionListener{
	class Imagenes{
		Icon icono;
		int num;
		public Imagenes(Icon im){
			icono = im;
			num = 0;
		}
	}
	class muestraImagen{
		JButton boton;
		boolean abierto;
		public muestraImagen(JButton im){
			boton = im;
			abierto = false;
		}
	}
	class copiaIP{
		int i;
		int j;
		Icon iconos;
	}
	/**********************************************************************************************/
	JPanel centro, sur;
	JComboBox n1, n2;
	JButton inicia;

	Imagenes m[];
	muestraImagen mi[][];
	Icon pares[][];
	
	int num1 = 0, num2 = 0, contadorInte = 0, click = 0;
	copiaIP copia = new copiaIP();
	JButton salir = new JButton("Salir");
	/**********************************************************************************************/
	public ParesV3(){
		//super("Juego de Pares");
		interfaz();
		escuchadores();
	}
	
	public void interfaz(){
		centro = new JPanel();
		sur = new JPanel();
		
		inicia = new JButton("Iniciar");
		inicia.setFont(new Font("Calibri",Font.BOLD,25));
		n1 = new JComboBox();
		n2 = new JComboBox();
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setSize(1280, 720);
		setLocationRelativeTo(null);
		setResizable(true);
		sur.add(n1);
		sur.add(n2);
		sur.add(inicia);
		sur.add(salir);
		add(centro);
		add(sur,BorderLayout.NORTH);
		crearCombos();
		setVisible(false);
		//setModal(true);
	}
	public void escuchadores(){
		n1.addActionListener(this);
		n2.addActionListener(this);
		inicia.addActionListener(this);
		salir.addActionListener(this);
	}
	int n = 0;
	public void actionPerformed(ActionEvent evt) {
		
		Icon im1;
		Icon im2;
		contadorInte = 0;
		if(evt.getSource() == salir){
			setVisible(false);
			setModal(false);
			return;
		}
		if(evt.getSource() instanceof JComboBox){
			if(evt.getSource() == n1){
				if(n1.getSelectedIndex()==0)
					return;
				num1 = Integer.parseInt(n1.getSelectedItem().toString());
				System.out.println(num1);
				return;
			}
			if(evt.getSource() == n2){
				if(n2.getSelectedIndex()==0)
					return;
				num2 = Integer.parseInt(n2.getSelectedItem().toString());
				System.out.println(num2);
				return;
			}
		}
		if(evt.getSource() == inicia){
			System.out.println("iniciando...");
			centro.removeAll();
			crearMatriz();
			return;
			
		}
		for(int i = 0; i < mi.length; i++){
			for(int j = 0; j < mi[i].length; j++){
				if(evt.getSource() == mi[i][j].boton){
					System.out.println("Entro");
					click++;
					n = cuentaBoolean();
					if(n == 0){
						mi[i][j].boton.setIcon(pares[i][j]);
						mi[i][j].abierto = true;
						mi[i][j].boton.removeActionListener(this);
						centro.updateUI();
						copia.iconos = mi[i][j].boton.getIcon();
						copia.i = i;
						copia.j = j;
						System.out.println(copia.i + "   " + i);
						return;
					}
					if(n == 1){
						mi[i][j].boton.setIcon(pares[i][j]);
						mi[i][j].abierto = true;
						mi[i][j].boton.removeActionListener(this);
						centro.updateUI();
					}
					mi[copia.i][copia.j].boton.setIcon(pares[copia.i][copia.j]);
					mi[i][j].boton.setIcon(pares[i][j]);
					mi[i][j].abierto = true;
					mi[i][j].boton.removeActionListener(this);
					n = cuentaBoolean();
					if(n == 2){
						System.out.println("pares?");
						im1 = mi[copia.i][copia.j].boton.getIcon();
						im2 = mi[i][j].boton.getIcon();
						if(im1 == im2){
							System.out.println("PAR");
							mi[i][j].boton.setDisabledIcon(mi[i][j].boton.getIcon());
							mi[copia.i][copia.j].boton.setDisabledIcon(mi[copia.i][copia.j].boton.getIcon());
							mi[i][j].boton.setEnabled(false);
							mi[copia.i][copia.j].boton.setEnabled(false);
							mi[i][j].abierto = false;
							mi[copia.i][copia.j].abierto = false;
						}
						mi[i][j].boton.update(mi[i][j].boton.getGraphics());
						try{
							Thread.sleep(500);
						}catch(Exception n){}
						mi[copia.i][copia.j].boton.addActionListener(this);
						mi[i][j].boton.addActionListener(this);
						limpiaPantalla();
						finale();
						return;
					}
				}
			}
		}
	}
	public void llenaImagenes(){
		int cont = 0;
		try{
			m[cont] = new Imagenes(new ImageIcon("i1.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i2.PNG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i3.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i4.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i5.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i6.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i7.PNG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i8.PNG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i9.PNG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i10.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i11.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i12.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i13.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i14.JPG"));
			cont++;
			m[cont] = new Imagenes(new ImageIcon("i15.JPG"));
		}catch(Exception e){}
	}
	public void crearMatriz(){
		m = new Imagenes[(num1*num2)/2];
		llenaImagenes();
		mi = new muestraImagen[num1][num2];
		pares = new Icon[num1][num2];
		centro.setLayout(new GridLayout(0, num2));
		System.out.println(num1 + "   " + num2);
		for(int i = 0; i < mi.length; i++){
			for(int j = 0; j < mi[i].length; j++){
				mi[i][j] = new muestraImagen(new JButton());
				int ene = MyRandom.nextInt(m.length);
				if(m[ene].num == 2){
					j--;
					continue;
				}
				mi[i][j].boton.setIcon(new ImageIcon("Pregunta.JPG"));
				pares[i][j] = new ImageIcon();
				pares[i][j] = m[ene].icono;
				m[ene].num++;
				mi[i][j].boton.addActionListener(this);
				centro.add(mi[i][j].boton);
			}
		}
		centro.updateUI();
	}
	public void crearCombos(){
		n1.addItem("Seleccione X");
		n1.addItem("2");
		n1.addItem("3");
		n1.addItem("4");
		n1.addItem("5");
		n1.setFont(new Font("Calibri",Font.BOLD,25));
		
		n2.addItem("Seleccione Y");
		n2.addItem("2");
		n2.addItem("4");
		n2.addItem("6");
		n2.setFont(new Font("Calibri",Font.BOLD,25));
	}
	public void finale(){
		int n = 0;
		for(int m = 0; m < mi.length; m++){
			for(int k = 0; k < mi[m].length; k++){
				if(!mi[m][k].boton.isEnabled()){
					n++;
				}
			}
		}System.out.println(n);
	}
	public void limpiaPantalla(){
		int n = 0;
		int habilitados = 0;
		System.out.println("limpiapantalla");
		for(int m = 0; m < mi.length; m++){
			for(int k = 0; k < mi[m].length; k++){
				if(mi[m][k].boton.isEnabled()){
					mi[m][k].boton.setIcon(new ImageIcon("Pregunta.JPG"));
					mi[m][k].abierto = false;
					
					habilitados++;
				}
				n++;
			}
		}
		System.out.println(habilitados);
	}
	public int cuentaBoolean(){
		int n = 0;
		for(int i = 0; i < mi.length; i++){
			for(int j = 0; j < mi[i].length; j++){
				if(mi[i][j].abierto){
					n++;
				}
			}
		}
		System.out.println(n);
		return n;
	}
	public static void main(String[] args) {
		new ParesV3();
	}
	

}
