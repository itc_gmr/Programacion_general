package joseProyectV2;
import java.awt.*;
import java.io.IOException;

import javax.swing.*;
public class Menu extends JFrame{
	public Menu() throws IOException{
		Vista vista = new Vista();
		Modelo modelo = new Modelo();
		Controlador controlador = new Controlador (vista, modelo);
		vista.setControlador(controlador);
	}
	public static void main(String[] args) throws IOException {
		new Menu();

	}

}
