package joseProyectV2;

import java.awt.BorderLayout;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Vector;

import javax.swing.*;

import Componentes.ComboListener;

public class ConCon extends JDialog{
	JTable tabla = new JTable();
	JScrollPane scroll = new JScrollPane();
	JComboBox tar;
	JPanel norte = new JPanel();
	
	Vector vector;
	String v[] = {"Fecha", "Tarjeta", "Lugar"};
	
	private int codigo;
	private final int numero = 216;
	
	public ConCon(JTable t, JScrollPane s){
		setTitle("Configuración");
		tabla = t;
		scroll = s;
		
		setVector();
		tar = new JComboBox();
		tar.setFont(new Font("Calibri",Font.BOLD,25));
		tar.setEditable(true);
		tar.setModel(new DefaultComboBoxModel(vector));
		tar.setSelectedIndex(-1);
		JTextField text = (JTextField)tar.getEditor().getEditorComponent();
		text.setFocusable(true);
		text.setText("");
		text.addKeyListener(new ComboListener(tar,vector));
		
		
		
		norte.add(tar);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setVisible(true);
	}

	private void setVector() {
		vector = new Vector();
		for(int a=0;a<v.length;a++){
			vector.add(v[a]);
		}
	}
	
	private void setTable()throws IOException{
		System.out.println("Entre");
		File F=new File("jose.dat");
		RandomAccessFile nom= new RandomAccessFile( F,"rw" );
		nom.seek(0);
		Object columnas[] = {"Codigo", "Fecha", "Concepto", "Monto", "Tarjeta", "Lugar"};
		Object data[][] = new Object [(int)nom.length()/numero][columnas.length];
		
		
		
		int cod;
		String fec;
		String con;
		Double mon;
		String tar;
		String lug;
		int cont = 0;
		System.out.println(nom.length()/numero);
		for(int i = 0; i<nom.length()/numero; i++){
			cod = nom.readInt();
			fec = nom.readUTF();
			con = nom.readUTF();
			mon = nom.readDouble();
			tar = nom.readUTF();
			lug = nom.readUTF();
			data[i][cont] = cod;
			cont++;
			data[i][cont] = fec;
			cont++;
			data[i][cont] = con;
			cont++;
			data[i][cont] = mon;
			cont++;
			data[i][cont] = tar;
			cont++;
			data[i][cont] = lug;
			cont = 0;
		}
		nom.close();
		System.out.println(data.length);
		tabla = new JTable(data, columnas);
		tabla.setEnabled(false);
		scroll = new JScrollPane(tabla);
		scroll.setSize(getWidth(), 300);
	}
}
