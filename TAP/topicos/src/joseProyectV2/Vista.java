package joseProyectV2;
import java.awt.*;
import javax.swing.*;
import Componentes.MyRandom;
import Componentes.JLeeReal;
import Componentes.JLeeNombre;
import Componentes.JLeeReal;
import Componentes.fecha;
import java.io.*;

public class Vista extends JFrame {
	JButton insertar, consulta;
	JLabel menu;
	
	MenuInsertar in;
	MenuConsultas mc;
	public Vista() throws IOException{
		insertar = new JButton("Insertar Gastos");
		consulta = new JButton("Consulta");
		
		
		
		//mc = new MenuConsultas();
		in = new MenuInsertar();
		
		setSize(600, 350);
		setLayout(new GridLayout(0,1));
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		add(insertar);
		add(consulta);
		
		setVisible(true);
		
	}
	
	public void setControlador(Controlador c) {
		insertar.addActionListener(c);
		consulta.addActionListener(c);
		
	}

}
