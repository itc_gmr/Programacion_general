package joseProyectV2;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;
import Componentes.*;
public class MenuConsultas<JasperPrint> extends JDialog implements ActionListener{
	private final int numero = 216;
	JDialog tablilla;
	JPanel centro, norte;
	JLabel consulta;
	JTable tabla;
	JScrollPane scroll;
	JButton exportar;
	
	public MenuConsultas() throws IOException{
		interfaz();
		escuchadores();
	}

	private void interfaz() throws IOException {
		tablilla = new JDialog();
		centro = new JPanel();
		norte = new JPanel();
		consulta = new JLabel("Consultas");
		exportar = new JButton("Exportar");
		
		ImageIcon ic = new ImageIcon(MyRandom.ajustarImagen1("salir.PNG", 50, 40).getImage());
		ImageIcon ie = new ImageIcon(MyRandom.ajustarImagen1("exportar.PNG", 50, 40).getImage());
		ImageIcon ir = new ImageIcon(MyRandom.ajustarImagen1("refrescar.PNG", 50, 40).getImage());
		
		exportar.setIcon(ie);
		
		consulta.setFont(new Font("Calibri",Font.BOLD,25));
		
		exportar.setFont(new Font("Calibri",Font.BOLD,25));
		
		setTable();
		norte.add(consulta);
		
		norte.add(exportar);
		//norte.add(salir);
		add(norte, BorderLayout.NORTH);
		add(scroll, BorderLayout.CENTER);
		
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		//setLocationRelativeTo(null);
		setSize(800, 400);
		setVisible(true);
	}
	
	public void setTable()throws IOException{
		/*if(tablilla.contains(tablilla.getWidth(), tablilla.getHeight())){
			tablilla.remove(scroll);
		}*/
		System.out.println("Entre");
		File F=new File("jose.dat");
		RandomAccessFile nom= new RandomAccessFile( F,"rw" );
		nom.seek(0);
		Object columnas[] = {"Codigo", "Fecha", "Concepto", "Monto", "Tarjeta", "Lugar"};
		Object data[][] = new Object [(int)nom.length()/numero][columnas.length];
		int cod;
		String fec;
		String con;
		Double mon;
		String tar;
		String lug;
		int cont = 0;
		System.out.println(nom.length()/numero);
		for(int i = 0; i<nom.length()/numero; i++){
			cod = nom.readInt();
			fec = nom.readUTF();
			con = nom.readUTF();
			mon = nom.readDouble();
			tar = nom.readUTF();
			lug = nom.readUTF();
			data[i][cont] = cod;
			cont++;
			data[i][cont] = fec;
			cont++;
			data[i][cont] = con;
			cont++;
			data[i][cont] = mon;
			cont++;
			data[i][cont] = tar;
			cont++;
			data[i][cont] = lug;
			cont = 0;
		}
		nom.close();
		System.out.println(data.length);
		tabla = new JTable(data, columnas);
		tabla.setEnabled(false);
		scroll = new JScrollPane(tabla);
		scroll.setSize(getWidth(), 300);
		/*tablilla.add(scroll, BorderLayout.CENTER);
		tablilla.setSize(scroll.getWidth(), scroll.getHeight());
		tablilla.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		tablilla.setVisible(true);*/
		add(scroll, BorderLayout.CENTER);
		update(getGraphics());
	}
	
	public void escuchadores(){
		exportar.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() == exportar){
			
		}
		
	}
	
	private void exportar(){
		System.out.println("Entre");
		JFileChooser file = new JFileChooser();
		file.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int resultado = file.showSaveDialog(this);
		if(resultado == JFileChooser.CANCEL_OPTION){
			return;
		}
		File archivo = file.getSelectedFile();
		try{
			System.out.println("Entre 2");
			PrintWriter salida = new PrintWriter(new FileWriter(archivo + ".csv"));
			String data [][] = matrizTabla();
			for(int i = 0; i < data.length; i++){
				salida.print(data[i][0]);
				for(int j = 0; j < data[i].length; j++){
					String word = data[i][j];
					if(word != null){
						salida.print("," + word);
					}else{
						salida.print(",");
					}
				}
				salida.println();
			}
			salida.close();
		}catch(IOException e){}
	}

	private String[][] matrizTabla() {
		int columna = tabla.getColumnCount();
		int fila = tabla.getRowCount();
		String matriz[][] = new String [fila][columna];
		for (int i = 0; i<fila;i++){
			for(int j = 0; j < columna; j++){
				matriz[i][j] = tabla.getValueAt(i, j) + "";
			}
		}
		return matriz;
		
		/*for(int fila = 0; fila < tabla.getRowCount(); fila++){
			tipo = new CamposBD(String.valueOf(tabla.getValueAt(fila, columna++)), String.valueOf(tabla.getValueAt(fila, columna++)), String.valueOf(tabla.getValueAt(fila, columna++)), String.valueOf(tabla.getValueAt(fila, columna++)), String.valueOf(tabla.getValueAt(fila, columna++)), String.valueOf(tabla.getValueAt(fila, columna++)));
			columna = 0;
			Resultados.add(tipo);
		}
		
		
		Date fecha = new Date();
		String fechita = "" + fecha.getTime();
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		fechita = dateFormat.format(fecha);

		map.put("Titulo", "Gastos");
		map.put("Fecha", fechita);
		
		//jPrint = JasperFillManager.fillReport(getTemplate(reportInput.getReportTemplate()), reportInput.getReportParams(), dataSource);
		
		for(int fila = 0; fila < tabla.getRowCount(); fila++){
			datos = datos + String.valueOf(tabla.getValueAt(fila, columna++)) + "  "  + String.valueOf(tabla.getValueAt(fila, columna++)) + "  " + String.valueOf(tabla.getValueAt(fila, columna++)) + "  " + String.valueOf(tabla.getValueAt(fila, columna++)) + "  " + String.valueOf(tabla.getValueAt(fila, columna++)) + "  " + String.valueOf(tabla.getValueAt(fila, columna++)) + "\n";
			columna = 0;
			System.out.println(datos);
		}
		JOptionPane.showMessageDialog(null, datos);
		*/
		
	}

}
