package joseProyectV2;
import java.awt.*;
import javax.swing.*;

import Componentes.*;

import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Vector;

import Componentes.ComboListener;
public class MenuInsertar extends JDialog implements ActionListener, KeyListener{
	private int codigo;
	private final int numero = 216;
	
	Vector vector;
	ListaDBL<String> V = new ListaDBL<String>();
	String v[] = {"Oro", "Clasica Banamex", "Clasica Inbursa"};
	
	
	fecha f = new fecha();
	JLeeNombre con, lug;
	JLeeReal mon;
	JButton guardar = new JButton("Guardar");
	JLabel fecha, concepto, monto, tarjeta, lugar;
	JComboBox tar;
	JButton salir = new JButton("Salir");
	public MenuInsertar(){
		
		
		guardar.setFont(new Font("Calibri",Font.BOLD,25));
		fecha = new JLabel("Fecha: ");
		fecha.setFont(new Font("Calibri",Font.BOLD,25));
		concepto = new JLabel("Concepto: ");
		concepto.setFont(new Font("Calibri",Font.BOLD,25));
		monto = new JLabel("Monto: ");
		monto.setFont(new Font("Calibri",Font.BOLD,25));
		tarjeta = new JLabel("Tarjeta: ");
		tarjeta.setFont(new Font("Calibri",Font.BOLD,25));
		lugar = new JLabel("Lugar: ");
		lugar.setFont(new Font("Calibri",Font.BOLD,25));
		
		con = new JLeeNombre(50);
		con.setFont(new Font("Calibri",Font.BOLD,25));
		
		
		lug = new JLeeNombre(50);
		lug.setFont(new Font("Calibri",Font.BOLD,25));
		mon = new JLeeReal(15);
		mon.setFont(new Font("Calibri",Font.BOLD,25));
		salir.setFont(new Font("Calibri",Font.BOLD,25));
		ImageIcon ic = new ImageIcon(MyRandom.ajustarImagen1("salir.PNG", 50, 40).getImage());
		salir.setIcon(ic);
		ImageIcon ig = new ImageIcon(MyRandom.ajustarImagen1("guardar.PNG", 50, 40).getImage());
		guardar.setIcon(ig);
		
		setSize(600, 350);
		setLayout(new GridLayout(0, 2));
		setLocationRelativeTo(null);
		setResizable(false);
		setModal(true);
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		setVector();
		tar = new JComboBox();
		tar.setFont(new Font("Calibri",Font.BOLD,25));
		tar.setEditable(true);
		tar.setModel(new DefaultComboBoxModel(vector));
		tar.setSelectedIndex(-1);
		JTextField text = (JTextField)tar.getEditor().getEditorComponent();
		text.setFocusable(true);
		text.setText("");
		text.addKeyListener(new ComboListener(tar,vector));
		
		add(fecha);
		add(f);
		add(concepto);
		add(con);
		add(monto);
		add(mon);
		add(tarjeta);
		add(tar);
		add(lugar);
		add(lug);
		add(guardar);
		add(salir);
		
		setVisible(false);
		setListener();
		
	}
	
	public void setListener(){
		guardar.addActionListener(this);
		salir.addActionListener(this);
		guardar.addKeyListener(this);
		salir.addKeyListener(this);
		f.addActionListener(this);
		con.addActionListener(this);
		mon.addActionListener(this);
		tar.addActionListener(this);
		lug.addActionListener(this);
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void keyTyped(KeyEvent evt) {
		char cTeclaPresionada = evt.getKeyChar();
		if(evt.getSource() == guardar){
			if(cTeclaPresionada == KeyEvent.VK_ENTER){
				guardar.doClick();
				f.grabFocus();
			}
		}
		if(evt.getSource() == salir){
			if(cTeclaPresionada == KeyEvent.VK_ENTER){
				salir.doClick();
			}
		}
		/*if(evt.getKeyCode() == evt.VK_ENTER){
			((JComponent) evt.getSource()).transferFocus();
		}*/
	}

	public void actionPerformed(ActionEvent evt) {
		
		((JComponent) evt.getSource()).transferFocus();
		if(evt.getSource() == salir){
			setVisible(false);
		}
		if(evt.getSource() == guardar){
			if(lug.getText().length() == 0 || mon.getText().length() == 0 || con.getText().length() == 0){
				JOptionPane.showMessageDialog(null, "Alguno de los campos esta vac�o, reingrese valores");
				return;
			}
			try {
				grabar(f.getText(), con.getText(), Double.parseDouble(mon.getText()), tar.getSelectedItem().toString(), lug.getText());
				con.setText("");
				mon.setText("");
				tar.setSelectedIndex(-1);
				lug.setText("");
			} catch (IOException e) {e.printStackTrace();}
		}
		
	}
	
	public void grabar(String fecha, String concepto, Double monto, String tarjeta, String lugar)throws IOException{
		File F=new File("jose.dat");
		RandomAccessFile arch= new RandomAccessFile( F,"rw" );
		arch.seek(arch.length());
		fecha = MyRandom.PonBlancos(fecha, 50);
		concepto = MyRandom.PonBlancos(concepto, 50);
		tarjeta = MyRandom.PonBlancos(tarjeta, 50);
		lugar = MyRandom.PonBlancos(lugar, 50);
		codigo = (int) (arch.length()/numero);
		codigo++;
		arch.writeInt(codigo);
		arch.writeUTF(fecha);
		arch.writeUTF(concepto);
		arch.writeDouble(monto);
		arch.writeUTF(tarjeta);
		arch.writeUTF(lugar);
		arch.close();
	}
	
	private void setVector(){
		vector = new Vector();
		for(int a=0;a<v.length;a++){
			vector.add(v[a]);
		}
	}
}
