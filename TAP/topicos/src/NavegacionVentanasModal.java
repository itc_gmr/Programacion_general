import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class NavegacionVentanasModal extends JFrame implements ActionListener{

	JButton [] VBtn;
	JButton BtnAtras;
	JDialog [] VP;
	int Cont=-1;
	public NavegacionVentanasModal(){
		super("Ventana #1");
		HazInterfaz();
		HazEscucha();
	}
	public void HazInterfaz(){
		
		VBtn=new JButton[14];
		for(int i=0;i<VBtn.length;i++){
			VBtn[i]=new JButton("Siguiente");
		}
		BtnAtras=new JButton("Regresar");
		add(VBtn[0],BorderLayout.EAST);
		VP=new JDialog[14];
		for(int i=0;i<VP.length;i++){
			VP[i]=new JDialog();
			VP[i].setTitle("Pantalla # "+(i+2));
			VP[i].setModal(true);
			VP[i].setSize(300,200);
			VP[i].setLocationRelativeTo(null);
			if(i<VP.length-2)
			    VP[i].add(VBtn[i+1],BorderLayout.EAST);
		}
		setSize(300,200);
		setLocationRelativeTo(null);
		setVisible(true);		

	}
	public void HazEscucha(){
		for(int i=0;i<VBtn.length;i++){
			VBtn[i].addActionListener(this);
		}
		for(int i=0;i<VP.length;i++)
			VP[i].addWindowListener(new EscuchaVentana());
		    
		BtnAtras.addActionListener(this);
	}
	public void actionPerformed(ActionEvent E){
		System.out.println("Cont "+Cont);	
		if(E.getSource()==BtnAtras){
			VP[Cont].setVisible(false);
			Cont--;
			if(Cont>=0) 
				VP[Cont].add(BtnAtras,BorderLayout.WEST);
			else
				setVisible(true);
			return;
		}
		Cont++;
		VP[Cont].add(BtnAtras,BorderLayout.WEST);
		VP[Cont].setVisible(true);
	}
	class EscuchaVentana implements WindowListener{
        public void windowActivated(WindowEvent e){
            System.out.println("windowActivated");
        }
 
        public void windowClosed(WindowEvent e){
            System.out.println("windowClosed");
        }
 
        public void windowClosing(WindowEvent e){
            System.out.println("windowClosing");
    		Cont--;
			if(Cont>=0) 
				VP[Cont].add(BtnAtras,BorderLayout.WEST);
			else
				setVisible(true);
			return;
  
        }
 
        public void windowDeactivated(WindowEvent e){
            System.out.println("windowDeactivated");
        }
 
        public void windowDeiconified(WindowEvent e){
            System.out.println("windowDeiconified");
        }
 
        public void windowIconified(WindowEvent e){
            System.out.println("windowIconified");
        }
 
        public void windowOpened(WindowEvent e){
            System.out.println("windowOpened");
        }
 
    }
	public static void main(String [] a){
		new NavegacionVentanasModal();
	}
}