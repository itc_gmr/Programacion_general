
import javax.swing.*;
import javax.swing.event.*;

import java.awt.*;
import java.awt.event.*;
public class Menu extends JFrame implements ActionListener, MenuListener{
JButton BtnSalvaComo;


    private JMenuBar mb;

    private JMenu menu1,Menu2,Menu3,MenuAutos;

    private JMenuItem mi1,mi2,mi3;
    private JMenuItem Md1,Md2,Md3,Salir;
    private JMenuItem A1,A2,A3,A4;
    
    
    JFrame MF;
    public Menu() {
    	super("Men� del sistema contable");
    	HazInterfaz();
    	HazEscucha();
    }
    public void HazInterfaz(){
        setLayout(null);
        MF=this;


      // creamos la barra del menu
        mb=new JMenuBar();
        setJMenuBar(mb);
        mb.setBackground(Color.cyan);


        // Elementos del men�

        menu1=new JMenu("Colores");
        mb.add(menu1);

        Menu2=new JMenu("D�as");
        mb.add(Menu2);

  //      Menu3=new JMenu("Salie JMenu");
  //      mb.add(new JTextField("200"));

	MenuAutos=new JMenu("Autos");
		mb.add(MenuAutos);


        MenuAutos.add(A1=new JMenuItem("Deportivos"));
        MenuAutos.add(A2=new JMenuItem("PickUP"));
        MenuAutos.add(A3=new JMenuItem("Clasicos"));
        MenuAutos.add(A4=new JMenuItem("Populares"));


		// Elementos de los colores
        mi1=new JMenuItem("Rojo");
       
        menu1.add(mi1);
        mi2=new JMenuItem("Verde");
        menu1.add(mi2);
        mi3=new JMenuItem("Azul");
        menu1.add(mi3);
		// Elementos de los d�as
        Md1=new JMenuItem("Trabajo");
        Menu2.add(Md1);
        Md2=new JMenuItem("Fin semana");
        Menu2.add(Md2);
        Md3=new JMenuItem("Tranquilidad");
		Menu2.add(Md3);

		Salir=new JMenuItem("Salir");

	//	Menu3.add(Salir);
		
		JMenu OpSalir=new JMenu("Salir");
		OpSalir.add(Salir);
		mb.add(OpSalir);
	



	    setBounds(10,20,400,200);
     	setLocationRelativeTo(null);
        setVisible(true);

    }

   public void HazEscucha(){
        mi1.addActionListener(this);
        mi2.addActionListener(this);
        mi3.addActionListener(this);

        Md1.addActionListener(this);
        Md2.addActionListener(this);
        Md3.addActionListener(this);

        A1.addActionListener(this);
        A2.addActionListener(this);
        A3.addActionListener(this);
        A4.addActionListener(this);


        Salir.addActionListener(this);

	}
	public void menuCanceled(MenuEvent E){
		System.out.println("Cancel");
	}
	public void menuSelected(MenuEvent E){
		System.out.println("selected");

	}
	public void menuDeselected(MenuEvent E){
		System.out.println("deselected");

	}

    public void actionPerformed(ActionEvent e) {

    	if(e.getSource()== A1  ){
    		System.out.println("ESTOUY EN AL OPCION UNO DE LOS AUTOS");
    		return;
    	}
    	Container f=this.getContentPane();
        if (e.getSource()==mi1) {
            f.setBackground(new Color(255,0,0));

            return;
        }
        if (e.getSource()==mi2) {
            f.setBackground(new Color(0,255,0));
        }
        if (e.getSource()==mi3) {
            f.setBackground(new Color(0,0,255));
        }
        if (e.getSource()==Md1) {
   			JOptionPane.showMessageDialog(MF, "Lunes-Jueves");
            return;
        }
        if (e.getSource()==Md2) {
   			JOptionPane.showMessageDialog(MF, "Viernes - Sabado");
            return;
        }

        if (e.getSource()==Md3) {
  // 			JOptionPane.showMessageDialog(MF, "Domingo");
            JDialog VentanaModal = new JDialog(this, "hija",Dialog.ModalityType.DOCUMENT_MODAL);
            BtnSalvaComo = new JButton("Salvar como...");
            BtnSalvaComo.addActionListener(this);
            VentanaModal.setLayout(new FlowLayout());
			VentanaModal.add(BtnSalvaComo);
            VentanaModal.setSize(300,300);
            VentanaModal.setLocation(150,150);
            VentanaModal.setVisible(true);

            return;
        }
        if (e.getSource()==Salir) {
   			System.exit(0);
        }
		if(e.getSource()==BtnSalvaComo){
			System.out.println("*****");
			FileDialog fileDialog = new FileDialog(this,"Guardar", FileDialog.SAVE);
            fileDialog.setSize(400,400);
            fileDialog.setVisible(true);
			return;
		}

    }

    public static void main(String[] ar) {
        new Menu();
    }
}