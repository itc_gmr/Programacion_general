package Componentes;
import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.*;

public class fecha extends JTextField{
	Date fecha = new Date();
	String fechita = "" + fecha.getTime();
	public fecha(){
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		fechita = dateFormat.format(fecha);
		setFont(new Font("Calibri",Font.BOLD,25));
		setText(fechita);
		setEditable(true);
	}
}
