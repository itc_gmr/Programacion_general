import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

public class TamanioBoton extends JFrame{
	private JButton boton;
	 public TamanioBoton() 	 {
	   add(new JLabel("Esta boton aumenta 50 pixeles por dimensión"));
	   setLayout(new FlowLayout());
	    boton = new JButton("Boton");
             boton.setSize(150,100);
        boton. setPreferredSize(new Dimension(80,85));
      
	    add(boton);
	    setSize(350, 500);
	    setVisible(true);
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
	    boton.addActionListener(new ActionListener()
	      {   public void actionPerformed(ActionEvent evt)
	          {  JButton boton = (JButton)evt.getSource();
		    boton.setSize(boton.getWidth() + 50, boton.getHeight() + 50);
		    repaint();
	          }
	       });
	    
	    
	    
	    
	 }
	 public static void main(String[] args)
	 {
	     new TamanioBoton();

	 }

}
