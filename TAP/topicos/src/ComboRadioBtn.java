import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
public class ComboRadioBtn extends JFrame implements ActionListener,ItemListener{
	JPanel P1;
	Box P2;
	JComboBox Cmb;
	JRadioButton R1,R2,R3;
	ButtonGroup Grupo;
	JButton [] VB;
	Random R=new Random();
	int [] V,VOrdenado;
	public ComboRadioBtn(){
		super("Valores aleatorios");
		HazInterfaz();
		HazEscucha();
	}
	public void HazInterfaz(){
		setSize(500,400);
		setLocationRelativeTo(null);
		P1=new JPanel();
		P2=Box.createVerticalBox();
		Cmb=new JComboBox();
		Cmb.addItem("Seleccione");
		Cmb.addItem("5");
		Cmb.addItem("10");
		Cmb.addItem("20");
		Cmb.addItem("30");
		P1.add(Cmb);
		R1=new JRadioButton("Asc");
		R2=new JRadioButton("Des");
		R3=new JRadioButton("Original");
		
		P1.add(R1);
		P1.add(R2);
		P1.add(R3);
		Grupo=new ButtonGroup();
		Grupo.add(R1);
		Grupo.add(R2);
		Grupo.add(R3);
		add(P1,BorderLayout.NORTH);
	//	paintAll(getGraphics());
		setVisible(true);
		
	}
	public void HazEscucha(){
		Cmb.addActionListener(this);
		Cmb.addItemListener(this);
		
		
		R1.addActionListener(this);
		R2.addActionListener(this);
		R3.addActionListener(this);
	}
	public void actionPerformed(ActionEvent E){
		if(E.getSource() instanceof JRadioButton){
			if(E.getSource()==R1){
				
				for(int i=0;i<VB.length;i++){
					VB[i].setText(""+VOrdenado[i]);
				}
				
			}
			
			if(E.getSource()==R2){
				int Sub=0;
				for(int i=VB.length-1;i>=0;i--){
					VB[Sub++].setText(""+VOrdenado[i]);
				}
				
			}
						
		}
		if(E.getSource() instanceof JComboBox){
			if(E.getSource()==Cmb){
				if(Cmb.getSelectedIndex()==0)
					return;
			   R3.setSelected(true);
			   P2.removeAll();
			   int NE=Integer.parseInt(Cmb.getSelectedItem().toString());
			   VB=new JButton[NE];
			   V=new int[NE];
			   VOrdenado=new int[NE];
			   for(int i=0;i<VB.length;i++){
				   V[i]=R.nextInt(201)+100;
				   VOrdenado[i]=V[i];
				   VB[i]=new JButton(""+V[i]);
				   P2.add(VB[i]);
			   }
				add(P2);   
			   Arrays.sort(VOrdenado);
			   System.out.println("Eleme"+NE);
			   paintAll(getGraphics());
			   return;
			}
			

			
		}
		
		
	}
	public void itemStateChanged(ItemEvent E) {
	System.out.println("escucho con item starte")	;
	}
	public static void main(String [] a){
		new ComboRadioBtn();
	}
}
