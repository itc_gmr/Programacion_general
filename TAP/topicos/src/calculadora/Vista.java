package calculadora;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Vista extends JFrame{
	/*JLeeReal*/ 
	JTextField pantalla;
	JPanel center;
	
	JButton numeros[][];
	JButton operadores[];
	Box right;
	
	
	public Vista(){
		super("Calculadora");
		interfaz();
	}
	
	public void interfaz(){
		pantalla = new JTextField();
		pantalla.setFont(new Font("Calibri",Font.BOLD,35));
		center = new JPanel();
		center.setLayout(new GridLayout(0,3,10,10));
		numeros = new JButton[4][3];
		operadores = new JButton[6];
		right=Box.createVerticalBox();
		setSize(450, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setNumerosSignos();
		add(pantalla, BorderLayout.NORTH);
		add(center, BorderLayout.CENTER);
		add(right, BorderLayout.EAST);
		setResizable(false);
		setVisible(true);
	}
	
	public void setControlador(Controlador C){
		for(int i = 0; i < numeros.length; i++){
			for(int j = 0; j < numeros[i].length; j++){
				numeros[i][j].addActionListener(C);
			}
		}
		for(int i = 0; i < operadores.length;i++){
			operadores[i].addActionListener(C);
		}
		
	}
	
	public void setNumerosSignos(){
		int m = 0, k = 0, l = 0;
		String ch1[] = {"1","2","3","4","5","6","7","8","9",".","0","C"};
		String ch2[] = {"*","+","-","/","�","="};
		for(int i = 0; i < numeros.length; i++){
			for(int j = 0; j < numeros[i].length; j++){
				numeros[i][j] = new JButton(ch1[m]+"");
				numeros[i][j].setFont(new Font("Calibri",Font.BOLD,25));
				numeros[i][j].setBackground(new Color(170,255,150));
				center.add(numeros[i][j]);
				m++;
			}
		}
		for(int i = 0; i < ch2.length;i++){
			operadores[i] = new JButton(ch2[i]+"");
			operadores[i].setFont(new Font("Calibri",Font.BOLD,25));
			operadores[i].setBackground(Color.cyan);
			right.add(operadores[i]);
			right.add(Box.createVerticalStrut(13));
		}
		
		operadores[5].setBackground(Color.ORANGE);
		
		
	}
}

