package calculadora;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Controlador implements ActionListener{
	private Modelo modelo;
	private Vista vista;
	public Controlador(){
		
	}
	public Controlador(Modelo m,Vista v){
		modelo = m;
		vista = v;
	}
	public void actionPerformed(ActionEvent evt) {
		for(int i = 0; i < vista.numeros.length; i++){
			for(int j = 0; j < vista.numeros[i].length; j++){
				if(evt.getSource() == vista.numeros[i][j]){
					modelo.insertaNumero(vista.numeros[i][j].getText());
					System.out.println(modelo.cap);
					vista.pantalla.setText(modelo.cap);
				}
			}
		}
		for(int i = 0; i < vista.operadores.length;i++){
			if(evt.getSource() == vista.operadores[i]){
				modelo.insertaOperador(vista.operadores[i].getText());
				vista.pantalla.setText(modelo.cap);
			}
		}
	}
}
