package calculadora;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class JLeeReal extends JTextField implements KeyListener{
	
	private int Tamano;
	
	public JLeeReal(){
		this(8);
	}
	public JLeeReal(int Tam){
		super(Tam);
		Tamano=Tam;
		addKeyListener(this);
	}
	public void keyPressed(KeyEvent e) {
		
	}
	public void keyReleased(KeyEvent e) {
		
	}
			public void keyTyped(KeyEvent e) {
					if(getText().length()==Tamano){
						e.consume();
						return;
					}
					char T=e.getKeyChar();
					if (  ! ( T>='0' && T<='9' || T=='.' || T=='-')    ){
						e.consume();
						return;
					}
					
					if(T=='.' && getText().indexOf(".") >=0 ){
						e.consume();
						return;
					}
					if(T=='-' && getText().length()>0){
						e.consume();
						return;
					}
					
			}
}
