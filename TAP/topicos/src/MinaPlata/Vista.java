package MinaPlata;
import Componentes.*;

import java.awt.BorderLayout;

import javax.swing.*;
public class Vista extends JFrame{
	JPanel norte;
	JButton inicia;
	
	public Vista(){
		super("Mina de Plata");
		//interfaz();
		inicia();
	}
	
	private void interfaz(){
		norte = new JPanel();
		inicia = new JButton("Iniciar");
		norte.add(inicia);
		
		add(norte, BorderLayout.NORTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800,600);
		setVisible(true);
	}
	public void setControlador(Controlador c) {
		//inicia.addActionListener(c);
	}
	public void inicia(){
		int capacidad = MyRandom.nextInt(10000, 20000);//esto, solo la mitad, recurso compartido!!
		
		int VT[] = new int [3];//esto
		Semaforo SC = new Semaforo(1);//esto
		Semaforo SR = new Semaforo(1);//esto
		
		int cantidadEu = MyRandom.nextInt(10, 30);
		int cantidadAs = MyRandom.nextInt(5, 7);
		pais Eu[] = new pais[cantidadEu];
		pais As[] = new pais[cantidadAs];
		
		Lista<paisReporte> EuR = new Lista<paisReporte>();//esto recurso compartido!!
		Lista<paisReporte> AsR = new Lista<paisReporte>();//esto recurso compartido!!
		//paisReporte EuR[] = new paisReporte[cantidadEu];
		//paisReporte AsR[] = new paisReporte[cantidadAs];
		
		for(int i = 0; i < Eu.length; i++){
			Eu[i] = new pais((i+100), capacidad/2, VT, SC, SR, EuR);
			Eu[i].start();
			//EuR[i] = new paisReporte();
		}
		for(int i = 0; i < As.length; i++){
			As[i] = new pais((i+200),  capacidad/2, VT, SC, SR, AsR);
			As[i].start();
			//AsR[i] = new paisReporte();
		}
		while(trabaja(Eu) || trabaja(As));
		Nodo<paisReporte> Aux = EuR.DameFrente();
		while(Aux != null){
			System.out.println(Aux.Info.noPais + "  " + Aux.Info.tipo + "  " +Aux.Info.cantidad + "  " + Aux.Info.repeticiones);
			Aux = Aux.Sig;
		}
		Aux = AsR.DameFrente();
		while(Aux != null){
			System.out.println(Aux.Info.noPais + "  " + Aux.Info.tipo + "  " +Aux.Info.cantidad + "  " + Aux.Info.repeticiones);
			Aux = Aux.Sig;
		}
	}
	
	public boolean trabaja(pais h[]){
		for(int i = 0; i < h.length; i++){
			if(h[i].isAlive())
				return true;
		}
		return false;
	}
}
class pais extends Thread{
	int noPais;
	int repeticiones;
	int cantidad;
	int tipo;
	
	int capacidad;
	int VT[];//RC
	Semaforo SC;
	Semaforo SR;
	Lista<paisReporte> R;//RC
	
	paisReporte obj;
	public pais(int noPais, int capacidad, int VT[], Semaforo SC, Semaforo SR, Lista<paisReporte> R){
		this.noPais = noPais;
		repeticiones = 0;
		cantidad = 0;
		tipo = 0;
		
		this.capacidad = capacidad;
		this.VT = VT;//RC
		this.SC = SC;
		this.SR = SR;
		this.R = R;//RC
		
		VT[0] = (int)(capacidad * 0.3);
		VT[1] = (int)(capacidad * 0.6);
		VT[2] = (int)(capacidad * 0.1);
		
		obj = new paisReporte();
	}
	
	public void run(){
		while(true){
			SC.Espera();
			if(checa(VT)){
				SC.libera();
				break;
			}
			cantidad = MyRandom.nextInt(1, 3);
			tipo = MyRandom.nextInt(3);
			VT[tipo] = VT[tipo] - cantidad;
			repeticiones++;
			SC.libera();
			SR.Espera();
			obj.cantidad = cantidad;
			obj.noPais = noPais;
			obj.repeticiones = repeticiones;
			obj.tipo = tipo+1;
			R.InsertaOrd(obj);
			SR.libera();
		}
	}
	private boolean checa(int v[]){
		for(int i = 0; i < v.length; i++){
			if(v[i] > 0){
				return false;
			}
		}
		return true;
	}
}
class paisReporte{
	int noPais;
	int repeticiones;
	int cantidad;
	int tipo;
	
	public paisReporte(){
		noPais = 0;
		repeticiones = 0;
		cantidad = 0;
		tipo = 0;
	}
	
	public String toString(){
		return MyRandom.PonBlancos(noPais+"", 4);
	}
}

