package MinaPlata;
import java.awt.event.*;
public class Controlador implements ActionListener{
	Vista vista;
	public Controlador(Vista vista) {
		this.vista = vista;
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(vista.inicia == evt.getSource()){
			vista.inicia.setEnabled(false);
			vista.inicia();
			return;
		}
	}

}
