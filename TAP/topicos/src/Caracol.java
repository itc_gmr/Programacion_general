import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Caracol extends JFrame implements ActionListener{
	int nE = MyRandom.nextInt(10,15);
	JButton bMatriz [][] = new JButton[nE][nE];
	
	JPanel up;
	JPanel down;
	JButton generar, limpiar;
	Color c3 = null;
	Color c1;
	Color c2;
	Color Aux;
	
	
	public Caracol(){
		HazInterfaz();
		escuchador();
	}
	
	public void HazInterfaz(){
		up = new JPanel();
		down = new JPanel();
		generar = new JButton("Genera Caracol");
		limpiar = new JButton("Limpiar Caracol");
		up.setLayout(new GridLayout(nE,nE,0,0));
		for(int i = 0; i < bMatriz.length; i++){
			for(int j = 0; j < bMatriz[i].length; j++){
				JButton nuevo =  new JButton();
				bMatriz[i][j] = nuevo;
				up.add(nuevo);
			}
		}
		down.add(generar);
		down.add(limpiar);
		setBounds(300, 100, 600, 600);
		add(up, BorderLayout.CENTER);
		add(down, BorderLayout.SOUTH);
		setVisible(true);
		setDefaultCloseOperation( EXIT_ON_CLOSE);
	}
	
	public void escuchador(){
		generar.addActionListener(this);
		limpiar.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent evt){
		if(evt.getSource()==generar){
			boolean ban = true;
			int nlimite = nE-1, inicio = 0, cont = 1;
			c1 = MyRandom.nextColor();
			c2 = MyRandom.nextColor();
			while(c1 == c2){
				c1 = MyRandom.nextColor();
			}
			Aux = c1;
			while(cont<=(nE*nE)){
				for(int i=inicio;i<=nlimite;i++){
					bMatriz[inicio][i].setBackground(Aux);
					try{
						Thread.sleep(15);
					}catch(Exception e){}
					bMatriz[inicio][i].update(bMatriz[inicio][i].getGraphics());

				}
				for(int i=inicio+1;i<=nlimite;i++){
					bMatriz[i][nlimite].setBackground(Aux);
					try{
						Thread.sleep(15);
					}catch(Exception e){}
					bMatriz[i][nlimite].update(bMatriz[i][nlimite].getGraphics());
				}
				for(int i=nlimite-1;i>=inicio;i--){
					bMatriz[nlimite][i].setBackground(Aux);
					try{
						Thread.sleep(15);
					}catch(Exception e){}
					bMatriz[nlimite][i].update(bMatriz[nlimite][i].getGraphics());
				}
				for(int i=nlimite-1;i>=inicio+1;i--){
					bMatriz[i][inicio].setBackground(Aux);
					try{
						Thread.sleep(15);
					}catch(Exception e){}
					bMatriz[i][inicio].update(bMatriz[i][inicio].getGraphics());
				}
				nlimite=nlimite-1;
				inicio = inicio+1;
				cont++;
				if(ban){
					Aux = c2;
					ban = false;
				}
				else{
					Aux = c1;
					ban = true;
				}
			}
		}
		
		if(evt.getSource() == limpiar){
			for(int i = 0; i < bMatriz.length; i++){
				for(int j = 0; j < bMatriz[i].length; j++){
					bMatriz[i][j].setBackground(c3);
				}
			}
		}
	}
	
	public static void main(String[] args) {
		new Caracol();

	}
}
