import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Arrays;
import java.util.Random;

import javax.swing.*;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class CombosDependientes extends JFrame implements ActionListener,ItemListener{
	JPanel P1;
	JComboBox Cmb,Cmb1,Cmb2;
	String [] Estados={"Seleccione","BC","Son","Sin","Nay","Col","Jal","Mich","Gue"};
	String [][] Cds={ {"Tij","Mex","Ense","Tecate"},{"Hermosillo","Obregon"},{"ClN","Mzn","LMM","Gve"},{} };
	String [][][] Cols={{ {"C1","C2"},{"C3","C4"},{"C5","C6"} },{},{},{}};
	
	public CombosDependientes(){
		super("Combs dependientes");
		HazInterfaz();
		HazEscucha();
	}
	public void HazInterfaz(){
		setSize(500,400);
		setLocationRelativeTo(null);
		P1=new JPanel();
		
		Cmb=new JComboBox(Estados);
		Cmb1=new JComboBox();
		Cmb2=new JComboBox();
	//	Cmb.setMaximumRowCount(3);

		P1.add(new JLabel("Estados"));
		P1.add(Cmb);
		P1.add(new JLabel("Ciudades"));
		P1.add(Cmb1);
		P1.add(new JLabel("Colonias"));
		P1.add(Cmb2);
		add(P1,BorderLayout.NORTH);
	//	paintAll(getGraphics());
		setVisible(true);
		
	}
	public void HazEscucha(){
		Cmb.addActionListener(this);
	//	Cmb.addItemListener(this);
		Cmb1.addActionListener(this);
		
	//	Cmb.addItemListener(this);
	//	Cmb1.addItemListener(this);

	}
	public void actionPerformed(ActionEvent E){
		if(E.getSource()==Cmb){
			if(Cmb.getSelectedIndex()==0)
				return;
		//	Cmb1.removeActionListener(this);;  

			int Indice=Cmb.getSelectedIndex()-1;
			System.out.println("indice "+Indice);
			Cmb1.removeAllItems();
			Cmb2.removeAllItems();
			Cmb1.addItem("Seleccione");
			for(int i=0;i<Cds[Indice].length;i++){
				Cmb1.addItem(Cds[Indice][i]);
			}
	//		Cmb1.addActionListener(this);	
			 return;
		}

		
	if(E.getSource()==Cmb1){
		if(Cmb1.getSelectedIndex()<=0)
			return;			
		int IndiceEdo=Cmb.getSelectedIndex()-1;
		int IndiceCd=Cmb1.getSelectedIndex()-1;

		Cmb2.removeAllItems();
		Cmb2.addItem("Seleccione");
		for(int i=0;i<Cols[IndiceEdo][IndiceCd].length;i++){
				Cmb2.addItem(Cols[IndiceEdo][IndiceCd][i]);
		}
			
		return;
	}

	}		
	
	public void itemStateChanged(ItemEvent E) {

	}
	public static void main(String [] a){
		new CombosDependientes();
	}
}