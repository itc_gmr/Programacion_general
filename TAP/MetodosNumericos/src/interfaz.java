   import java.awt.*; 
   import java.awt.event.*; 
   import javax.swing.*; 

    public class interfaz extends JFrame { 
	   Metodos m=new Metodos();
		es es=new es();
      private JButton euler,newton,kutt2,kutt4,taylor2,diferencias,lagrange,adams; 
      private Container container; 
      private FlowLayout layout;

       public interfaz() {
         super( "METODOS OPTMIZACION o algo as� jajaja" );
         layout = new FlowLayout();

         container = getContentPane(); 
         container.setLayout( layout ); 
         euler = new JButton( "Euler" ); 
         container.add( euler ); 
         euler.addActionListener( 
                new ActionListener() {
               
                   public void actionPerformed( ActionEvent event ) { 
							double xf=es.leed("Da el final");
							m.euler(xf);
							//m.eulerMejorado(1,1,0.1,1.6);
                     layout.layoutContainer( container );
                  }
               } 
            ); // addActionListener
      
         newton = new JButton( "Newton" ); 
         container.add( newton ); 
         newton.addActionListener( 
                new ActionListener() { 
                   public void actionPerformed( ActionEvent event ) {
						  //Limite			  
						  int lim=es.leei("Da un entero");
						  m.newton(lim);
							//m.newton(26);
                     layout.layoutContainer( container );
                  } 
               }
            );
      
         diferencias = new JButton( "Diferencias" ); 
         container.add( diferencias ); 
         diferencias.addActionListener( 
                new ActionListener(){ 
                   public void actionPerformed( ActionEvent event ) { 
       					// 					
							double []y={1,0.90909091,0.83333333,0.76923077};
							//2.inicio,3.fin.4.incremento
							double x=es.leed("da el inicio");
							double yy=es.leed("da el final");
							double z=es.leed("da el incremento");
							//m.diferenciasregresivas(y,1.4,1.6,0.1);
    						m.diferenciasregresivas(y,x,yy,z);    
		               layout.layoutContainer( container );
                  }
               }
            ); 
				
			lagrange = new JButton( "Lagrange" ); 
         container.add( lagrange ); 
         lagrange.addActionListener( 
                new ActionListener(){ 
                   public void actionPerformed( ActionEvent event ) { 
							double x[]={1,4,6};
							double f[]={1.5709,1.5727,1.5751};
							double p[]=m.interpolante(x, f);
							m.imprime(p);
							String cad=JOptionPane.showInputDialog("Da un valor de v para K(v)");
							double v=(double)Double.parseDouble(cad);
							double r=m.eval(p,3.5);
							System.out.println(" el valor del polinomio en el punto es: "+r);
                  	 layout.layoutContainer( container );
                  }
               }
            ); 
				
			taylor2 = new JButton( "Taylor 2" ); 
         container.add( taylor2 ); 
         taylor2.addActionListener( 
                new ActionListener(){ 
                   public void actionPerformed( ActionEvent event ) { 
							//m.tylor(2.0,-1/2);
   					double vh=es.leed("Da el valor de h1/");	 
           			m.taylor(vh);  
			            layout.layoutContainer( container );
                  }
               }
            );
				
			kutt2 = new JButton( "Kutt 2" ); 
         container.add( kutt2 ); 
         kutt2.addActionListener( 
                new ActionListener(){ 
                   public void actionPerformed( ActionEvent event ) { 
							//aki va algo
							double x=es.leed("Da valor de x");
							double y=es.leed("da valor de y");
							double h=es.leed("Da el valor de h(incremento)");
							double xf=es.leed("Da el final");
							m.rungeKutta2(x,y,h,xf);
                     layout.layoutContainer( container );
                  }
               }
            );
			
			kutt4 = new JButton( "Kutt 4" ); 
         container.add( kutt4 ); 
         kutt4.addActionListener( 
                new ActionListener(){ 
                   public void actionPerformed( ActionEvent event ) { 
							//aki va otra madre 
							double x=es.leed("Da valor de x");
							double y=es.leed("da valor de y");
							double h=es.leed("Da el valor de h(incremento)");
							double xf=es.leed("Da el final");
							m.rungeKutta4(x,y,h,xf);
							//m.rungeKutta4(1,1,0.1,1.6);
                     layout.layoutContainer( container );
                  }
               }
            );	
      
			adams = new JButton( "Adams" ); 
         container.add( adams ); 
         adams.addActionListener( 
                new ActionListener(){ 
                   public void actionPerformed( ActionEvent event ) { 
							m.adams();
							 layout.layoutContainer( container );
                  }
               }
            );
				
         setSize( 300, 375 ); 
         setVisible( true ); 
      } 
   
       public static void main( String args[] ) { 
		 	Metodos m=new Metodos();
         interfaz apli = new interfaz(); 
         apli.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE ); 
      } 
   } // end class