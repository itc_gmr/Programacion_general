import java.text.NumberFormat;
public class Metodos {
	/**
	 * metodo de la interpolante de lagrange
	 */
		
	public static double eval(double[] p, double x) {
		double s=p[p.length-1];
		for(int k=p.length-2;k>=0;k--){
			s*=x;	s+=p[k];}
		return s;
	}

	public static double[] interpolante(double x[],double f[]) {
		int m=x.length,n=m-1;
		double p[]=new double[m],ph[]=new double[2],a[]=new double[2];
		ph[0]=-x[0];ph[1]=1;a[1]=1;
		for(int i=1;i<=n;i++){
			a[0]=-x[i];ph=mul_pol(ph, a);
		}
		double b[]=new double[m],c[]=new double[m];
		for(int k=0;k<=n;k++){
			b[n]=ph[n+1];c[n]=b[n];
			for(int j=n-1;j>=0;j--){
				b[j]=b[j+1]*x[k]+ph[j+1];c[j]=c[j+1]*x[k]+b[j];
			}
			for(int i=0;i<=n;i++){
				p[i]+=f[k]*b[i]/c[0];
			}
		}return p;
	}

	public static double[] sum_pol(double a[],double b[]){
		int n=a.length,m=b.length,p=Math.max(m, n);
		double c[]=new double[p];
		for(int k=0;k<p;k++){
			double ai=(k>n)?0:a[k],bi=(k>m)?0:b[k];
			c[k]=ai+bi;
		}return c;
	}
	
	public static double[] resta_pol(double a[],double b[]){
		int n=a.length,m=b.length,p=Math.max(m, n);
		double c[]=new double[p];
		for(int k=0;k<p;k++){
			double ai;
			if(k>=n)ai=0;
			else ai=a[k];
			double bi;
			if(k>=m)bi=0;
			else bi=b[k];
			c[k]=ai-bi;
		}return c;
	}	
	
	public static double[] mul_pol(double a[],double b[]){
		int n=a.length,m=b.length;
		double c[]=new double[n+m-1];
		for (int i=0;i<n;i++)
			for (int j=0;j<m;j++)
				c[i+j]+=a[i]*b[j];
		return c;
	}

	public static void imprime(double[] p) {
			//for(int k=0;k<p.length;k++) System.out.print(p[k]+"\n ");
			System.out.println();
	}
	/**
	 * Metodo de Taylor
	 */
	 public void taylor(double vh){
	 	 double h=(1.0/vh);
       System.out.println("h 1/"+vh+ "= "+h);
       System.out.println("");
       double a=1.0,b=2.0,i;
       double f,f1,y=-1,x=a;
       for( i=a; i<=b;i=i+h){
           f=(1.0/Math.pow(x,2))-(y/x)-(Math.pow(y,2));
           f1=(-2.0/Math.pow(x,3))+(y/Math.pow(x,2))-(f*((1.0/x)+(2.0*y)));
           y=y+h*(f+(h*f1/2.0));
           double c=i+h;
           if(c==1.5 || c==2){System.out.println("y("+(i+h)+") = "+y);}
           x=x+h;
       }    
	 }
	 
	public void tylor(double x,double y0){
        double yy=0;
        for(int n = 4; n <= 7; n++){
           double y[]=new double [6],hx=(2-1)/Math.pow(2,n),h=hx,xx=1.0;
           System.out.println("1/h es "+"1/"+n+"  =  "+hx);
           y[0]=-1;
           for(int i = 1; i <= y.length; i++){
              xx = xx+(i*h);yy=y[i-1]+h*(ff(x,y[i-1])+h/2*fpr(x,y[i-1]));
           }System.out.println(yy + "\n");
        }
     }
     
     public double ff(double x1, double y1){
        return (1.0/(Math.pow(x1,2)))-(y1/x1)-(Math.pow(y1,2));
     }
      public double fpr(double x1, double y1){
        return -(2.0/Math.pow(x1,3))-((1.0/x1)+(2.0*y1))*ff(x1,y1)+(y1/(Math.pow(x1,2)));
     }
      /**
       * metodo de interpolación o de newton??
       */
      
      public void newton(int nn){
          double []x=new double[nn+1];
          double []d=new double[nn+1];
          System.out.println("N     MAXIMUN ERROR");
          for(int n=2;n<=nn;n+=2){
             double h=10./n;
             for(int i=0;i<=n;i++){
                x[i]=i*h-5;d[i]=fn1(x[i]);
             }
             for(int k=1;k<=n;k++){
                for(int i=0;i<=n-k;i++) d[i]=(d[i+1]-d[i])/(x[i+k]-x[i]);
             }
             double y=-5,errMax=0;
             for(int j=0;j<=100;j++){
                double pnofy=d[0];            
                for(int k=1;k<=n;k++) pnofy=d[k]+(y-x[k])*pnofy;
                double error=Math.abs(fn1(y)-pnofy);
                if(error>errMax) errMax=error;
                y=y+0.1;
             }
             System.out.println(n+"  "+errMax);
          }/*end for*/
       }
     
        public double fn1(double x){
          return 1/(1+x*x);
       }
        
        /**
         * metodo de euler
         */
        
        public double f(double x,double y){
            return 2*(x*y);
         }
        
        public void euler(double xf){
            double y=1,x=0.0,step=0.01;
            System.out.println("Metodo de Euler");
            System.out.println("x\ty");
            for(double i=step;i<=xf;i+=step){
               y=y+step*y;
               x=x+step;
               System.out.println(x+"\t"+y);
            }
         }
        
        /**
         * metodo de euler disque mejorado
         */
        
        public double eulerMejorado(double x0,double y0, double h,double xf){
            double x=x0,y=y0,fun,y1;
            System.out.println("\nMetodo de Euler Mejorado");
            System.out.println("x\t\ty");
            for(double i=x0;i<xf;i+=h){
               fun=f(x,y);
               y1=y+h*fun;
               x=x+h;
               y=y+h*((fun+f(x,y1))/2);
               System.out.println(x+"\t"+y);
            }
            return y;
         }
        
        /**
         * metodo de diferencias regresivas
         */
        public double fdr(double x,double y){
            return -y*y;
         }
        public void diferenciasregresivas(double y[],double x, double xf,double h){
            double f0=fdr(x,y[0]);
            double f1=fdr(x,y[1]);
            double f2=fdr(x,y[2]);
            double f3=fdr(x,y[3]);
            double yn=y[3];
            System.out.println("\nMetodo de diferencias regresivas");
            System.out.println("x\t\ty");
            for(double i=x; i<=xf;i+=h){
               yn=yn+(h/24)*(55*f3-59*f2+37*f1-9*f0);
               f0=f1;
               f1=f2;
               f2=f3;
               f3=fdr(x,yn);
               System.out.println(x+"\t"+yn);	    
               x=x+h;
            }
         }
        
        /**
         *metodo de rengekutta 2 
         */
        
        public void rungeKutta2(double x0,double y0, double h,double xf){
            double x=x0,y=y0,k1,k2;
            System.out.println("\nMetodo de RungeKutta2");
            System.out.println("x\t\ty");
            for(double i=x0;i<xf;i+=h){
               k1=h*f(x,y);
               k2=h*f(x+h/2,y+k1/2.0);
               y=y+k2;
               x=x+h;
               System.out.println(x+"\t"+y);
            }
         }
        
        /**
         * metodo de rengekutta 4
         */
        public void rungeKutta4(double x0,double y0, double h,double xf){
            double x=x0,y=y0,k1,k2,k3,k4;
            System.out.println("\nMetodo de RungeKutta4");
            System.out.println("x\t\ty");
            for(double i=x0;i<xf;i+=h){
               k1=h*f(x,y);
               k2=h*f(x+h/2,y+k1/2.0);
               k3=h*f(x+h/2,y+k2/2.0);
               k4=h*f(x+h,y+k3);
               y=(y+(k1+2*k2+2*k3+k4)/6.0);
               x=x+h;
               System.out.println(x+"\t"+y);
            }
         }
        
        /**
         * funcion original
         */
        
        public void funcionOriginal(double xi,double xf,double h){
            double x=xi;
            System.out.println("\nFuncion original");
            System.out.println("x\t\ty");
            for(double i=xi;i<=xf;i+=h){
               x=x+h;
               System.out.println(x+"\t"+(1/x));
            }
         }
			/**
			*Metodo de Adams
			*/
		public void adams(){
         double []yn= new double [8];
         yn[0]=1;
         yn[1]=0.909091;
         yn[2]=0.833333;
         yn[3]=0.7692307;
         double []FN= new double [8];
         FN[0]=-1;
         FN[1]=-0.82644628;
         FN[2]=-0.69444444;
         FN[3]=-0.59171598;
         double []y=new double [8];
         double []xn= {1,1.1,1.2,1.3,0,0,0,0};
         double h,x;
         int m=3;
         h=0.1; x=1.3;
         NumberFormat yy = NumberFormat.getInstance();
         yy.setMaximumFractionDigits(2);
			  NumberFormat ss = NumberFormat.getInstance();
         ss.setMaximumFractionDigits(12);

         
      
         for (m=3;m<7;m++){
            yn[m+1]=yn[m]+(h/24)*((55*FN[m])-(59*FN[m-1])+(37*FN[m-2]-(9*FN[m-3])));
            FN[m+1]=fn(yn[m+1]);
            x=x+h;
            y[m+1]=ff(x);
            xn[m+1]=x;
         }
         System.out.println("Xn\t yn\t \tFn\t\t +y(x)");
         for(int i=0; i<8; i++)
         {
         //System.out.println("hola");
            System.out.println((yy.format(xn[i]))+"\t"+(ss.format(yn[i]))+"\t"+(ss.format(FN[i]))+"\t"+(ss.format(y[i])));
            }
      }
   
      public static double ff(double x){
         double r =1/x;
         return r;
      }
   	
      static double fn(double x1){
         double re =-(x1*x1);
         return re;
      }
}