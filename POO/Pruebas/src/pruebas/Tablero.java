/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;

/**
 *
 * @author Guillermo Marquez
 */
public class Tablero 
{
    Casillas[] tablero;
    
    public Tablero()
    {
        this.tablero = new Casillas [100]; 
    }
    
    public void Crear()
    {
       for (int i = 0; i < this.tablero.length; i++)
       {
           Casillas ca = new Casillas();
           ca.disponible = true;
           ca.numCasilla = i + 1;
           switch (ca.numCasilla)
           {
               case 1:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
               case 2:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 3:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 4:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
                case 5:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 6:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 7:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 8:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 9:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
                case 10:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 11:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 12:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 13:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 14:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
                case 15:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 16:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 17:
                   ca.escalera = 0;
                   ca.serpiente = 1;
                   break;
                case 18:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 19:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 20:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 21:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
                case 22:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 23:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 24:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 25:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 26:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 27:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 28:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 29:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
                case 30:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 31:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 32:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 33:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 34:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 35:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 36:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 37:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 38:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 39:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 40:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 41:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 42:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 43:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 44:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 45:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 46:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 47:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 48:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 49:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 50:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 51:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
                case 52:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 53:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 54:
                   ca.escalera = 0;
                   ca.serpiente = 1;
                   break;
                case 55:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 56:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 57:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 58:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 59:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 60:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
                case 61:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 62:
                   ca.escalera = 0;
                   ca.serpiente = 1;
                   break;
                case 63:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 64:
                   ca.escalera = 0;
                   ca.serpiente = 1;
                   break;
                case 65:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 66:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 67:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 68:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 69:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 70:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 71:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
                case 72:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 73:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 74:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 75:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 76:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 77:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 78:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 79:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 80:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 81:
                   ca.escalera = 1;
                   ca.serpiente = 0;
                   break;
                case 82:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 83:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 84:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 85:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 86:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 87:
                   ca.escalera = 0;
                   ca.serpiente = 1;
                   break;
                case 88:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 89:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 90:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 91:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 92:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 93:
                   ca.escalera = 0;
                   ca.serpiente = 1;
                   break;
                case 94:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 95:
                   ca.escalera = 0;
                   ca.serpiente = 1;
                   break;
                case 96:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 97:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 98:
                   ca.escalera = 0;
                   ca.serpiente = 1;
                   break;
                case 99:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                case 100:
                   ca.escalera = 0;
                   ca.serpiente = 0;
                   break;
                                                                                                                
                                                        
           }
          
           tablero[i] = ca;
                       
       }
        
    }
    
    public void Imprimir()
    {
       
        
    }
    
    public void verificarGanador()
    {
        
    }
    
}
