package Serv;

public class alumno 
{
	String nombre;
	String apellido;
	int edad;
	String carrera;
	
	public void altaAlumno(String nombre,String apellido,int edad,String carrera)
	{
		this.nombre=nombre;
		this.apellido=apellido;
		this.edad=edad;
		this.carrera=carrera;
	}
	public void bajaAlumno()
	{
		this.nombre=null;
		this.apellido=null;
		this.edad=0;
		this.carrera=null;
	}
	public void mostrarAlumno()
	{
		System.out.println("nombre: "+this.nombre);
		System.out.println("apellido : "+this.apellido);
		System.out.println("edad : "+this.edad);
		System.out.println("carrera : "+this.carrera);
	}
}
