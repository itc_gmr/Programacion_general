package veinticinco_feb;

public class Calculadora {

	public static void main(String[] args) 
	{
		Operaciones suma=new Operaciones();
		System.out.println("la suma de 5+6 es "+suma.sumar(5, 6));
		
		Operaciones resta=new Operaciones();
		System.out.println("la resta de 5-1 es "+resta.restar(5, 1));
		
		Operaciones mult=new Operaciones();
		System.out.println("la multiplicacion de 4x7 es"+mult.multiplicar(4, 7));
		
		Operaciones dividir=new Operaciones();
		System.out.println("la division de 10/2 es "+dividir.dividir(10, 2));
		
		Operaciones porcen=new Operaciones();
		System.out.println("el 5% de 555 es "+porcen.porcentaje(555,5));
	}

}
