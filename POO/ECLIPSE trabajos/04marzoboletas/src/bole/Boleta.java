package bole;

public class Boleta 
{
	double promedio=0;
	int creditos=0;
	int reprobada=0;
	
	public void imprimir(Alumno a1)
	{
		System.out.print("Boleta de calificaciones");
		System.out.println("Alumno "+a1.nombre+" carrera "+a1.carrera);
		System.out.print("semestre "+a1.semestre);
		System.out.print("Numero de control "+a1.numControl);
		
		for(int i=0;i<a1.materias.length;i++)
		{
			System.out.println(a1.materias[i].nombre+" calificacion: "+a1.materias[i].calificacion+" creditos "+a1.materias[i].creditos);
			this.promedio=this.promedio+a1.materias[i].calificacion;
			this.creditos=this.creditos+a1.materias[i].creditos;
			
			if(a1.materias[i].calificacion<70)
				this.reprobada=0;
		}
		System.out.println("---------------------------------");
		System.out.println("promedio "+this.promedio/6);
		System.out.println("total de creditos "+this.creditos);
		System.out.println("materias reprobadas "+this.reprobada);
		
	}
}
