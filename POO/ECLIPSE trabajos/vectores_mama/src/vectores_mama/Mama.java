package vectores_mama;

public class Mama 
{
	String nombre;
	int edad;
	String estadoCivil;
	Hijo hijos[];
	int numHijos;
	
	public Mama(String nombre,int edad,String estadodoCivil, int numHijos)
	{
		this.nombre=nombre;
		this.edad=edad;
		this.estadoCivil=estadodoCivil;
		this.numHijos=numHijos;
		hijos=new Hijo[numHijos];
	}
	
	public void imprimir()
	{
		System.out.println("m�ma "+nombre);
		System.out.println("edad "+edad);
		System.out.println("estado civil "+estadoCivil);
		System.out.println("total de hijos "+numHijos);
		System.out.println("y estos son los datos de sus hijos ");
		System.out.println();
		if(numHijos>10)
			System.out.println("no tenian tele ");
		
		for(int i=0;i<hijos.length;i++)
		{
			System.out.println("hijo "+hijos[i].nombre);
			System.out.println("edad "+hijos[i].edad);
			System.out.println("sexo "+hijos[i].sexo);
			System.out.println("-------------------------");
		}
	}
}
