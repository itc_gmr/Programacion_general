package trece_feb;

public class Articulo 
{
	String tipo;
	int codigo;
	int precio;
	int excistencia;
	int descuento;
	
	public Articulo crearArticulo(String tipo,int codigo,int precio,int descuento,int excistencia)
	{
		this.tipo=tipo;
		this.codigo=codigo;
		this.precio=precio;
		this.descuento=descuento;
		this.excistencia=1;
		return this;
	}
	public void inventario(Articulo art)
	{
		if(art.excistencia==1)
			this.excistencia=0;
		else
			System.out.println("el producto esta agotado ");
	}
}
