package trece_feb;																
																				
public class Cliente 																	
{																					
	String cliente;																		
	int numCliente;																
																					
	public void crearCliente(String cliente,int numCliente)						
	{																			
		this.cliente=cliente;														
		this.numCliente=numCliente;												
	}																							
																					
	public void comprarArticulo(Articulo a)													
	{																							
		a.inventario(a);														
																							
		if(a.descuento!=0)															
		{																		
			int descuento;																
			descuento=a.precio*(15/100);	
			a.precio=a.precio-a.descuento;
		}																																										
																
	}																		
}																						
