package sa;

public class Banco_san 
{
	public static void main(String[] args) 
	{
		Donador donador1=new Donador();
		donador1.registrarDonador("carlos", "ORH+", 20);
		donador1.mostrarDonador(false);

		Donador donador2=new Donador();
		donador2.registrarDonador("remmy", "ORH+", 17);
		donador2.mostrarDonador(true);

		Donador donador3=new Donador();
		donador3.registrarDonador("betzabed", "ORH+", 19);
		donador3.mostrarDonador(false);

		Donador donador4=new Donador();
		donador4.registrarDonador("mario", "ORH+", 20);
		donador4.mostrarDonador(false);
	}

}
