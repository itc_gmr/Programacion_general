package diezinueve_feb;

public class Arma 
{
	String calibre;
	String tipo;
	int capacidad;
	double costo;
	int serie;
	String alcance;
	
	public Arma(String calibre,String tipo,int capacidad,double costo,int serie,String alcance)
	{
		this.calibre=calibre;
		this.tipo=tipo;
		this.capacidad=capacidad;
		this.costo=costo;
		this.serie=serie;
		this.alcance=alcance;
	}
	
	public Arma()
	{
		System.out.println("entro al metodo constructor");
	}
	
	public void setCalibre(String calibre)
	{
		this.calibre=calibre;
	}
	public String getCalibre()
	{
		return this.calibre;		
	}
	
	public void settipo(String tipo)
	{
		this.tipo=tipo;
	}
	public String getTipo()
	{
		return this.tipo;
	}
	
	public void setCapacidad(int capacidad)
	{
		this.capacidad=capacidad;
	}
	public int getCapacidad()
	{
		return this.capacidad;
	}
	
	public void setCosto(double costo)
	{
		this.costo=costo;
	}
	public double getCosto()
	{
		return this.costo;
	}
	
	public void setSerie(int serie)
	{
		this.serie=serie;
	}
	public int getSerie()
	{
		return this.serie;
	}
	
	public void setAlcance(String alcance)
	{
		this.alcance=alcance;
	}
	public String getAlcance()
	{
		return this.alcance;
	}
	
	
	
	
	
	
	
	

}
