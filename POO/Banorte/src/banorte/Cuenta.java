/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banorte;

/**
 *
 * @author ADM
 */
public class Cuenta 
{
    String tipo;
    double saldo;
    int numCuenta;
    String nomCliente;
    
    public Cuenta(String tipo, int numCuenta, String nomCliente)
    {
        this.tipo = tipo;
        this.numCuenta = numCuenta;
        this.nomCliente = nomCliente;
        this.saldo = 0.0;
    }
    
    public void imprimirSaldo()
    {
        System.out.println("Saldo:" + this.saldo);
    }
    
}
