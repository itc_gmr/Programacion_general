/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package banorte;

/**
 *
 * @author ADM
 */
public class Banorte {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        Nomina cta1 = new Nomina("Nomina", 234112775, "Guillermo Márquez Ruiz");
        cta1.imprimirSaldo();
        cta1.deposito(300);
        cta1.imprimirSaldo();
        cta1.retiro(152);
        cta1.imprimirSaldo();
        cta1.deposito(300);
        cta1.imprimirSaldo();
        
        Credito cta2 = new Credito("Nomina", 234112775, "Guillermo Márquez Ruiz");
        cta2.comprar(500);
    }
    
}
