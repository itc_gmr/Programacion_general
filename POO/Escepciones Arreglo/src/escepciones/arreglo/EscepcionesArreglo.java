/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package escepciones.arreglo;

/**
 *
 * @author ADM
 */
public class EscepcionesArreglo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        int[] numeros = new int[10];
        try{
            numeros[15] = 8;
        }
        catch(ArrayIndexOutOfBoundsException e){
            System.out.println("Error al ingresar al vector");
            System.out.println("Expn: " + e.getMessage());
        }
        finally{
            
            
        }
    }
    
}
