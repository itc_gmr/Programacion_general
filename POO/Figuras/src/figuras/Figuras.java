/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;
import java.util.ArrayList;
/**
 *
 * @author Guillermo
 */
public class Figuras {
 public static ArrayList<Figura> Figs = new ArrayList<Figura>();
    public static void main(String[] args) 
    {
        Figura f1 = new Cuadrado("Cuadrado");
        Figura f2 = new Triangulo("Triangulo");
        Figs.add(f1);
        Figs.add(f2);
        for (Figura f:Figs)
        {
            System.out.println("Nombre: " + f.nombre);
            System.out.println("Área: " + f.calcularArea(12, 13));
        }
                 
    }
    
}
