/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

/**
 *
 * @author Guillermo
 */
public class Cuadrado extends Figura 
{
    public Cuadrado(String n)
    {
        super(n);
    }
    @Override
    public double calcularArea(double a, double b)
    {
        return a * b;
    }
    
}
