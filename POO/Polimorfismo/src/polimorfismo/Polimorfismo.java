/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo;
import java.util.ArrayList;
/**
 *
 * @author ADM
 */
public class Polimorfismo {

   public static ArrayList<Empleado> trabajadores = new ArrayList<Empleado>();
   
    public static void main(String[] args) 
    {
        Empleado t1 = new Docente("Hugo", 50);
        Empleado t2 = new Intendente("Paco", 20);
        trabajadores.add(t1);
        trabajadores.add(t2);
        
        for (Empleado trabajador:trabajadores)
        {
           System.out.println("-----------------");
           System.out.println("Nombre: " + trabajador.nombre);
           System.out.println("Nombre: " + trabajador.edad);
           trabajador.checarEntrada();
           trabajador.trabajar();
        }
    }
    
}
