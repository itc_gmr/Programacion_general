/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo;

/**
 *
 * @author ADM
 */
public class Intendente extends Empleado 
{
    public Intendente(String nombre, int edad)
    {
        super(nombre, edad);
    }
    
    @Override
    public void trabajar()
    {
        System.out.println("Esta limpiando las aulas +clase(Docente)");
    }
    
}
