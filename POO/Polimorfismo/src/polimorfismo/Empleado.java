/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polimorfismo;

/**
 *
 * @author ADM
 */
public abstract class Empleado 
{
    protected String nombre;
    protected int edad;
    
    public Empleado (String nombre, int edad)
    {
        this.nombre = nombre;
        this.edad = edad;                           
    }
    
    public void checarEntrada()
    {
        System.out.println("Entrada registrada clase (Empleado)");
    }
    
    public abstract void trabajar();
            
    
}
