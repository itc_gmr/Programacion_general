/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea06;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;
/**
 *
 * @author Guillermo
 */
public class Archivo 
{
    public void crearArchivo(int[] lineas)
    {
        FileWriter archivo = null; //iniciar archivo
        try{
            archivo = new FileWriter("prueba.txt");
            for(int linea:lineas)
            {
                archivo.write(linea + "\n"); //Escribir archivo
                
            }
            archivo.close(); //cerrar archivo
        }
        catch(Exception ex){
            System.out.println("Mensaje: " + ex.getMessage());
        }
    
    }
    
    public void leerArchivo()
    {
        File archivo = new File("prueba.txt");
        Scanner a = null;
        try{
            a = new Scanner(archivo);
            System.out.println("-------Contenido del archivo------");
            while(a.hasNextLine()){
                String linea = a.nextLine();
                System.out.println(linea);
            }
        }
        catch(Exception ex){
             System.out.println("Mensaje: " + ex.getMessage());
        }
        finally{
            System.out.println("----------------------------");
            if(a != null)
            {
                a.close();
            }
        }
    }
    
     public void escribirArchivo(int[] lineas)
    {
        FileWriter archivo = null; //iniciar archivo
        try{
            archivo = new FileWriter("prueba.txt");
            for(int linea:lineas)
            {
                archivo.write(linea + "\n"); //Escribir archivo
                
            }
            archivo.close(); //cerrar archivo
        }
        catch(Exception ex){
            System.out.println("Mensaje: " + ex.getMessage());
        }
    
    }
     
      public void escribirArchivos(String[] lineas)
    {
        FileWriter archivo = null; //iniciar archivo
        try{
            archivo = new FileWriter("prueba.txt");
            for(String linea:lineas)
            {
                archivo.write(linea + "\n"); //Escribir archivo
                
            }
            archivo.close(); //cerrar archivo
        }
        catch(Exception ex){
            System.out.println("Mensaje: " + ex.getMessage());
        }
    
    }
    
}
